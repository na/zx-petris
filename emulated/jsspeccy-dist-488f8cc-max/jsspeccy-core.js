/**
 * @license JSSpeccy v2.2.1 - http://jsspeccy.zxdemo.org/
 * Copyright 2014 Matt Westcott <matt@west.co.tt> and contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

if (!window.DataView) window.DataView = jDataView;

function JSSpeccy(container, opts) {
	var self = {};

	if (typeof(container) === 'string') {
		container = document.getElementById(container);
	}
	if (!opts) {
		opts = {};
	}

	var originalDocumentTitle = document.title;


	/* == Z80 core == */
	/* define a list of rules to be triggered when the Z80 executes an opcode at a specified address;
		each rule is a tuple of (address, opcode, expression_to_run). If expression_to_run evaluates
		to false, the remainder of the opcode's execution is skipped */
	var z80Traps = [
		[0x056b, 0xc0, 'JSSpeccy.traps.tapeLoad()'],
		[0x0111, 0xc0, 'JSSpeccy.traps.tapeLoad()']
	];

	JSSpeccy.buildZ80({
		traps: z80Traps,
		applyContention: true
	});


	/* == Event mechanism == */
	function Event() {
		var self = {};
		var listeners = [];

		self.bind = function(callback) {
			listeners.push(callback);
		};
		self.unbind = function(callback) {
			for (var i = listeners.length - 1; i >= 0; i--) {
				if (listeners[i] == callback) listeners.splice(i, 1);
			}
		};
		self.trigger = function() {
			var args = arguments;
			/* event is considered 'cancelled' if any handler returned a value of false
				(specifically false, not just a falsy value). Exactly what this means is
				up to the caller - we just return false */
			var cancelled = false;
			for (var i = 0; i < listeners.length; i++) {
				cancelled = cancelled || (listeners[i].apply(null, args) === false);
			}
			return !cancelled;
		};

		return self;
	}

	function Setting(initialValue) {
		var self = {};

		var value = initialValue;

		self.onChange = Event();

		self.get = function() {
			return value;
		};
		self.set = function(newValue) {
			if (newValue == value) return;
			value = newValue;
			self.onChange.trigger(newValue);
		};
		return self;
	}

	self.settings = {
		'checkerboardFilter': Setting(opts.checkerboardFilter || false)
	};

	/* == Execution state == */
	self.isDownloading = false;
	self.isRunning = false;
	self.currentTape = null;
	var currentModel, spectrum;


	/* == Set up viewport == */
	var viewport = JSSpeccy.Viewport({
		container: container,
		scaleFactor: opts.scaleFactor || 2,
		onClickIcon: function() {self.start();}
	});

	if (!('dragToLoad' in opts) || opts['dragToLoad']) {
		/* set up drag event on canvas to load files */
		viewport.canvas.ondragenter = function() {
			// Needed for web browser compatibility
			return false;
		};
		viewport.canvas.ondragover = function () {
			// Needed for web browser compatibility
			return false;
		};
		viewport.canvas.ondrop = function(evt) {
			var files = evt.dataTransfer.files;
			self.loadLocalFile(files[0]);
			return false;
		};
	}

	function updateViewportIcon() {
		if (self.isDownloading) {
			viewport.showIcon('loading');
		} else if (!self.isRunning) {
			viewport.showIcon('play');
		} else {
			viewport.showIcon(null);
		}
	}


	/* == Keyboard control == */
	var keyboard = JSSpeccy.Keyboard();
	self.deactivateKeyboard = function() {
		keyboard.active = false;
	};
	self.activateKeyboard = function() {
		keyboard.active = true;
	};


	/* == Audio == */
	var soundBackend = JSSpeccy.SoundBackend();
	self.onChangeAudioState = Event();
	self.getAudioState = function() {
		return soundBackend.isEnabled;
	};
	self.setAudioState = function(requestedState) {
		var originalState = soundBackend.isEnabled;
		var newState = soundBackend.setAudioState(requestedState);
		if (originalState != newState) self.onChangeAudioState.trigger(newState);
	};

	/* == Snapshot / Tape file handling == */
	self.loadLocalFile = function(file, opts) {
		var reader = new FileReader();
		self.isDownloading = true;
		updateViewportIcon();
		reader.onloadend = function() {
			self.isDownloading = false;
			updateViewportIcon();
			self.loadFile(file.name, this.result, opts);
		};
		reader.readAsArrayBuffer(file);
	};
	self.loadFromUrl = function(url, opts) {
		var request = new XMLHttpRequest();

		request.addEventListener('error', function(e) {
			alert('Error loading from URL:' + url);
		});

		request.addEventListener('load', function(e) {
			self.isDownloading = false;
			updateViewportIcon();
			data = request.response;
			self.loadFile(url, data, opts);
			/* URL is not ideal for passing as the 'filename' argument - e.g. the file
			may be served through a server-side script with a non-indicative file
			extension - but it's better than nothing, and hopefully the heuristics
			in loadFile will figure out what it is either way.
			Ideally we'd look for a header like Content-Disposition for a better clue,
			but XHR (on Chrome at least) doesn't give us access to that. Grr. */
		});

		/* trigger XHR */
		request.open('GET', url, true);
		request.responseType = "arraybuffer";
		self.isDownloading = true;
		updateViewportIcon();
		request.send();
	};

	self.loadFile = function(name, data, opts) {
		if (!opts) opts = {};

		var fileType = 'unknown';
		if (name && name.match(/\.sna(\.zip)?$/i)) {
			fileType = 'sna';
		} else if (name && name.match(/\.tap(\.zip)?$/i)) {
			fileType = 'tap';
		} else if (name && name.match(/\.tzx(\.zip)?$/i)) {
			fileType = 'tzx';
		} else if (name && name.match(/\.z80(\.zip)?$/i)) {
			fileType = 'z80';
		} else {
			var signatureBytes = new Uint8Array(data, 0, 8);
			var signature = String.fromCharCode.apply(null, signatureBytes);
			if (signature == "ZXTape!\x1A") {
				fileType = 'tzx';
			} else if (data.byteLength === 49179 || data.byteLength === 131103 || data.byteLength === 147487) {
				fileType = 'sna';
			} else if (JSSpeccy.TapFile.isValid(data)) {
				fileType = 'tap';
			}
		}

		switch (fileType) {
			case 'sna':
				loadSnapshot(JSSpeccy.SnaFile(data));
				break;
			case 'z80':
				loadSnapshot(JSSpeccy.Z80File(data));
				break;
			case 'tap':
				loadTape(JSSpeccy.TapFile(data), opts);
				break;
			case 'tzx':
				loadTape(JSSpeccy.TzxFile(data), opts);
				break;
		}
	};

	/* Load a snapshot from a snapshot object (i.e. JSSpeccy.SnaFile or JSSpeccy.Z80File) */
	function loadSnapshot(snapshot) {
		self.setModel(snapshot.model);
		self.reset(); /* required for the scenario that setModel does not change the current
			active machine, and current machine state would interfere with the snapshot loading -
			e.g. paging is locked */
		spectrum.loadSnapshot(snapshot);
		if (!self.isRunning) {
			spectrum.drawFullScreen();
		}
	}
	function loadTape(tape, opts) {
		if (!opts) opts = {};
		self.currentTape = tape;
		if (opts.autoload) {
			var snapshotBuffer = JSSpeccy.autoloaders[currentModel.tapeAutoloader].buffer;
			var snapshot = JSSpeccy.Z80File(snapshotBuffer);
			loadSnapshot(snapshot);
		}
	}


	/* == Selecting Spectrum model == */
	self.onChangeModel = Event();
	self.getModel = function() {
		return currentModel;
	};
	self.setModel = function(newModel) {
		if (newModel != currentModel) {
			spectrum = JSSpeccy.Spectrum({
				viewport: viewport,
				keyboard: keyboard,
				model: newModel,
				soundBackend: soundBackend,
				controller: self,
				borderEnabled: ('border' in opts) ? opts.border : true
			});
			currentModel = newModel;
			initReferenceTime();
			self.onChangeModel.trigger(newModel);
		}
	};


	/* == Timing / main execution loop == */
	var msPerFrame;
	var remainingMs = 0; /* number of milliseconds that have passed that have not yet been
	'consumed' by running a frame of emulation */

	function initReferenceTime() {
		msPerFrame = (currentModel.frameLength * 1000) / currentModel.clockSpeed;
		remainingMs = 0;
		lastFrameStamp = performance.now();
	}

	var PERFORMANCE_FRAME_COUNT = 10;  /* average over this many frames when measuring performance */
	var performanceTotalMilliseconds = 0;
	var performanceFrameNum = 0;

	var requestAnimationFrame = (
		window.requestAnimationFrame || window.msRequestAnimationFrame ||
		window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		function(callback) {
			setTimeout(function() {
				callback(performance.now());
			}, 10);
		}
	);

	function tick() {
		if (!self.isRunning) return;

		stampBefore = performance.now();
		var timeElapsed = stampBefore - lastFrameStamp;
		remainingMs += stampBefore - lastFrameStamp;
		if (remainingMs > msPerFrame) {
			/* run a frame of emulation */
			spectrum.runFrame();
			var stampAfter = performance.now();

			if (opts.measurePerformance) {
				performanceTotalMilliseconds += (stampAfter - stampBefore);
				performanceFrameNum = (performanceFrameNum + 1) % PERFORMANCE_FRAME_COUNT;
				if (performanceFrameNum === 0) {
					document.title = originalDocumentTitle + ' ' + (performanceTotalMilliseconds / PERFORMANCE_FRAME_COUNT).toFixed(1) + " ms/frame; elapsed: " + timeElapsed;
					performanceTotalMilliseconds = 0;
				}
			}

			remainingMs -= msPerFrame;

			/* As long as requestAnimationFrame runs more frequently than the Spectrum's frame rate -
			which should normally be the case for a focused browser window (approx 60Hz vs 50Hz) -
			there should be either zero or one emulation frames run per call to tick(). If there's more
			than one emulation frame to run (i.e. remainingMs > msPerFrame at this point), we have
			insufficient performance to run at full speed (either the frame is taking more than 20ms to
			execute, or requestAnimationFrame is being called too infrequently). If so, clear
			remainingMs so that it doesn't grow indefinitely
			*/
			if (remainingMs > msPerFrame) remainingMs = 0;
		}
		lastFrameStamp = stampBefore;

		requestAnimationFrame(tick);
	}

	self.onStart = Event();
	self.start = function() {
		if (self.isRunning) return;
		self.isRunning = true;
		updateViewportIcon();
		self.onStart.trigger();

		initReferenceTime();

		requestAnimationFrame(tick);
	};
	self.onStop = Event();
	self.stop = function() {
		self.isRunning = false;
		updateViewportIcon();
		self.onStop.trigger();
	};
	self.reset = function() {
		spectrum.reset();
	};


	/* == Startup conditions == */
	self.setModel(JSSpeccy.Spectrum.MODEL_128K);

	if (opts.loadFile) {
		self.loadFromUrl(opts.loadFile, {'autoload': opts.autoload});
	}

	if (!('audio' in opts) || opts['audio']) {
		self.setAudioState(true);
	} else {
		self.setAudioState(false);
	}

	if (!('autostart' in opts) || opts['autostart']) {
		self.start();
	} else {
		self.stop();
	}


	return self;
}
JSSpeccy.traps = {};
JSSpeccy.traps.tapeLoad = function() {
	/* will be overridden when a JSSpeccy.Spectrum object is initialised */
};
JSSpeccy.Display = function(opts) {
	var self = {};
	
	var viewport = opts.viewport;
	var memory = opts.memory;
	var model = opts.model || JSSpeccy.Spectrum.MODEL_128K;
	var border = opts.borderEnabled;

	var checkerboardFilterEnabled = opts.settings.checkerboardFilter.get();
	opts.settings.checkerboardFilter.onChange.bind(function(newValue) {
		checkerboardFilterEnabled = newValue;
	});
	
	var palette = new Int32Array([
		/* RGBA dark */
		0x000000ff,
		0x2030c0ff,
		0xc04010ff,
		0xc040c0ff,
		0x40b010ff,
		0x50c0b0ff,
		0xe0c010ff,
		0xc0c0c0ff,
		/* RGBA bright */
		0x000000ff,
		0x3040ffff,
		0xff4030ff,
		0xff70f0ff,
		0x50e010ff,
		0x50e0ffff,
		0xffe850ff,
		0xffffffff
	]);

	var testUint8 = new Uint8Array(new Uint16Array([0x8000]).buffer);
	var isLittleEndian = (testUint8[0] === 0);
	if(isLittleEndian) {
		/* need to reverse the byte ordering of palette */
		for(var i = 0; i < 16; i++) {
			var color = palette[i];
			palette[i] = ((color << 24) & 0xff000000) | ((color << 8) & 0xff0000) | ((color >>> 8) & 0xff00) | ((color >>> 24) & 0xff);
		}
	}


	var LEFT_BORDER_CHARS = 4;
	var RIGHT_BORDER_CHARS = 4;
	var TOP_BORDER_LINES = 24;
	var BOTTOM_BORDER_LINES = 24;
	var TSTATES_PER_CHAR = 4;
	
	var TSTATES_UNTIL_ORIGIN = model.tstatesUntilOrigin;
	var TSTATES_PER_SCANLINE = model.tstatesPerScanline;
	self.frameLength = model.frameLength;
	
	var BEAM_X_MAX = 32 + (border ? RIGHT_BORDER_CHARS : 0);
	var BEAM_Y_MAX = 192 + (border ? BOTTOM_BORDER_LINES : 0);
	
	var CANVAS_WIDTH = 256 + (border ? ((LEFT_BORDER_CHARS + RIGHT_BORDER_CHARS) * 8) : 0);
	var CANVAS_HEIGHT = 192 + (border ? (TOP_BORDER_LINES + BOTTOM_BORDER_LINES) : 0);
	
	viewport.setResolution(CANVAS_WIDTH, CANVAS_HEIGHT);
	var ctx = viewport.canvas.getContext('2d');
	var imageData = ctx.getImageData(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
	var pixels = new Int32Array(imageData.data.buffer);

	/* for post-processing */
	var imageData2 = ctx.createImageData(imageData);
	var pixels2 = new Int32Array(imageData2.data.buffer);
	
	var borderColour = 7;
	self.setBorder = function(val) {
		borderColour = val;
	};
	
	var beamX, beamY; /* X character pos and Y pixel pos of beam at next screen event,
		relative to top left of non-border screen; negative / overlarge values are in the border */
	
	var pixelLineAddress; /* Address (relative to start of memory page) of the first screen byte in the current line */
	var attributeLineAddress; /* Address (relative to start of memory page) of the first attribute byte in the current line */
	var imageDataPos; /* offset into imageData buffer of current draw position */
	var currentLineStartTime;
	
	var flashPhase = 0;
	
	self.startFrame = function() {
		self.nextEventTime = currentLineStartTime = TSTATES_UNTIL_ORIGIN - (TOP_BORDER_LINES * TSTATES_PER_SCANLINE) - (LEFT_BORDER_CHARS * TSTATES_PER_CHAR);
		beamX = (border ? -LEFT_BORDER_CHARS : 0);
		beamY = (border ? -TOP_BORDER_LINES : 0);
		pixelLineAddress = 0x0000;
		attributeLineAddress = 0x1800;
		imageDataPos = 0;
		flashPhase = (flashPhase + 1) & 0x1f; /* FLASH has a period of 32 frames (16 on, 16 off) */
	};
	
	self.doEvent = function() {
		if (beamY < 0 | beamY >= 192 | beamX < 0 | beamX >= 32) {
			/* border */
			var color = palette[borderColour];
			pixels[imageDataPos++] = color;
			pixels[imageDataPos++] = color;
			pixels[imageDataPos++] = color;
			pixels[imageDataPos++] = color;
			pixels[imageDataPos++] = color;
			pixels[imageDataPos++] = color;
			pixels[imageDataPos++] = color;
			pixels[imageDataPos++] = color;
		} else {
			/* main screen area */
			var pixelByte = memory.readScreen( pixelLineAddress | beamX );
			var attributeByte = memory.readScreen( attributeLineAddress | beamX );
			
			var inkColor, paperColor;
			if ( (attributeByte & 0x80) && (flashPhase & 0x10) ) {
				/* FLASH: invert ink / paper */
				inkColor = palette[(attributeByte & 0x78) >> 3];
				paperColor = palette[(attributeByte & 0x07) | ((attributeByte & 0x40) >> 3)];
			} else {
				inkColor = palette[(attributeByte & 0x07) | ((attributeByte & 0x40) >> 3)];
				paperColor = palette[(attributeByte & 0x78) >> 3];
			}
			
			pixels[imageDataPos++] = (pixelByte & 0x80) ? inkColor : paperColor;
			pixels[imageDataPos++] = (pixelByte & 0x40) ? inkColor : paperColor;
			pixels[imageDataPos++] = (pixelByte & 0x20) ? inkColor : paperColor;
			pixels[imageDataPos++] = (pixelByte & 0x10) ? inkColor : paperColor;
			pixels[imageDataPos++] = (pixelByte & 0x08) ? inkColor : paperColor;
			pixels[imageDataPos++] = (pixelByte & 0x04) ? inkColor : paperColor;
			pixels[imageDataPos++] = (pixelByte & 0x02) ? inkColor : paperColor;
			pixels[imageDataPos++] = (pixelByte & 0x01) ? inkColor : paperColor;
		}
		
		/* increment beam / nextEventTime for next event */
		beamX++;
		if (beamX < BEAM_X_MAX) {
			self.nextEventTime += TSTATES_PER_CHAR;
		} else {
			beamX = (border ? -LEFT_BORDER_CHARS : 0);
			beamY++;
			
			if (beamY >= 0 && beamY < 192) {
				/* pixel address = 0 0 0 y7 y6 y2 y1 y0 | y5 y4 y3 x4 x3 x2 x1 x0 */
				pixelLineAddress = ( (beamY & 0xc0) << 5 ) | ( (beamY & 0x07) << 8 ) | ( (beamY & 0x38) << 2 );
				/* attribute address = 0 0 0 1 1 0 y7 y6 | y5 y4 y3 x4 x3 x2 x1 x0 */
				attributeLineAddress = 0x1800 | ( (beamY & 0xf8) << 2 );
			}
			
			if (beamY < BEAM_Y_MAX) {
				currentLineStartTime += TSTATES_PER_SCANLINE;
				self.nextEventTime = currentLineStartTime;
			} else {
				self.nextEventTime = null;
			}
		}
	};
	
	self.endFrame = function() {
		if (checkerboardFilterEnabled) {
			self.postProcess();
		} else {
			ctx.putImageData(imageData, 0, 0);
		}
	};

	self.drawFullScreen = function() {
		self.startFrame();
		while (self.nextEventTime) self.doEvent();
		self.endFrame();
	};

	self.postProcess = function() {
		var pix = pixels;
		pixels2.set(pix);
		var ofs = border ? (TOP_BORDER_LINES * CANVAS_WIDTH) + (LEFT_BORDER_CHARS << 3) : 0;
		var skip = border ? ((LEFT_BORDER_CHARS + RIGHT_BORDER_CHARS) << 3) : 0;
		var width = CANVAS_WIDTH;
		var x = 0, y = 1; /* 1-pixel top/bottom margin */
		var k0 = 0, k1 = 0, k2 = 0, k3 = 0, k4 = 0, k5 = 0, k6 = 0, k7 = 0, k8 = 0;
		var avg0, avg1, avg2;
		while (y++ < 191) {
			while (x++ < 256) {
				k0 = pix[ofs - 1]; k1 = pix[ofs]; k2 = pix[ofs + 1]; ofs += width;
				k3 = pix[ofs - 1]; k4 = pix[ofs]; k5 = pix[ofs + 1]; ofs += width;
				k6 = pix[ofs - 1]; k7 = pix[ofs]; k8 = pix[ofs + 1];
				
				var mixed = ((k4 !== k1 || k4 !== k7) && (k4 !== k3 || k4 !== k5));
				
				if (k4 === k0 && k4 === k2 && k4 !== k1 && k4 !== k3 && k4 !== k5) {
					pixels2[ofs - width] = (((k4 ^ k3) & 0xfefefefe) >> 1) + (k4 & k3);
				}
				else if (k4 === k6 && k4 === k8 && k4 !== k3 && k4 !== k5 && k4 !== k7) {
					pixels2[ofs - width] = (((k4 ^ k3) & 0xfefefefe) >> 1) + (k4 & k3);
				}
				else if (k4 === k0 && k4 === k6 && k4 !== k1 && k4 !== k3 && k4 !== k7) {
					pixels2[ofs - width] = (((k4 ^ k1) & 0xfefefefe) >> 1) + (k4 & k1);
				}
				else if (k4 === k2 && k4 === k8 && k4 !== k1 && k4 !== k5 && k4 !== k7) {
					pixels2[ofs - width] = (((k4 ^ k1) & 0xfefefefe) >> 1) + (k4 & k1);
				}
				else if (mixed) {
					avg0 = (((k3 ^ k5) & 0xfefefefe) >> 1) + (k3 & k5);
					avg1 = (((k1 ^ k7) & 0xfefefefe) >> 1) + (k1 & k7);
					avg2 = (((avg0 ^ avg1) & 0xfefefefe) >> 1) + (avg0 & avg1);
					avg2 = (((k4 ^ avg2) & 0xfefefefe) >> 1) + (k4 & avg2);
					pixels2[ofs - width] = (((k4 ^ avg2) & 0xfefefefe) >> 1) + (k4 & avg2);
				}
				ofs -= (width + width - 1);
			}
			ofs += skip;
			x = 0;
		}
		ctx.putImageData(imageData2, 0, 0);
	};

	return self;
};
JSSpeccy.IOBus = function(opts) {
	var self = {};
	
	var keyboard = opts.keyboard;
	var display = opts.display;
	var memory = opts.memory;
	var sound = opts.sound;
	var contentionTable = opts.contentionTable;
	var contentionTableLength = contentionTable.length;
	
	self.read = function(addr) {
		if ((addr & 0x0001) === 0x0000) {
			return keyboard.poll(addr);
		} else if ((addr & 0xc002) == 0xc000) {
			/* AY chip */
			return sound.readSoundRegister();
		} else if ((addr & 0x00e0) === 0x0000) {
			/* kempston joystick */
			return 0;
		} else {
			return 0xff;
		}
	};
	self.write = function(addr, val, tstates) {
		if (!(addr & 0x01)) {
			display.setBorder(val & 0x07);

			sound.updateBuzzer((val & 16) >> 4, tstates);
		}
		if (!(addr & 0x8002)) {
			memory.setPaging(val);
		}
		
		if ((addr & 0xc002) == 0xc000) {
			/* AY chip - register select */
			sound.selectSoundRegister( val & 0xF );
		}
		
		if ((addr & 0xc002) == 0x8000) {
			/* AY chip - data write */
			sound.writeSoundRegister(val, tstates);
		}
		
	};

	self.isULAPort = function(addr) {
		return ((addr & 0x0001) === 0x0000);
	};
	self.contend = function(addr, tstates) {
		return contentionTable[tstates % contentionTableLength];
	};

	return self;
};
JSSpeccy.Keyboard = function() {
	var self = {};
	self.active = true;
	
	var keyStates = [];
	for (var row = 0; row < 8; row++) {
		keyStates[row] = 0xff;
	}
	
	function keyDown(evt) {
		if (self.active) {
			registerKeyDown(evt.keyCode);
			if (!evt.metaKey) return false;
		}
	}
	function registerKeyDown(keyNum) {
		var keyCode = keyCodes[keyNum];
		if (keyCode == null) return;
		keyStates[keyCode.row] &= ~(keyCode.mask);
		if (keyCode.caps) keyStates[0] &= 0xfe;
	}
	function keyUp(evt) {
		registerKeyUp(evt.keyCode);
		if (self.active && !evt.metaKey) return false;
	}
	function registerKeyUp(keyNum) {
		var keyCode = keyCodes[keyNum];
		if (keyCode == null) return;
		keyStates[keyCode.row] |= keyCode.mask;
		if (keyCode.caps) keyStates[0] |= 0x01;
	}
	function keyPress(evt) {
		if (self.active && !evt.metaKey) return false;
	}
	
	var keyCodes = {
		49: {row: 3, mask: 0x01}, /* 1 */
		50: {row: 3, mask: 0x02}, /* 2 */
		51: {row: 3, mask: 0x04}, /* 3 */
		52: {row: 3, mask: 0x08}, /* 4 */
		53: {row: 3, mask: 0x10}, /* 5 */
		54: {row: 4, mask: 0x10}, /* 6 */
		55: {row: 4, mask: 0x08}, /* 7 */
		56: {row: 4, mask: 0x04}, /* 8 */
		57: {row: 4, mask: 0x02}, /* 9 */
		48: {row: 4, mask: 0x01}, /* 0 */
	
		81: {row: 2, mask: 0x01}, /* Q */
		87: {row: 2, mask: 0x02}, /* W */
		69: {row: 2, mask: 0x04}, /* E */
		82: {row: 2, mask: 0x08}, /* R */
		84: {row: 2, mask: 0x10}, /* T */
		89: {row: 5, mask: 0x10}, /* Y */
		85: {row: 5, mask: 0x08}, /* U */
		73: {row: 5, mask: 0x04}, /* I */
		79: {row: 5, mask: 0x02}, /* O */
		80: {row: 5, mask: 0x01}, /* P */
	
		65: {row: 1, mask: 0x01}, /* A */
		83: {row: 1, mask: 0x02}, /* S */
		68: {row: 1, mask: 0x04}, /* D */
		70: {row: 1, mask: 0x08}, /* F */
		71: {row: 1, mask: 0x10}, /* G */
		72: {row: 6, mask: 0x10}, /* H */
		74: {row: 6, mask: 0x08}, /* J */
		75: {row: 6, mask: 0x04}, /* K */
		76: {row: 6, mask: 0x02}, /* L */
		13: {row: 6, mask: 0x01}, /* enter */
	
		16: {row: 0, mask: 0x01}, /* caps */
		192: {row: 0, mask: 0x01}, /* backtick as caps - because firefox screws up a load of key codes when pressing shift */
		90: {row: 0, mask: 0x02}, /* Z */
		88: {row: 0, mask: 0x04}, /* X */
		67: {row: 0, mask: 0x08}, /* C */
		86: {row: 0, mask: 0x10}, /* V */
		66: {row: 7, mask: 0x10}, /* B */
		78: {row: 7, mask: 0x08}, /* N */
		77: {row: 7, mask: 0x04}, /* M */
		17: {row: 7, mask: 0x02}, /* sym - gah, firefox screws up ctrl+key too */
		32: {row: 7, mask: 0x01}, /* space */
		
		/* shifted combinations */
		8: {row: 4, mask: 0x01, caps: true}, /* backspace => caps + 0 */
		37: {row: 3, mask: 0x10, caps: true}, /* left arrow => caps + 5 */
		38: {row: 4, mask: 0x08, caps: true}, /* up arrow => caps + 7 */
		39: {row: 4, mask: 0x04, caps: true}, /* right arrow => caps + 8 */
		40: {row: 4, mask: 0x10, caps: true}, /* down arrow => caps + 6 */
		
		999: null
	};
	
	self.poll = function(addr) {
		var result = 0xff;
		for (var row = 0; row < 8; row++) {
			if (!(addr & (1 << (row+8)))) { /* bit held low, so scan this row */
				result &= keyStates[row];
			}
		}
		return result;
	}
	
	document.onkeydown = keyDown;
	document.onkeyup = keyUp;
	document.onkeypress = keyPress;
	
	return self;
}
// Sound routines for jsspeccy
// General sound routines and 48k buzzer emulation written by Darren Coles
// 128k Spectrum sound routines developed from DelphiSpec emulator (credits below).
// (c) 2013 Darren Coles
//
// Credits from DelphiSpec:
//
//  Routines for emulating the 128K Spectrum's AY-3-8912 sound generator
//
//  Author: James Bagg <chipmunk_uk_1@hotmail.com>
//
//   With minor optimisations and mods by
//           Chris Cowley <ccowley@grok.co.uk>
//
//   Translation to Delphi Object Pascal by
//           Jari Korhonen <jarit.korhonen@luukku.com>
//
//   Copyright (C)1999-2000 Grok Developments Ltd  and James Bagg
//   http://www.grok.co.uk/      http://www.chipmunks-corner.co.uk
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of the GNU General Public License
//   as published by the Free Software Foundation; either version 2
//   of the License, or (at your option) any later version.
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// *******************************************************************************/

JSSpeccy.SoundGenerator = function(opts) {
	var self = {};

	var clockSpeed = opts.model.clockSpeed;
	var frameLength = opts.model.frameLength;
	var backend = opts.soundBackend;
	var sampleRate = backend.sampleRate;
	var samplesPerFrame = Math.floor(sampleRate * frameLength / clockSpeed); /* TODO: account for this not being an integer by generating a variable number of samples per frame */

	var oversampleRate = 8;
	var buzzer_val = 0;

	var soundData = new Array();
	var soundDataFrameBytes = 0;

	var lastaudio = 0;

	var frameCount = 0;

	var audio = null;
	var audioContext = null;
	var audioNode = null;

	var WCount = 0;
	var lCounter = 0;

	var aySoundData = new Array;
	var soundDataAyFrameBytes = 0;


	var ayRegSelected = 0;
	var lastAyAudio = 0;

	//ay stuff
	var MAX_OUTPUT = 63;
    var AY_STEP = 32768;
    var MAXVOL  = 31;

	// AY register ID's
	var AY_AFINE = 0;
  	var AY_ACOARSE = 1;
  	var AY_BFINE = 2;
  	var AY_BCOARSE = 3;
  	var AY_CFINE = 4;
  	var AY_CCOARSE = 5;
  	var AY_NOISEPER = 6;
  	var AY_ENABLE = 7;
  	var AY_AVOL = 8;
  	var AY_BVOL = 9;
  	var AY_CVOL = 10;
  	var AY_EFINE = 11;
  	var AY_ECOARSE = 12;
  	var AY_ESHAPE = 13;
  	var AY_PORTA = 14;
  	var AY_PORTB = 15;
	
	var RegArray = new Int32Array(16);
    var VolTableArray 
	
	var AY8912_sampleRate = 0;
    var AY8912_register_latch=0;
    var AY8912_Regs = new Int32Array(16);
    var AY8912_UpdateStep = 0;//Double;
    var AY8912_PeriodA = 0;
    var AY8912_PeriodB = 0;
    var AY8912_PeriodC = 0;
    var AY8912_PeriodN = 0;
    var AY8912_PeriodE = 0;
    var AY8912_CountA = 0;
    var AY8912_CountB = 0;
    var AY8912_CountC = 0;
    var AY8912_CountN = 0;
    var AY8912_CountE = 0;
    var AY8912_VolA = 0;
    var AY8912_VolB = 0;
    var AY8912_VolC = 0;
    var AY8912_VolE = 0;
    var AY8912_EnvelopeA = 0;
    var AY8912_EnvelopeB = 0;
    var AY8912_EnvelopeC = 0;
    var AY8912_OutputA = 0;
    var AY8912_OutputB = 0;
    var AY8912_OutputC = 0;
    var AY8912_OutputN = 0;
    var AY8912_CountEnv = 0;
    var AY8912_Hold = 0;
    var AY8912_Alternate = 0;
    var AY8912_Attack = 0;
    var AY8912_Holding = 0;
    var AY8912_VolTable2 = new Int32Array(64);
	
	var AY_OutNoise = 0;
	AY8912_init(clockSpeed / 2, sampleRate, 8);

	function AY8912_reset() {
		AY8912_register_latch = 0;
		AY8912_OutputA = 0;
		AY8912_OutputB = 0;
		AY8912_OutputC = 0;
		AY8912_OutputN = 0xFF;
		AY8912_PeriodA = 0;
		AY8912_PeriodB = 0;
		AY8912_PeriodC = 0;
		AY8912_PeriodN = 0;
		AY8912_PeriodE = 0;
		AY8912_CountA = 0;
		AY8912_CountB = 0;
		AY8912_CountC = 0;
		AY8912_CountN = 0;
		AY8912_CountE = 0;
		AY8912_VolA = 0;
		AY8912_VolB = 0;
		AY8912_VolC = 0;
		AY8912_VolE = 0;
		AY8912_EnvelopeA = 0;
		AY8912_EnvelopeB = 0;
		AY8912_EnvelopeC = 0;
		AY8912_CountEnv = 0;
		AY8912_Hold = 0;
		AY8912_Alternate = 0;
		AY8912_Holding = 0;
		AY8912_Attack = 0;
		
		for (var i = 0; i<=AY_PORTA; i++) {
			AYWriteReg(i, 0);     //* AYWriteReg() uses the timer system; we cannot
		}                    //* call it at this time because the timer system
                          //* has not been initialized.
	}
	
	function AY8912_set_clock(clock) {
    // the AY_STEP clock for the tone and noise generators is the chip clock    
    //divided by 8; for the envelope generator of the AY-3-8912, it is half 
    // that much (clock/16), but the envelope of the YM2149 goes twice as    
    // fast, therefore again clock/8.                                        
    // Here we calculate the number of AY_STEPs which happen during one sample  
    // at the given sample rate. No. of events = sample rate / (clock/8).    */
    // AY_STEP is a multiplier used to turn the fraction into a fixed point     */
    // number.}
		var t1 = AY_STEP * AY8912_sampleRate * 8.0;
		AY8912_UpdateStep = t1 / clock;
	}

//
// ** set output gain
// **
// ** The gain is expressed in 0.2dB increments, e.g. a gain of 10 is an increase
// ** of 2dB. Note that the gain only affects sounds not playing at full volume,
// ** since the ones at full volume are already played at the maximum intensity
// ** allowed by the sound card.
// ** 0x00 is the default.
// ** 0xff is the maximum allowed value.
// 

	function AY8912_set_volume(volume,gain) {
		var i, out1, out2;	
    
		gain = gain & 0xFF;

		// increase max output basing on gain (0.2 dB per AY_STEP) */
		out1 = MAX_OUTPUT;
		out2 = MAX_OUTPUT;

		while (gain > 0) 
		{
			gain--;
			out1 = out1 * 1.023292992;  ///* = (10 ^ (0.2/20)) */
			out2 = out2 * 1.023292992;
		}

		//  calculate the volume.voltage conversion table 
		//  The AY-3-8912 has 16 levels, in a logarithmic scale (3dB per AY_STEP) 
		//  The YM2149 still has 16 levels for the tone generators, but 32 for 
		//  the envelope generator (1.5dB per AY_STEP).
		for (var i = 31; i>=0; i--) {
			//* limit volume to avoid clipping */
			if (out2 > MAX_OUTPUT) 
				AY8912_VolTable2[i] = MAX_OUTPUT
			else
				AY8912_VolTable2[i] = Math.round(out2);

			out1 = out1 / 1.188502227; // .188502227 '/* = 10 ^ (1.5/20) = 1.5dB */
			out2 = out2 / 1.188502227  // .188502227
		}
		AY8912_VolTable2[63] = MAX_OUTPUT;
	}

	function AYWriteReg(r,v) {
		var old;

		AY8912_Regs[r] = v;

	  //'/* A note about the period of tones, noise and envelope: for speed reasons,*/
	  //'/* we count down from the period to 0, but careful studies of the chip     */
	  //'/* output prove that it instead counts up from 0 until the counter becomes */
	  //'/* greater or equal to the period. This is an important difference when the*/
	  //'/* program is rapidly changing the period to modulate the sound.           */
	  //'/* To compensate for the difference, when the period is changed we adjust  */
	  //'/* our internal counter.                                                   */
	  //'/* Also, note that period = 0 is the same as period = 1. This is mentioned */
	  //'/* in the YM2203 data sheets. However, this does NOT apply to the Envelope */
	  //'/* period. In that case, period = 0 is half as period = 1. 
	  switch (r ) 
	  {
		case AY_AFINE:
		case AY_ACOARSE:
		  
			AY8912_Regs[AY_ACOARSE] = AY8912_Regs[AY_ACOARSE] & 0xF;

			old = AY8912_PeriodA;

			AY8912_PeriodA = Math.round((AY8912_Regs[AY_AFINE] + (256 * AY8912_Regs[AY_ACOARSE]))
			   *AY8912_UpdateStep);

			if (AY8912_PeriodA == 0)
			  AY8912_PeriodA = Math.round(AY8912_UpdateStep);

			AY8912_CountA = AY8912_CountA + (AY8912_PeriodA - old);

			if (AY8912_CountA <= 0) 
			  AY8912_CountA = 1;
		  break;
		case AY_BFINE:
		case AY_BCOARSE:
		  
			AY8912_Regs[AY_BCOARSE] = AY8912_Regs[AY_BCOARSE] & 0xF;

			old = AY8912_PeriodB;

			AY8912_PeriodB = Math.round((AY8912_Regs[AY_BFINE] + (256 * AY8912_Regs[AY_BCOARSE]))
			  * AY8912_UpdateStep);

			if (AY8912_PeriodB == 0) 
			  AY8912_PeriodB = Math.round(AY8912_UpdateStep);

			AY8912_CountB = AY8912_CountB + AY8912_PeriodB - old;

			if (AY8912_CountB <= 0) 
			  AY8912_CountB = 1;
		  break;

		case AY_CFINE:
		case AY_CCOARSE:
		  
			AY8912_Regs[AY_CCOARSE] = AY8912_Regs[AY_CCOARSE] & 0xF;

			old = AY8912_PeriodC;

			AY8912_PeriodC = Math.round((AY8912_Regs[AY_CFINE] + (256 * AY8912_Regs[AY_CCOARSE]))
			  * AY8912_UpdateStep);

			if (AY8912_PeriodC == 0) 
			  AY8912_PeriodC = Math.round(AY8912_UpdateStep);

			AY8912_CountC = AY8912_CountC + (AY8912_PeriodC - old);

			if (AY8912_CountC <= 0) 
			  AY8912_CountC = 1;
		  break;

		case AY_NOISEPER:
		  
			AY8912_Regs[AY_NOISEPER] = AY8912_Regs[AY_NOISEPER] & 0x1F;

			old = AY8912_PeriodN;

			AY8912_PeriodN = Math.round(AY8912_Regs[AY_NOISEPER] * AY8912_UpdateStep);

			if (AY8912_PeriodN == 0) 
			  AY8912_PeriodN = Math.round(AY8912_UpdateStep);

			AY8912_CountN = AY8912_CountN + (AY8912_PeriodN - old);

			if (AY8912_CountN <= 0) 
			  AY8912_CountN = 1;
		  break;

		case AY_AVOL:
		  
			AY8912_Regs[AY_AVOL] = AY8912_Regs[AY_AVOL] & 0x1F;

			AY8912_EnvelopeA = AY8912_Regs[AY_AVOL] & 0x10;

			if (AY8912_EnvelopeA != 0) 
				AY8912_VolA = AY8912_VolE
			else
			{
				if (AY8912_Regs[AY_AVOL] != 0) 
					AY8912_VolA = AY8912_VolTable2[AY8912_Regs[AY_AVOL] * 2 + 1]
				else
					AY8912_VolA = AY8912_VolTable2[0];
			}
		  break;

		case AY_BVOL:
		  
			AY8912_Regs[AY_BVOL] = AY8912_Regs[AY_BVOL] & 0x1F;

			AY8912_EnvelopeB = AY8912_Regs[AY_BVOL] & 0x10;

			if (AY8912_EnvelopeB != 0) 
				AY8912_VolB = AY8912_VolE
			else
			{
				if (AY8912_Regs[AY_BVOL] != 0) 
					AY8912_VolB = AY8912_VolTable2[AY8912_Regs[AY_BVOL] * 2 + 1]
				else
					AY8912_VolB = AY8912_VolTable2[0];
			};
		  break;

		case AY_CVOL:
		  
			AY8912_Regs[AY_CVOL] = AY8912_Regs[AY_CVOL] & 0x1F;

			AY8912_EnvelopeC = AY8912_Regs[AY_CVOL] & 0x10;

			if (AY8912_EnvelopeC != 0) 
				AY8912_VolC = AY8912_VolE
			else
			{
				if (AY8912_Regs[AY_CVOL] != 0) 
					AY8912_VolC = AY8912_VolTable2[AY8912_Regs[AY_CVOL] * 2 + 1]
				else
					AY8912_VolC = AY8912_VolTable2[0];
			};
		  break;

		case AY_EFINE:
		case AY_ECOARSE:
		  
			old = AY8912_PeriodE;

			AY8912_PeriodE = Math.round(((AY8912_Regs[AY_EFINE] + (256 * AY8912_Regs[AY_ECOARSE])))
			  * AY8912_UpdateStep);

			if (AY8912_PeriodE == 0) 
			  AY8912_PeriodE = Math.round(AY8912_UpdateStep / 2);

			AY8912_CountE = AY8912_CountE + (AY8912_PeriodE - old);

			if (AY8912_CountE <= 0) 
			  AY8912_CountE = 1
		  break;

		case AY_ESHAPE:
		  
			//'/* envelope shapes:
			//'C AtAlH
			//'0 0 x x  \___
			//'
			//'0 1 x x  /___
			//'
			//'1 0 0 0  \\\\
			//'
			//'1 0 0 1  \___
			//'
			//'1 0 1 0  \/\/
			//'          ___
			//'1 0 1 1  \
			//'
			//'1 1 0 0  ////
			//'          ___
			//'1 1 0 1  /
			//'
			//'1 1 1 0  /\/\
			//'
			//'1 1 1 1  /___
			//'
			//'The envelope counter on the AY-3-8910 has 16 AY_STEPs. On the YM2149 it
			//'has twice the AY_STEPs, happening twice as fast. Since the end result is
			//'just a smoother curve, we always use the YM2149 behaviour.
			//'*/}
			if (AY8912_Regs[AY_ESHAPE] != 0xFF) 
			{
			  AY8912_Regs[AY_ESHAPE] = AY8912_Regs[AY_ESHAPE] & 0xF;

			  if ((AY8912_Regs[AY_ESHAPE] & 0x4) == 0x4) 
				AY8912_Attack = MAXVOL
			  else
				AY8912_Attack = 0x0;

			  AY8912_Hold = AY8912_Regs[AY_ESHAPE] & 0x1;

			  AY8912_Alternate = AY8912_Regs[AY_ESHAPE] & 0x2;

			  AY8912_CountE = AY8912_PeriodE;

			  AY8912_CountEnv = MAXVOL; // &h1f

			  AY8912_Holding = 0;

			  AY8912_VolE = AY8912_VolTable2[AY8912_CountEnv ^ AY8912_Attack];

			  if (AY8912_EnvelopeA != 0) 
				AY8912_VolA = AY8912_VolE;

			  if (AY8912_EnvelopeB != 0) 
				AY8912_VolB = AY8912_VolE;

			  if (AY8912_EnvelopeC != 0) 
				AY8912_VolC = AY8912_VolE;
			}
		  break;
	  }
	}

	function AYReadReg(r) {
		return AY8912_Regs[r];
	}


	function AY8912_init(clock, sample_rate, sample_bits) {
	  AY8912_sampleRate = sample_rate;
	  AY8912_set_clock(clock);
	  AY8912_set_volume(255, 12);
	  AY8912_reset();
	  return 0;
	}

	function AY8912Update_8() {
		var Buffer_Length = 400;
			
		  //  The 8910 has three outputs, each output is the mix of one of the three 
		  //  tone generators and of the (single) noise generator. The two are mixed 
		  //  BEFORE going into the DAC. The formula to mix each channel is: 
		  //  (ToneOn | ToneDisable) & (NoiseOn | NoiseDisable). 
		  //  Note that this means that if both tone and noise are disabled, the output 
		  //  is 1, not 0, and can be modulated changing the volume. 
		  //  if the channels are disabled, set their output to 1, and increase the 
		  //  counter, if necessary, so they will not be inverted during this update. 
		  //  Setting the output to 1 is necessary because a disabled channel is locked 
		  //  into the ON state (see above); and it has no effect if the volume is 0. 
		  //  if the volume is 0, increase the counter, but don't touch the output. 

		  if ((AY8912_Regs[AY_ENABLE] & 0x1) == 0x1) {
		  
			if (AY8912_CountA <= (Buffer_Length * AY_STEP))
				AY8912_CountA = AY8912_CountA + (Buffer_Length * AY_STEP);

			AY8912_OutputA = 1;
		  }
		  else if (AY8912_Regs[AY_AVOL] == 0) {
		  
			  // note that I do count += Buffer_Length, NOT count = Buffer_Length + 1. You might think
			  // it's the same since the volume is 0, but doing the latter could cause
			  // interferencies when the program is rapidly modulating the volume.
			  if (AY8912_CountA <= (Buffer_Length * AY_STEP))
				AY8912_CountA = AY8912_CountA + (Buffer_Length * AY_STEP);
		  }

		  if ((AY8912_Regs[AY_ENABLE] & 0x2) == 0x2) {
		  
			  if (AY8912_CountB <= (Buffer_Length * AY_STEP)) 
				AY8912_CountB = AY8912_CountB + (Buffer_Length * AY_STEP);

			  AY8912_OutputB = 1;
		  }
		  else if (AY8912_Regs[AY_BVOL] == 0) {
			  if (AY8912_CountB <= (Buffer_Length * AY_STEP) )
				AY8912_CountB = AY8912_CountB + (Buffer_Length * AY_STEP);
		  }

		  if ((AY8912_Regs[AY_ENABLE] & 0x4) == 0x4) {
			  if (AY8912_CountC <= (Buffer_Length * AY_STEP)) 
				AY8912_CountC = AY8912_CountC + (Buffer_Length * AY_STEP);

			  AY8912_OutputC = 1;
		  }
		  else if ((AY8912_Regs[AY_CVOL] ==0 )) {
			  if (AY8912_CountC <= (Buffer_Length * AY_STEP)) 
				AY8912_CountC = AY8912_CountC + (Buffer_Length * AY_STEP);
		  }

		  // for the noise channel we must not touch OutputN - it's also not necessary 
		  // since we use AY_OutNoise. 
		  if ((AY8912_Regs[AY_ENABLE] & 0x38) == 0x38) { // all off 
			  if (AY8912_CountN <= (Buffer_Length * AY_STEP)) 
				AY8912_CountN = AY8912_CountN + (Buffer_Length * AY_STEP);
		  }

		AY_OutNoise = (AY8912_OutputN | AY8912_Regs[AY_ENABLE]);
	}

	function RenderSample() {
		
		var VolA,VolB,VolC,AY_Left,lOut1,lOut2,lOut3,AY_NextEvent;

		VolA = 0; VolB = 0; VolC = 0;

		//vola, volb and volc keep track of how long each square wave stays
		//in the 1 position during the sample period.

		AY_Left = AY_STEP;

		do {
			AY_NextEvent = 0;

			if (AY8912_CountN < AY_Left) 
				AY_NextEvent = AY8912_CountN
			else
				AY_NextEvent = AY_Left;

			if ((AY_OutNoise & 0x8) == 0x8) {
				if (AY8912_OutputA == 1)  VolA = VolA + AY8912_CountA;

				AY8912_CountA = AY8912_CountA - AY_NextEvent;

				//PeriodA is the half period of the square wave. Here, in each
				// loop I add PeriodA twice, so that at the end of the loop the
				// square wave is in the same status (0 or 1) it was at the start.
				// vola is also incremented by PeriodA, since the wave has been 1
				// exactly half of the time, regardless of the initial position.
				// If we exit the loop in the middle, OutputA has to be inverted
				// and vola incremented only if the exit status of the square
				// wave is 1.

				 while (AY8912_CountA <= 0) {
					AY8912_CountA = AY8912_CountA + AY8912_PeriodA;
					if (AY8912_CountA > 0) {
						if ((AY8912_Regs[AY_ENABLE] & 1) == 0)  AY8912_OutputA = AY8912_OutputA ^ 1;
						if (AY8912_OutputA!=0)  VolA = VolA + AY8912_PeriodA;
						break;
					}

					AY8912_CountA = AY8912_CountA + AY8912_PeriodA;
					VolA = VolA + AY8912_PeriodA;
				}
				if (AY8912_OutputA == 1)  VolA = VolA - AY8912_CountA;
			}
			else {
				AY8912_CountA = AY8912_CountA - AY_NextEvent;

				while (AY8912_CountA <= 0) {
					AY8912_CountA = AY8912_CountA + AY8912_PeriodA;
					if (AY8912_CountA > 0) {
						AY8912_OutputA = AY8912_OutputA ^ 1;
						break;
					}
					AY8912_CountA = AY8912_CountA + AY8912_PeriodA;
				}
			}

			if ((AY_OutNoise & 0x10) == 0x10) { 
				if (AY8912_OutputB == 1)  VolB = VolB + AY8912_CountB;
				AY8912_CountB = AY8912_CountB - AY_NextEvent;

				while (AY8912_CountB <= 0) {
					AY8912_CountB = AY8912_CountB + AY8912_PeriodB;
					if (AY8912_CountB > 0) {
						if ((AY8912_Regs[AY_ENABLE] & 2) == 0)  AY8912_OutputB = AY8912_OutputB ^ 1;
						if (AY8912_OutputB!=0)  VolB = VolB + AY8912_PeriodB;
						break;
					}
					AY8912_CountB = AY8912_CountB + AY8912_PeriodB;
					VolB = VolB + AY8912_PeriodB;
				}
				if (AY8912_OutputB == 1)  VolB = VolB - AY8912_CountB;
			}
			else {
				AY8912_CountB = AY8912_CountB - AY_NextEvent;

				while (AY8912_CountB <= 0) {
					AY8912_CountB = AY8912_CountB + AY8912_PeriodB;
					if (AY8912_CountB > 0)  {
						AY8912_OutputB = AY8912_OutputB ^ 1;
						break;
					}
					AY8912_CountB = AY8912_CountB + AY8912_PeriodB;
				}
			}

			if ((AY_OutNoise & 0x20) == 0x20)  {
				if (AY8912_OutputC == 1)  VolC = VolC + AY8912_CountC;
				AY8912_CountC = AY8912_CountC - AY_NextEvent;
				while (AY8912_CountC <= 0) {
					AY8912_CountC = AY8912_CountC + AY8912_PeriodC;
					if (AY8912_CountC > 0) {
						if ((AY8912_Regs[AY_ENABLE] & 4) == 0)  AY8912_OutputC = AY8912_OutputC ^ 1;
						if (AY8912_OutputC!=0)  VolC = VolC + AY8912_PeriodC;
						break;
					}

					AY8912_CountC = AY8912_CountC + AY8912_PeriodC;
					VolC = VolC + AY8912_PeriodC;
				}
				if (AY8912_OutputC == 1)  VolC = VolC - AY8912_CountC;
			}
			else {

				AY8912_CountC = AY8912_CountC - AY_NextEvent;
				while (AY8912_CountC <= 0) {
					AY8912_CountC = AY8912_CountC + AY8912_PeriodC;
					if (AY8912_CountC > 0) {
						AY8912_OutputC = AY8912_OutputC ^ 1;
						break;
					}
					AY8912_CountC = AY8912_CountC + AY8912_PeriodC;
				}
			}

			AY8912_CountN = AY8912_CountN - AY_NextEvent;
			if (AY8912_CountN <= 0) {
				//Is noise output going to change?
				AY8912_OutputN = Math.round(Math.random()*510);
				AY_OutNoise = (AY8912_OutputN | AY8912_Regs[AY_ENABLE]);
				AY8912_CountN = AY8912_CountN + AY8912_PeriodN;
			}

			AY_Left = AY_Left - AY_NextEvent;
		} while (AY_Left > 0);


		if (AY8912_Holding == 0) {
			AY8912_CountE = AY8912_CountE - AY_STEP;
			if (AY8912_CountE <= 0) {
				do {
					AY8912_CountEnv = AY8912_CountEnv - 1;
					AY8912_CountE = AY8912_CountE + AY8912_PeriodE;
				}
				while (AY8912_CountE <= 0);

				//check envelope current position
				if (AY8912_CountEnv < 0) {
					if (AY8912_Hold!=0) {
						if (AY8912_Alternate!=0) {
							AY8912_Attack = AY8912_Attack ^ MAXVOL; //0x1f
						}
						AY8912_Holding = 1;
						AY8912_CountEnv = 0;
					}
					else {
						//if CountEnv has looped an odd number of times (usually 1),
						//invert the output.
						if ((AY8912_Alternate!=0) & ((AY8912_CountEnv & 0x20) == 0x20)) {
							AY8912_Attack = AY8912_Attack ^ MAXVOL; //0x1f
						}

						AY8912_CountEnv = AY8912_CountEnv & MAXVOL;  //0x1f
					}
					
				}

				AY8912_VolE = AY8912_VolTable2[AY8912_CountEnv ^ AY8912_Attack];

				//reload volume
				if (AY8912_EnvelopeA != 0)  AY8912_VolA = AY8912_VolE;
				if (AY8912_EnvelopeB != 0)  AY8912_VolB = AY8912_VolE;
				if (AY8912_EnvelopeC != 0)  AY8912_VolC = AY8912_VolE;
			}
		}
		

		lOut1 = (VolA * AY8912_VolA) / 65535;
		lOut2 = (VolB * AY8912_VolB) / 65535;
		lOut3 = (VolC * AY8912_VolC) / 65535;

		return  (lOut1 + lOut2 + lOut3) / 63;
	}

	
	
	function fillBuffer(buffer) {
		var n = 0;
		
		for (var i=0; i<buffer.length; i++) {
			var avg = 0;
			for (var j=0; j<oversampleRate; j++) {
				avg = avg + soundData[n++];
			}
			avg = avg / oversampleRate;
			avg = avg * 0.7;
			avg = avg + aySoundData[i] / 2;
			
			buffer[i] = avg;
		}
		
		if (n>=soundData.Length) {
			soundData = new Array();
		}
		else {
			soundData.splice(0,n);
		}

		if (buffer.length>=aySoundData.Length) {
			aySoundData = new Array();
		}
		else {
			aySoundData.splice(0,buffer.length);
		}
		
	}
	backend.setSource(fillBuffer);

	function handleAySound(size) {
		if (!backend.isEnabled) return;
		size = Math.floor(size);
		while (size--) {
			WCount++;
			if (WCount==25) {
				AY8912Update_8();
				WCount = 0;
			}
			aySoundData.push(RenderSample());
			soundDataAyFrameBytes++;
		}	
	}
	
	self.updateBuzzer = function(val, currentTstates) {
		if (val==0) val = -1;

		if (buzzer_val!=val) {	
			var sound_size = (currentTstates - lastaudio) * sampleRate * oversampleRate / clockSpeed;
			self.createSoundData(sound_size, buzzer_val);			
			
			buzzer_val = val;			
			lastaudio = currentTstates;
		}
	}
	
	self.createSoundData = function (size, val) {
		if (!backend.isEnabled) return;
		size = Math.floor(size);
		if (size>=1) {
			for (var i=0; i<size; i++) {
				soundData.push(val);
			}
			soundDataFrameBytes+=size;
		}
	}

	self.endFrame = function() {

		var pad_val = 0;
		if (lastaudio) pad_val = buzzer_val;

		self.createSoundData(samplesPerFrame * oversampleRate - soundDataFrameBytes,pad_val);
		handleAySound(samplesPerFrame - soundDataAyFrameBytes);
		lastaudio = 0;
		lastAyAudio = 0;
		soundDataFrameBytes = 0;
		soundDataAyFrameBytes = 0;
		if (frameCount++<2) return;
		if (backend.isEnabled) {
			backend.notifyReady(soundData.length / oversampleRate);
		}

	}
	
	self.selectSoundRegister = function(reg) {
		ayRegSelected = reg;
	}

	self.writeSoundRegister = function(val, currentTstates) {

		var sound_size = (currentTstates - lastAyAudio) * sampleRate / clockSpeed;
		handleAySound(sound_size);
			
		lastAyAudio = currentTstates;

		AYWriteReg(ayRegSelected,val);
	}
	
	self.readSoundRegister = function() {
		return AYReadReg(ayRegSelected);
	}
	
	self.reset = function() {
		AY_OutNoise = 0;
		AY8912_init(clockSpeed / 2, sampleRate, 8);
	}

	return self;
};

JSSpeccy.SoundBackend = function() {
	var self = {};

	/* Regardless of the underlying implementation, an instance of SoundBackend exposes the API:
		sampleRate: sample rate required by this backend
		isEnabled: whether audio is currently enabled
		setSource(fn): specify a function fn to be called whenever we want to receive audio data.
			fn is passed a buffer object to be filled
		setAudioState(state): if state == true, enable audio; if state == false, disable.
			Return new state (may not match the passed in state - e.g. if sound is unavailable,
			will always return false)
		notifyReady(dataLength): tell the backend that there is dataLength samples of audio data
			ready to be received via the callback we set with setSource. Ignored for event-based
			backends (= Web Audio) that trigger the callback whenever they feel like it...
	*/

	var AudioContext = window.AudioContext || window.webkitAudioContext;
	var fillBuffer = null;

	if (AudioContext) {
		/* Use Web Audio API as backend */
		var audioContext = new AudioContext();
		var audioNode = null;
		
		//Web audio Api changed createJavaScriptNode to CreateScriptProcessor - we support both
		if (audioContext.createJavaScriptNode!=null) {
			audioNode = audioContext.createJavaScriptNode(8192, 1, 1);
		} else if (audioContext.createScriptProcessor!=null) {
			audioNode = audioContext.createScriptProcessor(8192, 1, 1);
		}

                if (audioNode!=null) {

			onAudioProcess = function(e) {
				var buffer = e.outputBuffer.getChannelData(0);
				fillBuffer(buffer);
			};

			self.sampleRate = 44100;
			self.isEnabled = false;
			self.setSource = function(fillBufferCallback) {
				fillBuffer = fillBufferCallback;
				if (self.isEnabled) {
					audioNode.onaudioprocess = onAudioProcess;
					audioNode.connect(audioContext.destination);
				};
			}
			self.setAudioState = function(state) {
				if (state) {
					/* enable */
					self.isEnabled = true;
					if (fillBuffer) {
						audioNode.onaudioprocess = onAudioProcess;
						audioNode.connect(audioContext.destination);
					}
					return true;
				} else {
					/* disable */
					self.isEnabled = false;
					audioNode.onaudioprocess = null;
					audioNode.disconnect(0);
					return false;
				}
			}
			self.notifyReady = function(dataLength) {
				/* do nothing */
			}

			return self;
                }
	}

	if (typeof(Audio) != 'undefined') {
		var audio = new Audio();
		if (audio.mozSetup) {
			/* Use Audio Data API as backend */
			self.sampleRate = 44100;
			audio.mozSetup(1, self.sampleRate);

			self.isEnabled = false;
			self.setAudioState = function(state) {
				self.isEnabled = state;
				return state;
			}

			self.setSource = function(fn) {
				fillBuffer = fn;
			}
			self.notifyReady = function(dataLength) {
				var buffer = new Float32Array(dataLength);
				fillBuffer(buffer);
				if (self.isEnabled) {
					var written = audio.mozWriteAudio(buffer);
				}
			}

			return self;
		}
	}

	/* use dummy no-sound backend. We still keep a handle to the callback function and
	call it on demand, so that it's not filling up a buffer indefinitely */
	self.sampleRate = 5500; /* something suitably low */
	self.isEnabled = false;
	self.setAudioState = function(state) {
		return false;
	}
	self.setSource = function(fn) {
		fillBuffer = fn;
	};
	self.notifyReady = function(dataLength) {
		var buffer = new Float32Array(dataLength);
		fillBuffer(buffer);
	}
	return self;

}
JSSpeccy.Memory = function(opts) {
	var self = {};
	var model = opts.model || JSSpeccy.Spectrum.MODEL_128K;

	var contentionTableLength = model.frameLength;

	var noContentionTable = model.noContentionTable;
	var contentionTable = model.contentionTable;

	function MemoryPage(data, isContended) {
		var self = {};
		self.memory = (data || new Uint8Array(0x4000));
		self.contentionTable = (isContended ? contentionTable : noContentionTable);
		return self;
	}
	
	var ramPages = [];
	for (var i = 0; i < 8; i++) {
		ramPages[i] = MemoryPage(null, i & 0x01); /* for MODEL_128K (and implicitly 48K), odd pages are contended */
	}

	var romPages = {
		'48.rom': MemoryPage(JSSpeccy.roms['48.rom']),
		'128-0.rom': MemoryPage(JSSpeccy.roms['128-0.rom']),
		'128-1.rom': MemoryPage(JSSpeccy.roms['128-1.rom'])
	};

	var scratch = MemoryPage();
	
	var readSlots = [
		model === JSSpeccy.Spectrum.MODEL_48K ? romPages['48.rom'].memory : romPages['128-0.rom'].memory,
		ramPages[5].memory,
		ramPages[2].memory,
		ramPages[0].memory
	];

	var writeSlots = [
		scratch.memory,
		ramPages[5].memory,
		ramPages[2].memory,
		ramPages[0].memory
	];

	var contentionBySlot = [
		noContentionTable,
		contentionTable,
		noContentionTable,
		noContentionTable
	];

	self.isContended = function(addr) {
		return (contentionBySlot[addr >> 14] == contentionTable);
	};

	self.contend = function(addr, tstate) {
		return contentionBySlot[addr >> 14][tstate % contentionTableLength];
	};

	self.read = function(addr) {
		var page = readSlots[addr >> 14];
		return page[addr & 0x3fff];
	};
	self.write = function(addr, val) {
		var page = writeSlots[addr >> 14];
		page[addr & 0x3fff] = val;
	};
	
	var screenPage = ramPages[5].memory;
	self.readScreen = function(addr) {
		return screenPage[addr];
	};

	var pagingIsLocked = false;
	if (model === JSSpeccy.Spectrum.MODEL_128K) {
		self.setPaging = function(val) {
			if (pagingIsLocked) return;
			var highMemoryPage = ramPages[val & 0x07];
			readSlots[3] = writeSlots[3] = highMemoryPage.memory;
			contentionBySlot[3] = highMemoryPage.contentionTable;
			readSlots[0] = (val & 0x10) ? romPages['128-1.rom'].memory : romPages['128-0.rom'].memory;
			screenPage = (val & 0x08) ? ramPages[7].memory : ramPages[5].memory;
			pagingIsLocked = val & 0x20;
		};
	} else {
		self.setPaging = function(val) {
		};
	}
	
	self.loadFromSnapshot = function(snapshotPages) {
		for (var p in snapshotPages) {
			var ramPage = ramPages[p].memory;
			var snapshotPage = snapshotPages[p];
			for (var i = 0; i < 0x4000; i++) {
				ramPage[i] = snapshotPage[i];
			}
		}
	};

	self.reset = function() {
		pagingIsLocked = false;
		self.setPaging(0);
	};

	return self;
};
JSSpeccy.roms = {};
JSSpeccy.roms['128-0.rom'] = new Uint8Array([243,1,43,105,11,120,177,32,251,195,199,0,0,0,0,0,239,16,0,201,0,0,0,0,239,24,0,201,0,0,0,0,239,32,0,201,0,0,0,0,227,245,126,35,35,34,90,91,43,102,111,241,195,92,0,0,229,33,72,0,229,33,0,91,229,33,56,0,229,195,0,91,225,201,1,253,127,175,243,237,121,50,92,91,251,61,253,119,0,195,33,3,34,88,91,33,20,91,227,229,42,88,91,227,195,0,91,245,197,1,253,127,58,92,91,238,16,243,50,92,91,237,121,251,193,241,201,205,0,91,229,42,90,91,227,201,243,58,92,91,230,239,50,92,91,1,253,127,237,121,251,195,195,0,33,216,6,24,3,33,202,7,8,1,253,127,58,92,91,245,230,239,243,50,92,91,237,121,195,230,5,8,241,1,253,127,243,50,92,91,237,121,251,8,201,42,139,91,233,6,8,120,217,61,1,253,127,237,121,33,0,192,17,1,192,1,255,63,62,255,119,190,32,81,175,119,190,32,76,237,176,217,16,223,50,136,91,14,253,22,255,30,191,66,62,14,237,121,67,62,255,237,121,24,56,0,195,175,23,195,56,24,195,207,30,195,4,31,195,74,0,195,162,3,195,42,24,195,168,24,195,45,1,195,5,10,195,163,17,195,216,6,195,202,7,195,163,8,195,240,8,239,1,59,201,217,120,211,254,24,254,66,62,7,237,121,67,62,255,237,121,17,0,91,33,107,0,1,88,0,237,176,62,207,50,93,91,49,255,91,62,4,205,100,28,221,33,236,235,221,34,131,91,221,54,10,0,221,54,11,192,221,54,12,0,33,236,43,62,1,34,133,91,50,135,91,62,5,205,100,28,33,255,255,34,180,92,17,175,62,1,168,0,235,239,97,22,235,35,34,123,92,43,1,64,0,237,67,56,92,34,178,92,33,0,60,34,54,92,42,178,92,35,249,237,86,253,33,58,92,253,203,1,230,251,33,11,0,34,95,91,175,50,97,91,50,99,91,50,101,91,33,0,236,34,36,255,62,80,50,100,91,33,10,0,34,148,91,34,150,91,33,182,92,34,79,92,17,137,5,1,21,0,235,237,176,235,43,34,87,92,35,34,83,92,34,75,92,54,128,35,34,89,92,54,13,35,54,128,35,34,97,92,34,99,92,34,101,92,62,56,50,141,92,50,143,92,50,72,92,175,50,19,236,62,7,211,254,33,35,5,34,9,92,253,53,198,253,53,202,33,158,5,17,16,92,1,14,0,237,176,253,203,1,142,253,54,0,255,253,54,49,2,239,107,13,239,4,60,17,97,5,205,125,5,253,54,49,2,253,203,2,238,33,255,91,34,129,91,205,69,31,62,56,50,17,236,50,15,236,205,132,37,205,32,31,195,159,37,33,102,91,203,198,253,54,0,255,253,54,49,2,33,29,91,229,237,115,61,92,33,186,2,34,139,91,205,142,34,205,203,34,202,248,33,254,40,202,248,33,254,45,202,248,33,254,43,202,248,33,205,224,34,202,248,33,205,69,31,58,14,236,205,32,31,254,4,194,175,23,205,151,34,202,175,23,225,201,253,203,0,126,32,1,201,42,89,92,34,93,92,239,251,25,120,177,194,247,3,223,254,13,200,205,239,33,253,203,2,118,32,3,239,110,13,253,203,2,182,205,69,31,33,13,236,203,118,32,7,35,126,254,0,204,129,56,205,32,31,33,60,92,203,158,62,25,253,150,79,50,140,92,253,203,1,254,253,54,10,1,33,0,62,229,33,29,91,229,237,115,61,92,33,33,3,34,139,91,195,56,24,237,123,178,92,51,33,255,91,34,129,91,118,253,203,1,174,33,102,91,203,86,40,18,205,69,31,221,42,131,91,1,20,0,221,9,205,86,29,205,32,31,58,58,92,60,245,33,0,0,253,116,55,253,116,38,34,11,92,33,1,0,34,22,92,239,176,22,253,203,55,174,239,110,13,253,203,2,238,241,71,254,10,56,10,254,29,56,4,198,20,24,2,198,7,239,239,21,62,32,215,120,254,29,56,18,214,29,6,0,79,33,108,4,9,9,94,35,86,205,125,5,24,6,17,145,19,239,10,12,175,17,54,21,239,10,12,237,75,69,92,239,27,26,62,58,215,253,78,13,6,0,239,27,26,239,151,16,58,58,92,60,40,27,254,9,40,4,254,21,32,3,253,52,13,1,3,0,17,112,92,33,68,92,203,126,40,1,9,237,184,253,54,10,255,253,203,1,158,33,102,91,203,134,195,203,37,62,16,1,0,0,195,78,3,237,67,73,92,205,69,31,120,177,40,8,237,67,73,92,237,67,8,236,205,32,31,42,93,92,235,33,239,3,229,42,97,92,55,237,82,229,96,105,239,110,25,32,6,239,184,25,239,232,25,193,121,61,176,32,19,205,69,31,229,42,73,92,205,74,51,34,73,92,225,205,32,31,24,40,197,3,3,3,3,43,237,91,83,92,213,239,85,22,225,34,83,92,193,197,19,42,97,92,43,43,237,184,42,73,92,235,193,112,43,113,43,115,43,114,241,201,140,4,151,4,166,4,176,4,193,4,212,4,224,4,224,4,243,4,1,5,18,5,35,5,49,5,66,5,78,5,97,5,77,69,82,71,69,32,101,114,114,111,242,87,114,111,110,103,32,102,105,108,101,32,116,121,112,229,67,79,68,69,32,101,114,114,111,242,84,111,111,32,109,97,110,121,32,98,114,97,99,107,101,116,243,70,105,108,101,32,97,108,114,101,97,100,121,32,101,120,105,115,116,243,73,110,118,97,108,105,100,32,110,97,109,229,70,105,108,101,32,100,111,101,115,32,110,111,116,32,101,120,105,115,244,73,110,118,97,108,105,100,32,100,101,118,105,99,229,73,110,118,97,108,105,100,32,98,97,117,100,32,114,97,116,229,73,110,118,97,108,105,100,32,110,111,116,101,32,110,97,109,229,78,117,109,98,101,114,32,116,111,111,32,98,105,231,78,111,116,101,32,111,117,116,32,111,102,32,114,97,110,103,229,79,117,116,32,111,102,32,114,97,110,103,229,84,111,111,32,109,97,110,121,32,116,105,101,100,32,110,111,116,101,243,127,32,49,57,56,54,32,83,105,110,99,108,97,105,114,32,82,101,115,101,97,114,99,104,32,76,116,228,26,230,127,213,215,209,26,19,135,48,245,201,244,9,168,16,75,244,9,196,21,83,129,15,196,21,82,52,91,47,91,80,128,1,0,6,0,11,0,1,0,1,0,6,0,16,0,225,1,253,127,175,243,50,92,91,237,121,251,237,123,61,92,126,50,94,91,60,254,30,48,3,239,93,91,61,253,119,0,42,93,92,34,95,92,239,197,22,201,62,127,219,254,31,216,62,254,219,254,31,216,205,172,5,20,251,8,17,74,91,213,253,203,2,158,229,42,61,92,94,35,86,167,33,127,16,237,82,32,56,225,237,123,61,92,209,209,237,83,61,92,229,17,16,6,213,233,56,9,40,4,205,172,5,7,225,24,239,254,13,40,14,42,90,91,229,239,133,15,225,34,90,91,225,24,221,225,58,92,91,246,16,245,195,74,91,225,17,61,6,213,233,216,200,24,211,239,24,0,239,140,28,253,203,1,126,40,20,239,241,43,121,61,176,40,4,205,172,5,36,26,230,223,254,80,194,18,25,42,93,92,126,254,59,194,18,25,239,32,0,239,130,28,253,203,1,126,40,7,239,153,30,237,67,113,91,239,24,0,254,13,40,5,254,58,194,18,25,205,161,24,237,75,113,91,120,177,32,4,205,172,5,37,33,184,6,94,35,86,35,235,124,254,37,48,10,167,237,66,48,5,235,35,35,24,236,235,94,35,86,237,83,95,91,201,50,0,165,10,110,0,212,4,44,1,195,1,88,2,224,0,176,4,110,0,96,9,54,0,192,18,25,0,128,37,11,0,33,97,91,126,167,40,6,54,0,35,126,55,201,205,214,5,243,217,237,91,95,91,42,95,91,203,60,203,29,183,6,250,217,14,253,22,255,30,191,66,62,14,237,121,237,120,246,240,230,251,67,237,121,103,66,237,120,230,128,40,9,217,5,217,32,244,175,245,24,57,237,120,230,128,32,241,237,120,230,128,32,235,217,1,253,255,62,128,8,25,0,0,0,0,43,124,181,32,251,237,120,230,128,202,75,7,8,55,31,56,13,8,195,49,7,8,183,31,56,4,8,195,49,7,55,245,217,124,246,4,67,237,121,217,98,107,1,7,0,183,237,66,43,124,181,32,251,1,253,255,25,25,25,237,120,230,128,40,8,43,124,181,32,245,241,251,201,237,120,230,128,32,236,237,120,230,128,32,230,98,107,1,2,0,203,60,203,29,183,237,66,1,253,255,62,128,8,0,0,0,0,25,43,124,181,32,251,237,120,230,128,202,183,7,8,55,31,56,13,8,195,157,7,8,183,31,56,4,8,195,157,7,33,97,91,54,1,35,119,241,251,201,245,58,101,91,183,40,15,61,50,101,91,32,4,241,195,114,8,241,50,15,92,201,241,254,163,56,13,42,90,91,229,239,82,11,225,34,90,91,55,201,33,59,92,203,134,254,32,32,2,203,198,254,127,56,2,62,63,254,32,56,23,245,33,99,91,52,58,100,91,190,48,8,205,34,8,62,1,50,99,91,241,195,163,8,254,13,32,14,175,50,99,91,62,13,205,163,8,62,10,195,163,8,254,6,32,31,237,75,99,91,30,0,28,12,121,184,40,8,214,8,40,4,48,250,24,242,213,62,32,205,202,7,209,29,200,24,245,254,22,40,9,254,23,40,5,254,16,216,24,9,50,14,92,62,2,50,101,91,201,50,14,92,62,2,50,101,91,201,87,58,14,92,254,22,40,8,254,23,63,192,58,15,92,87,58,100,91,186,40,2,48,6,71,122,144,87,24,242,122,183,202,34,8,58,99,91,186,200,213,62,32,205,202,7,209,24,242,245,14,253,22,255,30,191,66,62,14,237,121,205,214,5,237,120,230,64,32,247,42,95,91,17,2,0,183,237,82,235,241,47,55,6,11,243,197,245,62,254,98,107,1,253,191,210,218,8,230,247,237,121,24,6,246,8,237,121,24,0,43,124,181,32,251,0,0,0,241,193,183,31,16,218,251,201,33,114,91,54,43,33,121,9,205,95,9,205,21,9,33,128,9,205,95,9,33,114,91,175,190,40,3,53,24,231,33,130,9,205,95,9,201,33,113,91,54,255,205,38,9,33,113,91,175,190,200,53,24,244,17,0,192,237,75,113,91,55,203,16,55,203,16,121,47,79,175,245,213,197,205,109,9,193,209,30,0,40,1,90,241,179,245,5,203,58,203,58,213,197,48,234,193,209,241,6,3,197,245,205,163,8,241,193,16,247,201,70,35,126,229,197,205,163,8,193,225,35,16,245,201,239,170,34,71,4,175,55,31,16,253,166,201,6,27,49,27,76,0,3,1,10,2,27,50,243,197,17,55,0,33,60,0,25,16,253,77,68,239,48,0,243,213,253,225,229,221,225,253,54,16,255,1,201,255,221,9,221,54,3,60,221,54,1,255,221,54,4,15,221,54,5,5,221,54,33,0,221,54,10,0,221,54,11,0,221,54,22,255,221,54,23,0,221,54,24,0,239,241,43,243,221,115,6,221,114,7,221,115,12,221,114,13,235,9,221,117,8,221,116,9,193,197,5,72,6,0,203,33,253,229,225,9,221,229,193,113,35,112,183,253,203,16,22,193,5,197,221,112,2,32,156,193,253,54,39,26,253,54,40,11,253,229,225,1,43,0,9,235,33,49,10,1,13,0,237,176,22,7,30,248,205,124,14,22,11,30,255,205,124,14,20,205,124,14,24,76,239,164,1,5,52,223,117,244,56,117,5,56,201,62,127,219,254,31,216,62,254,219,254,31,201,1,17,0,24,3,1,0,0,253,229,225,9,253,117,35,253,116,36,253,126,16,253,119,34,253,54,33,1,201,94,35,86,213,221,225,201,253,110,35,253,102,36,35,35,253,117,35,253,116,36,201,205,79,10,253,203,34,30,56,6,205,103,10,205,92,11,253,203,33,38,56,5,205,110,10,24,233,205,145,15,213,205,66,15,209,253,126,16,254,255,32,5,205,147,14,251,201,27,205,118,15,205,193,15,205,145,15,24,232,72,90,89,88,87,85,86,77,84,41,40,78,79,33,205,227,14,216,221,52,6,192,221,52,7,201,229,14,0,205,197,10,56,8,254,38,32,15,62,128,225,201,253,126,33,253,182,16,253,119,16,24,243,254,35,32,3,12,24,225,254,36,32,3,13,24,218,203,111,32,6,245,62,12,129,79,241,230,223,214,65,218,34,15,254,7,210,34,15,197,6,0,79,33,249,13,9,126,193,129,225,201,229,213,221,110,6,221,102,7,17,0,0,126,254,48,56,24,254,58,48,20,35,229,205,80,11,214,48,38,0,111,25,56,4,235,225,24,230,195,26,15,221,117,6,221,116,7,213,193,209,225,201,33,0,0,6,10,25,56,234,16,251,235,201,205,62,10,56,8,205,147,14,251,205,172,5,20,205,197,10,218,162,13,205,240,13,6,0,203,33,33,202,13,9,94,35,86,235,205,132,11,24,217,201,233,205,197,10,218,161,13,254,33,200,24,245,205,29,11,121,254,9,210,18,15,203,39,203,39,71,203,39,128,221,119,3,201,201,221,126,11,60,254,5,202,42,15,221,119,11,17,12,0,205,39,12,221,126,6,119,35,221,126,7,119,201,221,126,22,17,23,0,183,250,240,11,205,39,12,221,126,6,190,32,27,35,221,126,7,190,32,20,221,53,22,221,126,22,183,240,221,203,10,70,200,221,54,22,0,175,24,27,221,126,22,60,254,5,202,42,15,221,119,22,205,39,12,221,126,6,119,35,221,126,7,119,221,126,11,17,12,0,205,39,12,126,221,119,6,35,126,221,119,7,221,53,11,240,221,54,11,0,221,203,10,198,201,221,229,225,25,6,0,79,203,33,9,201,205,29,11,120,183,194,18,15,121,254,60,218,18,15,254,241,210,18,15,221,126,2,183,192,6,0,197,225,41,41,229,193,253,229,239,43,45,243,253,225,253,229,253,229,225,1,43,0,9,253,33,58,92,229,33,118,12,34,90,91,33,20,91,227,229,195,0,91,243,239,162,45,243,253,225,253,113,39,253,112,40,201,205,29,11,121,254,64,210,18,15,47,95,22,7,205,124,14,201,205,29,11,121,254,16,210,18,15,221,119,4,221,94,2,62,8,131,87,89,205,124,14,201,221,94,2,62,8,131,87,30,31,221,115,4,201,205,29,11,121,254,8,210,18,15,6,0,33,232,13,9,126,253,119,41,201,205,29,11,22,11,89,205,124,14,20,88,205,124,14,201,205,29,11,121,61,250,18,15,254,16,210,18,15,221,119,1,201,205,29,11,121,205,163,17,201,253,54,16,255,201,205,25,14,218,129,13,205,172,13,205,180,13,175,221,119,33,205,200,14,205,29,11,121,183,202,18,15,254,13,210,18,15,254,10,56,19,205,0,14,205,116,13,115,35,114,205,116,13,35,115,35,114,35,24,6,221,113,5,205,0,14,205,116,13,205,227,14,254,95,32,44,205,197,10,205,29,11,121,254,10,56,18,229,213,205,0,14,225,25,75,66,235,225,115,35,114,89,80,24,201,221,113,5,229,213,205,0,14,225,25,235,225,195,59,13,115,35,114,195,156,13,221,126,33,60,254,11,202,58,15,221,119,33,201,205,200,14,221,54,33,1,205,172,13,205,180,13,221,78,5,229,205,0,14,225,115,35,114,195,156,13,225,35,35,229,201,225,253,126,33,253,182,16,253,119,16,201,221,229,225,1,34,0,9,201,229,253,229,225,1,17,0,9,6,0,221,78,2,203,33,9,209,115,35,114,235,201,251,12,133,11,144,11,165,11,166,11,194,11,50,12,132,12,149,12,173,12,186,12,206,12,221,12,238,12,246,12,0,4,11,13,8,12,14,10,1,15,0,33,183,10,237,177,201,9,11,0,2,4,5,7,229,6,0,33,12,14,9,22,0,94,225,201,128,6,9,12,18,24,36,48,72,96,4,8,16,254,48,216,254,58,63,201,79,221,126,3,129,254,128,210,50,15,79,221,126,2,183,32,14,121,47,230,127,203,63,203,63,22,6,95,205,124,14,221,113,0,221,126,2,254,3,208,33,150,16,6,0,121,214,21,48,5,17,191,15,24,7,79,203,33,9,94,35,86,235,221,86,2,203,34,93,205,124,14,20,92,205,124,14,221,203,4,102,200,22,13,253,126,41,95,205,124,14,201,197,1,253,255,237,81,1,253,191,237,89,193,201,197,1,253,255,237,121,237,120,193,201,22,7,30,255,205,124,14,22,8,30,0,205,124,14,20,205,124,14,20,205,124,14,205,79,10,253,203,34,30,56,6,205,103,10,205,141,17,253,203,33,38,56,5,205,110,10,24,233,253,33,58,92,201,229,213,221,110,6,221,102,7,43,126,254,32,40,250,254,13,40,246,221,117,6,221,116,7,209,225,201,229,213,197,221,110,6,221,102,7,124,221,190,9,32,9,125,221,190,8,32,3,55,24,10,126,254,32,40,9,254,13,40,5,183,193,209,225,201,35,221,117,6,221,116,7,24,218,205,147,14,251,205,172,5,41,205,147,14,251,205,172,5,39,205,147,14,251,205,172,5,38,205,147,14,251,205,172,5,31,205,147,14,251,205,172,5,40,205,147,14,251,205,172,5,42,205,79,10,253,203,34,30,56,33,205,103,10,205,209,10,254,128,40,23,205,32,14,221,126,2,254,3,48,10,22,8,130,87,221,94,4,205,124,14,205,110,17,253,203,33,38,216,205,110,10,24,207,229,253,110,39,253,102,40,1,100,0,183,237,66,229,193,225,11,120,177,32,251,27,122,179,32,230,201,17,255,255,205,74,10,253,203,34,30,56,18,213,94,35,86,235,94,35,86,213,225,193,183,237,66,56,2,197,209,253,203,33,38,56,5,205,110,10,24,221,253,115,37,253,114,38,201,175,253,119,42,205,79,10,253,203,34,30,218,90,16,205,103,10,253,229,225,1,17,0,9,6,0,221,78,2,203,33,9,94,35,86,235,229,94,35,86,235,253,94,37,253,86,38,183,237,82,235,225,40,5,115,35,114,24,94,221,126,2,254,3,48,9,22,8,130,87,30,0,205,124,14,205,141,17,221,229,225,1,33,0,9,53,32,13,205,92,11,253,126,33,253,166,16,32,54,24,23,253,229,225,1,17,0,9,6,0,221,78,2,203,33,9,94,35,86,19,19,114,43,115,205,209,10,79,253,126,33,253,166,16,32,17,121,254,128,40,12,205,32,14,253,126,33,253,182,42,253,119,42,253,203,33,38,56,6,205,110,10,195,200,15,17,1,0,205,118,15,205,79,10,253,203,42,30,48,23,205,103,10,221,126,2,254,3,48,10,22,8,130,87,221,94,4,205,124,14,205,110,17,253,203,33,38,216,205,110,10,24,217,191,15,220,14,7,14,61,13,127,12,204,11,34,11,130,10,235,9,93,9,214,8,87,8,223,7,110,7,3,7,159,6,64,6,230,5,145,5,65,5,246,4,174,4,107,4,44,4,240,3,183,3,130,3,79,3,32,3,243,2,200,2,161,2,123,2,87,2,54,2,22,2,248,1,220,1,193,1,168,1,144,1,121,1,100,1,80,1,61,1,44,1,27,1,11,1,252,0,238,0,224,0,212,0,200,0,189,0,178,0,168,0,159,0,150,0,141,0,133,0,126,0,119,0,112,0,106,0,100,0,94,0,89,0,84,0,79,0,75,0,71,0,67,0,63,0,59,0,56,0,53,0,50,0,47,0,45,0,42,0,40,0,37,0,35,0,33,0,31,0,30,0,28,0,26,0,25,0,24,0,22,0,21,0,20,0,19,0,18,0,17,0,16,0,15,0,14,0,13,0,12,0,12,0,11,0,11,0,10,0,9,0,9,0,8,0,221,126,1,183,248,246,144,205,163,17,221,126,0,205,163,17,221,126,4,203,167,203,39,203,39,203,39,205,163,17,201,221,126,1,183,248,246,128,205,163,17,221,126,0,205,163,17,62,64,205,163,17,201,111,1,253,255,62,14,237,121,1,253,191,62,250,237,121,30,3,29,32,253,0,0,0,0,125,22,8,31,111,210,201,17,62,254,237,121,24,6,62,250,237,121,24,0,30,2,29,32,253,0,198,0,125,21,32,227,0,0,198,0,0,0,62,254,237,121,30,6,29,32,253,201,33,102,91,203,238,24,19,33,102,91,203,230,24,12,33,102,91,203,254,24,5,33,102,91,203,246,33,102,91,203,158,223,254,33,194,190,19,33,102,91,203,222,231,195,190,19,205,172,5,11,34,116,91,221,126,0,50,113,91,221,110,11,221,102,12,34,114,91,221,110,13,221,102,14,34,120,91,221,110,15,221,102,16,34,118,91,183,40,10,254,3,40,6,221,126,14,50,118,91,221,229,225,35,17,103,91,1,10,0,237,176,33,102,91,203,110,194,173,27,33,113,91,17,122,91,1,7,0,237,176,205,46,28,58,122,91,71,58,113,91,184,32,6,254,3,40,18,56,4,205,172,5,29,58,102,91,203,119,32,58,203,127,202,219,18,58,102,91,203,119,40,4,205,172,5,28,42,123,91,237,91,114,91,124,181,40,8,237,82,48,4,205,172,5,30,42,125,91,124,181,32,3,42,116,91,58,113,91,167,32,3,42,83,92,205,126,19,201,237,75,114,91,197,3,239,48,0,54,128,235,209,229,205,126,19,225,239,206,8,201,237,91,114,91,42,125,91,229,124,181,32,6,19,19,19,235,24,9,42,123,91,235,55,237,82,56,9,17,5,0,25,68,77,239,5,31,225,58,113,91,167,40,47,124,181,40,11,43,70,43,78,43,3,3,3,239,232,25,42,89,92,43,237,75,114,91,197,3,3,3,58,127,91,245,239,85,22,35,241,119,209,35,115,35,114,35,205,126,19,201,33,102,91,203,142,237,91,83,92,42,89,92,43,239,229,25,237,75,114,91,42,83,92,239,85,22,35,237,75,118,91,9,34,75,92,58,121,91,103,230,192,32,16,58,120,91,111,34,66,92,253,54,10,0,33,102,91,203,206,42,83,92,237,91,114,91,43,34,87,92,35,24,179,122,179,200,205,75,28,201,239,140,28,253,203,1,126,200,245,239,241,43,241,201,231,205,133,19,200,245,121,176,40,29,33,10,0,237,66,56,22,213,197,33,103,91,6,10,62,32,119,35,16,252,193,225,17,103,91,237,176,241,201,205,172,5,33,239,140,28,253,203,1,126,40,64,1,17,0,58,116,92,167,40,2,14,34,239,48,0,213,221,225,6,11,62,32,18,19,16,252,221,54,1,255,239,241,43,33,246,255,11,9,3,48,17,58,116,92,167,32,4,205,172,5,14,120,177,40,10,1,10,0,221,229,225,35,235,237,176,223,254,228,32,83,58,116,92,254,3,202,25,18,231,239,178,40,48,21,33,0,0,253,203,1,118,40,2,203,249,58,116,92,61,40,25,205,172,5,1,194,25,18,253,203,1,126,40,25,78,35,126,221,119,11,35,126,221,119,12,35,221,113,14,62,1,203,113,40,1,60,221,119,0,235,231,254,41,32,216,231,205,161,24,235,195,25,21,254,170,32,31,58,116,92,254,3,202,25,18,231,205,161,24,221,54,11,0,221,54,12,27,33,0,64,221,117,13,221,116,14,24,77,254,175,32,79,58,116,92,254,3,202,25,18,231,239,72,32,32,12,58,116,92,167,202,25,18,239,230,28,24,15,239,130,28,223,254,44,40,12,58,116,92,167,202,25,18,239,230,28,24,4,231,239,130,28,205,161,24,239,153,30,221,113,11,221,112,12,239,153,30,221,113,13,221,112,14,96,105,221,54,0,3,24,68,254,202,40,9,205,161,24,221,54,14,128,24,23,58,116,92,167,194,25,18,231,239,130,28,205,161,24,239,153,30,221,113,13,221,112,14,221,54,0,0,42,89,92,237,91,83,92,55,237,82,221,117,11,221,116,12,42,75,92,237,82,221,117,15,221,116,16,235,58,102,91,203,95,194,29,18,58,116,92,167,32,4,239,112,9,201,239,97,7,201,33,245,238,203,134,203,206,42,73,92,124,181,32,3,34,6,236,58,219,249,245,42,154,252,205,74,51,34,215,249,205,34,50,205,214,48,241,183,40,12,245,205,223,48,235,205,106,50,241,61,24,241,14,0,205,180,48,65,58,21,236,79,197,213,205,223,48,58,245,238,203,79,40,29,213,229,17,32,0,25,203,70,40,17,35,86,35,94,183,42,73,92,237,82,32,5,33,245,238,203,198,225,209,197,229,1,35,0,237,176,225,193,213,197,235,33,245,238,203,70,40,42,6,0,42,6,236,124,181,40,14,229,205,65,46,225,48,18,43,4,34,6,236,24,235,205,65,46,212,99,46,33,245,238,54,0,120,193,197,72,71,205,17,42,193,209,121,4,184,48,149,58,245,238,203,79,40,33,203,71,32,29,42,73,92,124,181,40,8,34,154,252,205,34,50,24,9,34,154,252,205,82,51,34,73,92,209,193,195,54,21,209,193,191,245,121,72,205,180,48,235,245,205,4,54,241,17,35,0,25,12,185,48,243,241,200,205,7,42,205,120,43,42,6,236,43,124,181,34,6,236,32,242,195,17,42,201,6,0,58,21,236,87,195,94,59,6,0,229,72,205,180,48,205,106,50,225,208,205,223,48,197,229,33,35,0,25,58,21,236,79,184,40,14,197,197,1,35,0,237,176,193,121,4,184,32,244,193,225,205,24,54,1,35,0,237,176,55,193,201,6,0,205,43,50,208,197,229,58,21,236,79,205,180,48,205,30,49,48,38,27,33,35,0,25,235,197,120,185,40,12,197,1,35,0,237,184,193,120,13,185,56,244,235,19,193,225,205,44,54,1,35,0,237,176,55,193,201,225,193,201,213,38,0,104,25,87,120,94,114,83,35,60,254,32,56,247,123,254,0,209,201,213,33,32,0,25,229,87,62,31,24,7,94,114,83,184,40,4,61,43,24,246,123,254,0,225,209,201,177,201,188,190,195,175,180,147,145,146,149,152,152,152,152,152,152,152,127,129,46,108,110,112,72,148,86,63,65,43,23,31,55,119,68,15,89,43,67,45,81,58,109,66,13,73,92,68,21,93,1,61,2,6,0,103,30,6,203,14,103,25,6,12,83,26,0,238,28,12,111,26,4,61,6,204,6,14,129,25,4,0,171,29,14,120,33,14,140,33,14,213,33,14,98,24,12,170,33,13,2,26,14,117,27,8,0,128,30,3,79,30,0,95,30,13,13,26,0,107,13,9,0,220,34,6,0,58,31,14,171,25,14,235,25,3,66,30,9,14,190,33,12,167,33,14,116,33,14,113,27,11,11,11,11,8,0,248,3,9,14,174,33,7,7,7,7,7,7,8,0,122,30,6,0,148,34,14,140,26,6,44,10,0,54,23,6,0,229,22,14,65,6,10,44,10,12,240,26,14,12,28,14,229,27,12,43,27,14,23,35,253,203,1,190,239,251,25,175,50,71,92,61,50,58,92,24,1,231,239,191,22,253,52,13,250,18,25,223,6,0,254,13,202,99,24,254,58,40,234,33,33,24,229,79,231,121,214,206,48,19,198,206,33,169,23,254,163,40,22,33,172,23,254,164,40,15,195,18,25,79,33,220,22,9,78,9,24,3,42,116,92,126,35,34,116,92,1,253,23,197,79,254,32,48,12,33,181,24,6,0,9,78,9,229,223,5,201,223,185,194,18,25,231,201,205,214,5,56,4,205,172,5,20,253,203,10,126,194,168,24,42,66,92,203,124,40,20,33,254,255,34,69,92,42,97,92,43,237,91,89,92,27,58,68,92,24,54,239,110,25,58,68,92,40,28,167,32,70,71,126,230,192,120,40,18,205,172,5,255,193,253,203,1,126,200,42,85,92,62,192,166,192,175,254,1,206,0,86,35,94,237,83,69,92,35,94,35,86,235,25,35,34,85,92,235,34,93,92,87,30,0,253,54,10,255,21,253,114,13,202,192,23,20,239,139,25,40,11,205,172,5,22,253,203,1,126,192,193,193,223,254,13,40,182,254,58,202,192,23,195,18,25,36,67,70,30,76,32,83,94,77,134,87,136,6,2,5,239,222,28,191,193,204,161,24,235,42,116,92,78,35,70,235,197,201,239,222,28,191,193,204,161,24,235,42,116,92,78,35,70,235,229,33,248,24,34,90,91,33,20,91,227,229,96,105,227,195,0,91,201,239,31,28,201,193,239,86,28,205,161,24,201,239,108,28,201,231,239,122,28,201,239,130,28,201,205,172,5,11,239,140,28,201,253,203,1,126,253,203,2,134,40,3,239,77,13,241,58,116,92,214,167,239,252,33,205,161,24,42,143,92,34,141,92,33,145,92,126,7,174,230,170,174,119,201,239,190,28,201,241,58,102,91,230,15,50,102,91,58,116,92,214,116,50,116,92,202,235,17,61,202,242,17,61,202,249,17,195,0,18,193,253,203,1,126,40,16,42,101,92,17,251,255,25,34,101,92,239,233,52,218,99,24,195,193,23,254,205,32,9,231,205,14,25,205,161,24,24,24,205,161,24,42,101,92,54,0,35,54,0,35,54,1,35,54,0,35,54,0,35,34,101,92,239,22,29,201,231,205,249,24,253,203,1,126,40,46,223,34,95,92,42,87,92,126,254,44,40,11,30,228,239,134,29,48,4,205,172,5,13,35,34,93,92,126,239,86,28,223,34,87,92,42,95,92,253,54,38,0,34,93,92,126,223,254,44,40,195,205,161,24,201,253,203,1,126,32,11,239,251,36,254,44,196,161,24,231,24,245,62,228,239,57,30,201,239,103,30,1,0,0,239,69,30,24,3,239,153,30,120,177,32,4,237,75,178,92,197,237,91,75,92,42,89,92,43,239,229,25,239,107,13,42,101,92,17,50,0,25,209,237,82,48,8,42,180,92,167,237,82,48,4,205,172,5,21,237,83,178,92,209,225,193,237,123,178,92,51,197,229,237,115,61,92,213,201,209,253,102,13,36,227,51,237,75,69,92,197,229,237,115,61,92,213,239,103,30,1,20,0,239,5,31,201,193,225,209,122,254,62,40,15,59,227,235,237,115,61,92,197,34,66,92,253,114,10,201,213,229,205,172,5,6,253,203,1,126,40,5,62,206,195,254,25,253,203,1,246,239,141,44,48,22,231,254,36,32,5,253,203,1,182,231,254,40,32,60,231,254,41,40,32,239,141,44,210,18,25,235,231,254,36,32,2,235,231,235,1,6,0,239,85,22,35,35,54,14,254,44,32,3,231,24,224,254,41,32,19,231,254,61,32,14,231,58,59,92,245,239,251,36,241,253,174,1,230,64,194,18,25,205,161,24,201,33,14,236,54,255,205,32,31,239,176,22,42,89,92,1,3,0,239,85,22,33,110,27,237,91,89,92,1,3,0,237,176,205,107,2,205,32,31,239,176,22,42,89,92,1,1,0,239,85,22,42,89,92,54,225,205,107,2,205,83,27,237,123,61,92,225,33,3,19,229,33,19,0,229,33,8,0,229,62,32,50,92,91,195,0,91,33,0,0,229,62,32,50,92,91,195,0,91,42,79,92,17,5,0,25,17,10,0,235,25,235,1,4,0,237,176,253,203,48,158,253,203,1,166,201,239,34,34,62,3,24,2,62,2,253,54,2,0,239,48,37,40,3,239,1,22,239,24,0,239,112,32,56,24,239,24,0,254,59,40,4,254,44,32,8,239,32,0,205,14,25,24,8,239,230,28,24,3,239,222,28,205,161,24,239,37,24,201,237,115,129,91,49,255,91,205,151,28,237,75,114,91,33,247,255,246,255,237,66,205,243,28,1,9,0,33,113,91,205,172,29,42,116,91,237,75,114,91,205,172,29,205,86,29,62,5,205,100,28,237,123,129,91,201,239,24,0,254,33,194,18,25,239,32,0,205,161,24,62,2,239,1,22,237,115,129,91,49,255,91,205,210,32,62,5,205,100,28,237,123,129,91,201,239,24,0,254,33,194,18,25,205,147,19,205,161,24,237,115,129,91,49,255,91,205,95,31,62,5,205,100,28,237,123,129,91,201,237,115,129,91,49,255,91,205,53,29,33,113,91,1,9,0,205,55,30,62,5,205,100,28,237,123,129,91,201,237,115,129,91,49,255,91,66,75,205,55,30,205,86,29,62,5,205,100,28,237,123,129,91,201,229,197,33,129,28,6,0,79,9,78,243,58,92,91,230,248,177,50,92,91,1,253,127,237,121,251,193,225,201,1,3,4,6,7,0,17,103,91,221,229,225,6,10,26,19,190,35,192,16,249,201,205,18,29,40,4,205,172,5,32,221,229,1,236,63,221,9,221,225,48,99,33,236,255,62,255,205,243,28,33,102,91,203,214,221,229,209,33,103,91,1,10,0,237,176,221,203,19,198,221,126,10,221,119,16,221,126,11,221,119,17,221,126,12,221,119,18,175,221,119,13,221,119,14,221,119,15,62,5,205,100,28,221,229,225,1,236,255,9,34,131,91,201,237,91,133,91,8,58,135,91,79,8,203,127,32,9,25,137,34,133,91,50,135,91,201,25,137,56,245,205,172,5,3,62,4,205,100,28,221,33,236,235,237,91,131,91,183,221,229,225,237,82,200,205,135,28,32,3,246,255,201,1,236,255,221,9,24,230,205,18,29,32,4,205,172,5,35,221,126,10,221,119,16,221,126,11,221,119,17,221,126,12,221,119,18,62,5,205,100,28,201,62,4,205,100,28,221,203,19,70,200,221,203,19,134,33,102,91,203,150,221,110,16,221,102,17,221,126,18,221,94,10,221,86,11,221,70,12,183,237,82,152,203,20,203,20,203,47,203,28,203,47,203,28,221,117,13,221,116,14,221,119,15,221,110,16,221,102,17,221,126,18,1,236,255,221,9,221,117,10,221,116,11,221,119,12,201,120,177,200,229,17,0,192,235,237,82,40,29,56,27,229,237,66,48,13,96,105,193,183,237,66,227,17,0,192,213,24,40,225,225,17,0,0,213,213,24,31,96,105,17,32,0,183,237,82,56,5,227,66,75,24,5,225,17,0,0,213,197,17,152,91,237,176,193,229,33,152,91,62,4,205,100,28,221,94,16,221,86,17,221,126,18,205,100,28,237,160,122,179,40,25,120,177,194,5,30,62,4,205,100,28,221,115,16,221,114,17,62,5,205,100,28,225,193,24,136,62,4,205,100,28,221,52,18,221,126,18,17,0,192,205,100,28,24,212,120,177,200,229,17,0,192,235,237,82,40,36,56,34,229,237,66,48,18,96,105,193,183,237,66,227,17,0,0,213,17,0,192,213,235,24,36,225,225,17,0,0,213,213,213,235,24,25,96,105,17,32,0,183,237,82,56,5,227,66,75,24,5,225,17,0,0,213,197,229,17,152,91,62,4,205,100,28,221,110,16,221,102,17,221,126,18,205,100,28,237,160,124,181,40,37,120,177,194,145,30,62,4,205,100,28,221,117,16,221,116,17,62,5,205,100,28,209,193,33,152,91,120,177,40,2,237,176,235,193,195,55,30,62,4,205,100,28,221,52,18,221,126,18,33,0,192,205,100,28,24,200,245,58,92,91,245,229,213,197,221,33,106,91,221,115,16,221,114,17,221,54,18,4,205,172,29,62,5,205,100,28,193,209,225,9,235,9,235,241,1,253,127,243,237,121,50,92,91,251,1,0,0,241,201,245,58,92,91,245,229,213,197,221,33,106,91,221,117,16,221,116,17,221,54,18,4,235,205,55,30,24,200,8,62,0,243,205,58,31,241,34,88,91,42,129,91,237,115,129,91,249,251,42,88,91,245,8,201,197,1,253,127,237,121,50,92,91,193,201,8,243,241,34,88,91,42,129,91,237,115,129,91,249,42,88,91,245,62,7,205,58,31,251,8,201,205,18,29,32,4,205,172,5,35,221,110,13,221,102,14,221,126,15,205,243,28,253,229,253,42,131,91,1,236,255,221,9,253,110,10,253,102,11,253,126,12,253,225,221,94,10,221,86,11,221,70,12,183,237,82,152,203,20,203,20,203,47,203,28,203,47,203,28,1,20,0,221,9,221,117,16,221,116,17,221,119,18,1,236,255,221,9,221,110,10,221,102,11,221,86,12,1,20,0,221,9,122,205,100,28,58,92,91,95,1,253,127,62,7,243,237,121,217,221,110,10,221,102,11,221,86,12,122,205,100,28,58,92,91,95,1,253,127,217,62,7,243,237,121,221,126,16,214,1,221,119,16,48,20,221,126,17,214,1,221,119,17,48,10,221,126,18,214,1,221,119,18,56,49,237,89,126,44,32,17,36,32,14,8,20,122,205,100,28,58,92,91,95,33,0,192,8,217,243,237,89,119,44,32,15,36,32,12,20,122,205,100,28,58,92,91,95,33,0,192,217,24,172,62,4,205,100,28,62,0,33,20,0,205,243,28,221,94,13,221,86,14,221,78,15,122,7,203,17,7,203,17,122,230,63,87,221,229,213,17,236,255,221,25,209,221,110,10,221,102,11,221,126,12,183,237,82,145,203,116,32,3,203,244,61,221,117,10,221,116,11,221,119,12,221,110,16,221,102,17,221,126,18,183,237,82,145,203,116,32,3,203,244,61,221,117,16,221,116,17,221,119,18,221,229,225,213,237,91,131,91,183,237,82,209,32,177,237,91,131,91,225,229,183,237,82,68,77,225,229,17,20,0,25,235,225,27,43,237,184,42,131,91,17,20,0,25,34,131,91,201,62,4,205,100,28,33,33,33,1,43,33,221,33,236,235,205,214,5,221,229,227,237,91,131,91,183,237,82,225,40,32,84,93,229,197,205,138,28,193,225,48,14,80,89,229,197,205,138,28,193,225,56,3,221,229,193,17,236,255,221,25,24,208,229,33,43,33,183,237,66,225,200,96,105,205,53,33,24,185,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255,255,255,229,197,225,17,103,91,1,10,0,237,176,62,5,205,100,28,42,129,91,237,115,129,91,249,33,103,91,6,10,126,229,197,239,16,0,193,225,35,16,245,62,13,239,16,0,239,77,13,42,129,91,237,115,129,91,249,62,4,205,100,28,225,201,62,3,24,2,62,2,239,48,37,40,3,239,1,22,239,77,13,239,223,31,205,161,24,201,239,48,37,40,8,62,1,239,1,22,239,110,13,253,54,2,1,239,193,32,205,161,24,239,160,32,201,195,240,8,243,195,157,1,223,254,44,32,56,231,239,130,28,205,161,24,239,45,35,201,223,254,44,40,7,205,161,24,239,119,36,201,231,239,130,28,205,161,24,239,148,35,201,239,178,40,32,17,239,48,37,32,8,203,177,239,150,41,205,161,24,239,21,44,201,205,172,5,11,253,203,48,70,200,239,175,13,201,33,254,255,34,69,92,253,203,1,190,205,142,34,239,251,36,253,203,1,118,40,44,223,254,13,32,39,253,203,1,254,205,142,34,33,33,3,34,139,91,239,251,36,253,203,1,118,40,17,17,141,91,42,101,92,1,5,0,183,237,66,237,176,195,62,34,205,172,5,25,62,13,205,111,34,1,1,0,239,48,0,34,91,92,229,42,81,92,229,62,255,239,1,22,239,227,45,225,239,21,22,209,42,91,92,167,237,82,26,205,111,34,19,43,124,181,32,246,201,229,213,205,69,31,33,13,236,203,158,245,62,2,239,1,22,241,205,105,38,33,13,236,203,158,205,32,31,209,225,201,42,89,92,43,34,93,92,231,201,205,142,34,254,241,192,42,93,92,126,35,254,13,200,254,58,32,247,183,201,71,33,189,34,126,35,183,40,5,184,32,248,120,201,246,255,120,201,43,45,42,47,94,61,62,60,199,200,201,197,198,0,254,165,56,14,254,196,48,10,254,172,40,6,254,173,40,2,191,201,254,165,201,71,246,32,254,97,56,6,254,123,48,2,191,201,120,254,46,200,205,10,35,32,17,231,205,10,35,40,250,254,46,200,254,69,200,254,101,200,24,164,246,255,201,254,48,56,6,254,58,48,2,191,201,254,48,201,6,0,223,197,239,140,28,193,4,254,44,32,3,231,24,243,120,254,9,56,4,205,172,5,43,205,161,24,195,133,9,33,255,91,34,129,91,205,69,31,195,203,37,167,237,82,68,77,25,235,201,1,1,0,229,213,205,88,35,209,225,239,85,22,201,42,101,92,9,56,10,235,33,130,0,25,56,3,237,114,216,253,54,0,3,195,33,3,135,135,111,38,0,41,41,41,201,33,0,0,57,237,91,101,92,183,237,82,201,253,203,199,134,205,111,35,229,237,91,36,255,25,84,93,227,229,213,17,0,88,25,235,225,1,32,0,58,143,92,205,155,36,225,124,38,0,135,135,135,198,64,87,92,25,235,225,6,32,195,225,35,22,255,205,111,35,122,237,91,36,255,25,93,84,19,119,11,237,176,201,205,136,36,17,0,64,42,36,255,67,205,225,35,22,72,205,225,35,22,80,6,192,126,229,213,254,254,56,4,214,254,24,54,254,32,48,7,33,39,37,167,8,24,52,254,128,48,14,205,113,35,237,91,54,92,25,209,205,40,255,24,71,254,144,48,4,214,127,24,17,214,144,205,113,35,209,205,32,31,213,237,91,123,92,55,24,7,17,47,37,205,113,35,167,8,25,209,74,126,18,35,20,126,18,35,20,126,18,35,20,126,18,35,20,126,18,35,20,126,18,35,20,126,18,35,20,126,18,81,8,220,69,31,225,35,19,16,140,201,197,243,1,253,127,58,92,91,238,16,237,121,251,8,8,243,14,253,238,16,237,121,251,193,201,33,86,36,17,40,255,1,14,0,237,176,229,33,44,36,14,32,237,176,225,14,11,237,176,201,253,203,199,134,17,0,88,1,192,2,42,36,255,58,141,92,50,143,92,8,197,126,254,255,32,8,58,141,92,18,35,19,24,93,8,18,19,8,35,254,21,48,84,254,16,56,80,43,32,8,35,126,79,8,230,248,24,67,254,17,32,11,35,126,135,135,135,79,8,230,199,24,52,254,18,32,9,35,126,15,79,8,230,127,24,39,254,19,32,10,35,126,15,15,79,8,230,191,24,25,254,20,35,32,22,78,58,1,92,169,31,48,14,62,1,253,174,199,50,1,92,8,205,19,37,177,8,193,11,120,177,194,156,36,8,50,143,92,201,71,230,192,79,120,135,135,135,230,56,177,79,120,31,31,31,230,7,177,201,0,60,98,96,110,98,62,0,0,108,16,84,186,56,84,130,21,11,148,42,10,181,42,8,215,42,9,227,42,173,79,42,172,37,42,175,212,41,174,225,41,166,131,41,165,171,41,168,135,42,167,122,42,170,27,41,12,43,41,179,23,48,180,188,47,176,114,48,177,62,48,13,68,41,169,155,38,7,4,39,4,11,46,39,10,49,39,7,23,39,13,23,39,205,190,40,33,0,0,34,154,252,62,130,50,13,236,33,0,0,34,73,92,205,188,53,205,94,54,201,33,255,91,34,129,91,205,69,31,62,2,239,1,22,33,68,39,34,234,246,33,84,39,34,236,246,229,33,13,236,203,206,203,166,43,54,0,225,205,168,54,195,83,38,221,33,108,253,33,255,91,34,129,91,205,69,31,62,2,239,1,22,205,104,54,33,59,92,203,110,40,252,33,13,236,203,158,203,118,32,20,58,14,236,254,4,40,10,254,0,194,199,40,205,72,56,24,3,205,77,56,205,214,48,205,34,50,58,14,236,254,4,40,66,42,73,92,124,181,32,21,42,83,92,237,75,75,92,167,237,66,32,6,33,0,0,34,8,236,42,8,236,205,32,31,239,110,25,239,149,22,205,69,31,237,83,73,92,33,13,236,203,110,32,15,33,0,0,34,6,236,205,47,21,205,242,41,205,68,41,49,255,91,205,104,54,205,127,54,245,58,57,92,205,236,38,241,205,105,38,24,234,33,13,236,203,78,245,33,119,37,32,3,33,55,37,205,206,63,32,5,212,231,38,241,201,241,40,5,175,50,65,92,201,33,13,236,203,70,40,4,205,231,38,201,254,163,48,187,195,241,40,58,14,236,254,4,200,205,48,22,33,13,236,203,158,126,238,64,119,230,64,40,5,205,187,38,24,3,205,206,38,55,201,205,129,56,33,13,236,203,246,205,45,46,205,136,58,205,223,40,24,11,33,13,236,203,182,205,190,40,205,72,56,42,154,252,124,181,196,74,51,205,47,21,195,242,41,58,56,92,203,63,221,229,22,0,95,33,128,12,239,181,3,221,225,201,221,229,17,48,0,33,0,3,24,240,205,236,41,33,13,236,203,206,43,54,0,42,236,246,205,168,54,55,201,33,13,236,203,142,43,126,42,234,246,229,245,205,62,55,241,225,205,206,63,195,242,41,55,24,1,167,33,12,236,126,229,42,236,246,220,167,55,212,182,55,225,119,55,201,5,0,49,40,1,108,40,2,133,40,3,71,27,4,22,40,6,49,50,56,32,32,32,32,32,255,84,97,112,101,32,76,111,97,100,101,242,49,50,56,32,66,65,83,73,195,67,97,108,99,117,108,97,116,111,242,52,56,32,66,65,83,73,195,84,97,112,101,32,84,101,115,116,101,242,160,5,0,66,39,1,81,40,2,17,40,3,98,40,4,28,40,6,79,112,116,105,111,110,115,32,255,49,50,56,32,66,65,83,73,195,82,101,110,117,109,98,101,242,83,99,114,101,101,238,80,114,105,110,244,69,120,105,244,160,2,0,66,39,1,28,40,3,79,112,116,105,111,110,115,32,255,67,97,108,99,117,108,97,116,111,242,69,120,105,244,160,22,1,0,16,0,17,7,19,0,84,111,32,99,97,110,99,101,108,32,45,32,112,114,101,115,115,32,66,82,69,65,75,32,116,119,105,99,229,205,155,38,24,94,205,87,56,205,233,59,33,13,236,203,182,205,190,40,6,0,22,23,205,94,59,205,32,31,195,159,37,205,82,56,33,60,92,203,198,17,235,39,205,125,5,203,134,203,246,62,7,50,14,236,1,0,0,205,43,55,195,241,26,205,136,56,212,231,38,33,0,0,34,73,92,34,8,236,24,3,205,20,27,33,13,236,203,118,32,8,33,60,92,203,134,205,72,56,33,13,236,203,174,203,166,62,0,33,144,39,17,160,39,24,44,33,13,236,203,238,203,230,203,182,205,190,40,205,77,56,62,4,50,14,236,33,0,0,34,73,92,205,47,21,1,0,0,120,205,248,41,62,4,33,203,39,17,210,39,50,14,236,34,234,246,237,83,236,246,195,4,38,205,31,46,205,127,58,195,232,40,6,0,22,23,205,94,59,195,173,37,6,0,0,0,4,16,20,6,0,0,0,0,1,1,33,216,40,17,238,246,195,186,63,33,209,40,17,238,246,195,186,63,33,13,236,183,183,203,70,194,242,41,203,190,203,222,229,245,205,236,41,241,245,205,129,46,241,120,205,120,43,225,203,254,210,242,41,120,218,248,41,195,242,41,33,13,236,203,222,205,236,41,205,18,47,55,120,195,248,41,33,13,236,203,134,203,222,205,236,41,205,91,43,63,218,242,41,205,18,47,55,120,195,248,41,205,236,41,245,205,180,48,197,6,0,205,65,46,193,56,10,33,32,0,25,126,47,230,9,40,28,58,13,236,203,95,40,5,205,142,44,48,21,205,76,44,205,120,43,205,206,46,6,0,241,55,195,248,41,241,55,195,242,41,241,195,242,41,58,14,236,254,4,200,205,236,41,33,0,0,205,32,31,239,110,25,239,149,22,205,69,31,237,83,73,92,62,15,205,150,58,205,47,21,55,195,242,41,58,14,236,254,4,200,205,236,41,33,15,39,205,32,31,239,110,25,235,239,149,22,205,69,31,237,83,73,92,62,15,205,150,58,205,47,21,55,195,242,41,205,236,41,205,234,43,210,242,41,120,195,248,41,205,236,41,205,9,44,48,9,120,24,12,205,7,42,195,79,54,205,7,42,195,64,54,205,17,42,245,197,62,15,205,150,58,193,241,195,64,54,33,238,246,78,35,70,35,126,35,201,33,238,246,113,35,112,35,119,201,229,205,180,48,38,0,104,25,126,225,201,205,236,41,95,22,10,213,205,48,43,209,48,192,123,205,17,42,67,205,249,42,48,6,21,32,236,123,56,182,213,205,11,43,209,67,205,249,42,123,183,24,169,205,236,41,95,22,10,213,205,11,43,209,48,150,123,205,17,42,67,205,2,43,48,7,21,32,236,123,218,248,41,245,205,48,43,6,0,205,212,43,241,195,248,41,205,236,41,205,76,44,210,242,41,120,195,248,41,205,236,41,205,49,44,210,242,41,120,195,248,41,205,236,41,95,213,205,11,43,209,210,242,41,67,205,2,43,123,218,248,41,245,205,48,43,6,0,205,249,42,241,195,248,41,205,236,41,95,213,205,48,43,209,210,242,41,67,205,2,43,123,218,248,41,213,205,11,43,209,67,205,249,42,123,183,195,248,41,205,236,41,205,91,43,218,248,41,195,242,41,205,236,41,205,120,43,218,248,41,245,205,11,43,6,31,205,223,43,241,195,248,41,213,205,212,43,212,223,43,209,201,213,205,223,43,212,212,43,209,201,205,124,44,48,31,197,205,180,48,6,0,205,65,46,212,128,47,193,33,241,246,126,185,56,9,197,205,111,22,193,216,121,183,200,13,55,201,197,205,180,48,6,0,205,65,46,193,56,3,195,128,47,205,104,44,48,22,33,241,246,35,121,190,56,12,197,229,205,57,22,225,193,216,35,126,185,200,12,55,201,87,5,250,102,43,88,205,223,43,123,216,213,205,11,43,209,123,208,6,31,205,223,43,120,216,122,6,0,201,87,4,62,31,184,56,6,88,205,212,43,123,216,5,197,229,33,13,236,203,126,32,49,205,180,48,33,32,0,25,126,203,79,32,37,203,206,203,158,33,35,0,25,235,225,193,245,205,48,43,241,205,180,48,33,35,0,25,235,203,135,203,223,205,211,46,205,244,53,120,55,201,225,193,213,205,48,43,209,120,208,6,0,205,212,43,120,216,123,6,0,201,213,229,205,180,48,205,65,46,195,101,44,213,229,205,180,48,205,99,46,195,101,44,213,229,205,91,43,48,22,205,26,42,254,32,40,244,205,91,43,48,10,205,26,42,254,32,32,244,205,120,43,24,92,213,229,205,120,43,48,27,205,26,42,254,32,32,244,205,120,43,48,15,205,65,46,48,10,205,26,42,254,32,40,239,55,24,58,212,91,43,183,24,52,213,229,205,180,48,33,32,0,25,203,70,32,7,205,11,43,56,240,24,32,6,0,205,212,43,24,25,213,229,205,180,48,33,32,0,25,203,94,32,7,205,48,43,56,240,24,5,6,31,205,223,43,225,209,201,58,13,236,203,95,55,200,205,180,48,33,32,0,25,203,94,55,200,24,18,58,13,236,203,95,55,200,205,180,48,33,32,0,25,203,70,55,200,62,2,205,180,48,33,32,0,25,203,70,32,8,13,242,144,44,14,0,62,1,33,0,236,17,3,236,246,128,119,18,35,19,62,0,119,18,35,19,121,119,18,33,0,0,34,6,236,205,95,51,205,103,60,221,229,205,32,31,205,107,2,205,69,31,221,225,58,58,92,60,32,24,33,13,236,203,158,205,94,54,58,14,236,254,4,196,47,21,205,250,38,205,7,42,55,201,33,0,236,17,3,236,26,203,191,119,35,19,26,119,35,19,26,119,205,99,60,56,4,237,75,6,236,42,6,236,183,237,66,245,229,205,7,42,225,241,56,17,40,42,229,120,205,91,43,225,48,34,43,124,181,32,243,24,27,229,33,13,236,203,190,225,235,33,0,0,183,237,82,229,120,205,120,43,225,48,5,43,124,181,32,243,33,13,236,203,254,205,17,42,62,23,205,150,58,183,201,33,0,236,203,126,40,7,42,6,236,35,34,6,236,33,0,236,126,35,70,35,78,229,230,15,33,133,45,205,206,63,93,225,40,2,62,13,113,43,112,43,245,126,230,240,179,119,241,201,3,2,172,45,4,233,45,1,143,45,205,183,50,205,14,46,48,7,254,0,40,247,46,1,201,12,6,0,42,219,249,121,190,56,231,6,0,14,0,229,33,238,246,126,185,32,10,35,126,184,32,5,33,0,236,203,190,225,205,180,48,205,14,46,48,7,254,0,40,225,46,2,201,33,32,0,25,203,94,40,5,46,8,62,13,201,33,243,246,12,126,185,6,0,48,218,6,0,14,1,205,195,49,205,14,46,48,7,254,0,40,247,46,4,201,33,32,0,25,203,94,32,9,12,6,0,58,245,246,185,48,224,46,8,62,13,201,62,31,184,63,208,104,38,0,25,126,4,55,201,1,20,1,1,33,60,92,203,134,33,27,46,17,21,236,195,186,63,33,60,92,203,198,1,0,0,205,43,55,33,29,46,17,21,236,195,186,63,38,0,104,25,126,254,0,55,192,120,183,40,13,229,43,126,254,0,55,225,192,126,254,0,55,192,35,4,120,254,31,56,244,201,38,0,104,25,126,254,0,55,192,126,254,0,32,7,120,183,200,43,5,24,244,4,55,201,38,0,104,25,126,201,33,13,236,183,203,70,192,197,245,205,180,48,241,205,172,22,245,235,205,4,54,235,241,63,40,49,245,6,0,12,58,21,236,185,56,35,126,95,230,215,190,119,123,203,206,245,205,180,48,241,40,13,203,135,205,211,46,48,16,205,244,53,241,24,204,205,65,46,241,24,198,241,205,110,49,193,201,205,180,48,62,9,197,213,65,33,239,46,79,197,205,117,22,193,121,48,10,72,205,180,48,33,32,0,25,119,55,209,193,201,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,0,0,197,205,180,48,197,33,32,0,25,203,78,62,0,40,16,12,33,35,0,25,235,58,21,236,185,48,234,13,205,201,49,225,229,205,180,48,225,71,121,189,120,245,32,3,68,24,9,245,229,6,0,205,65,46,225,241,229,33,244,246,203,198,40,2,203,134,205,193,22,245,197,213,33,244,246,203,70,32,14,6,0,205,212,43,56,7,205,128,47,209,193,24,5,225,193,205,4,54,241,13,71,225,241,120,194,50,47,55,193,201,33,32,0,25,126,203,70,32,41,245,197,121,183,32,21,197,42,154,252,205,74,51,34,154,252,58,219,249,79,13,205,183,50,193,24,4,13,205,180,48,193,241,33,32,0,25,203,142,182,119,65,205,180,48,205,223,48,195,72,22,205,132,48,229,205,149,48,40,50,205,91,43,225,48,45,205,26,42,245,229,205,18,47,225,241,254,32,40,230,229,205,149,48,40,24,205,91,43,225,48,19,205,26,42,254,32,40,7,229,205,18,47,225,24,230,229,205,120,43,225,120,245,229,33,245,238,203,150,58,21,236,197,6,0,79,191,205,5,22,193,33,13,236,203,222,225,205,248,41,241,201,205,132,48,229,205,26,42,225,254,0,55,40,212,245,229,205,18,47,225,241,254,32,32,235,205,26,42,254,32,55,32,193,229,205,18,47,225,24,241,205,132,48,229,205,180,48,33,32,0,25,203,70,32,12,205,91,43,48,27,205,18,47,225,24,233,229,120,254,0,40,15,5,205,26,42,4,254,0,40,6,5,205,18,47,24,236,225,55,195,248,47,205,132,48,205,26,42,254,0,55,40,241,229,205,18,47,225,24,241,33,13,236,203,134,205,236,41,33,245,238,203,214,33,241,246,201,205,180,48,33,32,0,25,203,70,40,14,120,254,0,40,13,5,205,26,42,4,254,0,40,4,62,1,183,201,175,201,33,22,236,245,121,17,35,0,183,40,4,25,61,24,249,235,241,201,213,205,180,48,38,0,104,25,209,201,5,0,0,0,248,246,33,208,48,17,245,246,195,186,63,197,213,33,245,246,229,126,183,32,24,229,205,95,51,42,215,249,205,82,51,48,3,34,215,249,68,77,225,205,214,50,61,24,21,33,13,236,203,134,33,248,246,84,93,1,35,0,9,1,188,2,237,176,61,55,209,18,33,248,246,209,193,201,197,213,33,32,0,25,126,47,230,17,32,21,229,213,35,86,35,94,213,205,95,51,225,205,74,51,48,3,34,215,249,209,225,203,70,33,245,246,229,40,5,62,0,55,24,202,126,254,20,40,197,1,35,0,33,248,246,235,237,176,33,214,249,84,93,1,35,0,183,237,66,1,188,2,237,184,60,55,24,168,197,213,245,6,0,14,1,229,205,195,49,225,203,94,203,158,32,32,205,65,46,241,205,172,22,40,49,245,6,0,12,121,254,21,56,14,43,126,35,254,0,40,7,229,33,13,236,203,198,225,203,78,203,206,203,158,205,195,49,32,213,197,213,205,230,53,54,8,209,193,205,244,53,241,24,202,121,50,245,246,203,222,209,193,201,33,248,246,195,183,48,197,213,33,13,236,203,134,58,245,246,79,183,62,0,40,66,205,195,49,245,6,0,205,65,46,48,14,241,205,193,22,245,197,6,0,205,65,46,193,56,36,35,126,245,197,121,254,1,32,9,58,21,236,79,205,180,48,24,4,13,205,195,49,193,241,33,32,0,25,203,142,182,119,33,245,246,53,241,13,32,191,55,209,193,201,3,0,222,249,33,30,50,17,219,249,195,186,63,197,213,33,219,249,229,126,183,32,30,229,205,95,51,42,154,252,205,74,51,48,3,34,154,252,68,77,225,35,35,35,48,17,205,214,50,61,235,24,10,42,220,249,1,35,0,237,66,55,61,235,225,48,1,119,35,115,35,114,235,209,193,201,197,213,33,32,0,25,126,47,230,17,32,12,213,229,35,86,35,94,237,83,154,252,225,209,203,94,33,219,249,229,40,22,229,205,95,51,42,154,252,205,82,51,34,154,252,225,35,35,35,62,0,55,24,189,126,254,20,40,14,60,42,220,249,1,35,0,235,237,176,235,55,24,170,225,209,193,201,33,222,249,195,183,48,8,13,204,53,1,218,53,18,90,51,19,90,51,20,90,51,21,90,51,16,90,51,17,90,51,84,93,19,19,19,213,33,32,0,25,54,1,35,112,35,113,14,1,6,0,197,213,58,14,236,254,4,196,23,53,209,193,56,15,121,254,1,62,13,32,8,120,183,62,1,40,2,62,13,33,189,50,205,206,63,56,29,40,217,245,62,31,184,48,15,62,18,205,49,51,56,5,241,62,13,24,228,205,244,53,241,205,197,53,24,190,225,121,200,55,201,245,205,230,53,241,174,119,121,254,20,208,12,33,35,0,25,235,33,32,0,25,54,0,55,201,205,182,52,216,33,0,0,201,205,48,52,216,33,0,0,201,205,23,53,63,208,33,0,0,34,159,252,34,161,252,33,116,51,17,174,252,1,188,0,237,176,201,243,1,253,127,22,23,237,81,254,80,48,49,254,64,48,38,254,48,48,27,254,32,48,16,254,16,48,5,33,150,0,24,33,214,16,33,207,0,24,26,214,32,33,0,1,24,19,214,48,33,62,1,24,12,214,64,33,139,1,24,5,214,80,33,212,1,71,183,40,9,126,35,230,128,40,250,5,24,245,17,163,252,237,83,161,252,58,158,252,183,62,0,50,158,252,32,4,62,32,18,19,126,71,35,18,19,230,128,40,247,120,230,127,27,18,19,62,160,18,62,7,1,253,127,237,121,251,201,243,1,253,127,22,23,237,81,33,150,0,6,165,17,116,253,26,230,127,254,97,26,56,2,230,223,190,32,9,35,19,230,128,40,237,55,24,12,4,40,8,126,230,128,35,40,250,24,220,183,120,22,7,1,253,127,237,81,251,201,205,234,52,183,50,158,252,205,32,31,205,246,52,48,82,32,12,120,177,40,8,205,207,52,205,217,52,48,68,86,35,94,205,69,31,213,229,221,229,221,33,163,252,221,34,161,252,235,6,0,17,24,252,205,149,52,17,156,255,205,149,52,17,246,255,205,149,52,17,255,255,205,149,52,221,43,221,126,0,246,128,221,119,0,221,225,225,209,35,35,35,34,159,252,235,55,201,205,69,31,201,175,25,60,56,252,237,82,61,198,48,221,119,0,254,48,32,11,120,183,32,9,62,0,221,119,0,24,2,6,1,221,35,201,205,234,52,183,50,158,252,205,32,31,205,246,52,48,204,235,125,180,55,194,77,52,63,24,194,229,35,35,94,35,86,35,25,209,201,126,230,192,55,200,63,201,120,190,192,121,35,190,43,192,55,201,229,33,0,0,34,161,252,34,159,252,225,201,229,193,17,0,0,42,83,92,205,217,52,208,205,224,52,216,120,177,55,200,205,207,52,205,217,52,208,205,224,52,48,244,201,42,161,252,125,180,40,30,126,35,254,160,71,62,0,32,2,62,255,50,158,252,120,203,127,40,3,33,0,0,34,161,252,230,127,195,143,53,42,159,252,125,180,202,145,53,205,32,31,126,254,14,32,8,35,35,35,35,35,35,24,243,205,69,31,35,34,159,252,254,165,56,8,214,165,205,174,252,195,23,53,254,163,56,16,32,5,33,148,53,24,3,33,156,53,205,253,252,195,23,53,245,62,0,50,158,252,241,254,13,32,9,33,0,0,34,161,252,34,159,252,55,201,55,63,201,83,80,69,67,84,82,85,205,80,76,65,217,71,79,84,207,71,79,83,85,194,68,69,70,70,206,79,80,69,78,163,67,76,79,83,69,163,2,1,5,33,185,53,17,106,253,195,186,63,104,38,0,25,119,4,201,205,230,53,126,246,24,119,33,106,253,203,198,55,201,205,230,53,203,222,33,106,253,203,198,55,201,104,38,0,25,62,32,184,200,54,0,35,4,24,248,58,107,253,6,0,38,0,104,25,54,0,4,61,32,246,201,197,213,229,229,33,245,238,203,86,225,32,4,65,205,30,59,225,209,193,201,197,213,229,229,33,245,238,203,86,225,32,4,89,205,191,58,225,209,193,201,197,213,229,229,33,245,238,203,86,225,32,4,89,205,198,58,225,209,193,201,245,197,213,229,120,65,79,205,157,58,225,209,193,241,201,245,197,213,229,120,65,79,205,178,58,225,209,193,241,201,62,0,50,65,92,62,2,50,10,92,33,59,92,126,246,12,119,33,13,236,203,102,33,102,91,32,3,203,134,201,203,198,201,229,33,59,92,203,110,40,252,203,174,58,8,92,33,65,92,203,134,254,32,48,13,254,16,48,231,254,6,56,227,205,164,54,48,222,225,201,239,219,16,201,229,205,59,55,33,60,92,203,134,225,94,35,229,33,236,55,205,51,55,225,205,51,55,229,205,34,56,33,250,55,205,51,55,225,213,1,7,8,205,43,55,197,6,12,62,32,215,126,35,254,128,48,3,215,16,247,230,127,215,62,32,215,16,251,193,4,205,43,55,29,32,225,33,56,111,209,203,35,203,35,203,35,83,21,30,111,1,0,255,122,205,25,55,1,1,0,123,205,25,55,1,0,1,122,60,205,25,55,175,205,202,55,201,245,229,213,197,68,77,239,233,34,193,209,225,241,9,61,32,239,201,62,22,215,120,215,121,215,201,126,35,254,255,200,215,24,248,55,24,1,167,17,246,238,33,60,92,56,1,235,237,160,56,1,235,33,125,92,56,1,235,1,20,0,237,176,56,1,235,8,1,7,7,205,148,59,221,126,1,128,71,62,12,197,245,213,239,155,14,1,7,0,9,209,205,126,55,241,193,5,61,32,236,201,1,14,8,197,6,0,229,8,56,1,235,237,176,56,1,235,8,225,36,193,16,237,197,213,239,136,14,235,209,193,8,56,1,235,237,176,56,1,235,8,201,205,202,55,61,242,177,55,126,61,61,205,202,55,55,201,213,205,202,55,60,87,126,61,61,186,122,242,197,55,175,205,202,55,209,201,245,229,213,33,7,89,17,32,0,167,40,4,25,61,32,252,62,120,190,32,2,62,104,22,14,119,35,21,32,251,209,225,241,201,22,7,7,21,0,20,0,16,7,17,0,19,1,255,17,0,32,17,7,16,0,255,1,3,7,15,31,63,127,255,254,252,248,240,224,192,128,0,16,2,32,17,6,33,16,4,32,17,5,33,16,0,32,255,197,213,229,33,2,56,17,152,91,1,16,0,237,176,42,54,92,229,33,152,90,34,54,92,33,18,56,205,51,55,225,34,54,92,225,209,193,201,33,105,39,24,13,33,114,39,24,8,33,94,39,24,3,33,132,39,229,205,129,56,33,160,90,6,32,62,64,119,35,16,252,33,236,55,205,51,55,1,0,21,205,43,55,209,205,125,5,14,26,205,43,55,195,34,56,6,21,22,23,195,94,59,205,32,31,205,5,58,122,179,202,192,57,42,150,91,239,169,48,235,42,148,91,25,17,16,39,183,237,82,210,192,57,42,83,92,239,184,25,35,35,34,146,91,35,35,237,83,107,91,126,239,182,24,254,13,40,5,205,14,57,24,243,237,91,107,91,42,75,92,167,237,82,235,32,216,205,5,58,66,75,17,0,0,42,83,92,197,213,229,42,150,91,239,169,48,237,91,148,91,25,235,225,114,35,115,35,78,35,70,35,9,209,19,193,11,120,177,32,223,205,69,31,237,67,146,91,55,201,202,240,225,236,237,229,247,35,34,121,91,235,1,7,0,33,7,57,237,177,235,192,14,0,126,254,32,40,27,239,27,45,48,22,254,46,40,18,254,14,40,18,246,32,254,101,32,4,120,177,32,4,42,121,91,201,3,35,24,220,237,67,113,91,229,239,182,24,205,54,58,126,225,254,58,40,3,254,13,192,35,239,180,51,239,162,45,96,105,239,110,25,40,10,126,254,128,32,5,33,15,39,24,17,34,119,91,205,11,58,42,150,91,239,169,48,237,91,148,91,25,17,115,91,229,205,60,58,88,28,22,0,213,229,107,38,0,237,75,113,91,183,237,66,34,113,91,40,51,56,39,68,77,42,121,91,229,213,42,101,92,9,56,19,235,33,130,0,25,56,12,237,114,63,56,7,209,225,239,85,22,24,17,209,225,205,69,31,167,201,11,29,32,252,42,121,91,239,232,25,237,91,121,91,225,193,237,176,235,54,14,193,35,229,239,43,45,209,1,5,0,237,176,235,229,42,146,91,229,94,35,86,42,113,91,25,235,225,115,35,114,42,107,91,237,91,113,91,25,34,107,91,225,201,42,75,92,34,119,91,42,83,92,237,91,119,91,183,237,82,40,26,42,83,92,1,0,0,197,239,184,25,42,119,91,167,237,82,40,5,235,193,3,24,239,209,19,201,17,0,0,201,35,126,254,32,40,250,201,213,1,24,252,205,96,58,1,156,255,205,96,58,14,246,205,96,58,125,198,48,18,19,6,3,225,126,254,48,192,54,32,35,16,247,201,175,9,60,56,252,237,66,61,198,48,18,19,201,8,0,0,20,0,0,0,15,0,8,0,22,1,0,0,0,15,0,221,33,108,253,33,109,58,24,3,33,118,58,17,108,253,195,186,63,215,122,215,55,201,230,63,221,119,6,55,201,221,126,1,128,71,205,160,59,126,221,119,7,47,230,192,221,182,6,119,55,201,221,126,1,128,71,205,160,59,221,126,7,119,201,229,38,0,123,144,24,7,229,123,88,71,147,38,255,79,120,187,40,75,213,205,152,59,197,76,239,155,14,235,175,177,40,3,4,24,1,5,213,239,155,14,209,121,14,32,6,8,197,229,213,6,0,237,176,209,225,193,36,20,16,242,245,213,239,136,14,235,227,239,136,14,235,227,209,1,32,0,237,176,241,193,167,40,3,4,24,1,5,13,103,32,187,209,67,225,205,184,59,235,58,60,92,245,33,13,236,203,118,203,135,40,2,203,199,50,60,92,14,0,205,43,55,235,6,32,126,167,32,2,62,32,254,144,48,15,239,16,0,35,16,240,241,50,60,92,205,184,59,55,201,205,32,31,215,205,69,31,24,235,205,184,59,122,144,60,79,205,152,59,197,239,155,14,14,8,229,6,32,175,119,35,16,252,225,36,13,32,243,6,32,197,239,136,14,235,193,58,141,92,119,35,16,252,193,5,13,32,217,205,184,59,55,201,62,33,145,79,62,24,144,221,150,1,71,201,197,175,80,95,203,26,203,27,203,26,203,27,203,26,203,27,33,0,88,71,9,25,193,201,245,229,213,42,141,92,237,91,143,92,217,42,15,236,237,91,17,236,34,141,92,237,83,143,92,217,34,15,236,237,83,17,236,33,19,236,58,145,92,86,119,122,50,145,92,209,225,241,201,205,86,60,243,219,254,230,64,8,33,225,88,17,6,0,67,122,119,25,16,252,33,0,0,17,0,8,1,254,191,237,120,203,71,40,73,6,127,237,120,203,71,40,65,6,247,237,120,203,71,40,57,27,122,179,40,9,219,254,230,64,40,245,35,24,242,203,21,203,20,203,21,203,20,8,40,7,8,62,32,148,111,24,2,8,108,175,103,17,31,89,6,32,62,72,251,118,243,18,27,16,252,19,25,62,104,119,24,168,251,6,25,118,16,253,33,59,92,203,174,55,201,62,1,24,2,62,0,50,138,253,33,0,0,34,133,253,34,135,253,57,34,139,253,205,234,52,62,0,50,132,253,33,116,253,34,125,253,205,32,31,239,176,22,205,69,31,62,0,50,129,253,42,89,92,34,130,253,33,0,0,34,127,253,42,133,253,35,34,133,253,205,157,61,79,58,129,253,254,0,32,65,121,230,4,40,53,205,233,61,48,7,62,1,50,129,253,24,221,42,127,253,125,180,194,30,61,197,205,205,61,193,62,0,50,129,253,121,230,1,32,216,120,205,22,62,208,42,133,253,35,34,133,253,205,157,61,79,24,233,120,205,22,62,208,24,173,254,1,32,245,121,230,1,40,187,197,205,126,63,193,56,121,42,127,253,124,181,32,19,121,230,2,40,188,205,233,61,48,175,42,125,253,43,34,127,253,24,131,197,33,116,253,237,91,127,253,122,188,32,5,123,189,32,1,19,27,24,1,35,126,230,127,229,213,205,22,62,209,225,124,186,32,241,125,187,32,237,237,91,127,253,33,116,253,34,127,253,237,75,125,253,11,122,188,32,24,123,189,32,20,19,229,33,0,0,34,127,253,225,120,188,32,7,121,189,32,3,193,24,31,26,119,35,19,230,128,40,248,34,125,253,24,129,197,205,22,62,193,33,0,0,34,127,253,58,129,253,254,4,40,5,62,0,50,129,253,33,116,253,34,125,253,195,179,60,205,84,45,71,254,63,56,10,246,32,205,198,61,56,23,62,1,201,254,32,40,13,254,35,40,6,56,243,254,36,32,239,62,2,201,62,3,201,62,6,201,254,123,208,254,97,63,201,33,116,253,34,125,253,151,50,127,253,50,128,253,126,230,127,229,205,156,62,225,126,230,128,192,35,24,241,42,125,253,17,125,253,122,188,32,5,123,189,202,19,62,17,116,253,122,188,32,4,123,189,40,6,43,126,230,127,119,35,120,246,128,119,35,34,125,253,55,201,55,63,201,245,58,137,253,183,32,18,241,254,62,40,8,254,60,40,4,205,100,62,201,50,137,253,55,201,254,60,62,0,50,137,253,32,26,241,254,62,32,4,62,201,24,229,254,61,32,4,62,199,24,221,245,62,60,205,100,62,241,24,212,241,254,61,32,4,62,200,24,203,245,62,62,205,100,62,241,24,194,254,13,40,32,254,234,71,32,7,62,4,50,129,253,24,14,254,34,32,10,58,129,253,230,254,238,2,50,129,253,120,205,156,62,55,201,58,138,253,254,0,40,10,237,75,133,253,42,139,253,249,55,201,55,63,201,95,58,132,253,87,123,254,32,32,32,122,230,1,32,20,122,230,2,32,7,122,246,2,50,132,253,201,123,205,251,62,58,132,253,201,122,230,254,50,132,253,201,254,163,48,36,122,230,2,32,11,122,230,254,50,132,253,123,205,251,62,201,213,62,32,205,251,62,209,122,230,254,230,253,50,132,253,123,205,251,62,201,122,230,253,246,1,50,132,253,123,205,251,62,201,42,135,253,35,34,135,253,42,130,253,71,58,138,253,254,0,120,40,37,237,91,95,92,124,186,32,26,125,187,32,22,237,75,133,253,42,135,253,167,237,66,48,4,237,75,135,253,42,139,253,249,55,201,55,24,2,55,63,205,32,31,48,13,126,235,254,14,32,29,19,19,19,19,19,24,22,245,1,1,0,229,213,205,102,63,209,225,239,100,22,42,101,92,235,237,184,241,18,19,205,69,31,237,83,130,253,201,42,101,92,9,56,10,235,33,130,0,25,56,3,237,114,216,62,3,50,58,92,195,33,3,205,46,253,216,6,249,17,116,253,33,148,53,205,59,253,208,254,255,32,4,62,212,24,34,254,254,32,4,62,211,24,26,254,253,32,4,62,206,24,18,254,252,32,4,62,237,24,10,254,251,32,4,62,236,24,2,214,86,55,201,70,35,126,18,19,35,16,250,201,254,48,63,208,254,58,208,214,48,55,201,197,213,70,35,190,35,94,35,86,40,8,35,16,246,55,63,209,193,201,235,209,193,205,238,63,56,2,191,201,191,55,201,233,0,77,66,0,83,66,0,65,67,0,82,71,0,75,77,0,1]);
JSSpeccy.roms['48.rom'] = new Uint8Array([243,175,17,255,255,195,203,17,42,93,92,34,95,92,24,67,195,242,21,255,255,255,255,255,42,93,92,126,205,125,0,208,205,116,0,24,247,255,255,255,195,91,51,255,255,255,255,255,197,42,97,92,229,195,158,22,245,229,42,120,92,35,34,120,92,124,181,32,3,253,52,64,197,213,205,191,2,209,193,225,241,251,201,225,110,253,117,0,237,123,61,92,195,197,22,255,255,255,255,255,255,255,245,229,42,176,92,124,181,32,1,233,225,241,237,69,42,93,92,35,34,93,92,126,201,254,33,208,254,13,200,254,16,216,254,24,63,216,35,254,22,56,1,35,55,34,93,92,201,191,82,78,196,73,78,75,69,89,164,80,201,70,206,80,79,73,78,212,83,67,82,69,69,78,164,65,84,84,210,65,212,84,65,194,86,65,76,164,67,79,68,197,86,65,204,76,69,206,83,73,206,67,79,211,84,65,206,65,83,206,65,67,211,65,84,206,76,206,69,88,208,73,78,212,83,81,210,83,71,206,65,66,211,80,69,69,203,73,206,85,83,210,83,84,82,164,67,72,82,164,78,79,212,66,73,206,79,210,65,78,196,60,189,62,189,60,190,76,73,78,197,84,72,69,206,84,207,83,84,69,208,68,69,70,32,70,206,67,65,212,70,79,82,77,65,212,77,79,86,197,69,82,65,83,197,79,80,69,78,32,163,67,76,79,83,69,32,163,77,69,82,71,197,86,69,82,73,70,217,66,69,69,208,67,73,82,67,76,197,73,78,203,80,65,80,69,210,70,76,65,83,200,66,82,73,71,72,212,73,78,86,69,82,83,197,79,86,69,210,79,85,212,76,80,82,73,78,212,76,76,73,83,212,83,84,79,208,82,69,65,196,68,65,84,193,82,69,83,84,79,82,197,78,69,215,66,79,82,68,69,210,67,79,78,84,73,78,85,197,68,73,205,82,69,205,70,79,210,71,79,32,84,207,71,79,32,83,85,194,73,78,80,85,212,76,79,65,196,76,73,83,212,76,69,212,80,65,85,83,197,78,69,88,212,80,79,75,197,80,82,73,78,212,80,76,79,212,82,85,206,83,65,86,197,82,65,78,68,79,77,73,90,197,73,198,67,76,211,68,82,65,215,67,76,69,65,210,82,69,84,85,82,206,67,79,80,217,66,72,89,54,53,84,71,86,78,74,85,55,52,82,70,67,77,75,73,56,51,69,68,88,14,76,79,57,50,87,83,90,32,13,80,48,49,81,65,227,196,224,228,180,188,189,187,175,176,177,192,167,166,190,173,178,186,229,165,194,225,179,185,193,184,126,220,218,92,183,123,125,216,191,174,170,171,221,222,223,127,181,214,124,213,93,219,182,217,91,215,12,7,6,4,5,8,10,11,9,15,226,42,63,205,200,204,203,94,172,45,43,61,46,44,59,34,199,60,195,62,197,47,201,96,198,58,208,206,168,202,211,212,209,210,169,207,46,47,17,255,255,1,254,254,237,120,47,230,31,40,14,103,125,20,192,214,8,203,60,48,250,83,95,32,244,45,203,0,56,230,122,60,200,254,40,200,254,25,200,123,90,87,254,24,201,205,142,2,192,33,0,92,203,126,32,7,35,53,43,32,2,54,255,125,33,4,92,189,32,238,205,30,3,208,33,0,92,190,40,46,235,33,4,92,190,40,39,203,126,32,4,235,203,126,200,95,119,35,54,5,35,58,9,92,119,35,253,78,7,253,86,1,229,205,51,3,225,119,50,8,92,253,203,1,238,201,35,54,5,35,53,192,58,10,92,119,35,126,24,234,66,22,0,123,254,39,208,254,24,32,3,203,120,192,33,5,2,25,126,55,201,123,254,58,56,47,13,250,79,3,40,3,198,79,201,33,235,1,4,40,3,33,5,2,22,0,25,126,201,33,41,2,203,64,40,244,203,90,40,10,253,203,48,94,192,4,192,198,32,201,198,165,201,254,48,216,13,250,157,3,32,25,33,84,2,203,104,40,211,254,56,48,7,214,32,4,200,198,8,201,214,54,4,200,198,254,201,33,48,2,254,57,40,186,254,48,40,182,230,7,198,128,4,200,238,15,201,4,200,203,104,33,48,2,32,164,214,16,254,34,40,6,254,32,192,62,95,201,62,64,201,243,125,203,61,203,61,47,230,3,79,6,0,221,33,209,3,221,9,58,72,92,230,56,15,15,15,246,8,0,0,0,4,12,13,32,253,14,63,5,194,214,3,238,16,211,254,68,79,203,103,32,9,122,179,40,9,121,77,27,221,233,77,12,221,233,251,201,239,49,39,192,3,52,236,108,152,31,245,4,161,15,56,33,146,92,126,167,32,94,35,78,35,70,120,23,159,185,32,84,35,190,32,80,120,198,60,242,37,4,226,108,4,6,250,4,214,12,48,251,198,12,197,33,110,4,205,6,52,205,180,51,239,4,56,241,134,119,239,192,2,49,56,205,148,30,254,11,48,34,239,224,4,224,52,128,67,85,159,128,1,5,52,53,113,3,56,205,153,30,197,205,153,30,225,80,89,122,179,200,27,195,181,3,207,10,137,2,208,18,134,137,10,151,96,117,137,18,213,23,31,137,27,144,65,2,137,36,208,83,202,137,46,157,54,177,137,56,255,73,62,137,67,255,106,115,137,79,167,0,84,137,92,0,0,0,137,105,20,246,36,137,118,241,16,5,205,251,36,58,59,92,135,250,138,28,225,208,229,205,241,43,98,107,13,248,9,203,254,201,33,63,5,229,33,128,31,203,127,40,3,33,152,12,8,19,221,43,243,62,2,71,16,254,211,254,238,15,6,164,45,32,245,5,37,242,216,4,6,47,16,254,211,254,62,13,6,55,16,254,211,254,1,14,59,8,111,195,7,5,122,179,40,12,221,110,0,124,173,103,62,1,55,195,37,5,108,24,244,121,203,120,16,254,48,4,6,66,16,254,211,254,6,62,32,239,5,175,60,203,21,194,20,5,27,221,35,6,49,62,127,219,254,31,208,122,60,194,254,4,6,59,16,254,201,245,58,72,92,230,56,15,15,15,211,254,62,127,219,254,31,251,56,2,207,12,241,201,20,8,21,243,62,15,211,254,33,63,5,229,219,254,31,230,32,246,2,79,191,192,205,231,5,48,250,33,21,4,16,254,43,124,181,32,249,205,227,5,48,235,6,156,205,227,5,48,228,62,198,184,48,224,36,32,241,6,201,205,231,5,48,213,120,254,212,48,244,205,231,5,208,121,238,3,79,38,0,6,176,24,31,8,32,7,48,15,221,117,0,24,15,203,17,173,192,121,31,79,19,24,7,221,126,0,173,192,221,35,27,8,6,178,46,1,205,227,5,208,62,203,184,203,21,6,176,210,202,5,124,173,103,122,179,32,202,124,254,1,201,205,231,5,208,62,22,61,32,253,167,4,200,62,127,219,254,31,208,169,230,32,40,243,121,47,79,230,7,246,8,211,254,55,201,241,58,116,92,214,224,50,116,92,205,140,28,205,48,37,40,60,1,17,0,58,116,92,167,40,2,14,34,247,213,221,225,6,11,62,32,18,19,16,252,221,54,1,255,205,241,43,33,246,255,11,9,3,48,15,58,116,92,167,32,2,207,14,120,177,40,10,1,10,0,221,229,225,35,235,237,176,223,254,228,32,73,58,116,92,254,3,202,138,28,231,205,178,40,203,249,48,11,33,0,0,58,116,92,61,40,21,207,1,194,138,28,205,48,37,40,24,35,126,221,119,11,35,126,221,119,12,35,221,113,14,62,1,203,113,40,1,60,221,119,0,235,231,254,41,32,218,231,205,238,27,235,195,90,7,254,170,32,31,58,116,92,254,3,202,138,28,231,205,238,27,221,54,11,0,221,54,12,27,33,0,64,221,117,13,221,116,14,24,77,254,175,32,79,58,116,92,254,3,202,138,28,231,205,72,32,32,12,58,116,92,167,202,138,28,205,230,28,24,15,205,130,28,223,254,44,40,12,58,116,92,167,202,138,28,205,230,28,24,4,231,205,130,28,205,238,27,205,153,30,221,113,11,221,112,12,205,153,30,221,113,13,221,112,14,96,105,221,54,0,3,24,68,254,202,40,9,205,238,27,221,54,14,128,24,23,58,116,92,167,194,138,28,231,205,130,28,205,238,27,205,153,30,221,113,13,221,112,14,221,54,0,0,42,89,92,237,91,83,92,55,237,82,221,117,11,221,116,12,42,75,92,237,82,221,117,15,221,116,16,235,58,116,92,167,202,112,9,229,1,17,0,221,9,221,229,17,17,0,175,55,205,86,5,221,225,48,242,62,254,205,1,22,253,54,82,3,14,128,221,126,0,221,190,239,32,2,14,246,254,4,48,217,17,192,9,197,205,10,12,193,221,229,209,33,240,255,25,6,10,126,60,32,3,121,128,79,19,26,190,35,32,1,12,215,16,246,203,121,32,179,62,13,215,225,221,126,0,254,3,40,12,58,116,92,61,202,8,8,254,2,202,182,8,229,221,110,250,221,102,251,221,94,11,221,86,12,124,181,40,13,237,82,56,38,40,7,221,126,0,254,3,32,29,225,124,181,32,6,221,110,13,221,102,14,229,221,225,58,116,92,254,2,55,32,1,167,62,255,205,86,5,216,207,26,221,94,11,221,86,12,229,124,181,32,6,19,19,19,235,24,12,221,110,250,221,102,251,235,55,237,82,56,9,17,5,0,25,68,77,205,5,31,225,221,126,0,167,40,62,124,181,40,19,43,70,43,78,43,3,3,3,221,34,95,92,205,232,25,221,42,95,92,42,89,92,43,221,78,11,221,70,12,197,3,3,3,221,126,253,245,205,85,22,35,241,119,209,35,115,35,114,35,229,221,225,55,62,255,195,2,8,235,42,89,92,43,221,34,95,92,221,78,11,221,70,12,197,205,229,25,193,229,197,205,85,22,221,42,95,92,35,221,78,15,221,70,16,9,34,75,92,221,102,14,124,230,192,32,10,221,110,13,34,66,92,253,54,10,0,209,221,225,55,62,255,195,2,8,221,78,11,221,70,12,197,3,247,54,128,235,209,229,229,221,225,55,62,255,205,2,8,225,237,91,83,92,126,230,192,32,25,26,19,190,35,32,2,26,190,27,43,48,8,229,235,205,184,25,225,24,236,205,44,9,24,226,126,79,254,128,200,229,42,75,92,126,254,128,40,37,185,40,8,197,205,184,25,193,235,24,240,230,224,254,160,32,18,209,213,229,35,19,26,190,32,6,23,48,247,225,24,3,225,24,224,62,255,209,235,60,55,205,44,9,24,196,32,16,8,34,95,92,235,205,184,25,205,232,25,235,42,95,92,8,8,213,205,184,25,34,95,92,42,83,92,227,197,8,56,7,43,205,85,22,35,24,3,205,85,22,35,193,209,237,83,83,92,237,91,95,92,197,213,235,237,176,225,193,213,205,232,25,209,201,229,62,253,205,1,22,175,17,161,9,205,10,12,253,203,2,238,205,212,21,221,229,17,17,0,175,205,194,4,221,225,6,50,118,16,253,221,94,11,221,86,12,62,255,221,225,195,194,4,128,83,116,97,114,116,32,116,97,112,101,44,32,116,104,101,110,32,112,114,101,115,115,32,97,110,121,32,107,101,121,174,13,80,114,111,103,114,97,109,58,160,13,78,117,109,98,101,114,32,97,114,114,97,121,58,160,13,67,104,97,114,97,99,116,101,114,32,97,114,114,97,121,58,160,13,66,121,116,101,115,58,160,205,3,11,254,32,210,217,10,254,6,56,105,254,24,48,101,33,11,10,95,22,0,25,94,25,229,195,3,11,78,87,16,41,84,83,82,55,80,79,95,94,93,92,91,90,84,83,12,62,34,185,32,17,253,203,1,78,32,9,4,14,2,62,24,184,32,3,5,14,33,195,217,13,58,145,92,245,253,54,87,1,62,32,205,101,11,241,50,145,92,201,253,203,1,78,194,205,14,14,33,205,85,12,5,195,217,13,205,3,11,121,61,61,230,16,24,90,62,63,24,108,17,135,10,50,15,92,24,11,17,109,10,24,3,17,135,10,50,14,92,42,81,92,115,35,114,201,17,244,9,205,128,10,42,14,92,87,125,254,22,218,17,34,32,41,68,74,62,31,145,56,12,198,2,79,253,203,1,78,32,22,62,22,144,218,159,30,60,71,4,253,203,2,70,194,85,12,253,190,49,218,134,12,195,217,13,124,205,3,11,129,61,230,31,200,87,253,203,1,198,62,32,205,59,12,21,32,248,201,205,36,11,253,203,1,78,32,26,253,203,2,70,32,8,237,67,136,92,34,132,92,201,237,67,138,92,237,67,130,92,34,134,92,201,253,113,69,34,128,92,201,253,203,1,78,32,20,237,75,136,92,42,132,92,253,203,2,70,200,237,75,138,92,42,134,92,201,253,78,69,42,128,92,201,254,128,56,61,254,144,48,38,71,205,56,11,205,3,11,17,146,92,24,71,33,146,92,205,62,11,203,24,159,230,15,79,203,24,159,230,240,177,14,4,119,35,13,32,251,201,214,165,48,9,198,21,197,237,75,123,92,24,11,205,16,12,195,3,11,197,237,75,54,92,235,33,59,92,203,134,254,32,32,2,203,198,38,0,111,41,41,41,9,193,235,121,61,62,33,32,14,5,79,253,203,1,78,40,6,213,205,205,14,209,121,185,213,204,85,12,209,197,229,58,145,92,6,255,31,56,1,4,31,31,159,79,62,8,167,253,203,1,78,40,5,253,203,48,206,55,235,8,26,160,174,169,18,8,56,19,20,35,61,32,242,235,37,253,203,1,78,204,219,11,225,193,13,35,201,8,62,32,131,95,8,24,230,124,15,15,15,230,3,246,88,103,237,91,143,92,126,171,162,171,253,203,87,118,40,8,230,199,203,87,32,2,238,56,253,203,87,102,40,8,230,248,203,111,32,2,238,7,119,201,229,38,0,227,24,4,17,149,0,245,205,65,12,56,9,62,32,253,203,1,70,204,59,12,26,230,127,205,59,12,26,19,135,48,245,209,254,72,40,3,254,130,216,122,254,3,216,62,32,213,217,215,217,209,201,245,235,60,203,126,35,40,251,61,32,248,235,241,254,32,216,26,214,65,201,253,203,1,78,192,17,217,13,213,120,253,203,2,70,194,2,13,253,190,49,56,27,192,253,203,2,102,40,22,253,94,45,29,40,90,62,0,205,1,22,237,123,63,92,253,203,2,166,201,207,4,253,53,82,32,69,62,24,144,50,140,92,42,143,92,229,58,145,92,245,62,253,205,1,22,175,17,248,12,205,10,12,253,203,2,238,33,59,92,203,222,203,174,217,205,212,21,217,254,32,40,69,254,226,40,65,246,32,254,110,40,59,62,254,205,1,22,241,50,145,92,225,34,143,92,205,254,13,253,70,49,4,14,33,197,205,155,14,124,15,15,15,230,3,246,88,103,17,224,90,26,78,6,32,235,18,113,19,35,16,250,193,201,128,115,99,114,111,108,108,191,207,12,254,2,56,128,253,134,49,214,25,208,237,68,197,71,42,143,92,229,42,145,92,229,205,77,13,120,245,33,107,92,70,120,60,119,33,137,92,190,56,3,52,6,24,205,0,14,241,61,32,232,225,253,117,87,225,34,143,92,237,75,136,92,253,203,2,134,205,217,13,253,203,2,198,193,201,175,42,141,92,253,203,2,70,40,4,103,253,110,14,34,143,92,33,145,92,32,2,126,15,174,230,85,174,119,201,205,175,13,33,60,92,203,174,203,198,205,77,13,253,70,49,205,68,14,33,192,90,58,141,92,5,24,7,14,32,43,119,13,32,251,16,247,253,54,49,2,62,253,205,1,22,42,81,92,17,244,9,167,115,35,114,35,17,168,16,63,56,246,1,33,23,24,42,33,0,0,34,125,92,253,203,48,134,205,148,13,62,254,205,1,22,205,77,13,6,24,205,68,14,42,81,92,17,244,9,115,35,114,253,54,82,1,1,33,24,33,0,91,253,203,1,78,32,18,120,253,203,2,70,40,5,253,134,49,214,24,197,71,205,155,14,193,62,33,145,95,22,0,25,195,220,10,6,23,205,155,14,14,8,197,229,120,230,7,120,32,12,235,33,224,248,25,235,1,32,0,61,237,176,235,33,224,255,25,235,71,230,7,15,15,15,79,120,6,0,237,176,6,7,9,230,248,32,219,225,36,193,13,32,205,205,136,14,33,224,255,25,235,237,176,6,1,197,205,155,14,14,8,197,229,120,230,7,15,15,15,79,120,6,0,13,84,93,54,0,19,237,176,17,1,7,25,61,230,248,71,32,229,225,36,193,13,32,220,205,136,14,98,107,19,58,141,92,253,203,2,70,40,3,58,72,92,119,11,237,176,193,14,33,201,124,15,15,15,61,246,80,103,235,97,104,41,41,41,41,41,68,77,201,62,24,144,87,15,15,15,230,224,111,122,230,24,246,64,103,201,243,6,176,33,0,64,229,197,205,244,14,193,225,36,124,230,7,32,10,125,198,32,111,63,159,230,248,132,103,16,231,24,13,243,33,0,91,6,8,197,205,244,14,193,16,249,62,4,211,251,251,33,0,91,253,117,70,175,71,119,35,16,252,253,203,48,142,14,33,195,217,13,120,254,3,159,230,2,211,251,87,205,84,31,56,10,62,4,211,251,251,205,223,14,207,12,219,251,135,248,48,235,14,32,94,35,6,8,203,18,203,19,203,26,219,251,31,48,251,122,211,251,16,240,13,32,233,201,42,61,92,229,33,127,16,229,237,115,61,92,205,212,21,245,22,0,253,94,255,33,200,0,205,181,3,241,33,56,15,229,254,24,48,49,254,7,56,45,254,16,56,58,1,2,0,87,254,22,56,12,3,253,203,55,126,202,30,16,205,212,21,95,205,212,21,213,42,91,92,253,203,7,134,205,85,22,193,35,112,35,113,24,10,253,203,7,134,42,91,92,205,82,22,18,19,237,83,91,92,201,95,22,0,33,153,15,25,94,25,229,42,91,92,201,9,102,106,80,181,112,126,207,212,42,73,92,253,203,55,110,194,151,16,205,110,25,205,149,22,122,179,202,151,16,229,35,78,35,70,33,10,0,9,68,77,205,5,31,205,151,16,42,81,92,227,229,62,255,205,1,22,225,43,253,53,15,205,85,24,253,52,15,42,89,92,35,35,35,35,34,91,92,225,205,21,22,201,253,203,55,110,32,8,33,73,92,205,15,25,24,109,253,54,0,16,24,29,205,49,16,24,5,126,254,13,200,35,34,91,92,201,205,49,16,1,1,0,195,232,25,205,212,21,205,212,21,225,225,225,34,61,92,253,203,0,126,192,249,201,55,205,149,17,237,82,25,35,193,216,197,68,77,98,107,35,26,230,240,254,16,32,9,35,26,214,23,206,0,32,1,35,167,237,66,9,235,56,230,201,253,203,55,110,192,42,73,92,205,110,25,235,205,149,22,33,74,92,205,28,25,205,149,23,62,0,195,1,22,253,203,55,126,40,168,195,129,15,253,203,48,102,40,161,253,54,0,255,22,0,253,94,254,33,144,26,205,181,3,195,48,15,229,205,144,17,43,205,229,25,34,91,92,253,54,7,0,225,201,253,203,2,94,196,29,17,167,253,203,1,110,200,58,8,92,253,203,1,174,245,253,203,2,110,196,110,13,241,254,32,48,82,254,16,48,45,254,6,48,10,71,230,1,79,120,31,198,18,24,42,32,9,33,106,92,62,8,174,119,24,14,254,14,216,214,13,33,65,92,190,119,32,2,54,0,253,203,2,222,191,201,71,230,7,79,62,16,203,88,32,1,60,253,113,211,17,13,17,24,6,58,13,92,17,168,16,42,79,92,35,35,115,35,114,55,201,205,77,13,253,203,2,158,253,203,2,174,42,138,92,229,42,61,92,229,33,103,17,229,237,115,61,92,42,130,92,229,55,205,149,17,235,205,125,24,235,205,225,24,42,138,92,227,235,205,77,13,58,139,92,146,56,38,32,6,123,253,150,80,48,30,62,32,213,205,244,9,209,24,233,22,0,253,94,254,33,144,26,205,181,3,253,54,0,255,237,91,138,92,24,2,209,225,225,34,61,92,193,213,205,217,13,225,34,130,92,253,54,38,0,201,42,97,92,43,167,237,91,89,92,253,203,55,110,200,237,91,97,92,216,42,99,92,201,126,254,14,1,6,0,204,232,25,126,35,254,13,32,241,201,243,62,255,237,91,178,92,217,237,75,180,92,237,91,56,92,42,123,92,217,71,62,7,211,254,62,63,237,71,0,0,0,0,0,0,98,107,54,2,43,188,32,250,167,237,82,25,35,48,6,53,40,3,53,40,243,43,217,237,67,180,92,237,83,56,92,34,123,92,217,4,40,25,34,180,92,17,175,62,1,168,0,235,237,184,235,35,34,123,92,43,1,64,0,237,67,56,92,34,178,92,33,0,60,34,54,92,42,178,92,54,62,43,249,43,43,34,61,92,237,86,253,33,58,92,251,33,182,92,34,79,92,17,175,21,1,21,0,235,237,176,235,43,34,87,92,35,34,83,92,34,75,92,54,128,35,34,89,92,54,13,35,54,128,35,34,97,92,34,99,92,34,101,92,62,56,50,141,92,50,143,92,50,72,92,33,35,5,34,9,92,253,53,198,253,53,202,33,198,21,17,16,92,1,14,0,237,176,253,203,1,206,205,223,14,253,54,49,2,205,107,13,175,17,56,21,205,10,12,253,203,2,238,24,7,253,54,49,2,205,149,23,205,176,22,62,0,205,1,22,205,44,15,205,23,27,253,203,0,126,32,18,253,203,48,102,40,64,42,89,92,205,167,17,253,54,0,255,24,221,42,89,92,34,93,92,205,251,25,120,177,194,93,21,223,254,13,40,192,253,203,48,70,196,175,13,205,110,13,62,25,253,150,79,50,140,92,253,203,1,254,253,54,0,255,253,54,10,1,205,138,27,118,253,203,1,174,253,203,48,78,196,205,14,58,58,92,60,245,33,0,0,253,116,55,253,116,38,34,11,92,33,1,0,34,22,92,205,176,22,253,203,55,174,205,110,13,253,203,2,238,241,71,254,10,56,2,198,7,205,239,21,62,32,215,120,17,145,19,205,10,12,175,17,54,21,205,10,12,237,75,69,92,205,27,26,62,58,215,253,78,13,6,0,205,27,26,205,151,16,58,58,92,60,40,27,254,9,40,4,254,21,32,3,253,52,13,1,3,0,17,112,92,33,68,92,203,126,40,1,9,237,184,253,54,10,255,253,203,1,158,195,172,18,128,79,203,78,69,88,84,32,119,105,116,104,111,117,116,32,70,79,210,86,97,114,105,97,98,108,101,32,110,111,116,32,102,111,117,110,228,83,117,98,115,99,114,105,112,116,32,119,114,111,110,231,79,117,116,32,111,102,32,109,101,109,111,114,249,79,117,116,32,111,102,32,115,99,114,101,101,238,78,117,109,98,101,114,32,116,111,111,32,98,105,231,82,69,84,85,82,78,32,119,105,116,104,111,117,116,32,71,79,83,85,194,69,110,100,32,111,102,32,102,105,108,229,83,84,79,80,32,115,116,97,116,101,109,101,110,244,73,110,118,97,108,105,100,32,97,114,103,117,109,101,110,244,73,110,116,101,103,101,114,32,111,117,116,32,111,102,32,114,97,110,103,229,78,111,110,115,101,110,115,101,32,105,110,32,66,65,83,73,195,66,82,69,65,75,32,45,32,67,79,78,84,32,114,101,112,101,97,116,243,79,117,116,32,111,102,32,68,65,84,193,73,110,118,97,108,105,100,32,102,105,108,101,32,110,97,109,229,78,111,32,114,111,111,109,32,102,111,114,32,108,105,110,229,83,84,79,80,32,105,110,32,73,78,80,85,212,70,79,82,32,119,105,116,104,111,117,116,32,78,69,88,212,73,110,118,97,108,105,100,32,73,47,79,32,100,101,118,105,99,229,73,110,118,97,108,105,100,32,99,111,108,111,117,242,66,82,69,65,75,32,105,110,116,111,32,112,114,111,103,114,97,237,82,65,77,84,79,80,32,110,111,32,103,111,111,228,83,116,97,116,101,109,101,110,116,32,108,111,115,244,73,110,118,97,108,105,100,32,115,116,114,101,97,237,70,78,32,119,105,116,104,111,117,116,32,68,69,198,80,97,114,97,109,101,116,101,114,32,101,114,114,111,242,84,97,112,101,32,108,111,97,100,105,110,103,32,101,114,114,111,242,44,160,127,32,49,57,56,50,32,83,105,110,99,108,97,105,114,32,82,101,115,101,97,114,99,104,32,76,116,228,62,16,1,0,0,195,19,19,237,67,73,92,42,93,92,235,33,85,21,229,42,97,92,55,237,82,229,96,105,205,110,25,32,6,205,184,25,205,232,25,193,121,61,176,40,40,197,3,3,3,3,43,237,91,83,92,213,205,85,22,225,34,83,92,193,197,19,42,97,92,43,43,237,184,42,73,92,235,193,112,43,113,43,115,43,114,241,195,162,18,244,9,168,16,75,244,9,196,21,83,129,15,196,21,82,244,9,196,21,80,128,207,18,1,0,6,0,11,0,1,0,1,0,6,0,16,0,253,203,2,110,32,4,253,203,2,222,205,230,21,216,40,250,207,7,217,229,42,81,92,35,35,24,8,30,48,131,217,229,42,81,92,94,35,86,235,205,44,22,225,217,201,135,198,22,111,38,92,94,35,86,122,179,32,2,207,23,27,42,79,92,25,34,81,92,253,203,48,166,35,35,35,35,78,33,45,22,205,220,22,208,22,0,94,25,233,75,6,83,18,80,27,0,253,203,2,198,253,203,1,174,253,203,48,230,24,4,253,203,2,134,253,203,1,142,195,77,13,253,203,1,206,201,1,1,0,229,205,5,31,225,205,100,22,42,101,92,235,237,184,201,245,229,33,75,92,62,14,94,35,86,227,167,237,82,25,227,48,9,213,235,9,235,114,43,115,35,209,35,61,32,232,235,209,241,167,237,82,68,77,3,25,235,201,0,0,235,17,143,22,126,230,192,32,247,86,35,94,201,42,99,92,43,205,85,22,35,35,193,237,67,97,92,193,235,35,201,42,89,92,54,13,34,91,92,35,54,128,35,34,97,92,42,97,92,34,99,92,42,99,92,34,101,92,229,33,146,92,34,104,92,225,201,237,91,89,92,195,229,25,35,126,167,200,185,35,32,248,55,201,205,30,23,205,1,23,1,0,0,17,226,163,235,25,56,7,1,212,21,9,78,35,70,235,113,35,112,201,229,42,79,92,9,35,35,35,78,235,33,22,23,205,220,22,78,6,0,9,233,75,5,83,3,80,1,225,201,205,148,30,254,16,56,2,207,23,198,3,7,33,16,92,79,6,0,9,78,35,70,43,201,239,1,56,205,30,23,120,177,40,22,235,42,79,92,9,35,35,35,126,235,254,75,40,8,254,83,40,4,254,80,32,207,205,93,23,115,35,114,201,229,205,241,43,120,177,32,2,207,14,197,26,230,223,79,33,122,23,205,220,22,48,241,78,6,0,9,193,233,75,6,83,8,80,10,0,30,1,24,6,30,6,24,2,30,16,11,120,177,32,213,87,225,201,24,144,237,115,63,92,253,54,2,16,205,175,13,253,203,2,198,253,70,49,205,68,14,253,203,2,134,253,203,48,198,42,73,92,237,91,108,92,167,237,82,25,56,34,213,205,110,25,17,192,2,235,237,82,227,205,110,25,193,197,205,184,25,193,9,56,14,235,86,35,94,43,237,83,108,92,24,237,34,108,92,42,108,92,205,110,25,40,1,235,205,51,24,253,203,2,166,201,62,3,24,2,62,2,253,54,2,0,205,48,37,196,1,22,223,205,112,32,56,20,223,254,59,40,4,254,44,32,6,231,205,130,28,24,8,205,230,28,24,3,205,222,28,205,238,27,205,153,30,120,230,63,103,105,34,73,92,205,110,25,30,1,205,85,24,215,253,203,2,102,40,246,58,107,92,253,150,79,32,238,171,200,229,213,33,108,92,205,15,25,209,225,24,224,237,75,73,92,205,128,25,22,62,40,5,17,0,0,203,19,253,115,45,126,254,64,193,208,197,205,40,26,35,35,35,253,203,1,134,122,167,40,5,215,253,203,1,198,213,235,253,203,48,150,33,59,92,203,150,253,203,55,110,40,2,203,214,42,95,92,167,237,82,32,5,62,63,205,193,24,205,225,24,235,126,205,182,24,35,254,13,40,6,235,205,55,25,24,224,209,201,254,14,192,35,35,35,35,35,35,126,201,217,42,143,92,229,203,188,203,253,34,143,92,33,145,92,86,213,54,0,205,244,9,225,253,116,87,225,34,143,92,217,201,42,91,92,167,237,82,192,58,65,92,203,7,40,4,198,67,24,22,33,59,92,203,158,62,75,203,86,40,11,203,222,60,253,203,48,94,40,2,62,67,213,205,193,24,209,201,94,35,86,229,235,35,205,110,25,205,149,22,225,253,203,55,110,192,114,43,115,201,123,167,248,24,13,175,9,60,56,252,237,66,61,40,241,195,239,21,205,27,45,48,48,254,33,56,44,253,203,1,150,254,203,40,36,254,58,32,14,253,203,55,110,32,22,253,203,48,86,40,20,24,14,254,34,32,10,245,58,106,92,238,4,50,106,92,241,253,203,1,214,215,201,229,42,83,92,84,93,193,205,128,25,208,197,205,184,25,235,24,244,126,184,192,35,126,43,185,201,35,35,35,34,93,92,14,0,21,200,231,187,32,4,167,201,35,126,205,182,24,34,93,92,254,34,32,1,13,254,58,40,4,254,203,32,4,203,65,40,223,254,13,32,227,21,55,201,229,126,254,64,56,23,203,111,40,20,135,250,199,25,63,1,5,0,48,2,14,18,23,35,126,48,251,24,6,35,35,78,35,70,35,9,209,167,237,82,68,77,25,235,201,205,221,25,197,120,47,71,121,47,79,3,205,100,22,235,225,25,213,237,176,225,201,42,89,92,43,34,93,92,231,33,146,92,34,101,92,205,59,45,205,162,45,56,4,33,240,216,9,218,138,28,195,197,22,213,229,175,203,120,32,32,96,105,30,255,24,8,213,86,35,94,229,235,30,32,1,24,252,205,42,25,1,156,255,205,42,25,14,246,205,42,25,125,205,239,21,225,209,201,177,203,188,191,196,175,180,147,145,146,149,152,152,152,152,152,152,152,127,129,46,108,110,112,72,148,86,63,65,43,23,31,55,119,68,15,89,43,67,45,81,58,109,66,13,73,92,68,21,93,1,61,2,6,0,103,30,6,203,5,240,28,6,0,237,30,0,238,28,0,35,31,4,61,6,204,6,5,3,29,4,0,171,29,5,205,31,5,137,32,5,2,44,5,178,27,0,183,17,3,161,30,5,249,23,8,0,128,30,3,79,30,0,95,30,3,172,30,0,107,13,9,0,220,34,6,0,58,31,5,237,29,5,39,30,3,66,30,9,5,130,35,0,172,14,5,201,31,5,245,23,11,11,11,11,8,0,248,3,9,5,32,35,7,7,7,7,7,7,8,0,122,30,6,0,148,34,5,96,31,6,44,10,0,54,23,6,0,229,22,10,0,147,23,10,44,10,0,147,23,10,0,147,23,0,147,23,253,203,1,190,205,251,25,175,50,71,92,61,50,58,92,24,1,231,205,191,22,253,52,13,250,138,28,223,6,0,254,13,40,122,254,58,40,235,33,118,27,229,79,231,121,214,206,218,138,28,79,33,72,26,9,78,9,24,3,42,116,92,126,35,34,116,92,1,82,27,197,79,254,32,48,12,33,1,28,6,0,9,78,9,229,223,5,201,223,185,194,138,28,231,201,205,84,31,56,2,207,20,253,203,10,126,32,113,42,66,92,203,124,40,20,33,254,255,34,69,92,42,97,92,43,237,91,89,92,27,58,68,92,24,51,205,110,25,58,68,92,40,25,167,32,67,71,126,230,192,120,40,15,207,255,193,205,48,37,200,42,85,92,62,192,166,192,175,254,1,206,0,86,35,94,237,83,69,92,35,94,35,86,235,25,35,34,85,92,235,34,93,92,87,30,0,253,54,10,255,21,253,114,13,202,40,27,20,205,139,25,40,8,207,22,205,48,37,192,193,193,223,254,13,40,186,254,58,202,40,27,195,138,28,15,29,75,9,103,11,123,142,113,180,129,207,205,222,28,191,193,204,238,27,235,42,116,92,78,35,70,235,197,201,205,178,40,253,54,55,0,48,8,253,203,55,206,32,24,207,1,204,150,41,253,203,1,118,32,13,175,205,48,37,196,241,43,33,113,92,182,119,235,237,67,114,92,34,77,92,201,193,205,86,28,205,238,27,201,58,59,92,245,205,251,36,241,253,86,1,170,230,64,32,36,203,122,194,255,42,201,205,178,40,245,121,246,159,60,32,20,241,24,169,231,205,130,28,254,44,32,9,231,205,251,36,253,203,1,118,192,207,11,205,251,36,253,203,1,118,200,24,244,253,203,1,126,253,203,2,134,196,77,13,241,58,116,92,214,19,205,252,33,205,238,27,42,143,92,34,141,92,33,145,92,126,7,174,230,170,174,119,201,205,48,37,40,19,253,203,2,134,205,77,13,33,144,92,126,246,248,119,253,203,87,182,223,205,226,33,24,159,195,5,6,254,13,40,4,254,58,32,156,205,48,37,200,239,160,56,201,207,8,193,205,48,37,40,10,239,2,56,235,205,233,52,218,179,27,195,41,27,254,205,32,9,231,205,130,28,205,238,27,24,6,205,238,27,239,161,56,239,192,2,1,224,1,56,205,255,42,34,104,92,43,126,203,254,1,6,0,9,7,56,6,14,13,205,85,22,35,229,239,2,2,56,225,235,14,10,237,176,42,69,92,235,115,35,114,253,86,13,20,35,114,205,218,29,208,253,70,56,42,69,92,34,66,92,58,71,92,237,68,87,42,93,92,30,243,197,237,75,85,92,205,134,29,237,67,85,92,193,56,17,231,246,32,184,40,3,231,24,232,231,62,1,146,50,68,92,201,207,17,126,254,58,40,24,35,126,230,192,55,192,70,35,78,237,67,66,92,35,78,35,70,229,9,68,77,225,22,0,197,205,139,25,193,208,24,224,253,203,55,78,194,46,28,42,77,92,203,126,40,31,35,34,104,92,239,224,226,15,192,2,56,205,218,29,216,42,104,92,17,15,0,25,94,35,86,35,102,235,195,115,30,207,0,239,225,224,226,54,0,2,1,3,55,0,4,56,167,201,56,55,201,231,205,31,28,205,48,37,40,41,223,34,95,92,42,87,92,126,254,44,40,9,30,228,205,134,29,48,2,207,13,205,119,0,205,86,28,223,34,87,92,42,95,92,253,54,38,0,205,120,0,223,254,44,40,201,205,238,27,201,205,48,37,32,11,205,251,36,254,44,196,238,27,231,24,245,62,228,71,237,185,17,0,2,195,139,25,205,153,30,96,105,205,110,25,43,34,87,92,201,205,153,30,120,177,32,4,237,75,120,92,237,67,118,92,201,42,110,92,253,86,54,24,12,205,153,30,96,105,22,0,124,254,240,48,44,34,66,92,253,114,10,201,205,133,30,237,121,201,205,133,30,2,201,205,213,45,56,21,40,2,237,68,245,205,153,30,241,201,205,213,45,24,3,205,162,45,56,1,200,207,10,205,103,30,1,0,0,205,69,30,24,3,205,153,30,120,177,32,4,237,75,178,92,197,237,91,75,92,42,89,92,43,205,229,25,205,107,13,42,101,92,17,50,0,25,209,237,82,48,8,42,180,92,167,237,82,48,2,207,21,235,34,178,92,209,193,54,62,43,249,197,237,115,61,92,235,233,209,253,102,13,36,227,51,237,75,69,92,197,229,237,115,61,92,213,205,103,30,1,20,0,42,101,92,9,56,10,235,33,80,0,25,56,3,237,114,216,46,3,195,85,0,1,0,0,205,5,31,68,77,201,193,225,209,122,254,62,40,11,59,227,235,237,115,61,92,197,195,115,30,213,229,207,6,205,153,30,118,11,120,177,40,12,120,161,60,32,1,3,253,203,1,110,40,238,253,203,1,174,201,62,127,219,254,31,216,62,254,219,254,31,201,205,48,37,40,5,62,206,195,57,30,253,203,1,246,205,141,44,48,22,231,254,36,32,5,253,203,1,182,231,254,40,32,60,231,254,41,40,32,205,141,44,210,138,28,235,231,254,36,32,2,235,231,235,1,6,0,205,85,22,35,35,54,14,254,44,32,3,231,24,224,254,41,32,19,231,254,61,32,14,231,58,59,92,245,205,251,36,241,253,174,1,230,64,194,138,28,205,238,27,205,48,37,225,200,233,62,3,24,2,62,2,205,48,37,196,1,22,205,77,13,205,223,31,205,238,27,201,223,205,69,32,40,13,205,78,32,40,251,205,252,31,205,78,32,40,243,254,41,200,205,195,31,62,13,215,201,223,254,172,32,13,205,121,28,205,195,31,205,7,35,62,22,24,16,254,173,32,18,231,205,130,28,205,195,31,205,153,30,62,23,215,121,215,120,215,201,205,242,33,208,205,112,32,208,205,251,36,205,195,31,253,203,1,118,204,241,43,194,227,45,120,177,11,200,26,19,215,24,247,254,41,200,254,13,200,254,58,201,223,254,59,40,20,254,44,32,10,205,48,37,40,11,62,6,215,24,6,254,39,192,205,245,31,231,205,69,32,32,1,193,191,201,254,35,55,192,231,205,130,28,167,205,195,31,205,148,30,254,16,210,14,22,205,1,22,167,201,205,48,37,40,8,62,1,205,1,22,205,110,13,253,54,2,1,205,193,32,205,238,27,237,75,136,92,58,107,92,184,56,3,14,33,71,237,67,136,92,62,25,144,50,140,92,253,203,2,134,205,217,13,195,110,13,205,78,32,40,251,254,40,32,14,231,205,223,31,223,254,41,194,138,28,231,195,178,33,254,202,32,17,231,205,31,28,253,203,55,254,253,203,1,118,194,138,28,24,13,205,141,44,210,175,33,205,31,28,253,203,55,190,205,48,37,202,178,33,205,191,22,33,113,92,203,182,203,238,1,1,0,203,126,32,11,58,59,92,230,64,32,2,14,3,182,119,247,54,13,121,15,15,48,5,62,34,18,43,119,34,91,92,253,203,55,126,32,44,42,93,92,229,42,61,92,229,33,58,33,229,253,203,48,102,40,4,237,115,61,92,42,97,92,205,167,17,253,54,0,255,205,44,15,253,203,1,190,205,185,33,24,3,205,44,15,253,54,34,0,205,214,33,32,10,205,29,17,237,75,130,92,205,217,13,33,113,92,203,174,203,126,203,190,32,28,225,225,34,61,92,225,34,95,92,253,203,1,254,205,185,33,42,95,92,253,54,38,0,34,93,92,24,23,42,99,92,237,91,97,92,55,237,82,68,77,205,178,42,205,255,42,24,3,205,252,31,205,78,32,202,193,32,201,42,97,92,34,93,92,223,254,226,40,12,58,113,92,205,89,28,223,254,13,200,207,11,205,48,37,200,207,16,42,81,92,35,35,35,35,126,254,75,201,231,205,242,33,216,223,254,44,40,246,254,59,40,242,195,138,28,254,217,216,254,223,63,216,245,231,241,214,201,245,205,130,28,241,167,205,195,31,245,205,148,30,87,241,215,122,215,201,214,17,206,0,40,29,214,2,206,0,40,86,254,1,122,6,1,32,4,7,7,6,4,79,122,254,2,48,22,121,33,145,92,24,56,122,6,7,56,5,7,7,7,6,56,79,122,254,10,56,2,207,19,33,143,92,254,8,56,11,126,40,7,176,47,230,36,40,1,120,79,121,205,108,34,62,7,186,159,205,108,34,7,7,230,80,71,62,8,186,159,174,160,174,119,35,120,201,159,122,15,6,128,32,3,15,6,64,79,122,254,8,40,4,254,2,48,189,121,33,143,92,205,108,34,121,15,15,15,24,216,205,148,30,254,8,48,169,211,254,7,7,7,203,111,32,2,238,7,50,72,92,201,62,175,144,218,249,36,71,167,31,55,31,167,31,168,230,248,168,103,121,7,7,7,168,230,199,168,7,7,111,121,230,7,201,205,7,35,205,170,34,71,4,126,7,16,253,230,1,195,40,45,205,7,35,205,229,34,195,77,13,237,67,125,92,205,170,34,71,4,62,254,15,16,253,71,126,253,78,87,203,65,32,1,160,203,81,32,2,168,47,119,195,219,11,205,20,35,71,197,205,20,35,89,193,81,79,201,205,213,45,218,249,36,14,1,200,14,255,201,223,254,44,194,138,28,231,205,130,28,205,238,27,239,42,61,56,126,254,129,48,5,239,2,56,24,161,239,163,56,54,131,239,197,2,56,205,125,36,197,239,49,225,4,56,126,254,128,48,8,239,2,2,56,193,195,220,34,239,194,1,192,2,3,1,224,15,192,1,49,224,1,49,224,160,193,2,56,253,52,98,205,148,30,111,229,205,148,30,225,103,34,125,92,193,195,32,36,223,254,44,40,6,205,238,27,195,119,36,231,205,130,28,205,238,27,239,197,162,4,31,49,48,48,0,6,2,56,195,119,36,192,2,193,2,49,42,225,1,225,42,15,224,5,42,224,1,61,56,126,254,129,48,7,239,2,2,56,195,119,36,205,125,36,197,239,2,225,1,5,193,2,1,49,225,4,194,2,1,49,225,4,226,229,224,3,162,4,49,31,197,2,32,192,2,194,2,193,229,4,224,226,4,15,225,1,193,2,224,4,226,229,4,3,194,42,225,42,15,2,56,26,254,129,193,218,119,36,197,239,1,56,58,125,92,205,40,45,239,192,15,1,56,58,126,92,205,40,45,239,197,15,224,229,56,193,5,40,60,24,20,239,225,49,227,4,226,228,4,3,193,2,228,4,226,227,4,15,194,2,56,197,239,192,2,225,15,49,56,58,125,92,205,40,45,239,3,224,226,15,192,1,224,56,58,126,92,205,40,45,239,3,56,205,183,36,193,16,198,239,2,2,1,56,58,125,92,205,40,45,239,3,1,56,58,126,92,205,40,45,239,3,56,205,183,36,195,77,13,239,49,40,52,50,0,1,5,229,1,5,42,56,205,213,45,56,6,230,252,198,4,48,2,62,252,245,205,40,45,239,229,1,5,49,31,196,2,49,162,4,31,193,1,192,2,49,4,49,15,161,3,27,195,2,56,193,201,205,7,35,121,184,48,6,105,213,175,95,24,7,177,200,104,65,213,22,0,96,120,31,133,56,3,188,56,7,148,79,217,193,197,24,4,79,213,217,193,42,125,92,120,132,71,121,60,133,56,13,40,13,61,79,205,229,34,217,121,16,217,209,201,40,243,207,10,223,6,0,197,79,33,150,37,205,220,22,121,210,132,38,6,0,78,9,233,205,116,0,3,254,13,202,138,28,254,34,32,243,205,116,0,254,34,201,231,254,40,32,6,205,121,28,223,254,41,194,138,28,253,203,1,126,201,205,7,35,42,54,92,17,0,1,25,121,15,15,15,230,224,168,95,121,230,24,238,64,87,6,96,197,213,229,26,174,40,4,60,32,26,61,79,6,7,20,35,26,174,169,32,15,16,247,193,193,193,62,128,144,1,1,0,247,18,24,10,225,17,8,0,25,209,193,16,211,72,195,178,42,205,7,35,121,15,15,15,79,230,224,168,111,121,230,3,238,88,103,126,195,40,45,34,28,40,79,46,242,43,18,168,86,165,87,167,132,166,143,196,230,170,191,171,199,169,206,0,231,195,255,36,223,35,229,1,0,0,205,15,37,32,27,205,15,37,40,251,205,48,37,40,17,247,225,213,126,35,18,19,254,34,32,248,126,35,254,34,40,242,11,209,33,59,92,203,182,203,126,196,178,42,195,18,39,231,205,251,36,254,41,194,138,28,231,195,18,39,195,189,39,205,48,37,40,40,237,75,118,92,205,43,45,239,161,15,52,55,22,4,52,128,65,0,0,128,50,2,161,3,49,56,205,162,45,237,67,118,92,126,167,40,3,214,16,119,24,9,205,48,37,40,4,239,163,56,52,231,195,195,38,1,90,16,231,254,35,202,13,39,33,59,92,203,182,203,126,40,31,205,142,2,14,0,32,19,205,30,3,48,14,21,95,205,51,3,245,1,1,0,247,241,18,14,1,6,0,205,178,42,195,18,39,205,34,37,196,53,37,231,195,219,37,205,34,37,196,128,37,231,24,72,205,34,37,196,203,34,231,24,63,205,136,44,48,86,254,65,48,60,205,48,37,32,35,205,155,44,223,1,6,0,205,85,22,35,54,14,35,235,42,101,92,14,5,167,237,66,34,101,92,237,176,235,43,205,119,0,24,14,223,35,126,254,14,32,250,35,205,180,51,34,93,92,253,203,1,246,24,20,205,178,40,218,46,28,204,150,41,58,59,92,254,192,56,4,35,205,180,51,24,51,1,219,9,254,45,40,39,1,24,16,254,174,40,32,214,175,218,138,28,1,240,4,254,20,40,20,210,138,28,6,16,198,220,79,254,223,48,2,203,177,254,238,56,2,203,185,197,231,195,255,36,223,254,40,32,12,253,203,1,118,32,23,205,82,42,231,24,240,6,0,79,33,149,39,205,220,22,48,6,78,33,237,38,9,70,209,122,184,56,58,167,202,24,0,197,33,59,92,123,254,237,32,6,203,118,32,2,30,153,213,205,48,37,40,9,123,230,63,71,239,59,56,24,9,123,253,174,1,230,64,194,138,28,209,33,59,92,203,246,203,123,32,2,203,182,193,24,193,213,121,253,203,1,118,32,21,230,63,198,8,79,254,16,32,4,203,241,24,8,56,215,254,23,40,2,203,249,197,231,195,255,36,43,207,45,195,42,196,47,197,94,198,61,206,62,204,60,205,199,201,200,202,201,203,197,199,198,200,0,6,8,8,10,2,3,5,5,5,5,5,5,6,205,48,37,32,53,231,205,141,44,210,138,28,231,254,36,245,32,1,231,254,40,32,18,231,254,41,40,16,205,251,36,223,254,44,32,3,231,24,245,254,41,194,138,28,231,33,59,92,203,182,241,40,2,203,246,195,18,39,231,230,223,71,231,214,36,79,32,1,231,231,229,42,83,92,43,17,206,0,197,205,134,29,193,48,2,207,24,229,205,171,40,230,223,184,32,8,205,171,40,214,36,185,40,12,225,43,17,0,2,197,205,139,25,193,24,215,167,204,171,40,209,209,237,83,93,92,205,171,40,229,254,41,40,66,35,126,254,14,22,64,40,7,43,205,171,40,35,22,0,35,229,213,205,251,36,241,253,174,1,230,64,32,43,225,235,42,101,92,1,5,0,237,66,34,101,92,237,176,235,43,205,171,40,254,41,40,13,229,223,254,44,32,13,231,225,205,171,40,24,190,229,223,254,41,40,2,207,25,209,235,34,93,92,42,11,92,227,34,11,92,213,231,231,205,251,36,225,34,93,92,225,34,11,92,231,195,18,39,35,126,254,33,56,250,201,253,203,1,246,223,205,141,44,210,138,28,229,230,31,79,231,229,254,40,40,40,203,241,254,36,40,17,203,233,205,136,44,48,15,205,136,44,48,22,203,177,231,24,246,231,253,203,1,182,58,12,92,167,40,6,205,48,37,194,81,41,65,205,48,37,32,8,121,230,224,203,255,79,24,55,42,75,92,126,230,127,40,45,185,32,34,23,135,242,63,41,56,48,209,213,229,35,26,19,254,32,40,250,246,32,190,40,244,246,128,190,32,6,26,205,136,44,48,21,225,197,205,184,25,235,193,24,206,203,248,209,223,254,40,40,9,203,232,24,13,209,209,209,229,223,205,136,44,48,3,231,24,248,225,203,16,203,112,201,42,11,92,126,254,41,202,239,40,126,246,96,71,35,126,254,14,40,7,43,205,171,40,35,203,168,120,185,40,18,35,35,35,35,35,205,171,40,254,41,202,239,40,205,171,40,24,217,203,105,32,12,35,237,91,101,92,205,192,51,235,34,101,92,209,209,175,60,201,175,71,203,121,32,75,203,126,32,14,60,35,78,35,70,35,235,205,178,42,223,195,73,42,35,35,35,70,203,113,40,10,5,40,232,235,223,254,40,32,97,235,235,24,36,229,223,225,254,44,40,32,203,121,40,82,203,113,32,6,254,41,32,60,231,201,254,41,40,108,254,204,32,50,223,43,34,93,92,24,94,33,0,0,229,231,225,121,254,192,32,9,223,254,41,40,81,254,204,40,229,197,229,205,238,42,227,235,205,204,42,56,25,11,205,244,42,9,209,193,16,179,203,121,32,102,229,203,113,32,19,66,75,223,254,41,40,2,207,2,231,225,17,5,0,205,244,42,9,201,205,238,42,227,205,244,42,193,9,35,66,75,235,205,177,42,223,254,41,40,7,254,44,32,219,205,82,42,231,254,40,40,248,253,203,1,182,201,205,48,37,196,241,43,231,254,41,40,80,213,175,245,197,17,1,0,223,225,254,204,40,23,241,205,205,42,245,80,89,229,223,225,254,204,40,9,254,41,194,138,28,98,107,24,19,229,231,225,254,41,40,12,241,205,205,42,245,223,96,105,254,41,32,230,241,227,25,43,227,167,237,82,1,0,0,56,7,35,167,250,32,42,68,77,209,253,203,1,182,205,48,37,200,175,253,203,1,182,197,205,169,51,193,42,101,92,119,35,115,35,114,35,113,35,112,35,34,101,92,201,175,213,229,245,205,130,28,241,205,48,37,40,18,245,205,153,30,209,120,177,55,40,5,225,229,167,237,66,122,222,0,225,209,201,235,35,94,35,86,201,205,48,37,200,205,169,48,218,21,31,201,42,77,92,253,203,55,78,40,94,1,5,0,3,35,126,254,32,40,250,48,11,254,16,56,17,254,22,48,13,35,24,237,205,136,44,56,231,254,36,202,192,43,121,42,89,92,43,205,85,22,35,35,235,213,42,77,92,27,214,6,71,40,17,35,126,254,33,56,250,246,32,19,18,16,244,246,128,18,62,192,42,77,92,174,246,32,225,205,234,43,229,239,2,56,225,1,5,0,167,237,66,24,64,253,203,1,118,40,6,17,6,0,25,24,231,42,77,92,237,75,114,92,253,203,55,70,32,48,120,177,200,229,247,213,197,84,93,35,54,32,237,184,229,205,241,43,225,227,167,237,66,9,48,2,68,77,227,235,120,177,40,2,237,176,193,209,225,235,120,177,200,213,237,176,225,201,43,43,43,126,229,197,205,198,43,193,225,3,3,3,195,232,25,62,223,42,77,92,166,245,205,241,43,235,9,197,43,34,77,92,3,3,3,42,89,92,43,205,85,22,42,77,92,193,197,3,237,184,235,35,193,112,43,113,241,43,119,42,89,92,43,201,42,101,92,43,70,43,78,43,86,43,94,43,126,34,101,92,201,205,178,40,194,138,28,205,48,37,32,8,203,177,205,150,41,205,238,27,56,8,197,205,184,25,205,232,25,193,203,249,6,0,197,33,1,0,203,113,32,2,46,5,235,231,38,255,205,204,42,218,32,42,225,197,36,229,96,105,205,244,42,235,223,254,44,40,232,254,41,32,187,231,193,121,104,38,0,35,35,41,25,218,21,31,213,197,229,68,77,42,89,92,43,205,85,22,35,119,193,11,11,11,35,113,35,112,193,120,35,119,98,107,27,54,0,203,113,40,2,54,32,193,237,184,193,112,43,113,43,61,32,248,201,205,27,45,63,216,254,65,63,208,254,91,216,254,97,63,208,254,123,201,254,196,32,25,17,0,0,231,214,49,206,0,32,10,235,63,237,106,218,173,49,235,24,239,66,75,195,43,45,254,46,40,15,205,59,45,254,46,32,40,231,205,27,45,56,34,24,10,231,205,27,45,218,138,28,239,160,56,239,161,192,2,56,223,205,34,45,56,11,239,224,164,5,192,4,15,56,231,24,239,254,69,40,3,254,101,192,6,255,231,254,43,40,5,254,45,32,2,4,231,205,27,45,56,203,197,205,59,45,205,213,45,193,218,173,49,167,250,173,49,4,40,2,237,68,195,79,45,254,48,216,254,58,63,201,205,27,45,216,214,48,79,6,0,253,33,58,92,175,95,81,72,71,205,182,42,239,56,167,201,245,239,160,56,241,205,34,45,216,239,1,164,4,15,56,205,116,0,24,241,7,15,48,2,47,60,245,33,146,92,205,11,53,239,164,56,241,203,63,48,13,245,239,193,224,0,4,4,51,2,5,225,56,241,40,8,245,239,49,4,56,241,24,229,239,2,56,201,35,78,35,126,169,145,95,35,126,137,169,87,201,14,0,229,54,0,35,113,35,123,169,145,119,35,122,137,169,119,35,54,0,225,201,239,56,126,167,40,5,239,162,15,39,56,239,2,56,229,213,235,70,205,127,45,175,144,203,121,66,75,123,209,225,201,87,23,159,95,79,175,71,205,182,42,239,52,239,26,32,154,133,4,39,56,205,162,45,216,245,5,4,40,3,241,55,201,241,201,239,49,54,0,11,49,55,0,13,2,56,62,48,215,201,42,56,62,45,215,239,160,195,196,197,2,56,217,229,217,239,49,39,194,3,226,1,194,2,56,126,167,32,71,205,127,45,6,16,122,167,32,6,179,40,9,83,6,8,213,217,209,217,24,87,239,226,56,126,214,126,205,193,45,87,58,172,92,146,50,172,92,122,205,79,45,239,49,39,193,3,225,56,205,213,45,229,50,161,92,61,23,159,60,33,171,92,119,35,134,119,225,195,207,46,214,128,254,28,56,19,205,193,45,214,7,71,33,172,92,134,119,120,237,68,205,79,45,24,146,235,205,186,47,217,203,250,125,217,214,128,71,203,35,203,18,217,203,19,203,18,217,33,170,92,14,5,126,143,39,119,43,13,32,248,16,231,175,33,166,92,17,161,92,6,9,237,111,14,255,237,111,32,4,13,12,32,10,18,19,253,52,113,253,52,114,14,0,203,64,40,1,35,16,231,58,171,92,214,9,56,10,253,53,113,62,4,253,190,111,24,65,239,2,226,56,235,205,186,47,217,62,128,149,46,0,203,250,217,205,221,47,253,126,113,254,8,56,6,217,203,18,217,24,32,1,0,2,123,205,139,47,95,122,205,139,47,87,197,217,193,16,241,33,161,92,121,253,78,113,9,119,253,52,113,24,211,245,33,161,92,253,78,113,6,0,9,65,241,43,126,206,0,119,167,40,5,254,10,63,48,8,16,241,54,1,4,253,52,114,253,112,113,239,2,56,217,225,217,237,75,171,92,33,161,92,120,254,9,56,4,254,252,56,38,167,204,239,21,175,144,250,82,47,71,24,12,121,167,40,3,126,35,13,205,239,21,16,244,121,167,200,4,62,46,215,62,48,16,251,65,24,230,80,21,6,1,205,74,47,62,69,215,74,121,167,242,131,47,237,68,79,62,45,24,2,62,43,215,6,0,195,27,26,213,111,38,0,93,84,41,41,25,41,89,25,76,125,209,201,126,54,0,167,200,35,203,126,203,254,43,200,197,1,5,0,9,65,79,55,43,126,47,206,0,119,16,248,121,193,201,229,245,78,35,70,119,35,121,78,197,35,78,35,70,235,87,94,213,35,86,35,94,213,217,209,225,193,217,35,86,35,94,241,225,201,167,200,254,33,48,22,197,71,217,203,45,203,26,203,27,217,203,26,203,27,16,242,193,208,205,4,48,192,217,175,46,0,87,93,217,17,0,0,201,28,192,20,192,217,28,32,1,20,217,201,235,205,110,52,235,26,182,32,38,213,35,229,35,94,35,86,35,35,35,126,35,78,35,70,225,235,9,235,142,15,206,0,32,11,159,119,35,115,35,114,43,43,43,209,201,43,209,205,147,50,217,229,217,213,229,205,155,47,71,235,205,155,47,79,184,48,3,120,65,235,245,144,205,186,47,205,221,47,241,225,119,229,104,97,25,217,235,237,74,235,124,141,111,31,173,217,235,225,31,48,8,62,1,205,221,47,52,40,35,217,125,230,128,217,35,119,43,40,31,123,237,68,63,95,122,47,206,0,87,217,123,47,206,0,95,122,47,206,0,48,7,31,217,52,202,173,49,217,87,217,175,195,85,49,197,6,16,124,77,33,0,0,41,56,10,203,17,23,48,3,25,56,2,16,243,193,201,205,233,52,216,35,174,203,254,43,201,26,182,32,34,213,229,213,205,127,45,235,227,65,205,127,45,120,169,79,225,205,169,48,235,225,56,10,122,179,32,1,79,205,142,45,209,201,209,205,147,50,175,205,192,48,216,217,229,217,213,235,205,192,48,235,56,90,229,205,186,47,120,167,237,98,217,229,237,98,217,6,33,24,17,48,5,25,217,237,90,217,217,203,28,203,29,217,203,28,203,29,217,203,24,203,25,217,203,25,31,16,228,235,217,235,217,193,225,120,129,32,1,167,61,63,23,63,31,242,70,49,48,104,167,60,32,8,56,6,217,203,122,217,32,92,119,217,120,217,48,21,126,167,62,128,40,1,175,217,162,205,251,47,7,119,56,46,35,119,43,24,41,6,32,217,203,122,217,32,18,7,203,19,203,18,217,203,19,203,18,217,53,40,215,16,234,24,215,23,48,12,205,4,48,32,7,217,22,128,217,52,40,24,229,35,217,213,217,193,120,23,203,22,31,119,35,113,35,114,35,115,225,209,217,225,217,201,207,5,205,147,50,235,175,205,192,48,56,244,235,205,192,48,216,217,229,217,213,229,205,186,47,217,229,96,105,217,97,104,175,6,223,24,16,23,203,17,217,203,17,203,16,217,41,217,237,106,217,56,16,237,82,217,237,82,217,48,15,25,217,237,90,217,167,24,8,167,237,82,217,237,82,217,55,4,250,210,49,245,40,225,95,81,217,89,80,241,203,24,241,203,24,217,193,225,120,145,195,61,49,126,167,200,254,129,48,6,54,0,62,32,24,81,254,145,32,26,35,35,35,62,128,166,43,182,43,32,3,62,128,174,43,32,54,119,35,54,255,43,62,24,24,51,48,44,213,47,198,145,35,86,35,94,43,43,14,0,203,122,40,1,13,203,250,6,8,144,128,56,4,90,22,0,144,40,7,71,203,58,203,27,16,250,205,142,45,209,201,126,214,160,240,237,68,213,235,43,71,203,56,203,56,203,56,40,5,54,0,43,16,251,230,7,40,9,71,62,255,203,39,16,252,166,119,235,209,201,205,150,50,235,126,167,192,213,205,127,45,175,35,119,43,119,6,145,122,167,32,8,179,66,40,16,83,88,6,137,235,5,41,48,252,203,9,203,28,203,29,235,43,115,43,114,43,112,209,201,0,176,0,64,176,0,1,48,0,241,73,15,218,162,64,176,0,10,143,54,60,52,161,51,15,48,202,48,175,49,81,56,27,53,36,53,59,53,59,53,59,53,59,53,59,53,59,53,20,48,45,53,59,53,59,53,59,53,59,53,59,53,59,53,156,53,222,53,188,52,69,54,110,52,105,54,222,53,116,54,181,55,170,55,218,55,51,56,67,56,226,55,19,55,196,54,175,54,74,56,146,52,106,52,172,52,165,52,179,52,31,54,201,53,1,53,192,51,160,54,134,54,198,51,122,54,6,53,249,52,155,54,131,55,20,50,162,51,79,45,151,50,73,52,27,52,45,52,15,52,205,191,53,120,50,103,92,217,227,217,237,83,101,92,217,126,35,229,167,242,128,51,87,230,96,15,15,15,15,198,124,111,122,230,31,24,14,254,24,48,8,217,1,251,255,84,93,9,217,7,111,17,215,50,38,0,25,94,35,86,33,101,51,227,213,217,237,75,102,92,201,241,58,103,92,217,24,195,213,229,1,5,0,205,5,31,225,209,201,237,91,101,92,205,192,51,237,83,101,92,201,205,169,51,237,176,201,98,107,205,169,51,217,229,217,227,197,126,230,192,7,7,79,12,126,230,63,32,2,35,126,198,80,18,62,5,145,35,19,6,0,237,176,193,227,217,225,217,71,175,5,200,18,19,24,250,167,200,245,213,17,0,0,205,200,51,209,241,61,24,242,79,7,7,129,79,6,0,9,201,213,42,104,92,205,6,52,205,192,51,225,201,98,107,217,229,33,197,50,217,205,247,51,205,200,51,217,225,217,201,229,235,42,104,92,205,6,52,235,205,192,51,235,225,201,6,5,26,78,235,18,113,35,19,16,247,235,201,71,205,94,51,49,15,192,2,160,194,49,224,4,226,193,3,56,205,198,51,205,98,51,15,1,194,2,53,238,225,3,56,201,6,255,24,6,205,233,52,216,6,0,126,167,40,11,35,120,230,128,182,23,63,31,119,43,201,213,229,205,127,45,225,120,177,47,79,205,142,45,209,201,205,233,52,216,213,17,1,0,35,203,22,43,159,79,205,142,45,209,201,205,153,30,237,120,24,4,205,153,30,10,195,40,45,205,153,30,33,43,45,229,197,201,205,241,43,11,120,177,32,35,26,205,141,44,56,9,214,144,56,25,254,21,48,21,60,61,135,135,135,254,168,48,12,237,75,123,92,129,79,48,1,4,195,43,45,207,9,229,197,71,126,35,182,35,182,35,182,120,193,225,192,55,201,205,233,52,216,62,255,24,6,205,233,52,24,5,175,35,174,43,7,229,62,0,119,35,119,35,23,119,31,35,119,35,119,225,201,235,205,233,52,235,216,55,24,231,235,205,233,52,235,208,167,24,222,235,205,233,52,235,208,213,27,175,18,27,18,209,201,120,214,8,203,87,32,1,61,15,48,8,245,229,205,60,52,209,235,241,203,87,32,7,15,245,205,15,48,24,51,15,245,205,241,43,213,197,205,241,43,225,124,181,227,120,32,11,177,193,40,4,241,63,24,22,241,24,19,177,40,13,26,150,56,9,32,237,11,19,35,227,43,24,223,193,241,167,245,239,160,56,241,245,220,1,53,241,245,212,249,52,241,15,212,1,53,201,205,241,43,213,197,205,241,43,225,229,213,197,9,68,77,247,205,178,42,193,225,120,177,40,2,237,176,193,225,120,177,40,2,237,176,42,101,92,17,251,255,229,25,209,201,205,213,45,56,14,32,12,245,1,1,0,247,241,18,205,178,42,235,201,207,10,42,93,92,229,120,198,227,159,245,205,241,43,213,3,247,225,237,83,93,92,213,237,176,235,43,54,13,253,203,1,190,205,251,36,223,254,13,32,7,225,241,253,174,1,230,64,194,138,28,34,93,92,253,203,1,254,205,251,36,225,34,93,92,24,160,1,1,0,247,34,91,92,229,42,81,92,229,62,255,205,1,22,205,227,45,225,205,21,22,209,42,91,92,167,237,82,68,77,205,178,42,235,201,205,148,30,254,16,210,159,30,42,81,92,229,205,1,22,205,230,21,1,0,0,48,3,12,247,18,205,178,42,225,205,21,22,195,191,53,205,241,43,120,177,40,1,26,195,40,45,205,241,43,195,43,45,217,229,33,103,92,53,225,32,4,35,217,201,217,94,123,23,159,87,25,217,201,19,19,26,27,27,167,32,239,217,35,217,201,241,217,227,217,201,239,192,2,49,224,5,39,224,1,192,4,3,224,56,201,239,49,54,0,4,58,56,201,49,58,192,3,224,1,48,0,3,161,3,56,201,239,61,52,241,56,170,59,41,4,49,39,195,3,49,15,161,3,136,19,54,88,101,102,157,120,101,64,162,96,50,201,231,33,247,175,36,235,47,176,176,20,238,126,187,148,88,241,58,126,248,207,227,56,205,213,45,32,7,56,3,134,48,9,207,5,56,7,150,48,4,237,68,119,201,239,2,160,56,201,239,61,49,55,0,4,56,207,9,160,2,56,126,54,128,205,40,45,239,52,56,0,3,1,49,52,240,76,204,204,205,3,55,0,8,1,161,3,1,56,52,239,1,52,240,49,114,23,248,4,1,162,3,162,3,49,52,50,32,4,162,3,140,17,172,20,9,86,218,165,89,48,197,92,144,170,158,112,111,97,161,203,218,150,164,49,159,180,231,160,254,92,252,234,27,67,202,54,237,167,156,126,94,240,110,35,128,147,4,15,56,201,239,61,52,238,34,249,131,110,4,49,162,15,39,3,49,15,49,15,49,42,161,3,49,55,192,0,4,2,56,201,161,3,1,54,0,2,27,56,201,239,57,42,161,3,224,0,6,27,51,3,239,57,49,49,4,49,15,161,3,134,20,230,92,31,11,163,143,56,238,233,21,99,187,35,238,146,13,205,237,241,35,93,27,234,4,56,201,239,49,31,1,32,5,56,201,205,151,50,126,254,129,56,14,239,161,27,1,5,49,54,163,1,0,6,27,51,3,239,160,1,49,49,4,49,15,161,3,140,16,178,19,14,85,228,141,88,57,188,91,152,253,158,0,54,117,160,219,232,180,99,66,196,230,181,9,54,190,233,54,115,27,93,236,216,222,99,190,240,97,161,179,12,4,15,56,201,239,49,49,4,161,3,27,40,161,15,5,36,49,15,56,201,239,34,163,3,27,56,201,239,49,48,0,30,162,56,239,1,49,48,0,7,37,4,56,195,196,54,2,49,48,0,9,160,1,55,0,6,161,1,5,2,161,56,201,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,16,16,16,16,0,16,0,0,36,36,0,0,0,0,0,0,36,126,36,36,126,36,0,0,8,62,40,62,10,62,8,0,98,100,8,16,38,70,0,0,16,40,16,42,68,58,0,0,8,16,0,0,0,0,0,0,4,8,8,8,8,4,0,0,32,16,16,16,16,32,0,0,0,20,8,62,8,20,0,0,0,8,8,62,8,8,0,0,0,0,0,0,8,8,16,0,0,0,0,62,0,0,0,0,0,0,0,0,24,24,0,0,0,2,4,8,16,32,0,0,60,70,74,82,98,60,0,0,24,40,8,8,8,62,0,0,60,66,2,60,64,126,0,0,60,66,12,2,66,60,0,0,8,24,40,72,126,8,0,0,126,64,124,2,66,60,0,0,60,64,124,66,66,60,0,0,126,2,4,8,16,16,0,0,60,66,60,66,66,60,0,0,60,66,66,62,2,60,0,0,0,0,16,0,0,16,0,0,0,16,0,0,16,16,32,0,0,4,8,16,8,4,0,0,0,0,62,0,62,0,0,0,0,16,8,4,8,16,0,0,60,66,4,8,0,8,0,0,60,74,86,94,64,60,0,0,60,66,66,126,66,66,0,0,124,66,124,66,66,124,0,0,60,66,64,64,66,60,0,0,120,68,66,66,68,120,0,0,126,64,124,64,64,126,0,0,126,64,124,64,64,64,0,0,60,66,64,78,66,60,0,0,66,66,126,66,66,66,0,0,62,8,8,8,8,62,0,0,2,2,2,66,66,60,0,0,68,72,112,72,68,66,0,0,64,64,64,64,64,126,0,0,66,102,90,66,66,66,0,0,66,98,82,74,70,66,0,0,60,66,66,66,66,60,0,0,124,66,66,124,64,64,0,0,60,66,66,82,74,60,0,0,124,66,66,124,68,66,0,0,60,64,60,2,66,60,0,0,254,16,16,16,16,16,0,0,66,66,66,66,66,60,0,0,66,66,66,66,36,24,0,0,66,66,66,66,90,36,0,0,66,36,24,24,36,66,0,0,130,68,40,16,16,16,0,0,126,4,8,16,32,126,0,0,14,8,8,8,8,14,0,0,0,64,32,16,8,4,0,0,112,16,16,16,16,112,0,0,16,56,84,16,16,16,0,0,0,0,0,0,0,0,255,0,28,34,120,32,32,126,0,0,0,56,4,60,68,60,0,0,32,32,60,34,34,60,0,0,0,28,32,32,32,28,0,0,4,4,60,68,68,60,0,0,0,56,68,120,64,60,0,0,12,16,24,16,16,16,0,0,0,60,68,68,60,4,56,0,64,64,120,68,68,68,0,0,16,0,48,16,16,56,0,0,4,0,4,4,4,36,24,0,32,40,48,48,40,36,0,0,16,16,16,16,16,12,0,0,0,104,84,84,84,84,0,0,0,120,68,68,68,68,0,0,0,56,68,68,68,56,0,0,0,120,68,68,120,64,64,0,0,60,68,68,60,4,6,0,0,28,32,32,32,32,0,0,0,56,64,56,4,120,0,0,16,56,16,16,16,12,0,0,0,68,68,68,68,56,0,0,0,68,68,40,40,16,0,0,0,68,84,84,84,40,0,0,0,68,40,16,40,68,0,0,0,68,68,68,60,4,56,0,0,124,8,16,32,124,0,0,14,8,48,8,8,14,0,0,8,8,8,8,8,8,0,0,112,16,12,16,16,112,0,0,20,40,0,0,0,0,0,60,66,153,161,161,153,66,60]);
JSSpeccy.roms['128-1.rom'] = new Uint8Array([243,175,17,255,255,195,203,17,42,93,92,34,95,92,24,67,195,242,21,255,255,255,255,255,42,93,92,126,205,125,0,208,205,116,0,24,247,255,255,255,195,91,51,255,255,255,255,255,197,42,97,92,229,195,158,22,245,229,42,120,92,35,34,120,92,124,181,32,3,253,52,64,197,213,205,110,56,209,193,225,241,251,201,225,110,253,117,0,237,123,61,92,195,197,22,255,255,255,255,255,255,255,245,229,42,176,92,124,181,32,1,233,225,241,237,69,42,93,92,35,34,93,92,126,201,254,33,208,254,13,200,254,16,216,254,24,63,216,35,254,22,56,1,35,55,34,93,92,201,191,82,78,196,73,78,75,69,89,164,80,201,70,206,80,79,73,78,212,83,67,82,69,69,78,164,65,84,84,210,65,212,84,65,194,86,65,76,164,67,79,68,197,86,65,204,76,69,206,83,73,206,67,79,211,84,65,206,65,83,206,65,67,211,65,84,206,76,206,69,88,208,73,78,212,83,81,210,83,71,206,65,66,211,80,69,69,203,73,206,85,83,210,83,84,82,164,67,72,82,164,78,79,212,66,73,206,79,210,65,78,196,60,189,62,189,60,190,76,73,78,197,84,72,69,206,84,207,83,84,69,208,68,69,70,32,70,206,67,65,212,70,79,82,77,65,212,77,79,86,197,69,82,65,83,197,79,80,69,78,32,163,67,76,79,83,69,32,163,77,69,82,71,197,86,69,82,73,70,217,66,69,69,208,67,73,82,67,76,197,73,78,203,80,65,80,69,210,70,76,65,83,200,66,82,73,71,72,212,73,78,86,69,82,83,197,79,86,69,210,79,85,212,76,80,82,73,78,212,76,76,73,83,212,83,84,79,208,82,69,65,196,68,65,84,193,82,69,83,84,79,82,197,78,69,215,66,79,82,68,69,210,67,79,78,84,73,78,85,197,68,73,205,82,69,205,70,79,210,71,79,32,84,207,71,79,32,83,85,194,73,78,80,85,212,76,79,65,196,76,73,83,212,76,69,212,80,65,85,83,197,78,69,88,212,80,79,75,197,80,82,73,78,212,80,76,79,212,82,85,206,83,65,86,197,82,65,78,68,79,77,73,90,197,73,198,67,76,211,68,82,65,215,67,76,69,65,210,82,69,84,85,82,206,67,79,80,217,66,72,89,54,53,84,71,86,78,74,85,55,52,82,70,67,77,75,73,56,51,69,68,88,14,76,79,57,50,87,83,90,32,13,80,48,49,81,65,227,196,224,228,180,188,189,187,175,176,177,192,167,166,190,173,178,186,229,165,194,225,179,185,193,184,126,220,218,92,183,123,125,216,191,174,170,171,221,222,223,127,181,214,124,213,93,219,182,217,91,215,12,7,6,4,5,8,10,11,9,15,226,42,63,205,200,204,203,94,172,45,43,61,46,44,59,34,199,60,195,62,197,47,201,96,198,58,208,206,168,202,211,212,209,210,169,207,46,47,17,255,255,1,254,254,237,120,47,230,31,40,14,103,125,20,192,214,8,203,60,48,250,83,95,32,244,45,203,0,56,230,122,60,200,254,40,200,254,25,200,123,90,87,254,24,201,205,142,2,192,33,0,92,203,126,32,7,35,53,43,32,2,54,255,125,33,4,92,189,32,238,205,30,3,208,33,0,92,190,40,46,235,33,4,92,190,40,39,203,126,32,4,235,203,126,200,95,119,35,54,5,35,58,9,92,119,35,253,78,7,253,86,1,229,205,51,3,225,119,50,8,92,253,203,1,238,201,35,54,5,35,53,192,58,10,92,119,35,126,24,234,66,22,0,123,254,39,208,254,24,32,3,203,120,192,33,5,2,25,126,55,201,123,254,58,56,47,13,250,79,3,40,3,198,79,201,33,235,1,4,40,3,33,5,2,22,0,25,126,201,33,41,2,203,64,40,244,203,90,40,10,253,203,48,94,192,4,192,198,32,201,198,165,201,254,48,216,13,250,157,3,32,25,33,84,2,203,104,40,211,254,56,48,7,214,32,4,200,198,8,201,214,54,4,200,198,254,201,33,48,2,254,57,40,186,254,48,40,182,230,7,198,128,4,200,238,15,201,4,200,203,104,33,48,2,32,164,214,16,254,34,40,6,254,32,192,62,95,201,62,64,201,243,125,203,61,203,61,47,230,3,79,6,0,221,33,209,3,221,9,58,72,92,230,56,15,15,15,246,8,0,0,0,4,12,13,32,253,14,63,5,194,214,3,238,16,211,254,68,79,203,103,32,9,122,179,40,9,121,77,27,221,233,77,12,221,233,251,201,239,49,39,192,3,52,236,108,152,31,245,4,161,15,56,33,146,92,126,167,32,94,35,78,35,70,120,23,159,185,32,84,35,190,32,80,120,198,60,242,37,4,226,108,4,6,250,4,214,12,48,251,198,12,197,33,110,4,205,6,52,205,180,51,239,4,56,241,134,119,239,192,2,49,56,205,148,30,254,11,48,34,239,224,4,224,52,128,67,85,159,128,1,5,52,53,113,3,56,205,153,30,197,205,153,30,225,80,89,122,179,200,27,195,181,3,207,10,137,2,208,18,134,137,10,151,96,117,137,18,213,23,31,137,27,144,65,2,137,36,208,83,202,137,46,157,54,177,137,56,255,73,62,137,67,255,106,115,137,79,167,0,84,137,92,0,0,0,137,105,20,246,36,137,118,241,16,5,205,251,36,58,59,92,135,250,138,28,225,208,229,205,241,43,98,107,13,248,9,203,254,201,33,63,5,229,33,128,31,203,127,40,3,33,152,12,8,19,221,43,243,62,2,71,16,254,211,254,238,15,6,164,45,32,245,5,37,242,216,4,6,47,16,254,211,254,62,13,6,55,16,254,211,254,1,14,59,8,111,195,7,5,122,179,40,12,221,110,0,124,173,103,62,1,55,195,37,5,108,24,244,121,203,120,16,254,48,4,6,66,16,254,211,254,6,62,32,239,5,175,60,203,21,194,20,5,27,221,35,6,49,62,127,219,254,31,208,122,60,194,254,4,6,59,16,254,201,245,58,72,92,230,56,15,15,15,211,254,62,127,219,254,31,251,56,2,207,12,241,201,20,8,21,243,62,15,211,254,33,63,5,229,219,254,31,230,32,246,2,79,191,192,205,231,5,48,250,33,21,4,16,254,43,124,181,32,249,205,227,5,48,235,6,156,205,227,5,48,228,62,198,184,48,224,36,32,241,6,201,205,231,5,48,213,120,254,212,48,244,205,231,5,208,121,238,3,79,38,0,6,176,24,31,8,32,7,48,15,221,117,0,24,15,203,17,173,192,121,31,79,19,24,7,221,126,0,173,192,221,35,27,8,6,178,46,1,205,227,5,208,62,203,184,203,21,6,176,210,202,5,124,173,103,122,179,32,202,124,254,1,201,205,231,5,208,62,22,61,32,253,167,4,200,62,127,219,254,31,208,169,230,32,40,243,121,47,79,230,7,246,8,211,254,55,201,241,58,116,92,214,224,50,116,92,205,140,28,205,48,37,40,60,1,17,0,58,116,92,167,40,2,14,34,247,213,221,225,6,11,62,32,18,19,16,252,221,54,1,255,205,241,43,33,246,255,11,9,3,48,15,58,116,92,167,32,2,207,14,120,177,40,10,1,10,0,221,229,225,35,235,237,176,223,254,228,32,73,58,116,92,254,3,202,138,28,231,205,178,40,203,249,48,11,33,0,0,58,116,92,61,40,21,207,1,194,138,28,205,48,37,40,24,35,126,221,119,11,35,126,221,119,12,35,221,113,14,62,1,203,113,40,1,60,221,119,0,235,231,254,41,32,218,231,205,238,27,235,195,90,7,254,170,32,31,58,116,92,254,3,202,138,28,231,205,238,27,221,54,11,0,221,54,12,27,33,0,64,221,117,13,221,116,14,24,77,254,175,32,79,58,116,92,254,3,202,138,28,231,205,72,32,32,12,58,116,92,167,202,138,28,205,230,28,24,15,205,130,28,223,254,44,40,12,58,116,92,167,202,138,28,205,230,28,24,4,231,205,130,28,205,238,27,205,153,30,221,113,11,221,112,12,205,153,30,221,113,13,221,112,14,96,105,221,54,0,3,24,68,254,202,40,9,205,238,27,221,54,14,128,24,23,58,116,92,167,194,138,28,231,205,130,28,205,238,27,205,153,30,221,113,13,221,112,14,221,54,0,0,42,89,92,237,91,83,92,55,237,82,221,117,11,221,116,12,42,75,92,237,82,221,117,15,221,116,16,235,58,116,92,167,202,112,9,229,1,17,0,221,9,221,229,17,17,0,175,55,205,86,5,221,225,48,242,62,254,205,1,22,253,54,82,3,14,128,221,126,0,221,190,239,32,2,14,246,254,4,48,217,17,192,9,197,205,10,12,193,221,229,209,33,240,255,25,6,10,126,60,32,3,121,128,79,19,26,190,35,32,1,12,215,16,246,203,121,32,179,62,13,215,225,221,126,0,254,3,40,12,58,116,92,61,202,8,8,254,2,202,182,8,229,221,110,250,221,102,251,221,94,11,221,86,12,124,181,40,13,237,82,56,38,40,7,221,126,0,254,3,32,29,225,124,181,32,6,221,110,13,221,102,14,229,221,225,58,116,92,254,2,55,32,1,167,62,255,205,86,5,216,207,26,221,94,11,221,86,12,229,124,181,32,6,19,19,19,235,24,12,221,110,250,221,102,251,235,55,237,82,56,9,17,5,0,25,68,77,205,5,31,225,221,126,0,167,40,62,124,181,40,19,43,70,43,78,43,3,3,3,221,34,95,92,205,232,25,221,42,95,92,42,89,92,43,221,78,11,221,70,12,197,3,3,3,221,126,253,245,205,85,22,35,241,119,209,35,115,35,114,35,229,221,225,55,62,255,195,2,8,235,42,89,92,43,221,34,95,92,221,78,11,221,70,12,197,205,229,25,193,229,197,205,85,22,221,42,95,92,35,221,78,15,221,70,16,9,34,75,92,221,102,14,124,230,192,32,10,221,110,13,34,66,92,253,54,10,0,209,221,225,55,62,255,195,2,8,221,78,11,221,70,12,197,3,247,54,128,235,209,229,229,221,225,55,62,255,205,2,8,225,237,91,83,92,126,230,192,32,25,26,19,190,35,32,2,26,190,27,43,48,8,229,235,205,184,25,225,24,236,205,44,9,24,226,126,79,254,128,200,229,42,75,92,126,254,128,40,37,185,40,8,197,205,184,25,193,235,24,240,230,224,254,160,32,18,209,213,229,35,19,26,190,32,6,23,48,247,225,24,3,225,24,224,62,255,209,235,60,55,205,44,9,24,196,32,16,8,34,95,92,235,205,184,25,205,232,25,235,42,95,92,8,8,213,205,184,25,34,95,92,42,83,92,227,197,8,56,7,43,205,85,22,35,24,3,205,85,22,35,193,209,237,83,83,92,237,91,95,92,197,213,235,237,176,225,193,213,205,232,25,209,201,229,62,253,205,1,22,175,17,161,9,205,10,12,253,203,2,238,205,212,21,221,229,17,17,0,175,205,194,4,221,225,6,50,118,16,253,221,94,11,221,86,12,62,255,221,225,195,194,4,128,83,116,97,114,116,32,116,97,112,101,44,32,116,104,101,110,32,112,114,101,115,115,32,97,110,121,32,107,101,121,174,13,80,114,111,103,114,97,109,58,160,13,78,117,109,98,101,114,32,97,114,114,97,121,58,160,13,67,104,97,114,97,99,116,101,114,32,97,114,114,97,121,58,160,13,66,121,116,101,115,58,160,205,3,11,254,32,210,217,10,254,6,56,105,254,24,48,101,33,11,10,95,22,0,25,94,25,229,195,3,11,78,87,16,41,84,83,82,55,80,79,95,94,93,92,91,90,84,83,12,62,34,185,32,17,253,203,1,78,32,9,4,14,2,62,24,184,32,3,5,14,33,195,217,13,58,145,92,245,253,54,87,1,62,32,205,101,11,241,50,145,92,201,253,203,1,78,194,205,14,14,33,205,85,12,5,195,217,13,205,3,11,121,61,61,230,16,24,90,62,63,24,108,17,135,10,50,15,92,24,11,17,109,10,24,3,17,135,10,50,14,92,42,81,92,115,35,114,201,17,244,9,205,128,10,42,14,92,87,125,254,22,218,17,34,32,41,68,74,62,31,145,56,12,198,2,79,253,203,1,78,32,22,62,22,144,218,159,30,60,71,4,253,203,2,70,194,85,12,253,190,49,218,134,12,195,217,13,124,205,3,11,129,61,230,31,200,87,253,203,1,198,62,32,205,59,12,21,32,248,201,205,36,11,253,203,1,78,32,26,253,203,2,70,32,8,237,67,136,92,34,132,92,201,237,67,138,92,237,67,130,92,34,134,92,201,253,113,69,34,128,92,201,253,203,1,78,32,20,237,75,136,92,42,132,92,253,203,2,70,200,237,75,138,92,42,134,92,201,253,78,69,42,128,92,201,254,128,56,61,254,144,48,38,71,205,56,11,205,3,11,17,146,92,24,71,33,146,92,205,62,11,203,24,159,230,15,79,203,24,159,230,240,177,14,4,119,35,13,32,251,201,195,159,59,0,198,21,197,237,75,123,92,24,11,205,16,12,195,3,11,197,237,75,54,92,235,33,59,92,203,134,254,32,32,2,203,198,38,0,111,41,41,41,9,193,235,121,61,62,33,32,14,5,79,253,203,1,78,40,6,213,205,205,14,209,121,185,213,204,85,12,209,197,229,58,145,92,6,255,31,56,1,4,31,31,159,79,62,8,167,253,203,1,78,40,5,253,203,48,206,55,235,8,26,160,174,169,18,8,56,19,20,35,61,32,242,235,37,253,203,1,78,204,219,11,225,193,13,35,201,8,62,32,131,95,8,24,230,124,15,15,15,230,3,246,88,103,237,91,143,92,126,171,162,171,253,203,87,118,40,8,230,199,203,87,32,2,238,56,253,203,87,102,40,8,230,248,203,111,32,2,238,7,119,201,229,38,0,227,24,4,17,149,0,245,205,65,12,56,9,62,32,253,203,1,70,204,59,12,26,230,127,205,59,12,26,19,135,48,245,209,254,72,40,3,254,130,216,122,254,3,216,62,32,213,217,215,217,209,201,245,235,60,203,126,35,40,251,61,32,248,235,241,254,32,216,26,214,65,201,253,203,1,78,192,17,217,13,213,120,253,203,2,70,194,2,13,253,190,49,56,27,192,253,203,2,102,40,22,253,94,45,29,40,90,62,0,205,1,22,237,123,63,92,253,203,2,166,201,207,4,253,53,82,32,69,62,24,144,50,140,92,42,143,92,229,58,145,92,245,62,253,205,1,22,175,17,248,12,205,10,12,253,203,2,238,33,59,92,203,222,203,174,217,205,212,21,217,254,32,40,69,254,226,40,65,246,32,254,110,40,59,62,254,205,1,22,241,50,145,92,225,34,143,92,205,254,13,253,70,49,4,14,33,197,205,155,14,124,15,15,15,230,3,246,88,103,17,224,90,26,78,6,32,235,18,113,19,35,16,250,193,201,128,115,99,114,111,108,108,191,207,12,254,2,56,128,253,134,49,214,25,208,237,68,197,71,42,143,92,229,42,145,92,229,205,77,13,120,245,33,107,92,70,120,60,119,33,137,92,190,56,3,52,6,24,205,0,14,241,61,32,232,225,253,117,87,225,34,143,92,237,75,136,92,253,203,2,134,205,217,13,253,203,2,198,193,201,175,42,141,92,253,203,2,70,40,4,103,253,110,14,34,143,92,33,145,92,32,2,126,15,174,230,85,174,119,201,205,175,13,33,60,92,203,174,203,198,205,77,13,253,70,49,205,68,14,33,192,90,58,141,92,5,24,7,14,32,43,119,13,32,251,16,247,253,54,49,2,62,253,205,1,22,42,81,92,17,244,9,167,115,35,114,35,17,168,16,63,56,246,1,33,23,24,42,33,0,0,34,125,92,253,203,48,134,205,148,13,62,254,205,1,22,205,77,13,6,24,205,68,14,42,81,92,17,244,9,115,35,114,253,54,82,1,1,33,24,33,0,91,253,203,1,78,32,18,120,253,203,2,70,40,5,253,134,49,214,24,197,71,205,155,14,193,62,33,145,95,22,0,25,195,220,10,6,23,205,155,14,14,8,197,229,120,230,7,120,32,12,235,33,224,248,25,235,1,32,0,61,237,176,235,33,224,255,25,235,71,230,7,15,15,15,79,120,6,0,237,176,6,7,9,230,248,32,219,225,36,193,13,32,205,205,136,14,33,224,255,25,235,237,176,6,1,197,205,155,14,14,8,197,229,120,230,7,15,15,15,79,120,6,0,13,84,93,54,0,19,237,176,17,1,7,25,61,230,248,71,32,229,225,36,193,13,32,220,205,136,14,98,107,19,58,141,92,253,203,2,70,40,3,58,72,92,119,11,237,176,193,14,33,201,124,15,15,15,61,246,80,103,235,97,104,41,41,41,41,41,68,77,201,62,24,144,87,15,15,15,230,224,111,122,230,24,246,64,103,201,243,6,176,33,0,64,229,197,205,244,14,193,225,36,124,230,7,32,10,125,198,32,111,63,159,230,248,132,103,16,231,24,13,243,33,0,91,6,8,197,205,244,14,193,16,249,62,4,211,251,251,33,0,91,253,117,70,175,71,119,35,16,252,253,203,48,142,14,33,195,217,13,120,254,3,159,230,2,211,251,87,205,84,31,56,10,62,4,211,251,251,205,223,14,207,12,219,251,135,248,48,235,14,32,94,35,6,8,203,18,203,19,203,26,219,251,31,48,251,122,211,251,16,240,13,32,233,201,42,61,92,229,33,127,16,229,237,115,61,92,205,212,21,245,22,0,253,94,255,33,200,0,205,181,3,241,33,56,15,229,254,24,48,49,254,7,56,45,254,16,56,58,1,2,0,87,254,22,56,12,3,253,203,55,126,202,30,16,205,212,21,95,205,212,21,213,42,91,92,253,203,7,134,205,85,22,193,35,112,35,113,24,10,253,203,7,134,42,91,92,205,82,22,18,19,237,83,91,92,201,95,22,0,33,153,15,25,94,25,229,42,91,92,201,9,102,106,80,181,112,126,207,212,42,73,92,253,203,55,110,194,151,16,205,110,25,205,149,22,122,179,202,151,16,229,35,78,35,70,33,10,0,9,68,77,205,5,31,205,151,16,42,81,92,227,229,62,255,205,1,22,225,43,253,53,15,205,85,24,253,52,15,42,89,92,35,35,35,35,34,91,92,225,205,21,22,201,253,203,55,110,32,8,33,73,92,205,15,25,24,109,253,54,0,16,24,29,205,49,16,24,5,126,254,13,200,35,34,91,92,201,205,49,16,1,1,0,195,232,25,205,212,21,205,212,21,225,225,225,34,61,92,253,203,0,126,192,249,201,55,205,149,17,237,82,25,35,193,216,197,68,77,98,107,35,26,230,240,254,16,32,9,35,26,214,23,206,0,32,1,35,167,237,66,9,235,56,230,201,253,203,55,110,192,42,73,92,205,110,25,235,205,149,22,33,74,92,205,28,25,205,149,23,62,0,195,1,22,253,203,55,126,40,168,195,129,15,253,203,48,102,40,161,253,54,0,255,22,0,253,94,254,33,144,26,205,181,3,195,48,15,229,205,144,17,43,205,229,25,34,91,92,253,54,7,0,225,201,253,203,2,94,196,29,17,167,253,203,1,110,200,58,8,92,253,203,1,174,245,253,203,2,110,196,110,13,241,254,32,48,82,254,16,48,45,254,6,48,10,71,230,1,79,120,31,198,18,24,42,32,9,33,106,92,62,8,174,119,24,14,254,14,216,214,13,33,65,92,190,119,32,2,54,0,253,203,2,222,191,201,71,230,7,79,62,16,203,88,32,1,60,253,113,211,17,13,17,24,6,58,13,92,17,168,16,42,79,92,35,35,115,35,114,55,201,205,77,13,253,203,2,158,253,203,2,174,42,138,92,229,42,61,92,229,33,103,17,229,237,115,61,92,42,130,92,229,55,205,149,17,235,205,125,24,235,205,225,24,42,138,92,227,235,205,77,13,58,139,92,146,56,38,32,6,123,253,150,80,48,30,62,32,213,205,244,9,209,24,233,22,0,253,94,254,33,144,26,205,181,3,253,54,0,255,237,91,138,92,24,2,209,225,225,34,61,92,193,213,205,217,13,225,34,130,92,253,54,38,0,201,42,97,92,43,167,237,91,89,92,253,203,55,110,200,237,91,97,92,216,42,99,92,201,126,254,14,1,6,0,204,232,25,126,35,254,13,32,241,201,243,62,255,237,91,178,92,217,237,75,180,92,237,91,56,92,42,123,92,217,71,62,7,211,254,62,63,237,71,0,0,0,0,0,0,98,107,54,2,43,188,32,250,167,237,82,25,35,48,6,53,40,3,53,40,243,43,217,237,67,180,92,237,83,56,92,34,123,92,217,4,40,25,34,180,92,17,175,62,1,168,0,235,237,184,235,35,34,123,92,43,1,64,0,237,67,56,92,34,178,92,33,0,60,34,54,92,42,178,92,54,62,43,249,43,43,34,61,92,237,86,253,33,58,92,251,33,182,92,34,79,92,17,175,21,1,21,0,235,237,176,235,43,34,87,92,35,34,83,92,34,75,92,54,128,35,34,89,92,54,13,35,54,128,35,34,97,92,34,99,92,34,101,92,62,56,50,141,92,50,143,92,50,72,92,33,35,5,34,9,92,253,53,198,253,53,202,33,198,21,17,16,92,1,14,0,237,176,253,203,1,206,205,223,14,253,54,49,2,205,107,13,175,17,56,21,205,10,12,253,203,2,238,24,7,253,54,49,2,205,149,23,205,176,22,62,0,205,1,22,205,44,15,205,23,27,253,203,0,126,32,18,253,203,48,102,40,64,42,89,92,205,167,17,253,54,0,255,24,221,42,89,92,34,93,92,205,251,25,120,177,194,93,21,223,254,13,40,192,253,203,48,70,196,175,13,205,110,13,62,25,253,150,79,50,140,92,253,203,1,254,253,54,0,255,253,54,10,1,205,138,27,118,253,203,1,174,253,203,48,78,196,205,14,58,58,92,60,245,33,0,0,253,116,55,253,116,38,34,11,92,33,1,0,34,22,92,205,176,22,253,203,55,174,205,110,13,253,203,2,238,241,71,254,10,56,2,198,7,205,239,21,62,32,215,120,17,145,19,205,10,12,205,59,59,0,205,10,12,237,75,69,92,205,27,26,62,58,215,253,78,13,6,0,205,27,26,205,151,16,58,58,92,60,40,27,254,9,40,4,254,21,32,3,253,52,13,1,3,0,17,112,92,33,68,92,203,126,40,1,9,237,184,253,54,10,255,253,203,1,158,195,172,18,128,79,203,78,69,88,84,32,119,105,116,104,111,117,116,32,70,79,210,86,97,114,105,97,98,108,101,32,110,111,116,32,102,111,117,110,228,83,117,98,115,99,114,105,112,116,32,119,114,111,110,231,79,117,116,32,111,102,32,109,101,109,111,114,249,79,117,116,32,111,102,32,115,99,114,101,101,238,78,117,109,98,101,114,32,116,111,111,32,98,105,231,82,69,84,85,82,78,32,119,105,116,104,111,117,116,32,71,79,83,85,194,69,110,100,32,111,102,32,102,105,108,229,83,84,79,80,32,115,116,97,116,101,109,101,110,244,73,110,118,97,108,105,100,32,97,114,103,117,109,101,110,244,73,110,116,101,103,101,114,32,111,117,116,32,111,102,32,114,97,110,103,229,78,111,110,115,101,110,115,101,32,105,110,32,66,65,83,73,195,66,82,69,65,75,32,45,32,67,79,78,84,32,114,101,112,101,97,116,243,79,117,116,32,111,102,32,68,65,84,193,73,110,118,97,108,105,100,32,102,105,108,101,32,110,97,109,229,78,111,32,114,111,111,109,32,102,111,114,32,108,105,110,229,83,84,79,80,32,105,110,32,73,78,80,85,212,70,79,82,32,119,105,116,104,111,117,116,32,78,69,88,212,73,110,118,97,108,105,100,32,73,47,79,32,100,101,118,105,99,229,73,110,118,97,108,105,100,32,99,111,108,111,117,242,66,82,69,65,75,32,105,110,116,111,32,112,114,111,103,114,97,237,82,65,77,84,79,80,32,110,111,32,103,111,111,228,83,116,97,116,101,109,101,110,116,32,108,111,115,244,73,110,118,97,108,105,100,32,115,116,114,101,97,237,70,78,32,119,105,116,104,111,117,116,32,68,69,198,80,97,114,97,109,101,116,101,114,32,101,114,114,111,242,84,97,112,101,32,108,111,97,100,105,110,103,32,101,114,114,111,242,44,160,127,32,49,57,56,50,32,83,105,110,99,108,97,105,114,32,82,101,115,101,97,114,99,104,32,76,116,228,62,16,1,0,0,195,19,19,237,67,73,92,42,93,92,235,33,85,21,229,42,97,92,55,237,82,229,96,105,205,110,25,32,6,205,184,25,205,232,25,193,121,61,176,40,40,197,3,3,3,3,43,237,91,83,92,213,205,85,22,225,34,83,92,193,197,19,42,97,92,43,43,237,184,42,73,92,235,193,112,43,113,43,115,43,114,241,195,162,18,244,9,168,16,75,244,9,196,21,83,129,15,196,21,82,244,9,196,21,80,128,207,18,1,0,6,0,11,0,1,0,1,0,6,0,16,0,253,203,2,110,32,4,253,203,2,222,205,230,21,216,40,250,207,7,217,229,42,81,92,35,35,24,8,30,48,131,217,229,42,81,92,94,35,86,235,205,44,22,225,217,201,135,198,22,111,38,92,94,35,86,122,179,32,2,207,23,27,42,79,92,25,34,81,92,253,203,48,166,35,35,35,35,78,33,45,22,205,220,22,208,22,0,94,25,233,75,6,83,18,80,27,0,253,203,2,198,253,203,1,174,253,203,48,230,24,4,253,203,2,134,253,203,1,142,195,77,13,253,203,1,206,201,1,1,0,229,205,5,31,225,205,100,22,42,101,92,235,237,184,201,245,229,33,75,92,62,14,94,35,86,227,167,237,82,25,227,48,9,213,235,9,235,114,43,115,35,209,35,61,32,232,235,209,241,167,237,82,68,77,3,25,235,201,0,0,235,17,143,22,126,230,192,32,247,86,35,94,201,42,99,92,43,205,85,22,35,35,193,237,67,97,92,193,235,35,201,42,89,92,54,13,34,91,92,35,54,128,35,34,97,92,42,97,92,34,99,92,42,99,92,34,101,92,229,33,146,92,34,104,92,225,201,237,91,89,92,195,229,25,35,126,167,200,185,35,32,248,55,201,205,30,23,205,1,23,1,0,0,17,226,163,235,25,56,7,1,212,21,9,78,35,70,235,113,35,112,201,229,42,79,92,9,35,35,35,78,235,33,22,23,205,220,22,78,6,0,9,233,75,5,83,3,80,1,225,201,205,148,30,254,16,56,2,207,23,198,3,7,33,16,92,79,6,0,9,78,35,70,43,201,239,1,56,205,30,23,120,177,40,22,235,42,79,92,9,35,35,35,126,235,254,75,40,8,254,83,40,4,254,80,32,207,205,93,23,115,35,114,201,229,205,241,43,120,177,32,2,207,14,197,26,230,223,79,33,122,23,205,220,22,48,241,78,6,0,9,193,233,75,6,83,8,80,10,0,30,1,24,6,30,6,24,2,30,16,11,120,177,32,213,87,225,201,24,144,237,115,63,92,253,54,2,16,205,175,13,253,203,2,198,253,70,49,205,68,14,253,203,2,134,253,203,48,198,42,73,92,237,91,108,92,167,237,82,25,56,34,213,205,110,25,17,192,2,235,237,82,227,205,110,25,193,197,205,184,25,193,9,56,14,235,86,35,94,43,237,83,108,92,24,237,34,108,92,42,108,92,205,110,25,40,1,235,205,51,24,253,203,2,166,201,62,3,24,2,62,2,253,54,2,0,205,48,37,196,1,22,223,205,112,32,56,20,223,254,59,40,4,254,44,32,6,231,205,130,28,24,8,205,230,28,24,3,205,222,28,205,238,27,205,153,30,120,230,63,103,105,34,73,92,205,110,25,30,1,205,85,24,215,253,203,2,102,40,246,58,107,92,253,150,79,32,238,171,200,229,213,33,108,92,205,15,25,209,225,24,224,237,75,73,92,205,128,25,22,62,40,5,17,0,0,203,19,253,115,45,126,254,64,193,208,197,205,40,26,35,35,35,253,203,1,134,122,167,40,5,215,253,203,1,198,213,235,253,203,48,150,33,59,92,203,150,253,203,55,110,40,2,203,214,42,95,92,167,237,82,32,5,62,63,205,193,24,205,225,24,235,126,205,182,24,35,254,13,40,6,235,205,55,25,24,224,209,201,254,14,192,35,35,35,35,35,35,126,201,217,42,143,92,229,203,188,203,253,34,143,92,33,145,92,86,213,54,0,205,244,9,225,253,116,87,225,34,143,92,217,201,42,91,92,167,237,82,192,58,65,92,203,7,40,4,198,67,24,22,33,59,92,203,158,62,75,203,86,40,11,203,222,60,253,203,48,94,40,2,62,67,213,205,193,24,209,201,94,35,86,229,235,35,205,110,25,205,149,22,225,253,203,55,110,192,114,43,115,201,123,167,248,24,13,175,9,60,56,252,237,66,61,40,241,195,239,21,205,27,45,48,48,254,33,56,44,253,203,1,150,254,203,40,36,254,58,32,14,253,203,55,110,32,22,253,203,48,86,40,20,24,14,254,34,32,10,245,58,106,92,238,4,50,106,92,241,253,203,1,214,215,201,229,42,83,92,84,93,193,205,128,25,208,197,205,184,25,235,24,244,126,184,192,35,126,43,185,201,35,35,35,34,93,92,14,0,21,200,231,187,32,4,167,201,35,126,205,182,24,34,93,92,254,34,32,1,13,254,58,40,4,254,203,32,4,203,65,40,223,254,13,32,227,21,55,201,229,126,254,64,56,23,203,111,40,20,135,250,199,25,63,1,5,0,48,2,14,18,23,35,126,48,251,24,6,35,35,78,35,70,35,9,209,167,237,82,68,77,25,235,201,205,221,25,197,120,47,71,121,47,79,3,205,100,22,235,225,25,213,237,176,225,201,42,89,92,43,34,93,92,231,33,146,92,34,101,92,205,59,45,205,162,45,56,4,33,240,216,9,218,138,28,195,197,22,213,229,175,203,120,32,32,96,105,30,255,24,8,213,86,35,94,229,235,30,32,1,24,252,205,42,25,1,156,255,205,42,25,14,246,205,42,25,125,205,239,21,225,209,201,177,203,188,191,196,175,180,147,145,146,149,152,152,152,152,152,152,152,127,129,46,108,110,112,72,148,86,63,65,43,23,31,55,119,68,15,89,43,67,45,81,58,109,66,13,73,92,68,21,93,1,61,2,6,0,103,30,6,203,5,240,28,6,0,237,30,0,238,28,0,35,31,4,61,6,204,6,5,3,29,4,0,171,29,5,205,31,5,137,32,5,2,44,5,178,27,0,183,17,3,161,30,5,249,23,8,0,128,30,3,79,30,0,95,30,3,172,30,0,107,13,9,0,220,34,6,0,58,31,5,237,29,5,39,30,3,66,30,9,5,130,35,0,172,14,5,201,31,5,245,23,11,11,11,11,8,0,248,3,9,5,32,35,7,7,7,7,7,7,8,0,122,30,6,0,148,34,5,96,31,6,44,10,0,54,23,6,0,229,22,10,0,147,23,10,44,10,0,147,23,10,0,147,23,0,147,23,253,203,1,190,205,251,25,175,50,71,92,61,50,58,92,24,1,231,205,191,22,253,52,13,250,138,28,223,6,0,254,13,40,122,254,58,40,235,33,118,27,229,79,231,121,214,206,218,138,28,79,33,72,26,9,78,9,24,3,42,116,92,126,35,34,116,92,1,82,27,197,79,254,32,48,12,33,1,28,6,0,9,78,9,229,223,5,201,223,185,194,138,28,231,201,205,84,31,56,2,207,20,205,77,59,0,32,113,42,66,92,203,124,40,20,33,254,255,34,69,92,42,97,92,43,237,91,89,92,27,58,68,92,24,51,205,110,25,58,68,92,40,25,167,32,67,71,126,230,192,120,40,15,207,255,193,205,48,37,200,42,85,92,62,192,166,192,175,254,1,206,0,86,35,94,237,83,69,92,35,94,35,86,235,25,35,34,85,92,235,34,93,92,87,30,0,253,54,10,255,21,253,114,13,202,40,27,20,205,139,25,40,8,207,22,205,48,37,192,193,193,205,93,59,40,186,254,58,202,40,27,195,138,28,15,29,75,9,103,11,123,142,113,180,129,207,205,222,28,191,193,204,238,27,235,42,116,92,78,35,70,235,197,201,205,178,40,253,54,55,0,48,8,253,203,55,206,32,24,207,1,204,150,41,253,203,1,118,32,13,175,205,48,37,196,241,43,33,113,92,182,119,235,237,67,114,92,34,77,92,201,193,205,86,28,205,238,27,201,58,59,92,245,205,251,36,241,253,86,1,170,230,64,32,36,203,122,194,255,42,201,205,178,40,245,121,246,159,60,32,20,241,24,169,231,205,130,28,254,44,32,9,231,205,251,36,253,203,1,118,192,207,11,205,251,36,253,203,1,118,200,24,244,253,203,1,126,253,203,2,134,196,77,13,241,58,116,92,214,19,205,252,33,205,238,27,42,143,92,34,141,92,33,145,92,126,7,174,230,170,174,119,201,205,48,37,40,19,253,203,2,134,205,77,13,33,144,92,126,246,248,119,253,203,87,182,223,205,226,33,24,159,195,5,6,254,13,40,4,254,58,32,156,205,48,37,200,239,160,56,201,207,8,193,205,48,37,40,10,239,2,56,235,205,233,52,218,179,27,195,41,27,254,205,32,9,231,205,130,28,205,238,27,24,6,205,238,27,239,161,56,239,192,2,1,224,1,56,205,255,42,34,104,92,43,126,203,254,1,6,0,9,7,56,6,14,13,205,85,22,35,229,239,2,2,56,225,235,14,10,237,176,42,69,92,235,115,35,114,253,86,13,20,35,114,205,218,29,208,253,70,56,42,69,92,34,66,92,58,71,92,237,68,87,42,93,92,30,243,197,237,75,85,92,205,134,29,237,67,85,92,193,56,17,231,246,32,184,40,3,231,24,232,231,62,1,146,50,68,92,201,207,17,126,254,58,40,24,35,126,230,192,55,192,70,35,78,237,67,66,92,35,78,35,70,229,9,68,77,225,22,0,197,205,139,25,193,208,24,224,253,203,55,78,194,46,28,42,77,92,203,126,40,31,35,34,104,92,239,224,226,15,192,2,56,205,218,29,216,42,104,92,17,15,0,25,94,35,86,35,102,235,195,115,30,207,0,239,225,224,226,54,0,2,1,3,55,0,4,56,167,201,56,55,201,231,205,31,28,205,48,37,40,41,223,34,95,92,42,87,92,126,254,44,40,9,30,228,205,134,29,48,2,207,13,205,119,0,205,86,28,223,34,87,92,42,95,92,253,54,38,0,205,120,0,223,254,44,40,201,205,238,27,201,205,48,37,32,11,205,251,36,254,44,196,238,27,231,24,245,62,228,71,237,185,17,0,2,195,139,25,205,153,30,96,105,205,110,25,43,34,87,92,201,205,153,30,120,177,32,4,237,75,120,92,237,67,118,92,201,42,110,92,253,86,54,24,12,205,153,30,96,105,22,0,124,254,240,48,44,34,66,92,253,114,10,201,205,133,30,237,121,201,205,133,30,2,201,205,213,45,56,21,40,2,237,68,245,205,153,30,241,201,205,213,45,24,3,205,162,45,56,1,200,207,10,205,103,30,1,0,0,205,69,30,24,3,205,153,30,120,177,32,4,237,75,178,92,197,237,91,75,92,42,89,92,43,205,229,25,205,107,13,42,101,92,17,50,0,25,209,237,82,48,8,42,180,92,167,237,82,48,2,207,21,235,34,178,92,209,193,54,62,43,249,197,237,115,61,92,235,233,209,253,102,13,36,227,51,237,75,69,92,197,229,237,115,61,92,213,205,103,30,1,20,0,42,101,92,9,56,10,235,33,80,0,25,56,3,237,114,216,46,3,195,85,0,1,0,0,205,5,31,68,77,201,193,225,209,122,254,62,40,11,59,227,235,237,115,61,92,197,195,115,30,213,229,207,6,205,153,30,118,11,120,177,40,12,120,161,60,32,1,3,253,203,1,110,40,238,253,203,1,174,201,62,127,219,254,31,216,62,254,219,254,31,201,205,48,37,40,5,62,206,195,57,30,253,203,1,246,205,141,44,48,22,231,254,36,32,5,253,203,1,182,231,254,40,32,60,231,254,41,40,32,205,141,44,210,138,28,235,231,254,36,32,2,235,231,235,1,6,0,205,85,22,35,35,54,14,254,44,32,3,231,24,224,254,41,32,19,231,254,61,32,14,231,58,59,92,245,205,251,36,241,253,174,1,230,64,194,138,28,205,238,27,205,48,37,225,200,233,62,3,24,2,62,2,205,48,37,196,1,22,205,77,13,205,223,31,205,238,27,201,223,205,69,32,40,13,205,78,32,40,251,205,252,31,205,78,32,40,243,254,41,200,205,195,31,62,13,215,201,223,254,172,32,13,205,121,28,205,195,31,205,7,35,62,22,24,16,254,173,32,18,231,205,130,28,205,195,31,205,153,30,62,23,215,121,215,120,215,201,205,242,33,208,205,112,32,208,205,251,36,205,195,31,253,203,1,118,204,241,43,194,227,45,120,177,11,200,26,19,215,24,247,254,41,200,254,13,200,254,58,201,223,254,59,40,20,254,44,32,10,205,48,37,40,11,62,6,215,24,6,254,39,192,205,245,31,231,205,69,32,32,1,193,191,201,254,35,55,192,231,205,130,28,167,205,195,31,205,148,30,254,16,210,14,22,205,1,22,167,201,205,48,37,40,8,62,1,205,1,22,205,110,13,253,54,2,1,205,193,32,205,238,27,237,75,136,92,58,107,92,184,56,3,14,33,71,237,67,136,92,62,25,144,50,140,92,253,203,2,134,205,217,13,195,110,13,205,78,32,40,251,254,40,32,14,231,205,223,31,223,254,41,194,138,28,231,195,178,33,254,202,32,17,231,205,31,28,253,203,55,254,253,203,1,118,194,138,28,24,13,205,141,44,210,175,33,205,31,28,253,203,55,190,205,48,37,202,178,33,205,191,22,33,113,92,203,182,203,238,1,1,0,203,126,32,11,58,59,92,230,64,32,2,14,3,182,119,247,54,13,121,15,15,48,5,62,34,18,43,119,34,91,92,253,203,55,126,32,44,42,93,92,229,42,61,92,229,33,58,33,229,253,203,48,102,40,4,237,115,61,92,42,97,92,205,167,17,253,54,0,255,205,44,15,253,203,1,190,205,185,33,24,3,205,44,15,253,54,34,0,205,214,33,32,10,205,29,17,237,75,130,92,205,217,13,33,113,92,203,174,203,126,203,190,32,28,225,225,34,61,92,225,34,95,92,253,203,1,254,205,185,33,42,95,92,253,54,38,0,34,93,92,24,23,42,99,92,237,91,97,92,55,237,82,68,77,205,178,42,205,255,42,24,3,205,252,31,205,78,32,202,193,32,201,42,97,92,34,93,92,223,254,226,40,12,58,113,92,205,89,28,223,254,13,200,207,11,205,48,37,200,207,16,42,81,92,35,35,35,35,126,254,75,201,231,205,242,33,216,223,254,44,40,246,254,59,40,242,195,138,28,254,217,216,254,223,63,216,245,231,241,214,201,245,205,130,28,241,167,205,195,31,245,205,148,30,87,241,215,122,215,201,214,17,206,0,40,29,214,2,206,0,40,86,254,1,122,6,1,32,4,7,7,6,4,79,122,254,2,48,22,121,33,145,92,24,56,122,6,7,56,5,7,7,7,6,56,79,122,254,10,56,2,207,19,33,143,92,254,8,56,11,126,40,7,176,47,230,36,40,1,120,79,121,205,108,34,62,7,186,159,205,108,34,7,7,230,80,71,62,8,186,159,174,160,174,119,35,120,201,159,122,15,6,128,32,3,15,6,64,79,122,254,8,40,4,254,2,48,189,121,33,143,92,205,108,34,121,15,15,15,24,216,205,148,30,254,8,48,169,211,254,7,7,7,203,111,32,2,238,7,50,72,92,201,62,175,144,218,249,36,71,167,31,55,31,167,31,168,230,248,168,103,121,7,7,7,168,230,199,168,7,7,111,121,230,7,201,205,7,35,205,170,34,71,4,126,7,16,253,230,1,195,40,45,205,7,35,205,229,34,195,77,13,237,67,125,92,205,170,34,71,4,62,254,15,16,253,71,126,253,78,87,203,65,32,1,160,203,81,32,2,168,47,119,195,219,11,205,20,35,71,197,205,20,35,89,193,81,79,201,205,213,45,218,249,36,14,1,200,14,255,201,223,254,44,194,138,28,231,205,130,28,205,238,27,239,42,61,56,126,254,129,48,5,239,2,56,24,161,239,163,56,54,131,239,197,2,56,205,125,36,197,239,49,225,4,56,126,254,128,48,8,239,2,2,56,193,195,220,34,239,194,1,192,2,3,1,224,15,192,1,49,224,1,49,224,160,193,2,56,253,52,98,205,148,30,111,229,205,148,30,225,103,34,125,92,193,195,32,36,223,254,44,40,6,205,238,27,195,119,36,231,205,130,28,205,238,27,239,197,162,4,31,49,48,48,0,6,2,56,195,119,36,192,2,193,2,49,42,225,1,225,42,15,224,5,42,224,1,61,56,126,254,129,48,7,239,2,2,56,195,119,36,205,125,36,197,239,2,225,1,5,193,2,1,49,225,4,194,2,1,49,225,4,226,229,224,3,162,4,49,31,197,2,32,192,2,194,2,193,229,4,224,226,4,15,225,1,193,2,224,4,226,229,4,3,194,42,225,42,15,2,56,26,254,129,193,218,119,36,197,239,1,56,58,125,92,205,40,45,239,192,15,1,56,58,126,92,205,40,45,239,197,15,224,229,56,193,5,40,60,24,20,239,225,49,227,4,226,228,4,3,193,2,228,4,226,227,4,15,194,2,56,197,239,192,2,225,15,49,56,58,125,92,205,40,45,239,3,224,226,15,192,1,224,56,58,126,92,205,40,45,239,3,56,205,183,36,193,16,198,239,2,2,1,56,58,125,92,205,40,45,239,3,1,56,58,126,92,205,40,45,239,3,56,205,183,36,195,77,13,239,49,40,52,50,0,1,5,229,1,5,42,56,205,213,45,56,6,230,252,198,4,48,2,62,252,245,205,40,45,239,229,1,5,49,31,196,2,49,162,4,31,193,1,192,2,49,4,49,15,161,3,27,195,2,56,193,201,205,7,35,121,184,48,6,105,213,175,95,24,7,177,200,104,65,213,22,0,96,120,31,133,56,3,188,56,7,148,79,217,193,197,24,4,79,213,217,193,42,125,92,120,132,71,121,60,133,56,13,40,13,61,79,205,229,34,217,121,16,217,209,201,40,243,207,10,223,6,0,197,79,33,150,37,205,220,22,121,210,132,38,6,0,78,9,233,205,116,0,3,254,13,202,138,28,254,34,32,243,205,116,0,254,34,201,231,254,40,32,6,205,121,28,223,254,41,194,138,28,253,203,1,126,201,205,7,35,42,54,92,17,0,1,25,121,15,15,15,230,224,168,95,121,230,24,238,64,87,6,96,197,213,229,26,174,40,4,60,32,26,61,79,6,7,20,35,26,174,169,32,15,16,247,193,193,193,62,128,144,1,1,0,247,18,24,10,225,17,8,0,25,209,193,16,211,72,195,178,42,205,7,35,121,15,15,15,79,230,224,168,111,121,230,3,238,88,103,126,195,40,45,34,28,40,79,46,242,43,18,168,86,165,87,167,132,166,143,196,230,170,191,171,199,169,206,0,231,195,255,36,223,35,229,1,0,0,205,15,37,32,27,205,15,37,40,251,205,48,37,40,17,247,225,213,126,35,18,19,254,34,32,248,126,35,254,34,40,242,11,209,33,59,92,203,182,203,126,196,178,42,195,18,39,231,205,251,36,254,41,194,138,28,231,195,18,39,195,189,39,205,48,37,40,40,237,75,118,92,205,43,45,239,161,15,52,55,22,4,52,128,65,0,0,128,50,2,161,3,49,56,205,162,45,237,67,118,92,126,167,40,3,214,16,119,24,9,205,48,37,40,4,239,163,56,52,231,195,195,38,1,90,16,231,254,35,202,13,39,33,59,92,203,182,203,126,40,31,195,108,59,14,0,32,19,205,30,3,48,14,21,95,205,51,3,245,1,1,0,247,241,18,14,1,6,0,205,178,42,195,18,39,205,34,37,196,53,37,231,195,219,37,205,34,37,196,128,37,231,24,72,205,34,37,196,203,34,231,24,63,205,136,44,48,86,254,65,48,60,205,48,37,32,35,205,155,44,223,1,6,0,205,85,22,35,54,14,35,235,42,101,92,14,5,167,237,66,34,101,92,237,176,235,43,205,119,0,24,14,223,35,126,254,14,32,250,35,205,180,51,34,93,92,253,203,1,246,24,20,205,178,40,218,46,28,204,150,41,58,59,92,254,192,56,4,35,205,180,51,24,51,1,219,9,254,45,40,39,1,24,16,254,174,40,32,214,175,218,138,28,1,240,4,254,20,40,20,210,138,28,6,16,198,220,79,254,223,48,2,203,177,254,238,56,2,203,185,197,231,195,255,36,223,254,40,32,12,253,203,1,118,32,23,205,82,42,231,24,240,6,0,79,33,149,39,205,220,22,48,6,78,33,237,38,9,70,209,122,184,56,58,167,202,24,0,197,33,59,92,123,254,237,32,6,203,118,32,2,30,153,213,205,48,37,40,9,123,230,63,71,239,59,56,24,9,123,253,174,1,230,64,194,138,28,209,33,59,92,203,246,203,123,32,2,203,182,193,24,193,213,121,253,203,1,118,32,21,230,63,198,8,79,254,16,32,4,203,241,24,8,56,215,254,23,40,2,203,249,197,231,195,255,36,43,207,45,195,42,196,47,197,94,198,61,206,62,204,60,205,199,201,200,202,201,203,197,199,198,200,0,6,8,8,10,2,3,5,5,5,5,5,5,6,205,48,37,32,53,231,205,141,44,210,138,28,231,254,36,245,32,1,231,254,40,32,18,231,254,41,40,16,205,251,36,223,254,44,32,3,231,24,245,254,41,194,138,28,231,33,59,92,203,182,241,40,2,203,246,195,18,39,231,230,223,71,231,214,36,79,32,1,231,231,229,42,83,92,43,17,206,0,197,205,134,29,193,48,2,207,24,229,205,171,40,230,223,184,32,8,205,171,40,214,36,185,40,12,225,43,17,0,2,197,205,139,25,193,24,215,167,204,171,40,209,209,237,83,93,92,205,171,40,229,254,41,40,66,35,126,254,14,22,64,40,7,43,205,171,40,35,22,0,35,229,213,205,251,36,241,253,174,1,230,64,32,43,225,235,42,101,92,1,5,0,237,66,34,101,92,237,176,235,43,205,171,40,254,41,40,13,229,223,254,44,32,13,231,225,205,171,40,24,190,229,223,254,41,40,2,207,25,209,235,34,93,92,42,11,92,227,34,11,92,213,231,231,205,251,36,225,34,93,92,225,34,11,92,231,195,18,39,35,126,254,33,56,250,201,253,203,1,246,223,205,141,44,210,138,28,229,230,31,79,231,229,254,40,40,40,203,241,254,36,40,17,203,233,205,136,44,48,15,205,136,44,48,22,203,177,231,24,246,231,253,203,1,182,58,12,92,167,40,6,205,48,37,194,81,41,65,205,48,37,32,8,121,230,224,203,255,79,24,55,42,75,92,126,230,127,40,45,185,32,34,23,135,242,63,41,56,48,209,213,229,35,26,19,254,32,40,250,246,32,190,40,244,246,128,190,32,6,26,205,136,44,48,21,225,197,205,184,25,235,193,24,206,203,248,209,223,254,40,40,9,203,232,24,13,209,209,209,229,223,205,136,44,48,3,231,24,248,225,203,16,203,112,201,42,11,92,126,254,41,202,239,40,126,246,96,71,35,126,254,14,40,7,43,205,171,40,35,203,168,120,185,40,18,35,35,35,35,35,205,171,40,254,41,202,239,40,205,171,40,24,217,203,105,32,12,35,237,91,101,92,205,192,51,235,34,101,92,209,209,175,60,201,175,71,203,121,32,75,203,126,32,14,60,35,78,35,70,35,235,205,178,42,223,195,73,42,35,35,35,70,203,113,40,10,5,40,232,235,223,254,40,32,97,235,235,24,36,229,223,225,254,44,40,32,203,121,40,82,203,113,32,6,254,41,32,60,231,201,254,41,40,108,254,204,32,50,223,43,34,93,92,24,94,33,0,0,229,231,225,121,254,192,32,9,223,254,41,40,81,254,204,40,229,197,229,205,238,42,227,235,205,204,42,56,25,11,205,244,42,9,209,193,16,179,203,121,32,102,229,203,113,32,19,66,75,223,254,41,40,2,207,2,231,225,17,5,0,205,244,42,9,201,205,238,42,227,205,244,42,193,9,35,66,75,235,205,177,42,223,254,41,40,7,254,44,32,219,205,82,42,231,254,40,40,248,253,203,1,182,201,205,48,37,196,241,43,231,254,41,40,80,213,175,245,197,17,1,0,223,225,254,204,40,23,241,205,205,42,245,80,89,229,223,225,254,204,40,9,254,41,194,138,28,98,107,24,19,229,231,225,254,41,40,12,241,205,205,42,245,223,96,105,254,41,32,230,241,227,25,43,227,167,237,82,1,0,0,56,7,35,167,250,32,42,68,77,209,253,203,1,182,205,48,37,200,175,253,203,1,182,197,205,169,51,193,42,101,92,119,35,115,35,114,35,113,35,112,35,34,101,92,201,175,213,229,245,205,130,28,241,205,48,37,40,18,245,205,153,30,209,120,177,55,40,5,225,229,167,237,66,122,222,0,225,209,201,235,35,94,35,86,201,205,48,37,200,205,169,48,218,21,31,201,42,77,92,253,203,55,78,40,94,1,5,0,3,35,126,254,32,40,250,48,11,254,16,56,17,254,22,48,13,35,24,237,205,136,44,56,231,254,36,202,192,43,121,42,89,92,43,205,85,22,35,35,235,213,42,77,92,27,214,6,71,40,17,35,126,254,33,56,250,246,32,19,18,16,244,246,128,18,62,192,42,77,92,174,246,32,225,205,234,43,229,239,2,56,225,1,5,0,167,237,66,24,64,253,203,1,118,40,6,17,6,0,25,24,231,42,77,92,237,75,114,92,253,203,55,70,32,48,120,177,200,229,247,213,197,84,93,35,54,32,237,184,229,205,241,43,225,227,167,237,66,9,48,2,68,77,227,235,120,177,40,2,237,176,193,209,225,235,120,177,200,213,237,176,225,201,43,43,43,126,229,197,205,198,43,193,225,3,3,3,195,232,25,62,223,42,77,92,166,245,205,241,43,235,9,197,43,34,77,92,3,3,3,42,89,92,43,205,85,22,42,77,92,193,197,3,237,184,235,35,193,112,43,113,241,43,119,42,89,92,43,201,42,101,92,43,70,43,78,43,86,43,94,43,126,34,101,92,201,205,178,40,194,138,28,205,48,37,32,8,203,177,205,150,41,205,238,27,56,8,197,205,184,25,205,232,25,193,203,249,6,0,197,33,1,0,203,113,32,2,46,5,235,231,38,255,205,204,42,218,32,42,225,197,36,229,96,105,205,244,42,235,223,254,44,40,232,254,41,32,187,231,193,121,104,38,0,35,35,41,25,218,21,31,213,197,229,68,77,42,89,92,43,205,85,22,35,119,193,11,11,11,35,113,35,112,193,120,35,119,98,107,27,54,0,203,113,40,2,54,32,193,237,184,193,112,43,113,43,61,32,248,201,205,27,45,63,216,254,65,63,208,254,91,216,254,97,63,208,254,123,201,254,196,32,25,17,0,0,231,214,49,206,0,32,10,235,63,237,106,218,173,49,235,24,239,66,75,195,43,45,254,46,40,15,205,59,45,254,46,32,40,231,205,27,45,56,34,24,10,231,205,27,45,218,138,28,239,160,56,239,161,192,2,56,223,205,34,45,56,11,239,224,164,5,192,4,15,56,231,24,239,254,69,40,3,254,101,192,6,255,231,254,43,40,5,254,45,32,2,4,231,205,27,45,56,203,197,205,59,45,205,213,45,193,218,173,49,167,250,173,49,4,40,2,237,68,195,79,45,254,48,216,254,58,63,201,205,27,45,216,214,48,79,6,0,253,33,58,92,175,95,81,72,71,205,182,42,239,56,167,201,245,239,160,56,241,205,34,45,216,239,1,164,4,15,56,205,116,0,24,241,7,15,48,2,47,60,245,33,146,92,205,11,53,239,164,56,241,203,63,48,13,245,239,193,224,0,4,4,51,2,5,225,56,241,40,8,245,239,49,4,56,241,24,229,239,2,56,201,35,78,35,126,169,145,95,35,126,137,169,87,201,14,0,229,54,0,35,113,35,123,169,145,119,35,122,137,169,119,35,54,0,225,201,239,56,126,167,40,5,239,162,15,39,56,239,2,56,229,213,235,70,205,127,45,175,144,203,121,66,75,123,209,225,201,87,23,159,95,79,175,71,205,182,42,239,52,239,26,32,154,133,4,39,56,205,162,45,216,245,5,4,40,3,241,55,201,241,201,239,49,54,0,11,49,55,0,13,2,56,62,48,215,201,42,56,62,45,215,239,160,195,196,197,2,56,217,229,217,239,49,39,194,3,226,1,194,2,56,126,167,32,71,205,127,45,6,16,122,167,32,6,179,40,9,83,6,8,213,217,209,217,24,87,239,226,56,126,214,126,205,193,45,87,58,172,92,146,50,172,92,122,205,79,45,239,49,39,193,3,225,56,205,213,45,229,50,161,92,61,23,159,60,33,171,92,119,35,134,119,225,195,207,46,214,128,254,28,56,19,205,193,45,214,7,71,33,172,92,134,119,120,237,68,205,79,45,24,146,235,205,186,47,217,203,250,125,217,214,128,71,203,35,203,18,217,203,19,203,18,217,33,170,92,14,5,126,143,39,119,43,13,32,248,16,231,175,33,166,92,17,161,92,6,9,237,111,14,255,237,111,32,4,13,12,32,10,18,19,253,52,113,253,52,114,14,0,203,64,40,1,35,16,231,58,171,92,214,9,56,10,253,53,113,62,4,253,190,111,24,65,239,2,226,56,235,205,186,47,217,62,128,149,46,0,203,250,217,205,221,47,253,126,113,254,8,56,6,217,203,18,217,24,32,1,0,2,123,205,139,47,95,122,205,139,47,87,197,217,193,16,241,33,161,92,121,253,78,113,9,119,253,52,113,24,211,245,33,161,92,253,78,113,6,0,9,65,241,43,126,206,0,119,167,40,5,254,10,63,48,8,16,241,54,1,4,253,52,114,253,112,113,239,2,56,217,225,217,237,75,171,92,33,161,92,120,254,9,56,4,254,252,56,38,167,204,239,21,175,144,250,82,47,71,24,12,121,167,40,3,126,35,13,205,239,21,16,244,121,167,200,4,62,46,215,62,48,16,251,65,24,230,80,21,6,1,205,74,47,62,69,215,74,121,167,242,131,47,237,68,79,62,45,24,2,62,43,215,6,0,195,27,26,213,111,38,0,93,84,41,41,25,41,89,25,76,125,209,201,126,54,0,167,200,35,203,126,203,254,43,200,197,1,5,0,9,65,79,55,43,126,47,206,0,119,16,248,121,193,201,229,245,78,35,70,119,35,121,78,197,35,78,35,70,235,87,94,213,35,86,35,94,213,217,209,225,193,217,35,86,35,94,241,225,201,167,200,254,33,48,22,197,71,217,203,45,203,26,203,27,217,203,26,203,27,16,242,193,208,205,4,48,192,217,175,46,0,87,93,217,17,0,0,201,28,192,20,192,217,28,32,1,20,217,201,235,205,110,52,235,26,182,32,38,213,35,229,35,94,35,86,35,35,35,126,35,78,35,70,225,235,9,235,142,15,206,0,32,11,159,119,35,115,35,114,43,43,43,209,201,43,209,205,147,50,217,229,217,213,229,205,155,47,71,235,205,155,47,79,184,48,3,120,65,235,245,144,205,186,47,205,221,47,241,225,119,229,104,97,25,217,235,237,74,235,124,141,111,31,173,217,235,225,31,48,8,62,1,205,221,47,52,40,35,217,125,230,128,217,35,119,43,40,31,123,237,68,63,95,122,47,206,0,87,217,123,47,206,0,95,122,47,206,0,48,7,31,217,52,202,173,49,217,87,217,175,195,85,49,197,6,16,124,77,33,0,0,41,56,10,203,17,23,48,3,25,56,2,16,243,193,201,205,233,52,216,35,174,203,254,43,201,26,182,32,34,213,229,213,205,127,45,235,227,65,205,127,45,120,169,79,225,205,169,48,235,225,56,10,122,179,32,1,79,205,142,45,209,201,209,205,147,50,175,205,192,48,216,217,229,217,213,235,205,192,48,235,56,90,229,205,186,47,120,167,237,98,217,229,237,98,217,6,33,24,17,48,5,25,217,237,90,217,217,203,28,203,29,217,203,28,203,29,217,203,24,203,25,217,203,25,31,16,228,235,217,235,217,193,225,120,129,32,1,167,61,63,23,63,31,242,70,49,48,104,167,60,32,8,56,6,217,203,122,217,32,92,119,217,120,217,48,21,126,167,62,128,40,1,175,217,162,205,251,47,7,119,56,46,35,119,43,24,41,6,32,217,203,122,217,32,18,7,203,19,203,18,217,203,19,203,18,217,53,40,215,16,234,24,215,23,48,12,205,4,48,32,7,217,22,128,217,52,40,24,229,35,217,213,217,193,120,23,203,22,31,119,35,113,35,114,35,115,225,209,217,225,217,201,207,5,205,147,50,235,175,205,192,48,56,244,235,205,192,48,216,217,229,217,213,229,205,186,47,217,229,96,105,217,97,104,175,6,223,24,16,23,203,17,217,203,17,203,16,217,41,217,237,106,217,56,16,237,82,217,237,82,217,48,15,25,217,237,90,217,167,24,8,167,237,82,217,237,82,217,55,4,250,210,49,245,40,225,95,81,217,89,80,241,203,24,241,203,24,217,193,225,120,145,195,61,49,126,167,200,254,129,48,6,54,0,62,32,24,81,254,145,32,26,35,35,35,62,128,166,43,182,43,32,3,62,128,174,43,32,54,119,35,54,255,43,62,24,24,51,48,44,213,47,198,145,35,86,35,94,43,43,14,0,203,122,40,1,13,203,250,6,8,144,128,56,4,90,22,0,144,40,7,71,203,58,203,27,16,250,205,142,45,209,201,126,214,160,240,237,68,213,235,43,71,203,56,203,56,203,56,40,5,54,0,43,16,251,230,7,40,9,71,62,255,203,39,16,252,166,119,235,209,201,205,150,50,235,126,167,192,213,205,127,45,175,35,119,43,119,6,145,122,167,32,8,179,66,40,16,83,88,6,137,235,5,41,48,252,203,9,203,28,203,29,235,43,115,43,114,43,112,209,201,0,176,0,64,176,0,1,48,0,241,73,15,218,162,64,176,0,10,143,54,60,52,161,51,15,48,202,48,175,49,81,56,27,53,36,53,59,53,59,53,59,53,59,53,59,53,59,53,20,48,45,53,59,53,59,53,59,53,59,53,59,53,59,53,156,53,222,53,188,52,69,54,110,52,105,54,222,53,116,54,181,55,170,55,218,55,51,56,67,56,226,55,19,55,196,54,175,54,74,56,146,52,106,52,172,52,165,52,179,52,31,54,201,53,1,53,192,51,160,54,134,54,198,51,122,54,6,53,249,52,155,54,131,55,20,50,162,51,79,45,151,50,73,52,27,52,45,52,15,52,205,191,53,120,50,103,92,217,227,217,237,83,101,92,217,126,35,229,167,242,128,51,87,230,96,15,15,15,15,198,124,111,122,230,31,24,14,254,24,48,8,217,1,251,255,84,93,9,217,7,111,17,215,50,38,0,25,94,35,86,33,101,51,227,213,217,237,75,102,92,201,241,58,103,92,217,24,195,213,229,1,5,0,205,5,31,225,209,201,237,91,101,92,205,192,51,237,83,101,92,201,205,169,51,237,176,201,98,107,205,169,51,217,229,217,227,197,126,230,192,7,7,79,12,126,230,63,32,2,35,126,198,80,18,62,5,145,35,19,6,0,237,176,193,227,217,225,217,71,175,5,200,18,19,24,250,167,200,245,213,17,0,0,205,200,51,209,241,61,24,242,79,7,7,129,79,6,0,9,201,213,42,104,92,205,6,52,205,192,51,225,201,98,107,217,229,33,197,50,217,205,247,51,205,200,51,217,225,217,201,229,235,42,104,92,205,6,52,235,205,192,51,235,225,201,6,5,26,78,235,18,113,35,19,16,247,235,201,71,205,94,51,49,15,192,2,160,194,49,224,4,226,193,3,56,205,198,51,205,98,51,15,1,194,2,53,238,225,3,56,201,6,255,24,6,205,233,52,216,6,0,126,167,40,11,35,120,230,128,182,23,63,31,119,43,201,213,229,205,127,45,225,120,177,47,79,205,142,45,209,201,205,233,52,216,213,17,1,0,35,203,22,43,159,79,205,142,45,209,201,205,153,30,237,120,24,4,205,153,30,10,195,40,45,205,153,30,33,43,45,229,197,201,205,241,43,11,120,177,32,35,26,205,141,44,56,9,214,144,56,25,254,21,48,21,60,61,135,135,135,254,168,48,12,237,75,123,92,129,79,48,1,4,195,43,45,207,9,229,197,71,126,35,182,35,182,35,182,120,193,225,192,55,201,205,233,52,216,62,255,24,6,205,233,52,24,5,175,35,174,43,7,229,62,0,119,35,119,35,23,119,31,35,119,35,119,225,201,235,205,233,52,235,216,55,24,231,235,205,233,52,235,208,167,24,222,235,205,233,52,235,208,213,27,175,18,27,18,209,201,120,214,8,203,87,32,1,61,15,48,8,245,229,205,60,52,209,235,241,203,87,32,7,15,245,205,15,48,24,51,15,245,205,241,43,213,197,205,241,43,225,124,181,227,120,32,11,177,193,40,4,241,63,24,22,241,24,19,177,40,13,26,150,56,9,32,237,11,19,35,227,43,24,223,193,241,167,245,239,160,56,241,245,220,1,53,241,245,212,249,52,241,15,212,1,53,201,205,241,43,213,197,205,241,43,225,229,213,197,9,68,77,247,205,178,42,193,225,120,177,40,2,237,176,193,225,120,177,40,2,237,176,42,101,92,17,251,255,229,25,209,201,205,213,45,56,14,32,12,245,1,1,0,247,241,18,205,178,42,235,201,207,10,42,93,92,229,120,198,227,159,245,205,241,43,213,3,247,225,237,83,93,92,213,237,176,235,43,54,13,253,203,1,190,205,251,36,223,254,13,32,7,225,241,253,174,1,230,64,194,138,28,34,93,92,253,203,1,254,205,251,36,225,34,93,92,24,160,1,1,0,247,34,91,92,229,42,81,92,229,62,255,205,1,22,205,227,45,225,205,21,22,209,42,91,92,167,237,82,68,77,205,178,42,235,201,205,148,30,254,16,210,159,30,42,81,92,229,205,1,22,205,230,21,1,0,0,48,3,12,247,18,205,178,42,225,205,21,22,195,191,53,205,241,43,120,177,40,1,26,195,40,45,205,241,43,195,43,45,217,229,33,103,92,53,225,32,4,35,217,201,217,94,123,23,159,87,25,217,201,19,19,26,27,27,167,32,239,217,35,217,201,241,217,227,217,201,239,192,2,49,224,5,39,224,1,192,4,3,224,56,201,239,49,54,0,4,58,56,201,49,58,192,3,224,1,48,0,3,161,3,56,201,239,61,52,241,56,170,59,41,4,49,39,195,3,49,15,161,3,136,19,54,88,101,102,157,120,101,64,162,96,50,201,231,33,247,175,36,235,47,176,176,20,238,126,187,148,88,241,58,126,248,207,227,56,205,213,45,32,7,56,3,134,48,9,207,5,56,7,150,48,4,237,68,119,201,239,2,160,56,201,239,61,49,55,0,4,56,207,9,160,2,56,126,54,128,205,40,45,239,52,56,0,3,1,49,52,240,76,204,204,205,3,55,0,8,1,161,3,1,56,52,239,1,52,240,49,114,23,248,4,1,162,3,162,3,49,52,50,32,4,162,3,140,17,172,20,9,86,218,165,89,48,197,92,144,170,158,112,111,97,161,203,218,150,164,49,159,180,231,160,254,92,252,234,27,67,202,54,237,167,156,126,94,240,110,35,128,147,4,15,56,201,239,61,52,238,34,249,131,110,4,49,162,15,39,3,49,15,49,15,49,42,161,3,49,55,192,0,4,2,56,201,161,3,1,54,0,2,27,56,201,239,57,42,161,3,224,0,6,27,51,3,239,57,49,49,4,49,15,161,3,134,20,230,92,31,11,163,143,56,238,233,21,99,187,35,238,146,13,205,237,241,35,93,27,234,4,56,201,239,49,31,1,32,5,56,201,205,151,50,126,254,129,56,14,239,161,27,1,5,49,54,163,1,0,6,27,51,3,239,160,1,49,49,4,49,15,161,3,140,16,178,19,14,85,228,141,88,57,188,91,152,253,158,0,54,117,160,219,232,180,99,66,196,230,181,9,54,190,233,54,115,27,93,236,216,222,99,190,240,97,161,179,12,4,15,56,201,239,49,49,4,161,3,27,40,161,15,5,36,49,15,56,201,239,34,163,3,27,56,201,239,49,48,0,30,162,56,239,1,49,48,0,7,37,4,56,195,196,54,2,49,48,0,9,160,1,55,0,6,161,1,5,2,161,56,201,221,229,253,203,1,102,40,3,205,66,58,205,191,2,221,225,201,14,253,22,255,30,191,66,62,7,237,121,237,96,62,14,237,121,237,120,246,240,111,201,66,62,14,237,121,67,237,105,201,66,62,14,237,121,237,120,201,125,230,254,111,24,233,125,246,1,111,24,227,16,254,201,197,6,16,205,179,56,193,16,247,201,197,205,159,56,193,230,32,40,2,16,245,201,197,205,159,56,193,230,32,32,2,16,245,201,205,127,56,6,1,24,5,205,127,56,6,4,197,205,159,56,193,230,32,40,64,175,197,245,205,173,56,6,163,205,192,56,32,49,205,167,56,24,2,255,255,6,43,205,179,56,205,159,56,203,111,40,4,241,55,24,3,241,55,63,31,245,205,173,56,6,38,205,179,56,205,167,56,6,35,205,179,56,241,193,16,196,201,241,193,205,173,56,175,50,136,91,60,55,63,201,205,127,56,58,136,91,230,128,32,87,205,159,56,230,32,40,228,58,136,91,167,32,11,60,50,136,91,62,76,50,137,91,24,66,58,137,91,61,50,137,91,32,57,175,50,136,91,50,137,91,50,138,91,205,167,56,6,33,205,192,56,32,182,205,173,56,6,36,205,204,56,40,172,205,167,56,6,15,205,182,56,205,223,56,32,159,203,255,230,240,50,136,91,175,203,63,201,175,55,201,175,60,55,201,205,56,57,58,136,91,47,230,192,192,221,33,138,91,6,5,197,205,216,56,194,58,58,203,127,40,33,205,223,56,32,122,193,197,79,221,126,0,203,64,40,12,203,57,203,57,203,57,203,57,230,240,24,2,230,15,177,221,119,0,193,203,64,32,2,221,43,16,203,30,128,221,33,136,91,33,63,58,6,3,221,126,0,166,40,33,203,123,40,66,197,245,120,24,2,255,255,61,203,39,203,39,203,39,246,7,71,241,203,39,218,19,58,16,249,88,193,32,37,221,35,35,16,212,203,123,32,7,123,230,252,40,2,29,29,58,138,91,230,8,40,6,123,230,127,198,18,95,123,198,90,95,175,201,193,201,175,60,201,15,255,242,30,128,58,120,92,230,1,32,4,205,160,57,192,33,0,92,203,126,32,12,126,254,91,56,7,35,53,43,32,2,54,255,125,33,4,92,189,32,233,205,174,58,192,123,33,0,92,190,40,42,235,33,4,92,190,40,35,203,126,32,4,235,203,126,200,95,119,35,54,10,35,58,9,92,203,63,119,35,205,215,58,115,123,50,8,92,33,59,92,203,238,201,35,54,10,35,53,192,58,10,92,203,63,119,35,94,24,230,123,33,102,91,203,70,40,6,254,109,48,26,175,201,254,128,48,20,254,108,32,246,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,175,60,201,229,123,214,91,22,0,95,33,102,91,203,70,40,5,33,19,59,24,37,33,37,59,254,17,56,30,33,33,59,254,21,40,23,254,22,40,19,24,3,0,255,255,254,23,40,10,33,24,59,254,33,48,3,33,19,59,25,94,225,201,46,13,51,50,49,41,40,42,47,45,57,56,55,43,54,53,52,48,165,13,166,167,168,169,170,11,12,7,9,10,8,172,173,174,175,176,177,178,179,180,253,203,1,102,32,5,175,17,54,21,201,33,15,1,227,195,0,91,253,203,1,102,32,5,253,203,10,126,201,33,18,1,24,236,253,203,1,102,32,4,223,254,13,201,33,21,1,24,221,205,142,2,14,0,32,13,205,30,3,48,8,21,95,205,51,3,195,87,38,253,203,1,102,202,96,38,243,205,160,57,251,32,12,205,174,58,32,7,205,215,58,123,195,87,38,14,0,195,96,38,254,163,40,12,254,164,40,8,214,165,210,95,11,195,86,11,253,203,1,102,40,242,17,201,59,213,214,163,17,210,59,40,3,17,218,59,62,4,245,195,23,12,55,253,203,1,78,192,195,3,11,83,80,69,67,84,82,85,205,80,76,65,217,195,1,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,195,160,57,195,16,60,195,16,60,195,16,60,195,16,60,62,127,219,254,31,216,62,254,219,254,31,216,62,7,211,254,62,2,205,1,22,175,50,60,92,62,22,215,175,215,175,215,30,8,67,80,120,61,203,23,203,23,203,23,130,61,50,143,92,33,143,60,75,126,215,35,13,32,250,16,231,67,21,32,227,33,0,72,84,93,19,175,119,1,255,15,237,176,235,17,0,89,1,0,2,237,176,243,17,112,3,46,7,1,153,0,11,120,177,32,251,125,238,16,111,211,254,27,122,179,32,237,1,0,0,11,120,177,32,251,11,120,177,32,251,24,217,19,0,49,57,19,1,56,54,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,16,16,16,0,16,0,0,36,36,0,0,0,0,0,0,36,126,36,36,126,36,0,0,8,62,40,62,10,62,8,0,98,100,8,16,38,70,0,0,16,40,16,42,68,58,0,0,8,16,0,0,0,0,0,0,4,8,8,8,8,4,0,0,32,16,16,16,16,32,0,0,0,20,8,62,8,20,0,0,0,8,8,62,8,8,0,0,0,0,0,0,8,8,16,0,0,0,0,62,0,0,0,0,0,0,0,0,24,24,0,0,0,2,4,8,16,32,0,0,60,70,74,82,98,60,0,0,24,40,8,8,8,62,0,0,60,66,2,60,64,126,0,0,60,66,12,2,66,60,0,0,8,24,40,72,126,8,0,0,126,64,124,2,66,60,0,0,60,64,124,66,66,60,0,0,126,2,4,8,16,16,0,0,60,66,60,66,66,60,0,0,60,66,66,62,2,60,0,0,0,0,16,0,0,16,0,0,0,16,0,0,16,16,32,0,0,4,8,16,8,4,0,0,0,0,62,0,62,0,0,0,0,16,8,4,8,16,0,0,60,66,4,8,0,8,0,0,60,74,86,94,64,60,0,0,60,66,66,126,66,66,0,0,124,66,124,66,66,124,0,0,60,66,64,64,66,60,0,0,120,68,66,66,68,120,0,0,126,64,124,64,64,126,0,0,126,64,124,64,64,64,0,0,60,66,64,78,66,60,0,0,66,66,126,66,66,66,0,0,62,8,8,8,8,62,0,0,2,2,2,66,66,60,0,0,68,72,112,72,68,66,0,0,64,64,64,64,64,126,0,0,66,102,90,66,66,66,0,0,66,98,82,74,70,66,0,0,60,66,66,66,66,60,0,0,124,66,66,124,64,64,0,0,60,66,66,82,74,60,0,0,124,66,66,124,68,66,0,0,60,64,60,2,66,60,0,0,254,16,16,16,16,16,0,0,66,66,66,66,66,60,0,0,66,66,66,66,36,24,0,0,66,66,66,66,90,36,0,0,66,36,24,24,36,66,0,0,130,68,40,16,16,16,0,0,126,4,8,16,32,126,0,0,14,8,8,8,8,14,0,0,0,64,32,16,8,4,0,0,112,16,16,16,16,112,0,0,16,56,84,16,16,16,0,0,0,0,0,0,0,0,255,0,28,34,120,32,32,126,0,0,0,56,4,60,68,60,0,0,32,32,60,34,34,60,0,0,0,28,32,32,32,28,0,0,4,4,60,68,68,60,0,0,0,56,68,120,64,60,0,0,12,16,24,16,16,16,0,0,0,60,68,68,60,4,56,0,64,64,120,68,68,68,0,0,16,0,48,16,16,56,0,0,4,0,4,4,4,36,24,0,32,40,48,48,40,36,0,0,16,16,16,16,16,12,0,0,0,104,84,84,84,84,0,0,0,120,68,68,68,68,0,0,0,56,68,68,68,56,0,0,0,120,68,68,120,64,64,0,0,60,68,68,60,4,6,0,0,28,32,32,32,32,0,0,0,56,64,56,4,120,0,0,16,56,16,16,16,12,0,0,0,68,68,68,68,56,0,0,0,68,68,40,40,16,0,0,0,68,84,84,84,40,0,0,0,68,40,16,40,68,0,0,0,68,68,68,60,4,56,0,0,124,8,16,32,124,0,0,14,8,48,8,8,14,0,0,8,8,8,8,8,8,0,0,112,16,12,16,16,112,0,0,20,40,0,0,0,0,0,60,66,153,161,161,153,66,60]);
JSSpeccy.autoloaders = {};
JSSpeccy.autoloaders['tape_128.z80'] = new Uint8Array([66,2,2,0,63,5,0,0,70,255,0,9,14,17,0,33,24,155,54,56,0,1,0,58,92,226,92,0,0,1,54,0,107,5,4,16,0,0,14,0,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,136,58,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,206,1,3,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,100,0,236,237,237,14,0,69,57,163,57,219,2,124,56,69,57,177,51,214,92,208,92,27,1,63,5,113,7,226,92,203,92,20,91,33,24,29,91,0,62,20,27,0,60,66,66,126,66,66,0,0,124,66,124,66,66,124,0,0,60,66,64,64,66,60,0,0,120,68,66,66,68,120,0,0,126,64,124,64,64,126,0,0,126,64,124,64,64,64,0,0,60,66,64,78,66,60,0,0,66,66,126,66,66,66,0,0,62,8,8,8,8,62,0,0,2,2,2,66,66,60,0,0,68,72,112,72,68,66,0,0,237,237,5,64,126,0,0,66,102,90,66,66,66,0,0,66,98,82,74,70,66,0,0,60,66,66,66,66,60,0,0,124,66,66,124,64,64,0,0,60,66,66,82,74,60,0,0,124,66,66,124,68,66,0,0,60,64,60,2,66,60,0,0,254,237,237,5,16,0,0,237,237,5,66,60,0,4,1,4,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,64,0,4,1,5,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,64,0,4,1,6,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,64,0,4,1,7,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,64,0,200,3,8,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,202,0,1,254,1,254,1,237,237,225,0,254,0,0,0,0,64,0,0,4,237,237,17,0,3,252,3,252,3,237,237,33,0,254,237,237,7,0,16,237,237,9,0,124,124,126,60,68,0,16,0,16,237,237,165,0,16,56,120,56,0,64,56,56,4,56,28,237,237,15,0,7,248,7,248,7,237,237,33,0,16,56,0,28,56,120,28,56,16,0,0,0,120,28,56,56,56,0,66,66,64,66,72,0,56,68,0,28,56,237,237,163,0,16,4,68,68,0,64,68,4,60,68,32,237,237,15,0,15,240,15,240,15,237,237,33,0,16,68,0,32,4,68,32,68,16,0,0,0,68,32,68,64,64,0,124,66,124,66,112,0,16,84,48,32,68,237,237,163,0,16,60,68,120,0,64,68,60,68,120,32,237,237,15,0,31,224,31,224,31,237,237,33,0,16,68,0,32,60,68,32,120,16,0,62,0,68,32,120,56,56,0,66,124,64,126,72,0,16,84,16,32,120,237,237,163,0,16,68,120,64,0,64,68,68,68,64,32,237,237,15,0,63,192,63,192,63,237,237,33,0,16,68,0,32,68,68,32,64,16,0,0,0,120,32,64,4,4,0,66,68,64,66,68,0,16,84,16,32,64,237,237,163,0,16,60,64,60,0,126,56,60,60,60,32,237,237,15,0,127,128,127,128,127,237,237,33,0,16,56,0,28,60,68,28,60,12,0,0,0,64,32,60,120,120,0,124,66,126,66,66,0,12,40,56,28,60,237,237,165,0,64,237,237,23,0,255,0,255,0,255,237,237,45,0,64,237,237,19,0,237,237,255,56,237,237,255,56,237,237,162,56,237,237,11,71,237,237,15,64,66,114,116,108,104,64,237,237,64,56,245,197,1,253,127,58,92,91,238,16,243,50,92,91,237,121,251,193,241,201,205,0,91,229,42,90,91,227,201,243,58,92,91,230,239,50,92,91,1,253,127,237,121,251,195,195,0,33,216,6,24,3,33,202,7,8,1,253,127,58,92,91,245,230,239,243,50,92,91,237,121,195,230,5,8,241,1,253,127,243,50,92,91,237,121,251,8,201,97,7,46,21,16,207,0,11,0,0,0,0,80,0,17,237,237,26,0,243,91,236,235,236,43,1,1,59,0,33,3,237,237,7,0,10,0,10,0,1,3,7,15,31,63,127,255,254,252,248,240,224,192,128,237,237,36,0,69,57,163,57,219,2,124,56,108,253,219,2,124,56,108,253,77,0,245,39,206,11,252,80,5,23,243,13,33,24,0,0,84,0,0,0,39,31,247,2,231,63,43,39,231,63,122,38,17,13,103,38,0,255,0,0,0,13,3,33,13,13,35,2,0,0,0,22,0,1,0,6,0,11,0,1,0,1,0,6,0,16,237,237,26,0,60,64,0,255,156,32,82,255,237,237,5,0,255,254,255,1,56,0,0,203,92,0,0,182,92,187,92,203,92,208,92,202,92,204,92,204,92,207,92,0,0,209,92,243,92,243,92,127,146,92,0,2,237,237,8,0,1,23,0,0,190,0,0,88,255,237,237,5,0,4,23,0,64,253,80,33,24,4,23,1,56,0,56,237,237,34,0,87,255,255,255,244,9,168,16,75,244,9,196,21,83,129,15,196,21,82,52,91,47,91,80,128,128,239,34,34,13,128,0,255,237,237,9,32,0,0,0,128,237,237,18,0,128,13,206,92,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,45,0,4,1,9,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,64,0,132,1,10,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,35,0,192,237,237,21,0,128,255,56,0,56,0,0,0,20,237,237,255,0,237,237,255,0,237,237,226,0,32,237,237,5,0,5,23,0,64,252,80,33,24,5,23,1,56,0,56,237,237,113,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,112,0,237,237,14,56,237,237,255,0,237,237,248,0,68,39,84,39,0,0,0,4,16,20,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,124,0,1,5,0,0,20,0,0,0,15,237,237,255,0,237,237,255,0,237,237,143,0]);
JSSpeccy.autoloaders['tape_48.z80'] = new Uint8Array([66,2,2,0,63,5,0,0,74,255,63,10,14,17,0,33,23,155,54,0,0,1,0,58,92,226,92,0,0,1,54,0,107,5,0,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,117,47,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,1,4,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,64,0,229,1,5,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,87,0,243,13,206,11,227,80,206,11,228,80,29,23,220,10,206,11,231,80,26,23,220,10,215,24,56,0,56,0,13,25,207,92,169,24,6,3,7,92,177,51,177,51,214,92,208,92,27,1,93,22,63,5,113,7,226,92,203,92,118,27,3,19,0,62,0,60,66,66,126,66,66,0,0,124,66,124,66,66,124,0,0,60,66,64,64,66,60,0,0,120,68,66,66,68,120,0,0,126,64,124,64,64,126,0,0,126,64,124,64,64,64,0,0,60,66,64,78,66,60,0,0,66,66,126,66,66,66,0,0,62,8,8,8,8,62,0,0,2,2,2,66,66,60,0,0,68,72,112,72,68,66,0,0,237,237,5,64,126,0,0,66,102,90,66,66,66,0,0,66,98,82,74,70,66,0,0,60,66,66,66,66,60,0,0,124,66,66,124,64,64,0,0,60,66,66,82,74,60,0,0,124,66,66,124,68,66,0,0,60,64,60,2,66,60,0,0,254,237,237,5,16,0,0,237,237,5,66,60,0,177,1,8,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,24,0,237,237,255,56,237,237,255,56,237,237,255,56,56,56,56,237,237,255,0,0,255,0,0,0,13,5,35,13,13,35,5,237,237,5,0,1,0,6,0,11,0,1,0,1,0,6,0,16,237,237,26,0,60,64,0,255,140,1,84,255,237,237,5,0,255,254,255,1,56,0,0,203,92,0,0,182,92,182,92,203,92,208,92,202,92,204,92,207,92,207,92,0,0,209,92,243,92,243,92,27,146,92,16,2,237,237,8,0,1,26,0,0,187,1,0,88,255,0,0,33,0,91,33,23,0,64,224,80,33,24,33,23,1,56,0,56,237,237,34,0,87,255,255,255,244,9,168,16,75,244,9,196,21,83,129,15,196,21,82,244,9,196,21,80,128,128,239,34,34,13,128,0,255,237,237,9,32,0,0,0,128,237,237,18,0,128,13,206,92,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,255,0,237,237,45,0]);
JSSpeccy.SnaFile = function(data) {
	var mode128 = false, snapshot = null, len = data.byteLength, sna;

	switch (len) {
		case 131103:
		case 147487:
			mode128 = true;
		case 49179:
			sna = new DataView(data, 0, mode128 ? 49182 : len);
			snapshot = {
				model: (mode128
					? JSSpeccy.Spectrum.MODEL_128K
					: JSSpeccy.Spectrum.MODEL_48K),
				registers: {},
				ulaState: {},
			/* construct byte arrays of length 0x4000 at the appropriate offsets into the data stream */
				memoryPages: {
					5: new Uint8Array(data, 0x0000 + 27, 0x4000),
					2: new Uint8Array(data, 0x4000 + 27, 0x4000)
				}
			};

			if (mode128) {
				var page = (sna.getUint8(49181) & 7);
				snapshot.memoryPages[page] = new Uint8Array(data, 0x8000 + 27, 0x4000);

				for (var i = 0, ptr = 49183; i < 8; i++) {
					if (typeof snapshot.memoryPages[i] === 'undefined') {
						snapshot.memoryPages[i] = new Uint8Array(data, ptr, 0x4000);
						ptr += 0x4000;
					}
				}
			}
			else
				snapshot.memoryPages[0] = new Uint8Array(data, 0x8000 + 27, 0x4000);

			snapshot.registers['IR'] = (sna.getUint8(0) << 8) | sna.getUint8(20);
			snapshot.registers['HL_'] = sna.getUint16(1, true);
			snapshot.registers['DE_'] = sna.getUint16(3, true);
			snapshot.registers['BC_'] = sna.getUint16(5, true);
			snapshot.registers['AF_'] = sna.getUint16(7, true);
			snapshot.registers['HL'] = sna.getUint16(9, true);
			snapshot.registers['DE'] = sna.getUint16(11, true);
			snapshot.registers['BC'] = sna.getUint16(13, true);
			snapshot.registers['IY'] = sna.getUint16(15, true);
			snapshot.registers['IX'] = sna.getUint16(17, true);
			snapshot.registers['iff1'] = snapshot.registers['iff2'] = (sna.getUint8(19) & 0x04) >> 2;
			snapshot.registers['AF'] = sna.getUint16(21, true);

			if (mode128) {
				snapshot.registers['SP'] = sna.getUint16(23, true);
				snapshot.registers['PC'] = sna.getUint16(49179, true);
				snapshot.ulaState.pagingFlags = sna.getUint8(49181);
			}
			else {
				/* peek memory at SP to get proper value of PC */
				var sp = sna.getUint16(23, true);
				var l = sna.getUint8(sp - 16384 + 27);
				sp = (sp + 1) & 0xffff;
				var h = sna.getUint8(sp - 16384 + 27);
				sp = (sp + 1) & 0xffff;
				snapshot.registers['PC'] = (h << 8) | l;
				snapshot.registers['SP'] = sp;
			}

			snapshot.registers['im'] = sna.getUint8(25);
			snapshot.ulaState.borderColour = sna.getUint8(26);
			break;

		default:
			throw "Cannot handle SNA snapshots of length " + len;
	}

	return snapshot;
}
JSSpeccy.Spectrum = function(opts) {
	var self = {};

	model = opts.model || JSSpeccy.Spectrum.MODEL_128K;

	var viewport = opts.viewport;
	var keyboard = opts.keyboard;
	var controller = opts.controller;
	var soundBackend = opts.soundBackend;

	var memory = JSSpeccy.Memory({
		model: model
	});

	var display = JSSpeccy.Display({
		viewport: viewport,
		memory: memory,
		model: model,
		borderEnabled: opts.borderEnabled,
		settings: {
			'checkerboardFilter': controller.settings.checkerboardFilter
		}
	});

	var sound = JSSpeccy.SoundGenerator({
		model: model,
		soundBackend: soundBackend
	});

	var ioBus = JSSpeccy.IOBus({
		keyboard: keyboard,
		display: display,
		memory: memory,
		sound: sound,
		contentionTable: model.contentionTable
	});

	var processor = JSSpeccy.Z80({
		memory: memory,
		ioBus: ioBus,
		display: display
	});

	/* internal state to allow picking up mid-frame (e.g. when loading from a snapshot) */
	var startNextFrameWithInterrupt = true;

	self.runFrame = function() {
		display.startFrame();
		if (startNextFrameWithInterrupt) {
			processor.requestInterrupt();
		}
		processor.runFrame(model.frameLength);
		display.endFrame();
		sound.endFrame();
		processor.setTstates(processor.getTstates() - model.frameLength);
		startNextFrameWithInterrupt = true;
	};
	self.reset = function() {
		processor.reset();
		memory.reset();
		sound.reset();
	};

	self.loadSnapshot = function(snapshot) {
		memory.loadFromSnapshot(snapshot.memoryPages);
		if ('pagingFlags' in snapshot.ulaState) {
			memory.setPaging(snapshot.ulaState.pagingFlags);
		}
		processor.loadFromSnapshot(snapshot.registers);
		display.setBorder(snapshot.ulaState.borderColour);
		if ('tstates' in snapshot) {
			processor.setTstates(snapshot.tstates);
			startNextFrameWithInterrupt = false;
		}
	};

	self.drawFullScreen = function() {
		display.drawFullScreen();
	};

	JSSpeccy.traps.tapeLoad = function() {
		if (!controller.currentTape) return true; /* no current tape, so return from trap;
			resume the trapped instruction */
		var block = controller.currentTape.getNextLoadableBlock();
		if (!block) return true; /* no loadable blocks on tape, so return from trap */

		var success = true;

		var expectedBlockType = processor.getA_();
		var startAddress = processor.getIX();
		var requestedLength = processor.getDE();

		var actualBlockType = block[0];
		if (expectedBlockType != actualBlockType) {
			success = false;
		} else {
			/* block type is the one we're looking for */
			if (processor.getCarry_()) {
				/* perform a LOAD */
				var offset = 0;
				var checksum = actualBlockType;

				while (offset < requestedLength) {
					var loadedByte = block[offset+1];
					if (typeof(loadedByte) == 'undefined') {
						/* have run out of bytes to load - indicate error */
						success = false;
						break;
					}
					memory.write((startAddress + offset) & 0xffff, loadedByte);
					checksum ^= loadedByte;
					offset++;
				}

				/* if the full quota of bytes has been loaded, compare checksums now */
				var expectedChecksum = block[offset+1];
				success = (checksum === expectedChecksum);
			} else {
				/* perform a VERIFY */
				success = true; /* for now, just report success. TODO: do VERIFY properly... */
			}
		}

		processor.setCarry(success); /* set or reset carry flag to indicate success or failure respectively */
		processor.setPC(0x05e2); /* address at which to exit tape trap */
		return false; /* cancel execution of the opcode where this trap happened */
	};

	return self;
};


JSSpeccy.buildContentionTables = function(model) {
	function buildTable(pattern) {
		var table = new Uint8Array(model.frameLength);
		for (var line = 0; line < 192; line++) {
			var lineStartTime = model.tstatesUntilOrigin + (line * model.tstatesPerScanline);
			for (var x = 0; x < 128; x++) {
				table[lineStartTime + x] = pattern[x % 8];
			}
		}
		return table;
	}

	model.contentionTable = buildTable(model.contentionPattern);
	model.noContentionTable = buildTable([0,0,0,0,0,0,0,0]);
};

JSSpeccy.Spectrum.MODEL_48K = {
	id: '48k',
	name: 'Spectrum 48K',
	tapeAutoloader: 'tape_48.z80',
	tstatesUntilOrigin: 14336,
	tstatesPerScanline: 224,
	frameLength: 69888,
	clockSpeed: 3500000,
	contentionPattern: [6,5,4,3,2,1,0,0]
};
JSSpeccy.buildContentionTables(JSSpeccy.Spectrum.MODEL_48K);

JSSpeccy.Spectrum.MODEL_128K = {
	id: '128k',
	name: 'Spectrum 128K',
	tapeAutoloader: 'tape_128.z80',
	tstatesUntilOrigin: 14362,
	tstatesPerScanline: 228,
	frameLength: 70908,
	clockSpeed: 3546900,
	contentionPattern: [6,5,4,3,2,1,0,0]
};
JSSpeccy.buildContentionTables(JSSpeccy.Spectrum.MODEL_128K);

JSSpeccy.Spectrum.MODELS = [
	JSSpeccy.Spectrum.MODEL_48K,
	JSSpeccy.Spectrum.MODEL_128K
];
JSSpeccy.TapFile = function(data) {
	var self = {};

	var i = 0;
	var blocks = [];
	var tap = new DataView(data);

	while ((i+1) < data.byteLength) {
		var blockLength = tap.getUint16(i, true);
		i += 2;
		blocks.push(new Uint8Array(data, i, blockLength));
		i += blockLength;
	}

	var nextBlockIndex = 0;
	self.getNextLoadableBlock = function() {
		if (blocks.length === 0) return null;
		var block = blocks[nextBlockIndex];
		nextBlockIndex = (nextBlockIndex + 1) % blocks.length;
		return block;
	};

	return self;
};

JSSpeccy.TapFile.isValid = function(data) {
	/* test whether the given ArrayBuffer is a valid TAP file, i.e. EOF is consistent with the
	block lengths we read from the file */
	var pos = 0;
	var tap = new DataView(data);

	while (pos < data.byteLength) {
		if (pos + 1 >= data.byteLength) return false; /* EOF in the middle of a length word */
		var blockLength = tap.getUint16(pos, true);
		pos += blockLength + 2;
	}

	return (pos == data.byteLength); /* file is a valid TAP if pos is exactly at EOF and no further */
};
JSSpeccy.TzxFile = function(data) {
	var self = {};

	var blocks = [];
	var tzx = new DataView(data);

	var signature = "ZXTape!\x1A";
	for (var i = 0; i < signature.length; i++) {
		if (signature.charCodeAt(i) != tzx.getUint8(i)) {
			alert("Not a valid TZX file");
			return null;
		}
	}

	var offset = 0x0a;

	while (offset < data.byteLength) {
		var blockType = tzx.getUint8(offset);
		offset++;
		switch (blockType) {
			case 0x10:
				var pause = tzx.getUint16(offset, true);
				offset += 2;
				var dataLength = tzx.getUint16(offset, true);
				offset += 2;
				blocks.push({
					'type': 'StandardSpeedData',
					'pause': pause,
					'data': new Uint8Array(data, offset, dataLength)
				});
				offset += dataLength;
				break;
			case 0x11:
				var pilotPulseLength = tzx.getUint16(offset, true); offset += 2;
				var syncPulse1Length = tzx.getUint16(offset, true); offset += 2;
				var syncPulse2Length = tzx.getUint16(offset, true); offset += 2;
				var zeroBitLength = tzx.getUint16(offset, true); offset += 2;
				var oneBitLength = tzx.getUint16(offset, true); offset += 2;
				var pilotPulseCount = tzx.getUint16(offset, true); offset += 2;
				var lastByteMask = tzx.getUint8(offset); offset += 1;
				var pause = tzx.getUint16(offset, true); offset += 2;
				var dataLength = tzx.getUint16(offset, true) | (tzx.getUint8(offset+2) << 16); offset += 3;
				blocks.push({
					'type': 'TurboSpeedData',
					'pilotPulseLength': pilotPulseLength,
					'syncPulse1Length': syncPulse1Length,
					'syncPulse2Length': syncPulse2Length,
					'zeroBitLength': zeroBitLength,
					'oneBitLength': oneBitLength,
					'pilotPulseCount': pilotPulseCount,
					'lastByteMask': lastByteMask,
					'pause': pause,
					'data': new Uint8Array(data, offset, dataLength)
				});
				offset += dataLength;
				break;
			case 0x12:
				var pulseLength = tzx.getUint16(offset, true); offset += 2;
				var pulseCount = tzx.getUint16(offset, true); offset += 2;
				blocks.push({
					'type': 'PureTone',
					'pulseLength': pulseLength,
					'pulseCount': pulseCount
				});
				break;
			case 0x13:
				var pulseCount = tzx.getUint8(offset); offset += 1;
				blocks.push({
					'type': 'PulseSequence',
					'pulseLengths': new Uint16Array(data, offset, pulseCount)
				});
				offset += (pulseCount * 2);
				break;
			case 0x14:
				var zeroBitLength = tzx.getUint16(offset, true); offset += 2;
				var oneBitLength = tzx.getUint16(offset, true); offset += 2;
				var lastByteMask = tzx.getUint8(offset); offset += 1;
				var pause = tzx.getUint16(offset, true); offset += 2;
				var dataLength = tzx.getUint16(offset, true) | (tzx.getUint8(offset+2) << 16); offset += 3;
				blocks.push({
					'type': 'PureData',
					'zeroBitLength': zeroBitLength,
					'oneBitLength': oneBitLength,
					'lastByteMask': lastByteMask,
					'pause': pause,
					'data': new Uint8Array(data, offset, dataLength)
				});
				offset += dataLength;
				break;
			case 0x15:
				var tstatesPerSample = tzx.getUint16(offset, true); offset += 2;
				var pause = tzx.getUint16(offset, true); offset += 2;
				var lastByteMask = tzx.getUint8(offset); offset += 1;
				var dataLength = tzx.getUint16(offset, true) | (tzx.getUint8(offset+2) << 16); offset += 3;
				blocks.push({
					'type': 'DirectRecording',
					'tstatesPerSample': tstatesPerSample,
					'lastByteMask': lastByteMask,
					'pause': pause,
					'data': new Uint8Array(data, offset, dataLength)
				});
				offset += dataLength;
				break;
			case 0x20:
				var pause = tzx.getUint16(offset, true); offset += 2;
				blocks.push({
					'type': 'Pause',
					'pause': pause
				});
				break;
			case 0x21:
				var nameLength = tzx.getUint8(offset); offset += 1;
				var nameBytes = new Uint8Array(data, offset, nameLength);
				offset += nameLength;
				var name = String.fromCharCode.apply(null, nameBytes);
				blocks.push({
					'type': 'GroupStart',
					'name': name
				});
				break;
			case 0x22:
				blocks.push({
					'type': 'GroupEnd'
				});
				break;
			case 0x23:
				var jumpOffset = tzx.getUint16(offset, true); offset += 2;
				blocks.push({
					'type': 'JumpToBlock',
					'offset': jumpOffset
				});
				break;
			case 0x24:
				var repeatCount = tzx.getUint16(offset, true); offset += 2;
				blocks.push({
					'type': 'LoopStart',
					'repeatCount': repeatCount
				});
				break;
			case 0x25:
				blocks.push({
					'type': 'LoopEnd'
				});
				break;
			case 0x26:
				var callCount = tzx.getUint16(offset, true); offset += 2;
				blocks.push({
					'type': 'CallSequence',
					'offsets': new Uint16Array(data, offset, callCount)
				});
				offset += (callCount * 2);
				break;
			case 0x27:
				blocks.push({
					'type': 'ReturnFromSequence'
				});
				break;
			case 0x28:
				var blockLength = tzx.getUint16(offset, true); offset += 2;
				/* This is a silly block. Don't bother parsing it further. */
				blocks.push({
					'type': 'Select',
					'data': new Uint8Array(data, offset, blockLength)
				});
				offset += blockLength;
				break;
			case 0x30:
				var textLength = tzx.getUint8(offset); offset += 1;
				var textBytes = new Uint8Array(data, offset, textLength);
				offset += textLength;
				var text = String.fromCharCode.apply(null, textBytes);
				blocks.push({
					'type': 'TextDescription',
					'text': text
				});
				break;
			case 0x31:
				var displayTime = tzx.getUint8(offset); offset += 1;
				var textLength = tzx.getUint8(offset); offset += 1;
				var textBytes = new Uint8Array(data, offset, textLength);
				offset += textLength;
				var text = String.fromCharCode.apply(null, textBytes);
				blocks.push({
					'type': 'MessageBlock',
					'displayTime': displayTime,
					'text': text
				});
				break;
			case 0x32:
				var blockLength = tzx.getUint16(offset, true); offset += 2;
				blocks.push({
					'type': 'ArchiveInfo',
					'data': new Uint8Array(data, offset, blockLength)
				});
				offset += blockLength;
				break;
			case 0x33:
				var blockLength = tzx.getUint8(offset) * 3; offset += 1;
				blocks.push({
					'type': 'HardwareType',
					'data': new Uint8Array(data, offset, blockLength)
				});
				offset += blockLength;
				break;
			case 0x35:
				var identifierBytes = new Uint8Array(data, offset, 10);
				offset += 10;
				var identifier = String.fromCharCode.apply(null, identifierBytes);
				var dataLength = tzx.getUint32(offset, true);
				blocks.push({
					'type': 'CustomInfo',
					'identifier': identifier,
					'data': new Uint8Array(data, offset, dataLength)
				});
				offset += dataLength;
				break;
			case 0x5A:
				offset += 9;
				blocks.push({
					'type': 'Glue'
				});
				break;
			default:
				/* follow extension rule: next 4 bytes = length of block */
				var blockLength = tzx.getUint32(offset, true);
				offset += 4;
				blocks.push({
					'type': 'unknown',
					'data': new Uint8Array(data, offset, blockLength)
				});
				offset += blockLength;
		}
	}

	var nextBlockIndex = 0;
	var loopToBlockIndex;
	var repeatCount;
	var callStack = [];

	self.getNextMeaningfulBlock = function() {
		var startedAtZero = (nextBlockIndex === 0);
		while (true) {
			if (nextBlockIndex >= blocks.length) {
				if (startedAtZero) return null; /* have looped around; quit now */
				nextBlockIndex = 0;
				startedAtZero = true;
			}
			var block = blocks[nextBlockIndex];
			switch (block.type) {
				case 'StandardSpeedData':
				case 'TurboSpeedData':
				case 'PureTone':
				case 'PulseSequence':
				case 'PureData':
				case 'DirectRecording':
				case 'Pause':
					/* found a meaningful block */
					nextBlockIndex++;
					return block;
				case 'JumpToBlock':
					nextBlockIndex += block.offset;
					break;
				case 'LoopStart':
					loopToBlockIndex = nextBlockIndex + 1;
					repeatCount = block.repeatCount;
					nextBlockIndex++;
					break;
				case 'LoopEnd':
					repeatCount--;
					if (repeatCount > 0) {
						nextBlockIndex = loopToBlockIndex;
					} else {
						nextBlockIndex++;
					}
					break;
				case 'CallSequence':
					/* push the future destinations (where to go on reaching a ReturnFromSequence block)
						onto the call stack in reverse order, starting with the block immediately
						after the CallSequence (which we go to when leaving the sequence) */
					callStack.unshift(nextBlockIndex+1);
					for (var i = block.offsets.length - 1; i >= 0; i--) {
						callStack.unshift(nextBlockIndex + block.offsets[i]);
					}
					/* now visit the first destination on the list */
					nextBlockIndex = callStack.shift();
					break;
				case 'ReturnFromSequence':
					nextBlockIndex = callStack.shift();
					break;
				default:
					/* not one of the types we care about; skip past it */
					nextBlockIndex++;
			}
		}
	};

	self.getNextLoadableBlock = function() {
		while (true) {
			var block = self.getNextMeaningfulBlock();
			if (!block) return null;
			if (block.type == 'StandardSpeedData' || block.type == 'TurboSpeedData') {
				return block.data;
			}
			/* FIXME: avoid infinite loop if the TZX file consists only of meaningful but non-loadable blocks */
		}
	};

	return self;
};
JSSpeccy.Viewport = function(opts) {
	var self = {};
	var container = opts.container;
	var scaleFactor = opts.scaleFactor || 2;

	var positioner = document.createElement('div');
	container.appendChild(positioner);
	positioner.style.position = 'relative';

	self.canvas = document.createElement('canvas');
	positioner.appendChild(self.canvas);

	var statusIcon = document.createElement('div');
	positioner.appendChild(statusIcon);
	statusIcon.style.position = 'absolute';
	statusIcon.style.width = '64px';
	statusIcon.style.height = '64px';
	statusIcon.style.backgroundColor = 'rgba(127, 127, 127, 0.7)';
	statusIcon.style.borderRadius = '4px';
	statusIcon.style.backgroundPosition = 'center';
	statusIcon.style.backgroundRepeat = 'no-repeat';
	statusIcon.style.display = 'none';
	statusIcon.style.cursor = 'inherit';

	var currentIcon = 'none';
	self.showIcon = function(icon) {
		switch (icon) {
			case 'loading':
				statusIcon.style.display = 'block';
				statusIcon.style.backgroundImage = 'url(data:image/gif;base64,R0lGODlhIAAgAKIAAIiIiJCQkPDw8KCgoLCwsMDAwNDQ0ODg4CH/C05FVFNDQVBFMi4wAwEAAAAh+QQJCgAAACwAAAAAIAAgAAADugi6zCSkyUmXuC2EKkdpl8AMJMcEl8GES0AO5nrBCqu8W6wU12FhAFdJtzhcPgAbjrgYhDYsIU0hzAEGh0NkYThKXK3XwErIHgw057YiHnLNZyunLW8a4LqqqXyYmuoVAUhMhExtJC4EBYuMhIckjJGDOo+JkpOFmYFMgBR6HJ8mdBNSbqJtDYd/U6VWrU2AUieIYWMjtC83tISyV0O9hru5urZEwMNByDFLxLd+p37KQZ2eddKaGcUxCQAh+QQJCgAAACwAAAAAIAAgAAADvwi6zCSjyUnXEacVUuXgjCAyl9AxwXEY4ag87qmoR6SI5i0W8kKoLF2ugOsxDCpQMYADGQEDWgBQNIgyi8BgMFUECgWbAnngNR427Za7GIDB3ZSYsl433G9C91RnS75vZidqe3Rgcx2FiYhPjTJ9WwFqfY2QlnZPlpKWjp09iolGhIORPX2gAJNbRpB3lItZdXuqe34omF6lubZQpZhrqIurvWm4rLq4wE+TbcOpxny6xAzQHcbQksuK1Z63wRIJACH5BAkKAAAALAAAAAAgACAAAAO8CLrMc6PJSZc5ppVYW+DLITKGIHRMUGyMeCyD+aLLWgThqBxmQcO23Kwg+zEIK44LEDAJQApcR7XCLYmCTM1EWHwGUgWS1SA8xU6BLzpog6NkyiM9g7nb4U76LPm6f1hrFV95HQaFFAGIRow/d26Kj22NkpV/jJWRko2cjIuJRoQoojSPi36Tjo8Nm1MgqGGwMIh+KXhet3Zvl5CctQCXv5i5l8C5P8LFwqXHxcZQKM7OTJ8TiqypnYNvPwkAIfkECQoAAAAsAAAAACAAIAAAA70IusxTo8lJFymkkVhbCE0hMsZxdEwwrIxYLIN5ovAKKu5SHhmtqCzcCEAwGXwOm/AVkHEWtwpwAMoVjC2B4AmMAlZUSewW0wpeP3AYoPKKDWbBMal2U+KHZ0qtrxC0aFJKPgV2Ex9IiYoLcWYEU2qKjWZ8YJKTj5V9i5wUhp5IXSiiNHyGkJsUlQ2rHSpQdbBgUWt7QWm1pAqzX0G8ilO7QcGKv5bCtTTEvVzHPr/MdInO0Smfh27UnRJtSAkAIfkECQoAAAAsAAAAACAAIAAAA8AIusxBoclJ1yCkjVhb4AuWhUXROUPKiEtQmqeVgqzyDnGb4ko9vDnGjsNylUAK5CQwBLB+hVHvcOABmBvhTPLjDAzUgxS7vWYrAXDYoNmVT+GDwYpy5whUKQWrrBT6S4BBgzEChodiZHaDh40CbotBjgJVijuEmIN/QYISBYYwaG8djno6kR0DB40HbahLVgSNoYogZ3V0n4+nSE2XAE2EZAq/w4TBv8CjJ8bKVs0xwc5adCfJ0w6dEh+umcy3JwkAIfkECQoAAAAsAAAAACAAIAAAA78IuswxoclJ17gNVhkiu8NHEJsDfpgSjGFpXR5wKgPrLk8qp/V4o5pdaDWKqUo5zanXohUKsZwRZFQFA89nM3kNUlbZAglIvYWhE25zM9AiYT/vpvOr2xeHvN4AAfnteoEHfn91ggd8ajp3jBtoN1UVBAICBW9yFZSUg2mEkAaaAgYZno4CLQMHmpZWZTSRoAIHDAWqW3AvShgBmmN1STQpBZp3rjMAq7/Hy8RxuEIMqgKRFMfQtL4udECNc5gVCQAh+QQJCgAAACwAAAAAIAAgAAADvAi6zDGhyUnXuC3EmrfF38A5lxguDzgqpVeG3pqawAvMa9Oy2BzfI1+tZ1OkPEfGLgMxlprO5ePHfNJ4T+rEqp2pOEnwRTvR5My5tLpRaLsJEOt35a4X5PORvQD3FteAdGQSgzoHBwRBYzmHhwZXKFZnBo0HBjqSHAQCJgOUh4lRiyxUBQIClwsEhzRhWBGnAjexkDI2sQqmAgeAS7gKB6cFajgKvzXHK0sAyQanhUpfyQB3xD/TgRKboSsJACH5BAkKAAAALAAAAAAgACAAAAPBCLrMMaHJSde4LcSat8XfwDmXGC4POCqlV4bemprAC8xr07LYHN8jX61nU6Q8R8YuAzGWms7l48d80nhP6sSqnak4SfBFO9HkzLm0WmeFeIu5thy+kmvk67waKksPCgVXZWM5gIZdVmcEhgUEbE8jAwcbAYw0XhsDAgUSBQcHBkqASIQLBwICoagCN58HgisEqxGrCgSfoWurnAC1CgafjmkFvr2oFq5qq8LGrAvAk2moBwzFtrAViw2n1HocmswjCQAh+QQJCgAAACwAAAAAIAAgAAADwAi6zDGhyUnXuC3EmrfF38A5lxguDzgqpVeG3pqawAvMa9Oy2BzfI1+tZ1OkPEfGLgMxlprO5ePHfNJ4T+rEqp2pOEnwRTvR5My5tFpnhXiLubYcvpJr5Ou8Gh00pMMVBQICBXFZW4OJZ20NB4kCB1cSAwUbXh6CiQRGAps6BQWeUR4Eg4ULBqYAB6w3oJVBOokRrAcsr3mOhAq1CwSgkhyZtrytRrhpujS9vqBkEoN+C8wWz5OnqAfSehUDB6IjCQAh+QQJCgAAACwAAAAAIAAgAAADwAi6zDGhyUnXuC3EmrfF38A5lxguDzgqpVeG3pqawAvMa9Oy2BzfI1+tZ1OkPEfGLgMxlprO5ePHfNJ4T+rEqp2pOEnwRTvR5My5tFpnhRAE8Lhg3b7I5fR24H2fr/9paEEFgWMjBXAEOVxbB3gybQ0GcgZXElNRhgqIcAcmAZ6XRTeaNXCECwYHB4QFrqSlFD8DqwcRrqg4a6oHigC4MGoEqwYLwJlqtTTHWGQSxAzMRs4SA74LBAXXgBQDBZYVCQAh+QQJCgAAACwAAAAAIAAgAAADuQi6zDGhyUnXuC3EmrfF38A5lxguDzgqpVeG3pqawAvMa9Oy2BzfI1+tZ1OkXAJBgbHLQIylp6KQTBKgP2eUpjhUBQduZZsFEL6C3HFEVeZuZUlg+a7beeQzOl3flvZJdn4Qemh3h2pxGXQyF4oKBAcHVytkEwMGkpJqfg2ZmgaPDxszUmagJnNiN0WspgOSlJAFBSIvQhw/c7QRNjh2tLV4MH3BJ1AqK8EuyU3Kwscoj046yYhypiMJACH5BAkKAAAALAAAAAAgACAAAAPACLrMMaHJSde4LcTaSGnXwIScIwjHiCnPWiqnICokfW0vUMTW2s452OkDIP1wrBIhFjHWWDcLiqg4nAySx+YHWexOh9lAQJ1wXQpDTGBAVkJRycDKe7Xcki8hqMkFykGBgXAhDwYHiImChCGJjimDjIaPkIKWfnhmQCV3JQMFBZtvcJl/oKB+jB2nBQSZC1pQhRanBDOxGU8AnSygm7NOcRVuR0U+ujmzxkDKqS5PP5FdNr/CnmjIfXxuyJdZ0y8JADs=)';
				statusIcon.style.cursor = 'inherit';
				break;
			case 'play':
				statusIcon.style.display = 'block';
				statusIcon.style.backgroundImage = 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAclBMVEX///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8GMOiGAAAAJXRSTlMAeAIBJwX5egTIKvd0LMYl9W/AIfRpzrsf8WO3G+5fsRjtWKwWm6Sr4gAAAIBJREFUeF6l01sSgjAMhWEs3kBtFVRQ0NbLv/8tOm4g5yF5/mYyuZzqX8tVZdfiehCA9WZrA2haAdjtgw0gJgE4noINoOsF4HypbQDDKAC3e20DmGYBeDwFgFwE4PUWAD5fCVwtcnGNOc2uVQ+j69xd73q5mFxv37SO6Inwivj/AIKqMd+eZ3xLAAAAAElFTkSuQmCC)';
				statusIcon.style.cursor = 'pointer';
				break;
			default:
				statusIcon.style.display = 'none';
		}
	};
	if (opts.onClickIcon) {
		statusIcon.onclick = opts.onClickIcon;
	}

	self.setResolution = function(width, height) {
		container.style.width = width * scaleFactor + 'px';
		
		self.canvas.width = width;
		self.canvas.height = height;
		
		self.canvas.style.width = width * scaleFactor + 'px';
		self.canvas.style.height = height * scaleFactor + 'px';
		statusIcon.style.top = (height * scaleFactor / 2 - 32) + 'px';
		statusIcon.style.left = (width * scaleFactor / 2 - 32) + 'px';
	};

	return self;
};
// Generated by CoffeeScript 2.5.1
(function() {
  /*
  Z80 core.
  To avoid mass repetition of code across the numerous instruction variants 
  the code for this component is built up programmatically and evaluated in
  the global scope. CoffeeScript is used here for its support of multi-line
  strings, and expression interpolation in strings.
  */
  /*
  Registers are stored in a typed array as a way of automatically casting
  calculations to 8/16 bit, and to allow accessing them interchangeably as
  register pairs or individual registers by having two arrays backed by the
  same buffer. For the latter to work, we need to find out the endianness
  of the host processor, as typed arrays are native-endian
  	(http://lists.w3.org/Archives/Public/public-script-coord/2010AprJun/0048.html, 
  	http://cat-in-136.blogspot.com/2011/03/javascript-typed-array-use-native.html)
  */
  window.JSSpeccy.buildZ80 = function(opts) {
    /*
    	Opcode generator functions: each returns a string of Javascript that performs the opcode
    	when executed within this module's scope. Note that instructions with DDCBnn opcodes also
    	require an 'offset' variable to be defined as nn (as a signed byte).
    */
    /*
    Assemble and evaluate the final JS code for the Z80 component.
    The indirection on 'eval' causes most browsers to evaluate it in the global
    scope, giving a significant speed boost
    */
    /*
    	Boilerplate generator: a helper to deal with classes of opcodes which perform
    	the same task on different types of operands: e.g. XOR B, XOR (HL), XOR nn, XOR (IX+nn).
    	This function accepts the parameter in question, and returns a set of canned strings
    	for use in the opcode runner body:
    	'getter': a block of code that performs any necessary memory access etc in order to
    		make 'v' a valid expression;
    	'v': an expression with no side effects, evaluating to the operand's value. (Must also be a valid lvalue for assignment)
    	'trunc': an expression such as '& 0xff' to truncate v back to its proper range, if appropriate
    	'setter': a block of code that writes an updated value back to its proper location, if any

    	Passing hasIXOffsetAlready = true indicates that we have already read the offset value of (IX+nn)/(IY+nn)
    	into a variable 'offset' (necessary because DDCB/FFCB instructions put this before the final opcode byte).
    */
    /*
    Given a table mapping opcodes to Javascript snippets (and optionally a fallback
    table for opcodes that aren't defined in the first one), build an enormous
    switch statement for them
    */
    var ADC_A, ADC_HL_RR, ADD_A, ADD_RR_RR, AND_A, BIT_N_R, BIT_N_iHLi, BIT_N_iRRpNNi, CALL_C_NN, CALL_NN, CCF, CPD, CPDR, CPI, CPIR, CPIR_CPDR, CPI_CPD, CPL, CP_A, DAA, DEC, DEC_RR, DI, DJNZ_N, EI, EXX, EX_RR_RR, EX_iSPi_RR, FLAG_3, FLAG_5, FLAG_C, FLAG_H, FLAG_N, FLAG_P, FLAG_S, FLAG_V, FLAG_Z, HALT, IM, INC, INC_RR, IND, INDR, INI, INIR, INIR_INDR, INI_IND, IN_A_N, IN_F_iCi, IN_R_iCi, JP_C_NN, JP_NN, JP_RR, JR_C_N, JR_N, LDBITOP, LDD, LDDR, LDI, LDIR, LDIR_LDDR, LDI_LDD, LDSHIFTOP, LD_A_iNNi, LD_RR_NN, LD_RR_RR, LD_RR_iNNi, LD_R_N, LD_R_R, LD_R_iRRi, LD_R_iRRpNNi, LD_iNNi_A, LD_iNNi_RR, LD_iRRi_N, LD_iRRi_R, LD_iRRpNNi_N, LD_iRRpNNi_R, NEG, NOP, OPCODE_RUN_STRINGS, OPCODE_RUN_STRINGS_CB, OPCODE_RUN_STRINGS_DD, OPCODE_RUN_STRINGS_DDCB, OPCODE_RUN_STRINGS_ED, OPCODE_RUN_STRINGS_FD, OPCODE_RUN_STRINGS_FDCB, OR_A, OTDR, OTIR, OTIR_OTDR, OUTD, OUTI, OUTI_OUTD, OUT_iCi_0, OUT_iCi_R, OUT_iNi_A, POP_RR, PUSH_RR, RES, RET, RETN, RET_C, RL, RLA, RLC, RLCA, RLD, RR, RRA, RRC, RRCA, RRD, RST, SBC_A, SBC_HL_RR, SCF, SET, SHIFT, SLA, SLL, SRA, SRL, SUB_A, XOR_A, defineZ80JS, endianTestBuffer, endianTestUint16, endianTestUint8, generateddfdOpcodeSet, generateddfdcbOpcodeSet, getParamBoilerplate, indirectEval, isBigEndian, opcodeSwitch, rA, rA_, rB, rB_, rC, rC_, rD, rD_, rE, rE_, rF, rF_, rH, rH_, rI, rIXH, rIXL, rIYH, rIYL, rL, rL_, rR, registerIndexes, registerPairIndexes, rpAF, rpAF_, rpBC, rpBC_, rpDE, rpDE_, rpHL, rpHL_, rpIR, rpIX, rpIY, rpPC, rpSP, setUpStateJS;
    if (opts == null) {
      opts = {};
    }
    endianTestBuffer = new ArrayBuffer(2);
    endianTestUint16 = new Uint16Array(endianTestBuffer);
    endianTestUint8 = new Uint8Array(endianTestBuffer);
    endianTestUint16[0] = 0x0100;
    isBigEndian = endianTestUint8[0] === 0x01;
    // Offsets into register set when read as register pairs
    rpAF = 0;
    rpBC = 1;
    rpDE = 2;
    rpHL = 3;
    rpAF_ = 4;
    rpBC_ = 5;
    rpDE_ = 6;
    rpHL_ = 7;
    rpIX = 8;
    rpIY = 9;
    rpIR = 10;
    rpSP = 11;
    rpPC = 12;
    registerPairIndexes = {
      'IX': 8,
      'IY': 9
    };
    if (isBigEndian) {
      rA = 0;
      rF = 1;
      rB = 2;
      rC = 3;
      rD = 4;
      rE = 5;
      rH = 6;
      rL = 7;
      rA_ = 8;
      rF_ = 9;
      rB_ = 10;
      rC_ = 11;
      rD_ = 12;
      rE_ = 13;
      rH_ = 14;
      rL_ = 15;
      rIXH = 16;
      rIXL = 17;
      rIYH = 18;
      rIYL = 19;
      rI = 20;
      rR = 21;
      registerIndexes = {
        A: 0,
        F: 1,
        B: 2,
        C: 3,
        D: 4,
        E: 5,
        H: 6,
        L: 7,
        IXH: 16,
        IXL: 17,
        IYH: 18,
        IYL: 19
      };
    } else {
      // little-endian
      rF = 0;
      rA = 1;
      rC = 2;
      rB = 3;
      rE = 4;
      rD = 5;
      rL = 6;
      rH = 7;
      rF_ = 8;
      rA_ = 9;
      rC_ = 10;
      rB_ = 11;
      rE_ = 12;
      rD_ = 13;
      rL_ = 14;
      rH_ = 15;
      rIXL = 16;
      rIXH = 17;
      rIYL = 18;
      rIYH = 19;
      rR = 20;
      rI = 21;
      registerIndexes = {
        F: 0,
        A: 1,
        C: 2,
        B: 3,
        E: 4,
        D: 5,
        L: 6,
        H: 7,
        IXL: 16,
        IXH: 17,
        IYL: 18,
        IYH: 19
      };
    }
    FLAG_C = 0x01;
    FLAG_N = 0x02;
    FLAG_P = 0x04;
    FLAG_V = 0x04;
    FLAG_3 = 0x08;
    FLAG_H = 0x10;
    FLAG_5 = 0x20;
    FLAG_Z = 0x40;
    FLAG_S = 0x80;
    // JS block setting up internal Z80 state and lookup tables
    setUpStateJS = `var memory = opts.memory;
var ioBus = opts.ioBus;
var display = opts.display;

var registerBuffer = new ArrayBuffer(26);
/* Expose registerBuffer as both register pairs and individual registers */
var regPairs = new Uint16Array(registerBuffer);
var regs = new Uint8Array(registerBuffer);

var tstates = 0; /* number of tstates since start of this frame */
var iff1 = 0;
var iff2 = 0;
var im = 0;
var halted = false;

/* tables for setting Z80 flags */

/*
	Whether a half carry occurred or not can be determined by looking at
	the 3rd bit of the two arguments and the result; these are hashed
	into this table in the form r12, where r is the 3rd bit of the
	result, 1 is the 3rd bit of the 1st argument and 2 is the
	third bit of the 2nd argument; the tables differ for add and subtract
	operations
*/
var halfcarryAddTable = new Uint8Array([0, ${FLAG_H}, ${FLAG_H}, ${FLAG_H}, 0, 0, 0, ${FLAG_H}]);
var halfcarrySubTable = new Uint8Array([0, 0, ${FLAG_H}, 0, ${FLAG_H}, 0, ${FLAG_H}, ${FLAG_H}]);

/*
	Similarly, overflow can be determined by looking at the 7th bits; again
	the hash into this table is r12
*/
var overflowAddTable = new Uint8Array([0, 0, 0, ${FLAG_V}, ${FLAG_V}, 0, 0, 0]);
var overflowSubTable = new Uint8Array([0, ${FLAG_V}, 0, 0, 0, 0, ${FLAG_V}, 0]);

var sz53Table = new Uint8Array(0x100); /* The S, Z, 5 and 3 bits of the index */
var parityTable = new Uint8Array(0x100); /* The parity of the lookup value */
var sz53pTable = new Uint8Array(0x100); /* OR the above two tables together */

for (var i = 0; i < 0x100; i++) {
	sz53Table[i] = i & ( ${FLAG_3 | FLAG_5 | FLAG_S} );
	var j = i;
	var parity = 0;
	for (var k = 0; k < 8; k++) {
		parity ^= j & 1;
		j >>=1;
	}

	parityTable[i] = (parity ? 0 : ${FLAG_P});
	sz53pTable[i] = sz53Table[i] | parityTable[i];
	
	sz53Table[0] |= ${FLAG_Z};
	sz53pTable[0] |= ${FLAG_Z};
}

var interruptible = true;
var interruptPending = false;
var opcodePrefix = '';`;
    getParamBoilerplate = function(param, hasIXOffsetAlready = false) {
      var getter, match, regNum, rp;
      if (param.match(/^[AFBCDEHL]|I[XY][HL]$/)) {
        regNum = registerIndexes[param];
        return {
          'getter': '',
          'v': `regs[${regNum}]`,
          'trunc': '',
          'setter': ''
        };
      } else if (param === '(HL)') {
        return {
          'getter': `var val = READMEM(regPairs[${rpHL}]);`,
          'v': 'val',
          'trunc': '& 0xff',
          'setter': `CONTEND_READ_NO_MREQ(regPairs[${rpHL}], 1);
WRITEMEM(regPairs[${rpHL}], val);`
        };
      } else if (param === 'nn') {
        return {
          'getter': `var val = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;`,
          'v': 'val',
          'trunc': '& 0xff',
          'setter': ''
        };
      } else if ((match = param.match(/^\((I[XY])\+nn\)$/))) {
        rp = registerPairIndexes[match[1]];
        if (hasIXOffsetAlready) {
          getter = '';
        } else {
          getter = `var offset = READMEM(regPairs[${rpPC}]);
if (offset & 0x80) offset -= 0x100;
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
regPairs[${rpPC}]++;`;
        }
        getter += `var addr = (regPairs[${rp}] + offset) & 0xffff;
var val = READMEM(addr);`;
        return {
          'getter': getter,
          'v': 'val',
          'trunc': '& 0xff',
          'setter': `CONTEND_READ_NO_MREQ(addr, 1);
WRITEMEM(addr, val);`
        };
      } else if (param === 'add') {
        return {
          // special case for incorporating ADD/SUB into DAA calculation using a custom variable 'add'
          'getter': '',
          'v': 'add',
          'trunc': '',
          'setter': ''
        };
      } else {
        throw `Unknown param format: ${param}`;
      }
    };
    ADC_A = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}

var adctemp = regs[${rA}] + ${operand.v} + (regs[${rF}] & ${FLAG_C});
var lookup = ( (regs[${rA}] & 0x88) >> 3 ) | ( (${operand.v} & 0x88) >> 2 ) | ( (adctemp & 0x88) >> 1 );
regs[${rA}] = adctemp;
regs[${rF}] = ( adctemp & 0x100 ? ${FLAG_C} : 0 ) | halfcarryAddTable[lookup & 0x07] | overflowAddTable[lookup >> 4] | sz53Table[regs[${rA}]];`;
    };
    ADC_HL_RR = function(rp2) {
      return `var add16temp = regPairs[${rpHL}] + regPairs[${rp2}] + (regs[${rF}] & ${FLAG_C});
var lookup = (
	( (regPairs[${rpHL}] & 0x8800) >> 11 ) |
	( (regPairs[${rp2}] & 0x8800) >> 10 ) |
	( (add16temp & 0x8800) >>  9 )
);
regPairs[${rpHL}] = add16temp;
regs[${rF}] = (
	(add16temp & 0x10000 ? ${FLAG_C} : 0) |
	overflowAddTable[lookup >> 4] |
	(regs[${rH}] & ${FLAG_3 | FLAG_5 | FLAG_S}) |
	halfcarryAddTable[lookup & 0x07] |
	(regPairs[${rpHL}] ? 0 : ${FLAG_Z})
);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);`;
    };
    ADD_A = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}

var addtemp = regs[${rA}] + ${operand.v};
var lookup = ( (regs[${rA}] & 0x88) >> 3 ) | ( (${operand.v} & 0x88) >> 2 ) | ( (addtemp & 0x88) >> 1 );
regs[${rA}] = addtemp;
regs[${rF}] = ( addtemp & 0x100 ? ${FLAG_C} : 0 ) | halfcarryAddTable[lookup & 0x07] | overflowAddTable[lookup >> 4] | sz53Table[regs[${rA}]];`;
    };
    ADD_RR_RR = function(rp1, rp2) {
      return `var add16temp = regPairs[${rp1}] + regPairs[${rp2}];
var lookup = ( (regPairs[${rp1}] & 0x0800) >> 11 ) | ( (regPairs[${rp2}] & 0x0800) >> 10 ) | ( (add16temp & 0x0800) >>  9 );
regPairs[${rp1}] = add16temp;
regs[${rF}] = ( regs[${rF}] & ( ${FLAG_V | FLAG_Z | FLAG_S} ) ) | ( add16temp & 0x10000 ? ${FLAG_C} : 0 ) | ( ( add16temp >> 8 ) & ( ${FLAG_3 | FLAG_5} ) ) | halfcarryAddTable[lookup];
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);`;
    };
    AND_A = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}

regs[${rA}] &= ${operand.v};
regs[${rF}] = ${FLAG_H} | sz53pTable[regs[${rA}]];`;
    };
    BIT_N_iRRpNNi = function(bit, rp) { // requires 'offset'
      var updateSignFlag;
      if (bit === 7) {
        updateSignFlag = `if (value & 0x80) regs[${rF}] |= ${FLAG_S};`;
      } else {
        updateSignFlag = "";
      }
      return `var addr = (regPairs[${rp}] + offset) & 0xffff;
var value = READMEM(addr);
regs[${rF}] = ( regs[${rF}] & ${FLAG_C} ) | ${FLAG_H} | ( ( addr >> 8 ) & ${FLAG_3 | FLAG_5} );
if ( !(value & ${0x01 << bit}) ) regs[${rF}] |= ${FLAG_P | FLAG_Z};
${updateSignFlag}
CONTEND_READ_NO_MREQ(addr, 1);`;
    };
    BIT_N_iHLi = function(bit) {
      var updateSignFlag;
      if (bit === 7) {
        updateSignFlag = `if (value & 0x80) regs[${rF}] |= ${FLAG_S};`;
      } else {
        updateSignFlag = "";
      }
      return `var addr = regPairs[${rpHL}];
var value = READMEM(addr);
CONTEND_READ_NO_MREQ(addr, 1);
regs[${rF}] = ( regs[${rF}] & ${FLAG_C} ) | ${FLAG_H} | ( value & ${FLAG_3 | FLAG_5} );
if( !(value & ${0x01 << bit}) ) regs[${rF}] |= ${FLAG_P | FLAG_Z};
${updateSignFlag}`;
    };
    BIT_N_R = function(bit, r) {
      var updateSignFlag;
      if (bit === 7) {
        updateSignFlag = `if (regs[${r}] & 0x80) regs[${rF}] |= ${FLAG_S};`;
      } else {
        updateSignFlag = "";
      }
      return `regs[${rF}] = ( regs[${rF}] & ${FLAG_C} ) | ${FLAG_H} | ( regs[${r}] & ${FLAG_3 | FLAG_5} );
if( !(regs[${r}] & ${0x01 << bit}) ) regs[${rF}] |= ${FLAG_P | FLAG_Z};
${updateSignFlag}`;
    };
    CALL_C_NN = function(flag, sense) {
      var condition;
      condition = `regs[${rF}] & ${flag}`;
      if (!sense) {
        condition = `!(${condition})`;
      }
      return `if (${condition}) {
	var l = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
	var h = READMEM(regPairs[${rpPC}]);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	regPairs[${rpPC}]++;
	regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rpPC}] >> 8);
	regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rpPC}] & 0xff);
	regPairs[${rpPC}] = (h<<8) | l;
} else {
	CONTEND_READ(regPairs[${rpPC}], 3);
	regPairs[${rpPC}]++;
	CONTEND_READ(regPairs[${rpPC}], 3);
	regPairs[${rpPC}]++;
}`;
    };
    CALL_NN = function() {
      return `var l = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var h = READMEM(regPairs[${rpPC}]);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
regPairs[${rpPC}]++;
regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rpPC}] >> 8);
regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rpPC}] & 0xff);
regPairs[${rpPC}] = (h<<8) | l;`;
    };
    CCF = function() {
      return `regs[${rF}] = ( regs[${rF}] & ${FLAG_P | FLAG_Z | FLAG_S} ) | ( (regs[${rF}] & ${FLAG_C}) ? ${FLAG_H} : ${FLAG_C} ) | ( regs[${rA}] & ${FLAG_3 | FLAG_5} );`;
    };
    CP_A = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}

var cptemp = regs[${rA}] - ${operand.v};
var lookup = ( (regs[${rA}] & 0x88) >> 3 ) | ( (${operand.v} & 0x88) >> 2 ) | ( (cptemp & 0x88) >> 1 );
regs[${rF}] = ( cptemp & 0x100 ? ${FLAG_C} : ( cptemp ? 0 : ${FLAG_Z} ) ) | ${FLAG_N} | halfcarrySubTable[lookup & 0x07] | overflowSubTable[lookup >> 4] | ( ${operand.v} & ${FLAG_3 | FLAG_5} ) | ( cptemp & ${FLAG_S} );`;
    };
    CPI_CPD = function(modifier) {
      return `var value = READMEM(regPairs[${rpHL}]);
var bytetemp = (regs[${rA}] - value) & 0xff;
var lookup = ((regs[${rA}] & 0x08) >> 3) | ((value & 0x08) >> 2) | ((bytetemp & 0x08) >> 1);
var originalHL = regPairs[${rpHL}];
CONTEND_READ_NO_MREQ(originalHL, 1);
CONTEND_READ_NO_MREQ(originalHL, 1);
CONTEND_READ_NO_MREQ(originalHL, 1);
CONTEND_READ_NO_MREQ(originalHL, 1);
CONTEND_READ_NO_MREQ(originalHL, 1);
regPairs[${rpHL}]${modifier}; regPairs[${rpBC}]--;
regs[${rF}] = (regs[${rF}] & ${FLAG_C}) | (regPairs[${rpBC}] ? ${FLAG_V | FLAG_N} : ${FLAG_N}) | halfcarrySubTable[lookup] | (bytetemp ? 0 : ${FLAG_Z}) | (bytetemp & ${FLAG_S});
if (regs[${rF}] & ${FLAG_H}) bytetemp--;
regs[${rF}] |= (bytetemp & ${FLAG_3}) | ( (bytetemp & 0x02) ? ${FLAG_5} : 0 );`;
    };
    CPIR_CPDR = function(modifier) {
      return `${CPI_CPD(modifier)}
if ((regs[${rF}] & ${FLAG_V | FLAG_Z}) == ${FLAG_V}) {
	regPairs[${rpPC}] -= 2;
	CONTEND_READ_NO_MREQ(originalHL, 1);
	CONTEND_READ_NO_MREQ(originalHL, 1);
	CONTEND_READ_NO_MREQ(originalHL, 1);
	CONTEND_READ_NO_MREQ(originalHL, 1);
	CONTEND_READ_NO_MREQ(originalHL, 1);
}`;
    };
    CPD = function() {
      return CPI_CPD('--');
    };
    CPI = function() {
      return CPI_CPD('++');
    };
    CPDR = function() {
      return CPIR_CPDR('--');
    };
    CPIR = function() {
      return CPIR_CPDR('++');
    };
    DAA = function() {
      var addClause, subClause;
      subClause = SUB_A('add');
      addClause = ADD_A('add');
      return `var add = 0;
var carry = regs[${rF}] & ${FLAG_C};
if( ( regs[${rF}] & ${FLAG_H} ) || ( ( regs[${rA}] & 0x0f ) > 9 ) ) add = 6;
if( carry || ( regs[${rA}] > 0x99 ) ) add |= 0x60;
if( regs[${rA}] > 0x99 ) carry = ${FLAG_C};
if( regs[${rF}] & ${FLAG_N} ) {
	${subClause}
} else {
	${addClause}
}
regs[${rF}] = ( regs[${rF}] & ${~(FLAG_C | FLAG_P)} ) | carry | parityTable[regs[${rA}]];`;
    };
    CPL = function() {
      return `regs[${rA}] ^= 0xff;
regs[${rF}] = (regs[${rF}] & ${FLAG_C | FLAG_P | FLAG_Z | FLAG_S}) | (regs[${rA}] & ${FLAG_3 | FLAG_5}) | ${FLAG_N | FLAG_H};`;
    };
    DEC = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}

regs[${rF}] = (regs[${rF}] & ${FLAG_C} ) | ( ${operand.v} & 0x0f ? 0 : ${FLAG_H} ) | ${FLAG_N};
${operand.v} = (${operand.v} - 1) ${operand.trunc};

${operand.setter}
regs[${rF}] |= (${operand.v} == 0x7f ? ${FLAG_V} : 0) | sz53Table[${operand.v}];`;
    };
    DEC_RR = function(rp) {
      return `regPairs[${rp}]--;
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);`;
    };
    DI = function() {
      return `iff1 = iff2 = 0;`;
    };
    DJNZ_N = function() {
      return `CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
regs[${rB}]--;
if (regs[${rB}]) {
	/* take branch */
	var offset = READMEM(regPairs[${rpPC}]);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	regPairs[${rpPC}]++;
	regPairs[${rpPC}] += (offset & 0x80 ? offset - 0x100 : offset);
} else {
	/* do not take branch */
	CONTEND_READ(regPairs[${rpPC}], 3);
	regPairs[${rpPC}]++;
}`;
    };
    EI = function() {
      return `iff1 = iff2 = 1;
interruptible = false;`;
    };
    EX_iSPi_RR = function(rp) {
      return `var l = READMEM(regPairs[${rpSP}]);
var spPlus1 = (regPairs[${rpSP}] + 1) & 0xffff;
var h = READMEM(spPlus1);
CONTEND_READ_NO_MREQ(spPlus1, 1);
WRITEMEM(spPlus1, regPairs[${rp}] >> 8);
WRITEMEM(regPairs[${rpSP}], regPairs[${rp}] & 0xff);
regPairs[${rp}] = (h<<8) | l;
CONTEND_WRITE_NO_MREQ(regPairs[${rpSP}], 1);
CONTEND_WRITE_NO_MREQ(regPairs[${rpSP}], 1);`;
    };
    EX_RR_RR = function(rp1, rp2) {
      return `var temp = regPairs[${rp1}];
regPairs[${rp1}] = regPairs[${rp2}];
regPairs[${rp2}] = temp;`;
    };
    EXX = function() {
      return `var wordtemp;
wordtemp = regPairs[${rpBC}]; regPairs[${rpBC}] = regPairs[${rpBC_}]; regPairs[${rpBC_}] = wordtemp;
wordtemp = regPairs[${rpDE}]; regPairs[${rpDE}] = regPairs[${rpDE_}]; regPairs[${rpDE_}] = wordtemp;
wordtemp = regPairs[${rpHL}]; regPairs[${rpHL}] = regPairs[${rpHL_}]; regPairs[${rpHL_}] = wordtemp;`;
    };
    HALT = function() {
      return `halted = true;
regPairs[${rpPC}]--;`;
    };
    IM = function(val) {
      return `im = ${val};`;
    };
    IN_A_N = function() {
      return `var val = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var port = (regs[${rA}] << 8) | val;
CONTEND_PORT_EARLY(port);
regs[${rA}] = ioBus.read(port);
CONTEND_PORT_LATE(port);`;
    };
    IN_F_iCi = function() {
      // as IN_R_iCi, but result is written to a local variable rather than a register
      return `var port = regPairs[${rpBC}];
CONTEND_PORT_EARLY(port);
var result = ioBus.read(port);
CONTEND_PORT_LATE(port);
regs[${rF}] = (regs[${rF}] & ${FLAG_C}) | sz53pTable[result];`;
    };
    IN_R_iCi = function(r) {
      return `var port = regPairs[${rpBC}];
CONTEND_PORT_EARLY(port);
regs[${r}] = ioBus.read(port);
CONTEND_PORT_LATE(port);
regs[${rF}] = (regs[${rF}] & ${FLAG_C}) | sz53pTable[regs[${r}]];`;
    };
    INC = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}

regs[${rF}] = (regs[${rF}] & ${FLAG_C}) | (${operand.v} & 0x0f ? 0 : ${FLAG_H}) | ${FLAG_N};
${operand.v} = (${operand.v} + 1) ${operand.trunc};

${operand.setter}
regs[${rF}] = (regs[${rF}] & ${FLAG_C}) | ( ${operand.v} == 0x80 ? ${FLAG_V} : 0 ) | ( ${operand.v} & 0x0f ? 0 : ${FLAG_H} ) | sz53Table[${operand.v}];`;
    };
    INC_RR = function(rp) {
      return `regPairs[${rp}]++;
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);`;
    };
    INI_IND = function(modifier) {
      return `CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_PORT_EARLY(regPairs[${rpBC}]);
var initemp = ioBus.read(regPairs[${rpBC}]);
CONTEND_PORT_LATE(regPairs[${rpBC}]);
WRITEMEM(regPairs[${rpHL}], initemp);
regs[${rB}]--;
var originalHL = regPairs[${rpHL}];
regPairs[${rpHL}]${modifier}${modifier};
var initemp2 = (initemp + regs[${rC}] ${modifier} 1) & 0xff;

regs[${rF}] = (initemp & 0x80 ? ${FLAG_N} : 0) | ((initemp2 < initemp) ? ${FLAG_H | FLAG_C} : 0 ) | ( parityTable[ (initemp2 & 0x07) ^ regs[${rB}] ] ? ${FLAG_P} : 0 ) | sz53Table[regs[${rB}]];`;
    };
    INIR_INDR = function(modifier) {
      return `${INI_IND(modifier)}
if (regs[${rB}]) {
	CONTEND_WRITE_NO_MREQ(originalHL, 1);
	CONTEND_WRITE_NO_MREQ(originalHL, 1);
	CONTEND_WRITE_NO_MREQ(originalHL, 1);
	CONTEND_WRITE_NO_MREQ(originalHL, 1);
	CONTEND_WRITE_NO_MREQ(originalHL, 1);
	regPairs[${rpPC}] -= 2;
}`;
    };
    INI = function() {
      return INI_IND('+');
    };
    IND = function() {
      return INI_IND('-');
    };
    INIR = function() {
      return INIR_INDR('+');
    };
    INDR = function() {
      return INIR_INDR('-');
    };
    JP_C_NN = function(flag, sense) {
      var condition;
      condition = `regs[${rF}] & ${flag}`;
      if (!sense) {
        condition = `!(${condition})`;
      }
      return `if (${condition}) {
	var l = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
	var h = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
	regPairs[${rpPC}] = (h<<8) | l;
} else {
	CONTEND_READ(regPairs[${rpPC}], 3);
	regPairs[${rpPC}]++;
	CONTEND_READ(regPairs[${rpPC}], 3);
	regPairs[${rpPC}]++;
}`;
    };
    JP_RR = function(rp) {
      return `regPairs[${rpPC}] = regPairs[${rp}];`;
    };
    JP_NN = function() {
      return `var l = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var h = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
regPairs[${rpPC}] = (h<<8) | l;`;
    };
    JR_C_N = function(flag, sense) {
      var condition;
      condition = `regs[${rF}] & ${flag}`;
      if (!sense) {
        condition = `!(${condition})`;
      }
      return `if (${condition}) {
	var offset = READMEM(regPairs[${rpPC}]);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
	regPairs[${rpPC}]++;
	regPairs[${rpPC}] += (offset & 0x80 ? offset - 0x100 : offset);
} else {
	CONTEND_READ(regPairs[${rpPC}], 3);
	regPairs[${rpPC}]++; /* skip past offset byte */
}`;
    };
    JR_N = function() {
      return `var offset = READMEM(regPairs[${rpPC}]);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
regPairs[${rpPC}]++;
regPairs[${rpPC}] += (offset & 0x80 ? offset - 0x100 : offset);`;
    };
    LD_A_iNNi = function() {
      return `var l = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var h = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var addr = (h<<8) | l;
regs[${rA}] = READMEM(addr);`;
    };
    LD_iNNi_A = function() {
      return `var l = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var h = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var addr = (h<<8) | l;
WRITEMEM(addr, regs[${rA}]);`;
    };
    LD_iNNi_RR = function(rp) {
      return `var l = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var h = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var addr = (h<<8) | l;
WRITEMEM(addr, regPairs[${rp}] & 0xff);
addr = (addr + 1) & 0xffff;
WRITEMEM(addr, regPairs[${rp}] >> 8);`;
    };
    LD_iRRi_N = function(rp) {
      return `var n = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
WRITEMEM(regPairs[${rp}], n);`;
    };
    LD_iRRi_R = function(rp, r) {
      return `WRITEMEM(regPairs[${rp}], regs[${r}]);`;
    };
    LD_iRRpNNi_N = function(rp) {
      return `var offset = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
if (offset & 0x80) offset -= 0x100;
var addr = (regPairs[${rp}] + offset) & 0xffff;

var val = READMEM(regPairs[${rpPC}]);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
regPairs[${rpPC}]++;
WRITEMEM(addr, val);`;
    };
    LD_iRRpNNi_R = function(rp, r) {
      return `var offset = READMEM(regPairs[${rpPC}]);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
regPairs[${rpPC}]++;
if (offset & 0x80) offset -= 0x100;
var addr = (regPairs[${rp}] + offset) & 0xffff;

WRITEMEM(addr, regs[${r}]);`;
    };
    LD_R_iRRi = function(r, rp) {
      return `regs[${r}] = READMEM(regPairs[${rp}]);`;
    };
    LD_R_iRRpNNi = function(r, rp) {
      return `var offset = READMEM(regPairs[${rpPC}]);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
regPairs[${rpPC}]++;
if (offset & 0x80) offset -= 0x100;
var addr = (regPairs[${rp}] + offset) & 0xffff;

regs[${r}] = READMEM(addr);`;
    };
    LD_R_N = function(r) {
      return `regs[${r}] = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;`;
    };
    LD_R_R = function(r1, r2) {
      var output;
      if (r1 === rI || r2 === rI || r1 === rR || r2 === rR) {
        output = `CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
regs[${r1}] = regs[${r2}];`;
        if (r1 === rA) {
          output += `regs[${rF}] = (regs[${rF}] & ${FLAG_C}) | sz53Table[regs[${rA}]] | ( iff2 ? ${FLAG_V} : 0 );`;
        }
        return output;
      } else {
        return `regs[${r1}] = regs[${r2}];`;
      }
    };
    LD_RR_iNNi = function(rp, shifted) {
      return `var l = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var h = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var addr = (h<<8) | l;
l = READMEM(addr);
addr = (addr + 1) & 0xffff;
h = READMEM(addr);
regPairs[${rp}] = (h<<8) | l;`;
    };
    LD_RR_NN = function(rp) {
      return `var l = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
var h = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
regPairs[${rp}] = (h<<8) | l;`;
    };
    LD_RR_RR = function(rp1, rp2) {
      // only used for LD SP,HL/IX/IY
      return `regPairs[${rp1}] = regPairs[${rp2}];
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);`;
    };
    LDBITOP = function(regName, opcode, bit, rp) {
      var regNum;
      // load (rp+nn) into register regName, perform opcode, write back to (rp+nn)
      regNum = registerIndexes[regName];
      return `var addr = (regPairs[${rp}] + offset) & 0xffff;
regs[${regNum}] = READMEM(addr);
${opcode(bit, regName)}
CONTEND_READ_NO_MREQ(addr, 1);
WRITEMEM(addr, regs[${regNum}]);`;
    };
    LDI_LDD = function(modifier) {
      return `var bytetemp = READMEM(regPairs[${rpHL}]);
regPairs[${rpBC}]--;
WRITEMEM(regPairs[${rpDE}],bytetemp);
var originalDE = regPairs[${rpDE}];
regPairs[${rpDE}]${modifier}; regPairs[${rpHL}]${modifier};
bytetemp = (bytetemp + regs[${rA}]) & 0xff;
regs[${rF}] = (regs[${rF}] & ${FLAG_C | FLAG_Z | FLAG_S}) | (regPairs[${rpBC}] ? ${FLAG_V} : 0) | (bytetemp & ${FLAG_3}) | ((bytetemp & 0x02) ? ${FLAG_5} : 0);
CONTEND_READ_NO_MREQ(originalDE, 1);
CONTEND_READ_NO_MREQ(originalDE, 1);`;
    };
    LDIR_LDDR = function(modifier) {
      return `${LDI_LDD(modifier)}
if (regPairs[${rpBC}]) {
	regPairs[${rpPC}]-=2;
	CONTEND_READ_NO_MREQ(originalDE, 1);
	CONTEND_READ_NO_MREQ(originalDE, 1);
	CONTEND_READ_NO_MREQ(originalDE, 1);
	CONTEND_READ_NO_MREQ(originalDE, 1);
	CONTEND_READ_NO_MREQ(originalDE, 1);
}`;
    };
    LDI = function() {
      return LDI_LDD('++');
    };
    LDD = function() {
      return LDI_LDD('--');
    };
    LDIR = function() {
      return LDIR_LDDR('++');
    };
    LDDR = function() {
      return LDIR_LDDR('--');
    };
    LDSHIFTOP = function(regName, opcode, rp) {
      var regNum;
      // load (rp+nn) into register regName, perform opcode, write back to (rp+nn)
      regNum = registerIndexes[regName];
      return `var addr = (regPairs[${rp}] + offset) & 0xffff;
regs[${regNum}] = READMEM(addr);
${opcode(regName)}
CONTEND_READ_NO_MREQ(addr, 1);
WRITEMEM(addr, regs[${regNum}]);`;
    };
    NEG = function() {
      return `var val = regs[${rA}];
var subtemp = -val;
var lookup = ( (val & 0x88) >> 2 ) | ( (subtemp & 0x88) >> 1 );
regs[${rA}] = subtemp;
regs[${rF}] = ( subtemp & 0x100 ? ${FLAG_C} : 0 ) | ${FLAG_N} | halfcarrySubTable[lookup & 0x07] | overflowSubTable[lookup >> 4] | sz53Table[regs[${rA}]];`;
    };
    NOP = function() {
      return `		`;
    };
    OR_A = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}

regs[${rA}] |= ${operand.v};
regs[${rF}] = sz53pTable[regs[${rA}]];`;
    };
    OUT_iCi_0 = function(r) {
      return `CONTEND_PORT_EARLY(regPairs[${rpBC}]);
ioBus.write(regPairs[${rpBC}], 0, tstates);
CONTEND_PORT_LATE(regPairs[${rpBC}]);`;
    };
    OUT_iCi_R = function(r) {
      return `CONTEND_PORT_EARLY(regPairs[${rpBC}]);
ioBus.write(regPairs[${rpBC}], regs[${r}], tstates);
CONTEND_PORT_LATE(regPairs[${rpBC}]);`;
    };
    OUT_iNi_A = function() {
      return `var port = (regs[${rA}] << 8) | READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
CONTEND_PORT_EARLY(port);
ioBus.write(port, regs[${rA}], tstates);
CONTEND_PORT_LATE(port);`;
    };
    OUTI_OUTD = function(modifier) {
      return `CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
var outitemp = READMEM(regPairs[${rpHL}]);
regs[${rB}]--;	/* This does happen first, despite what the specs say */
CONTEND_PORT_EARLY(regPairs[${rpBC}]);
ioBus.write(regPairs[${rpBC}], outitemp, tstates);
CONTEND_PORT_LATE(regPairs[${rpBC}]);

regPairs[${rpHL}]${modifier};
outitemp2 = (outitemp + regs[${rL}]) & 0xff;
regs[${rF}] = (outitemp & 0x80 ? ${FLAG_N} : 0) | ( (outitemp2 < outitemp) ? ${FLAG_H | FLAG_C} : 0) | (parityTable[ (outitemp2 & 0x07) ^ regs[${rB}] ] ? ${FLAG_P} : 0 ) | sz53Table[ regs[${rB}] ];`;
    };
    OTIR_OTDR = function(modifier) {
      return `${OUTI_OUTD(modifier)}
if (regs[${rB}]) {
	regPairs[${rpPC}]-=2;
	CONTEND_READ_NO_MREQ(regPairs[${rpBC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpBC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpBC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpBC}], 1);
	CONTEND_READ_NO_MREQ(regPairs[${rpBC}], 1);
}`;
    };
    OUTD = function() {
      return OUTI_OUTD('--');
    };
    OUTI = function() {
      return OUTI_OUTD('++');
    };
    OTDR = function() {
      return OTIR_OTDR('--');
    };
    OTIR = function() {
      return OTIR_OTDR('++');
    };
    POP_RR = function(rp) {
      return `var l = READMEM(regPairs[${rpSP}]); regPairs[${rpSP}]++;
var h = READMEM(regPairs[${rpSP}]); regPairs[${rpSP}]++;
regPairs[${rp}] = (h<<8) | l;`;
    };
    PUSH_RR = function(rp) {
      return `CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rp}] >> 8);
regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rp}] & 0xff);`;
    };
    RES = function(bit, param) {
      var hexMask, operand;
      operand = getParamBoilerplate(param, true);
      hexMask = 0xff ^ (1 << bit);
      return `${operand.getter}
${operand.v} &= ${hexMask};
${operand.setter}`;
    };
    RET = function() {
      return `var l = READMEM(regPairs[${rpSP}]); regPairs[${rpSP}]++;
var h = READMEM(regPairs[${rpSP}]); regPairs[${rpSP}]++; 
regPairs[${rpPC}] = (h<<8) | l;`;
    };
    RET_C = function(flag, sense) {
      var condition;
      condition = `regs[${rF}] & ${flag}`;
      if (!sense) {
        condition = `!(${condition})`;
      }
      return `CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
if (${condition}) {
	var l = READMEM(regPairs[${rpSP}]); regPairs[${rpSP}]++;
	var h = READMEM(regPairs[${rpSP}]); regPairs[${rpSP}]++;
	regPairs[${rpPC}] = (h<<8) | l;
}`;
    };
    RETN = function() {
      return `iff1 = iff2;
${RET()}`;
    };
    RL = function(param) {
      var operand;
      operand = getParamBoilerplate(param, true);
      return `${operand.getter}
var rltemp = ${operand.v};
${operand.v} = ( (${operand.v} << 1) | (regs[${rF}] & ${FLAG_C}) ) ${operand.trunc};
regs[${rF}] = ( rltemp >> 7 ) | sz53pTable[${operand.v}];
${operand.setter}`;
    };
    RLA = function() {
      return `var bytetemp = regs[${rA}];
regs[${rA}] = (regs[${rA}] << 1) | (regs[${rF}] & ${FLAG_C});
regs[${rF}] = (regs[${rF}] & ${FLAG_P | FLAG_Z | FLAG_S}) | (regs[${rA}] & ${FLAG_3 | FLAG_5}) | (bytetemp >> 7);`;
    };
    RLC = function(param) {
      var operand;
      operand = getParamBoilerplate(param, true);
      return `${operand.getter}
${operand.v} = ( (${operand.v} << 1) | (${operand.v} >> 7) ) ${operand.trunc};
regs[${rF}] = (${operand.v} & ${FLAG_C}) | sz53pTable[${operand.v}];
${operand.setter}`;
    };
    RLD = function() {
      return `var bytetemp =  READMEM(regPairs[${rpHL}]);
CONTEND_READ_NO_MREQ(regPairs[${rpHL}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpHL}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpHL}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpHL}], 1);
var val = (bytetemp << 4) | (regs[${rA}] & 0x0f);
WRITEMEM(regPairs[${rpHL}], val);
regs[${rA}] = (regs[${rA}] & 0xf0) | (bytetemp >> 4);
regs[${rF}] = (regs[${rF}] & ${FLAG_C}) | sz53pTable[regs[${rA}]];`;
    };
    RLCA = function() {
      return `regs[${rA}] = (regs[${rA}] << 1) | (regs[${rA}] >> 7);
regs[${rF}] = (regs[${rF}] & ${FLAG_P | FLAG_Z | FLAG_S}) | (regs[${rA}] & ${FLAG_C | FLAG_3 | FLAG_5});`;
    };
    RR = function(param) {
      var operand;
      operand = getParamBoilerplate(param, true);
      return `${operand.getter}
var rrtemp = ${operand.v};
${operand.v} = ( (${operand.v} >> 1) | ( regs[${rF}] << 7 ) ) ${operand.trunc};
regs[${rF}] = (rrtemp & ${FLAG_C}) | sz53pTable[${operand.v}];
${operand.setter}`;
    };
    RRA = function() {
      return `var bytetemp = regs[${rA}];
regs[${rA}] = (bytetemp >> 1) | (regs[${rF}] << 7);
regs[${rF}] = (regs[${rF}] & ${FLAG_P | FLAG_Z | FLAG_S}) | (regs[${rA}] & ${FLAG_3 | FLAG_5}) | (bytetemp & ${FLAG_C});`;
    };
    RRC = function(param) {
      var operand;
      operand = getParamBoilerplate(param, true);
      return `${operand.getter}
regs[${rF}] = ${operand.v} & ${FLAG_C};
${operand.v} = ( (${operand.v} >> 1) | (${operand.v} << 7) ) ${operand.trunc};
regs[${rF}] |= sz53pTable[${operand.v}];
${operand.setter}`;
    };
    RRCA = function() {
      return `regs[${rF}] = (regs[${rF}] & ${FLAG_P | FLAG_Z | FLAG_S}) | (regs[${rA}] & ${FLAG_C});
regs[${rA}] = (regs[${rA}] >> 1) | (regs[${rA}] << 7);
regs[${rF}] |= (regs[${rA}] & ${FLAG_3 | FLAG_5});`;
    };
    RRD = function() {
      return `var bytetemp = READMEM(regPairs[${rpHL}]);
CONTEND_READ_NO_MREQ(regPairs[${rpHL}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpHL}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpHL}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpHL}], 1);
var val = (regs[${rA}] << 4) | (bytetemp >> 4);
WRITEMEM(regPairs[${rpHL}], val);
regs[${rA}] = (regs[${rA}] & 0xf0) | (bytetemp & 0x0f);
regs[${rF}] = (regs[${rF}] & ${FLAG_C}) | sz53pTable[regs[${rA}]];`;
    };
    RST = function(addr) {
      return `CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rpPC}] >> 8);
regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rpPC}] & 0xff);
regPairs[${rpPC}] = ${addr};`;
    };
    SBC_A = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}
var sbctemp = regs[${rA}] - ${operand.v} - (regs[${rF}] & ${FLAG_C});
var lookup = ( (regs[${rA}] & 0x88) >> 3 ) | ( (${operand.v} & 0x88) >> 2 ) | ( (sbctemp & 0x88) >> 1 );
regs[${rA}] = sbctemp;
regs[${rF}] = ( sbctemp & 0x100 ? ${FLAG_C} : 0 ) | ${FLAG_N} | halfcarrySubTable[lookup & 0x07] | overflowSubTable[lookup >> 4] | sz53Table[regs[${rA}]];`;
    };
    SBC_HL_RR = function(rp) {
      return `var sub16temp = regPairs[${rpHL}] - regPairs[${rp}] - (regs[${rF}] & ${FLAG_C});
var lookup = ( (regPairs[${rpHL}] & 0x8800) >> 11 ) | ( (regPairs[${rp}] & 0x8800) >> 10 ) | ( (sub16temp & 0x8800) >>  9 );
regPairs[${rpHL}] = sub16temp;
regs[${rF}] = ( sub16temp & 0x10000 ? ${FLAG_C} : 0 ) | ${FLAG_N} | overflowSubTable[lookup >> 4] | (regs[${rH}] & ${FLAG_3 | FLAG_5 | FLAG_S}) | halfcarrySubTable[lookup&0x07] | (regPairs[${rpHL}] ? 0 : ${FLAG_Z});
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);
CONTEND_READ_NO_MREQ(regPairs[${rpIR}], 1);`;
    };
    SCF = function() {
      return `regs[${rF}] = (regs[${rF}] & ${FLAG_P | FLAG_Z | FLAG_S}) | (regs[${rA}] & ${FLAG_3 | FLAG_5}) | ${FLAG_C};`;
    };
    SET = function(bit, param) {
      var hexMask, operand;
      hexMask = 1 << bit;
      operand = getParamBoilerplate(param, true);
      return `${operand.getter}
${operand.v} |= ${hexMask};
${operand.setter}`;
    };
    SHIFT = function(prefix) {
      // Fake instruction for shifted opcodes - passes control to a secondary opcode table
      return `opcodePrefix = '${prefix}';
interruptible = false;`;
    };
    SLA = function(param) {
      var operand;
      operand = getParamBoilerplate(param, true);
      return `${operand.getter}
regs[${rF}] = ${operand.v} >> 7;
${operand.v} = (${operand.v} << 1) ${operand.trunc};
regs[${rF}] |= sz53pTable[${operand.v}];
${operand.setter}`;
    };
    SLL = function(param) {
      var operand;
      operand = getParamBoilerplate(param, true);
      return `${operand.getter}
regs[${rF}] =  ${operand.v} >> 7;
${operand.v} = (((${operand.v}) << 1) ${operand.trunc}) | 0x01;
regs[${rF}] |= sz53pTable[${operand.v}];
${operand.setter}`;
    };
    SRA = function(param) {
      var operand;
      operand = getParamBoilerplate(param, true);
      return `${operand.getter}
regs[${rF}] = ${operand.v} & ${FLAG_C};
${operand.v} = ( (${operand.v} & 0x80) | (${operand.v} >> 1) ) ${operand.trunc};
regs[${rF}] |= sz53pTable[${operand.v}];
${operand.setter}`;
    };
    SRL = function(param) {
      var operand;
      operand = getParamBoilerplate(param, true);
      return `${operand.getter}
regs[${rF}] =  ${operand.v} & ${FLAG_C};
${operand.v} >>= 1;
regs[${rF}] |= sz53pTable[${operand.v}];
${operand.setter}`;
    };
    SUB_A = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}
var subtemp = regs[${rA}] - ${operand.v};
var lookup = ( (regs[${rA}] & 0x88) >> 3 ) | ( (${operand.v} & 0x88) >> 2 ) | ( (subtemp & 0x88) >> 1 );
regs[${rA}] = subtemp;
regs[${rF}] = ( subtemp & 0x100 ? ${FLAG_C} : 0 ) | ${FLAG_N} | halfcarrySubTable[lookup & 0x07] | overflowSubTable[lookup >> 4] | sz53Table[regs[${rA}]];`;
    };
    XOR_A = function(param) {
      var operand;
      operand = getParamBoilerplate(param);
      return `${operand.getter}
regs[${rA}] ^= ${operand.v};
regs[${rF}] = sz53pTable[regs[${rA}]];`;
    };
    opcodeSwitch = function(runStringTable, fallbackTable = {}, traps = []) {
      var action, address, clauses, i, j, opcode, relevantTraps, runString, trapCode;
      clauses = [];
      for (i = j = 0; j < 256; i = ++j) {
        runString = runStringTable[i];
        if (runString == null) {
          runString = fallbackTable[i];
        }
        if (runString != null) {
          relevantTraps = (function() {
            var k, len, results;
            results = [];
            for (k = 0, len = traps.length; k < len; k++) {
              [address, opcode, action] = traps[k];
              if (opcode === i) {
                results.push([address, action]);
              }
            }
            return results;
          })();
          trapCode = (function() {
            var k, len, results;
            results = [];
            for (k = 0, len = relevantTraps.length; k < len; k++) {
              [address, action] = relevantTraps[k];
              results.push(`if (regPairs[${rpPC}] == ${(address + 1) & 0xffff} && !(${action})) break;`);
            }
            return results;
          })();
          clauses.push(`case ${i}:
	${trapCode.join("\n")}
	${runString}
	break;`);
        }
      }
      return `switch (opcode) {
	${clauses.join('')}
	default:
		var addr = regPairs[${rpPC}] - 1;
		throw("Unimplemented opcode " + opcode + " in page ${runStringTable[0x100]} - PC = " + addr);
}`;
    };
    // Tables mapping opcodes to Javascript snippets
    OPCODE_RUN_STRINGS_CB = {
      0x00: RLC("B"), // RLC B
      0x01: RLC("C"), // RLC C
      0x02: RLC("D"), // RLC D
      0x03: RLC("E"), // RLC E
      0x04: RLC("H"), // RLC H
      0x05: RLC("L"), // RLC L
      0x06: RLC("(HL)"), // RLC (HL)
      0x07: RLC("A"), // RLC A
      0x08: RRC("B"), // RRC B
      0x09: RRC("C"), // RRC C
      0x0a: RRC("D"), // RRC D
      0x0b: RRC("E"), // RRC E
      0x0c: RRC("H"), // RRC H
      0x0d: RRC("L"), // RRC L
      0x0e: RRC("(HL)"), // RRC (HL)
      0x0f: RRC("A"), // RRC A
      0x10: RL('B'), // RL B
      0x11: RL('C'), // RL C
      0x12: RL('D'), // RL D
      0x13: RL('E'), // RL E
      0x14: RL('H'), // RL H
      0x15: RL('L'), // RL L
      0x16: RL('(HL)'), // RL (HL)
      0x17: RL('A'), // RL A
      0x18: RR('B'), // RR B
      0x19: RR('C'), // RR C
      0x1a: RR('D'), // RR D
      0x1b: RR('E'), // RR E
      0x1c: RR('H'), // RR H
      0x1d: RR('L'), // RR L
      0x1e: RR('(HL)'), // RR (HL)
      0x1f: RR('A'), // RR A
      0x20: SLA('B'), // SLA B
      0x21: SLA('C'), // SLA C
      0x22: SLA('D'), // SLA D
      0x23: SLA('E'), // SLA E
      0x24: SLA('H'), // SLA H
      0x25: SLA('L'), // SLA L
      0x26: SLA('(HL)'), // SLA (HL)
      0x27: SLA('A'), // SLA A
      0x28: SRA('B'), // SRA B
      0x29: SRA('C'), // SRA C
      0x2a: SRA('D'), // SRA D
      0x2b: SRA('E'), // SRA E
      0x2c: SRA('H'), // SRA H
      0x2d: SRA('L'), // SRA L
      0x2e: SRA('(HL)'), // SRA (HL)
      0x2f: SRA('A'), // SRA A
      0x30: SLL('B'), // SLL B
      0x31: SLL('C'), // SLL C
      0x32: SLL('D'), // SLL D
      0x33: SLL('E'), // SLL E
      0x34: SLL('H'), // SLL H
      0x35: SLL('L'), // SLL L
      0x36: SLL('(HL)'), // SLL (HL)
      0x37: SLL('A'), // SLL A
      0x38: SRL('B'), // SRL B
      0x39: SRL('C'), // SRL C
      0x3a: SRL('D'), // SRL D
      0x3b: SRL('E'), // SRL E
      0x3c: SRL('H'), // SRL H
      0x3d: SRL('L'), // SRL L
      0x3e: SRL('(HL)'), // SRL (HL)
      0x3f: SRL('A'), // SRL A
      0x40: BIT_N_R(0, rB), // BIT 0,B
      0x41: BIT_N_R(0, rC), // BIT 0,C
      0x42: BIT_N_R(0, rD), // BIT 0,D
      0x43: BIT_N_R(0, rE), // BIT 0,E
      0x44: BIT_N_R(0, rH), // BIT 0,H
      0x45: BIT_N_R(0, rL), // BIT 0,L
      0x46: BIT_N_iHLi(0), // BIT 0,(HL)
      0x47: BIT_N_R(0, rA), // BIT 0,A
      0x48: BIT_N_R(1, rB), // BIT 1,B
      0x49: BIT_N_R(1, rC), // BIT 1,C
      0x4A: BIT_N_R(1, rD), // BIT 1,D
      0x4B: BIT_N_R(1, rE), // BIT 1,E
      0x4C: BIT_N_R(1, rH), // BIT 1,H
      0x4D: BIT_N_R(1, rL), // BIT 1,L
      0x4E: BIT_N_iHLi(1), // BIT 1,(HL)
      0x4F: BIT_N_R(1, rA), // BIT 1,A
      0x50: BIT_N_R(2, rB), // BIT 2,B
      0x51: BIT_N_R(2, rC), // BIT 2,C
      0x52: BIT_N_R(2, rD), // BIT 2,D
      0x53: BIT_N_R(2, rE), // BIT 2,E
      0x54: BIT_N_R(2, rH), // BIT 2,H
      0x55: BIT_N_R(2, rL), // BIT 2,L
      0x56: BIT_N_iHLi(2), // BIT 2,(HL)
      0x57: BIT_N_R(2, rA), // BIT 2,A
      0x58: BIT_N_R(3, rB), // BIT 3,B
      0x59: BIT_N_R(3, rC), // BIT 3,C
      0x5A: BIT_N_R(3, rD), // BIT 3,D
      0x5B: BIT_N_R(3, rE), // BIT 3,E
      0x5C: BIT_N_R(3, rH), // BIT 3,H
      0x5D: BIT_N_R(3, rL), // BIT 3,L
      0x5E: BIT_N_iHLi(3), // BIT 3,(HL)
      0x5F: BIT_N_R(3, rA), // BIT 3,A
      0x60: BIT_N_R(4, rB), // BIT 4,B
      0x61: BIT_N_R(4, rC), // BIT 4,C
      0x62: BIT_N_R(4, rD), // BIT 4,D
      0x63: BIT_N_R(4, rE), // BIT 4,E
      0x64: BIT_N_R(4, rH), // BIT 4,H
      0x65: BIT_N_R(4, rL), // BIT 4,L
      0x66: BIT_N_iHLi(4), // BIT 4,(HL)
      0x67: BIT_N_R(4, rA), // BIT 4,A
      0x68: BIT_N_R(5, rB), // BIT 5,B
      0x69: BIT_N_R(5, rC), // BIT 5,C
      0x6A: BIT_N_R(5, rD), // BIT 5,D
      0x6B: BIT_N_R(5, rE), // BIT 5,E
      0x6C: BIT_N_R(5, rH), // BIT 5,H
      0x6D: BIT_N_R(5, rL), // BIT 5,L
      0x6E: BIT_N_iHLi(5), // BIT 5,(HL)
      0x6F: BIT_N_R(5, rA), // BIT 5,A
      0x70: BIT_N_R(6, rB), // BIT 6,B
      0x71: BIT_N_R(6, rC), // BIT 6,C
      0x72: BIT_N_R(6, rD), // BIT 6,D
      0x73: BIT_N_R(6, rE), // BIT 6,E
      0x74: BIT_N_R(6, rH), // BIT 6,H
      0x75: BIT_N_R(6, rL), // BIT 6,L
      0x76: BIT_N_iHLi(6), // BIT 6,(HL)
      0x77: BIT_N_R(6, rA), // BIT 6,A
      0x78: BIT_N_R(7, rB), // BIT 7,B
      0x79: BIT_N_R(7, rC), // BIT 7,C
      0x7A: BIT_N_R(7, rD), // BIT 7,D
      0x7B: BIT_N_R(7, rE), // BIT 7,E
      0x7C: BIT_N_R(7, rH), // BIT 7,H
      0x7D: BIT_N_R(7, rL), // BIT 7,L
      0x7E: BIT_N_iHLi(7), // BIT 7,(HL)
      0x7F: BIT_N_R(7, rA), // BIT 7,A
      0x80: RES(0, 'B'), // RES 0,B
      0x81: RES(0, 'C'), // RES 0,C
      0x82: RES(0, 'D'), // RES 0,D
      0x83: RES(0, 'E'), // RES 0,E
      0x84: RES(0, 'H'), // RES 0,H
      0x85: RES(0, 'L'), // RES 0,L
      0x86: RES(0, '(HL)'), // RES 0,(HL)
      0x87: RES(0, 'A'), // RES 0,A
      0x88: RES(1, 'B'), // RES 1,B
      0x89: RES(1, 'C'), // RES 1,C
      0x8A: RES(1, 'D'), // RES 1,D
      0x8B: RES(1, 'E'), // RES 1,E
      0x8C: RES(1, 'H'), // RES 1,H
      0x8D: RES(1, 'L'), // RES 1,L
      0x8E: RES(1, '(HL)'), // RES 1,(HL)
      0x8F: RES(1, 'A'), // RES 1,A
      0x90: RES(2, 'B'), // RES 2,B
      0x91: RES(2, 'C'), // RES 2,C
      0x92: RES(2, 'D'), // RES 2,D
      0x93: RES(2, 'E'), // RES 2,E
      0x94: RES(2, 'H'), // RES 2,H
      0x95: RES(2, 'L'), // RES 2,L
      0x96: RES(2, '(HL)'), // RES 2,(HL)
      0x97: RES(2, 'A'), // RES 2,A
      0x98: RES(3, 'B'), // RES 3,B
      0x99: RES(3, 'C'), // RES 3,C
      0x9A: RES(3, 'D'), // RES 3,D
      0x9B: RES(3, 'E'), // RES 3,E
      0x9C: RES(3, 'H'), // RES 3,H
      0x9D: RES(3, 'L'), // RES 3,L
      0x9E: RES(3, '(HL)'), // RES 3,(HL)
      0x9F: RES(3, 'A'), // RES 3,A
      0xA0: RES(4, 'B'), // RES 4,B
      0xA1: RES(4, 'C'), // RES 4,C
      0xA2: RES(4, 'D'), // RES 4,D
      0xA3: RES(4, 'E'), // RES 4,E
      0xA4: RES(4, 'H'), // RES 4,H
      0xA5: RES(4, 'L'), // RES 4,L
      0xA6: RES(4, '(HL)'), // RES 4,(HL)
      0xA7: RES(4, 'A'), // RES 4,A
      0xA8: RES(5, 'B'), // RES 5,B
      0xA9: RES(5, 'C'), // RES 5,C
      0xAA: RES(5, 'D'), // RES 5,D
      0xAB: RES(5, 'E'), // RES 5,E
      0xAC: RES(5, 'H'), // RES 5,H
      0xAD: RES(5, 'L'), // RES 5,L
      0xAE: RES(5, '(HL)'), // RES 5,(HL)
      0xAF: RES(5, 'A'), // RES 5,A
      0xB0: RES(6, 'B'), // RES 6,B
      0xB1: RES(6, 'C'), // RES 6,C
      0xB2: RES(6, 'D'), // RES 6,D
      0xB3: RES(6, 'E'), // RES 6,E
      0xB4: RES(6, 'H'), // RES 6,H
      0xB5: RES(6, 'L'), // RES 6,L
      0xB6: RES(6, '(HL)'), // RES 6,(HL)
      0xB7: RES(6, 'A'), // RES 6,A
      0xB8: RES(7, 'B'), // RES 7,B
      0xB9: RES(7, 'C'), // RES 7,C
      0xBA: RES(7, 'D'), // RES 7,D
      0xBB: RES(7, 'E'), // RES 7,E
      0xBC: RES(7, 'H'), // RES 7,H
      0xBD: RES(7, 'L'), // RES 7,L
      0xBE: RES(7, '(HL)'), // RES 7,(HL)
      0xBF: RES(7, 'A'), // RES 7,A
      0xC0: SET(0, 'B'), // SET 0,B
      0xC1: SET(0, 'C'), // SET 0,C
      0xC2: SET(0, 'D'), // SET 0,D
      0xC3: SET(0, 'E'), // SET 0,E
      0xC4: SET(0, 'H'), // SET 0,H
      0xC5: SET(0, 'L'), // SET 0,L
      0xC6: SET(0, '(HL)'), // SET 0,(HL)
      0xC7: SET(0, 'A'), // SET 0,A
      0xC8: SET(1, 'B'), // SET 1,B
      0xC9: SET(1, 'C'), // SET 1,C
      0xCA: SET(1, 'D'), // SET 1,D
      0xCB: SET(1, 'E'), // SET 1,E
      0xCC: SET(1, 'H'), // SET 1,H
      0xCD: SET(1, 'L'), // SET 1,L
      0xCE: SET(1, '(HL)'), // SET 1,(HL)
      0xCF: SET(1, 'A'), // SET 1,A
      0xD0: SET(2, 'B'), // SET 2,B
      0xD1: SET(2, 'C'), // SET 2,C
      0xD2: SET(2, 'D'), // SET 2,D
      0xD3: SET(2, 'E'), // SET 2,E
      0xD4: SET(2, 'H'), // SET 2,H
      0xD5: SET(2, 'L'), // SET 2,L
      0xD6: SET(2, '(HL)'), // SET 2,(HL)
      0xD7: SET(2, 'A'), // SET 2,A
      0xD8: SET(3, 'B'), // SET 3,B
      0xD9: SET(3, 'C'), // SET 3,C
      0xDA: SET(3, 'D'), // SET 3,D
      0xDB: SET(3, 'E'), // SET 3,E
      0xDC: SET(3, 'H'), // SET 3,H
      0xDD: SET(3, 'L'), // SET 3,L
      0xDE: SET(3, '(HL)'), // SET 3,(HL)
      0xDF: SET(3, 'A'), // SET 3,A
      0xE0: SET(4, 'B'), // SET 4,B
      0xE1: SET(4, 'C'), // SET 4,C
      0xE2: SET(4, 'D'), // SET 4,D
      0xE3: SET(4, 'E'), // SET 4,E
      0xE4: SET(4, 'H'), // SET 4,H
      0xE5: SET(4, 'L'), // SET 4,L
      0xE6: SET(4, '(HL)'), // SET 4,(HL)
      0xE7: SET(4, 'A'), // SET 4,A
      0xE8: SET(5, 'B'), // SET 5,B
      0xE9: SET(5, 'C'), // SET 5,C
      0xEA: SET(5, 'D'), // SET 5,D
      0xEB: SET(5, 'E'), // SET 5,E
      0xEC: SET(5, 'H'), // SET 5,H
      0xED: SET(5, 'L'), // SET 5,L
      0xEE: SET(5, '(HL)'), // SET 5,(HL)
      0xEF: SET(5, 'A'), // SET 5,A
      0xF0: SET(6, 'B'), // SET 6,B
      0xF1: SET(6, 'C'), // SET 6,C
      0xF2: SET(6, 'D'), // SET 6,D
      0xF3: SET(6, 'E'), // SET 6,E
      0xF4: SET(6, 'H'), // SET 6,H
      0xF5: SET(6, 'L'), // SET 6,L
      0xF6: SET(6, '(HL)'), // SET 6,(HL)
      0xF7: SET(6, 'A'), // SET 6,A
      0xF8: SET(7, 'B'), // SET 7,B
      0xF9: SET(7, 'C'), // SET 7,C
      0xFA: SET(7, 'D'), // SET 7,D
      0xFB: SET(7, 'E'), // SET 7,E
      0xFC: SET(7, 'H'), // SET 7,H
      0xFD: SET(7, 'L'), // SET 7,L
      0xFE: SET(7, '(HL)'), // SET 7,(HL)
      0xFF: SET(7, 'A'), // SET 7,A
      0x100: 'cb'
    };
    // Generate the opcode runner lookup table for either the DDCB or FDCB set
    generateddfdcbOpcodeSet = function(prefix) {
      var rh, rhn, rl, rln, rp, rpn;
      if (prefix === 'DDCB') {
        rp = rpIX;
        rh = rIXH;
        rl = rIXL;
        rpn = 'IX';
        rhn = 'IXH';
        rln = 'IXL'; // prefix == 'FDCB'
      } else {
        rp = rpIY;
        rh = rIYH;
        rl = rIYL;
        rpn = 'IY';
        rhn = 'IYH';
        rln = 'IYL';
      }
      return {
        0x00: LDSHIFTOP('B', RLC, rp), // LD B,RLC (REGISTER+dd)
        0x01: LDSHIFTOP('C', RLC, rp), // LD C,RLC (REGISTER+dd)
        0x02: LDSHIFTOP('D', RLC, rp), // LD D,RLC (REGISTER+dd)
        0x03: LDSHIFTOP('E', RLC, rp), // LD E,RLC (REGISTER+dd)
        0x04: LDSHIFTOP('H', RLC, rp), // LD H,RLC (REGISTER+dd)
        0x05: LDSHIFTOP('L', RLC, rp), // LD L,RLC (REGISTER+dd)
        0x06: RLC(`(${rpn}+nn)`),
        0x07: LDSHIFTOP('A', RLC, rp), // LD A,RLC (REGISTER+dd)
        0x08: LDSHIFTOP('B', RRC, rp), // LD B,RRC (REGISTER+dd)
        0x09: LDSHIFTOP('C', RRC, rp), // LD C,RRC (REGISTER+dd)
        0x0A: LDSHIFTOP('D', RRC, rp), // LD D,RRC (REGISTER+dd)
        0x0B: LDSHIFTOP('E', RRC, rp), // LD E,RRC (REGISTER+dd)
        0x0C: LDSHIFTOP('H', RRC, rp), // LD H,RRC (REGISTER+dd)
        0x0D: LDSHIFTOP('L', RRC, rp), // LD L,RRC (REGISTER+dd)
        0x0E: RRC(`(${rpn}+nn)`),
        0x0F: LDSHIFTOP('A', RRC, rp), // LD A,RRC (REGISTER+dd)
        0x10: LDSHIFTOP('B', RL, rp), // LD B,RL (REGISTER+dd)
        0x11: LDSHIFTOP('C', RL, rp), // LD C,RL (REGISTER+dd)
        0x12: LDSHIFTOP('D', RL, rp), // LD D,RL (REGISTER+dd)
        0x13: LDSHIFTOP('E', RL, rp), // LD E,RL (REGISTER+dd)
        0x14: LDSHIFTOP('H', RL, rp), // LD H,RL (REGISTER+dd)
        0x15: LDSHIFTOP('L', RL, rp), // LD L,RL (REGISTER+dd)
        0x16: RL(`(${rpn}+nn)`),
        0x17: LDSHIFTOP('A', RL, rp), // LD A,RL (REGISTER+dd)
        0x18: LDSHIFTOP('B', RR, rp), // LD B,RR (REGISTER+dd)
        0x19: LDSHIFTOP('C', RR, rp), // LD C,RR (REGISTER+dd)
        0x1A: LDSHIFTOP('D', RR, rp), // LD D,RR (REGISTER+dd)
        0x1B: LDSHIFTOP('E', RR, rp), // LD E,RR (REGISTER+dd)
        0x1C: LDSHIFTOP('H', RR, rp), // LD H,RR (REGISTER+dd)
        0x1D: LDSHIFTOP('L', RR, rp), // LD L,RR (REGISTER+dd)
        0x1E: RR(`(${rpn}+nn)`),
        0x1F: LDSHIFTOP('A', RR, rp), // LD A,RR (REGISTER+dd)
        0x20: LDSHIFTOP('B', SLA, rp), // LD B,SLA (REGISTER+dd)
        0x21: LDSHIFTOP('C', SLA, rp), // LD C,SLA (REGISTER+dd)
        0x22: LDSHIFTOP('D', SLA, rp), // LD D,SLA (REGISTER+dd)
        0x23: LDSHIFTOP('E', SLA, rp), // LD E,SLA (REGISTER+dd)
        0x24: LDSHIFTOP('H', SLA, rp), // LD H,SLA (REGISTER+dd)
        0x25: LDSHIFTOP('L', SLA, rp), // LD L,SLA (REGISTER+dd)
        0x26: SLA(`(${rpn}+nn)`),
        0x27: LDSHIFTOP('A', SLA, rp), // LD A,SLA (REGISTER+dd)
        0x28: LDSHIFTOP('B', SRA, rp), // LD B,SRA (REGISTER+dd)
        0x29: LDSHIFTOP('C', SRA, rp), // LD C,SRA (REGISTER+dd)
        0x2A: LDSHIFTOP('D', SRA, rp), // LD D,SRA (REGISTER+dd)
        0x2B: LDSHIFTOP('E', SRA, rp), // LD E,SRA (REGISTER+dd)
        0x2C: LDSHIFTOP('H', SRA, rp), // LD H,SRA (REGISTER+dd)
        0x2D: LDSHIFTOP('L', SRA, rp), // LD L,SRA (REGISTER+dd)
        0x2E: SRA(`(${rpn}+nn)`),
        0x2F: LDSHIFTOP('A', SRA, rp), // LD A,SRA (REGISTER+dd)
        0x30: LDSHIFTOP('B', SLL, rp), // LD B,SLL (REGISTER+dd)
        0x31: LDSHIFTOP('C', SLL, rp), // LD C,SLL (REGISTER+dd)
        0x32: LDSHIFTOP('D', SLL, rp), // LD D,SLL (REGISTER+dd)
        0x33: LDSHIFTOP('E', SLL, rp), // LD E,SLL (REGISTER+dd)
        0x34: LDSHIFTOP('H', SLL, rp), // LD H,SLL (REGISTER+dd)
        0x35: LDSHIFTOP('L', SLL, rp), // LD L,SLL (REGISTER+dd)
        0x36: SLL(`(${rpn}+nn)`),
        0x37: LDSHIFTOP('A', SLL, rp), // LD A,SLL (REGISTER+dd)
        0x38: LDSHIFTOP('B', SRL, rp), // LD B,SRL (REGISTER+dd)
        0x39: LDSHIFTOP('C', SRL, rp), // LD C,SRL (REGISTER+dd)
        0x3A: LDSHIFTOP('D', SRL, rp), // LD D,SRL (REGISTER+dd)
        0x3B: LDSHIFTOP('E', SRL, rp), // LD E,SRL (REGISTER+dd)
        0x3C: LDSHIFTOP('H', SRL, rp), // LD H,SRL (REGISTER+dd)
        0x3D: LDSHIFTOP('L', SRL, rp), // LD L,SRL (REGISTER+dd)
        0x3E: SRL(`(${rpn}+nn)`),
        0x3F: LDSHIFTOP('A', SRL, rp), // LD A,SRL (REGISTER+dd)
        0x40: BIT_N_iRRpNNi(0, rp), // BIT 0,(IX+nn)
        0x41: BIT_N_iRRpNNi(0, rp), // BIT 0,(IX+nn)
        0x42: BIT_N_iRRpNNi(0, rp), // BIT 0,(IX+nn)
        0x43: BIT_N_iRRpNNi(0, rp), // BIT 0,(IX+nn)
        0x44: BIT_N_iRRpNNi(0, rp), // BIT 0,(IX+nn)
        0x45: BIT_N_iRRpNNi(0, rp), // BIT 0,(IX+nn)
        0x46: BIT_N_iRRpNNi(0, rp), // BIT 0,(IX+nn)
        0x47: BIT_N_iRRpNNi(0, rp), // BIT 0,(IX+nn)
        0x48: BIT_N_iRRpNNi(1, rp), // BIT 1,(IX+nn)
        0x49: BIT_N_iRRpNNi(1, rp), // BIT 1,(IX+nn)
        0x4A: BIT_N_iRRpNNi(1, rp), // BIT 1,(IX+nn)
        0x4B: BIT_N_iRRpNNi(1, rp), // BIT 1,(IX+nn)
        0x4C: BIT_N_iRRpNNi(1, rp), // BIT 1,(IX+nn)
        0x4D: BIT_N_iRRpNNi(1, rp), // BIT 1,(IX+nn)
        0x4E: BIT_N_iRRpNNi(1, rp), // BIT 1,(IX+nn)
        0x4F: BIT_N_iRRpNNi(1, rp), // BIT 1,(IX+nn)
        0x50: BIT_N_iRRpNNi(2, rp), // BIT 2,(IX+nn)
        0x51: BIT_N_iRRpNNi(2, rp), // BIT 2,(IX+nn)
        0x52: BIT_N_iRRpNNi(2, rp), // BIT 2,(IX+nn)
        0x53: BIT_N_iRRpNNi(2, rp), // BIT 2,(IX+nn)
        0x54: BIT_N_iRRpNNi(2, rp), // BIT 2,(IX+nn)
        0x55: BIT_N_iRRpNNi(2, rp), // BIT 2,(IX+nn)
        0x56: BIT_N_iRRpNNi(2, rp), // BIT 2,(IX+nn)
        0x57: BIT_N_iRRpNNi(2, rp), // BIT 2,(IX+nn)
        0x58: BIT_N_iRRpNNi(3, rp), // BIT 3,(IX+nn)
        0x59: BIT_N_iRRpNNi(3, rp), // BIT 3,(IX+nn)
        0x5A: BIT_N_iRRpNNi(3, rp), // BIT 3,(IX+nn)
        0x5B: BIT_N_iRRpNNi(3, rp), // BIT 3,(IX+nn)
        0x5C: BIT_N_iRRpNNi(3, rp), // BIT 3,(IX+nn)
        0x5D: BIT_N_iRRpNNi(3, rp), // BIT 3,(IX+nn)
        0x5E: BIT_N_iRRpNNi(3, rp), // BIT 3,(IX+nn)
        0x5F: BIT_N_iRRpNNi(3, rp), // BIT 3,(IX+nn)
        0x60: BIT_N_iRRpNNi(4, rp), // BIT 4,(IX+nn)
        0x61: BIT_N_iRRpNNi(4, rp), // BIT 4,(IX+nn)
        0x62: BIT_N_iRRpNNi(4, rp), // BIT 4,(IX+nn)
        0x63: BIT_N_iRRpNNi(4, rp), // BIT 4,(IX+nn)
        0x64: BIT_N_iRRpNNi(4, rp), // BIT 4,(IX+nn)
        0x65: BIT_N_iRRpNNi(4, rp), // BIT 4,(IX+nn)
        0x66: BIT_N_iRRpNNi(4, rp), // BIT 4,(IX+nn)
        0x67: BIT_N_iRRpNNi(4, rp), // BIT 4,(IX+nn)
        0x68: BIT_N_iRRpNNi(5, rp), // BIT 5,(IX+nn)
        0x69: BIT_N_iRRpNNi(5, rp), // BIT 5,(IX+nn)
        0x6A: BIT_N_iRRpNNi(5, rp), // BIT 5,(IX+nn)
        0x6B: BIT_N_iRRpNNi(5, rp), // BIT 5,(IX+nn)
        0x6C: BIT_N_iRRpNNi(5, rp), // BIT 5,(IX+nn)
        0x6D: BIT_N_iRRpNNi(5, rp), // BIT 5,(IX+nn)
        0x6E: BIT_N_iRRpNNi(5, rp), // BIT 5,(IX+nn)
        0x6F: BIT_N_iRRpNNi(5, rp), // BIT 5,(IX+nn)
        0x70: BIT_N_iRRpNNi(6, rp), // BIT 6,(IX+nn)
        0x71: BIT_N_iRRpNNi(6, rp), // BIT 6,(IX+nn)
        0x72: BIT_N_iRRpNNi(6, rp), // BIT 6,(IX+nn)
        0x73: BIT_N_iRRpNNi(6, rp), // BIT 6,(IX+nn)
        0x74: BIT_N_iRRpNNi(6, rp), // BIT 6,(IX+nn)
        0x75: BIT_N_iRRpNNi(6, rp), // BIT 6,(IX+nn)
        0x76: BIT_N_iRRpNNi(6, rp), // BIT 6,(IX+nn)
        0x77: BIT_N_iRRpNNi(6, rp), // BIT 6,(IX+nn)
        0x78: BIT_N_iRRpNNi(7, rp), // BIT 7,(IX+nn)
        0x79: BIT_N_iRRpNNi(7, rp), // BIT 7,(IX+nn)
        0x7A: BIT_N_iRRpNNi(7, rp), // BIT 7,(IX+nn)
        0x7B: BIT_N_iRRpNNi(7, rp), // BIT 7,(IX+nn)
        0x7C: BIT_N_iRRpNNi(7, rp), // BIT 7,(IX+nn)
        0x7D: BIT_N_iRRpNNi(7, rp), // BIT 7,(IX+nn)
        0x7E: BIT_N_iRRpNNi(7, rp), // BIT 7,(IX+nn)
        0x7F: BIT_N_iRRpNNi(7, rp), // BIT 7,(IX+nn)
        0x80: LDBITOP('B', RES, 0, rp), // LD B,RES 0,(IX+dd)
        0x81: LDBITOP('C', RES, 0, rp), // LD C,RES 0,(IX+dd)
        0x82: LDBITOP('D', RES, 0, rp), // LD D,RES 0,(IX+dd)
        0x83: LDBITOP('E', RES, 0, rp), // LD E,RES 0,(IX+dd)
        0x84: LDBITOP('H', RES, 0, rp), // LD H,RES 0,(IX+dd)
        0x85: LDBITOP('L', RES, 0, rp), // LD L,RES 0,(IX+dd)
        0x86: RES(0, `(${rpn}+nn)`),
        0x87: LDBITOP('A', RES, 0, rp), // LD A,RES 0,(IX+dd)
        0x88: LDBITOP('B', RES, 1, rp), // LD B,RES 1,(IX+dd)
        0x89: LDBITOP('C', RES, 1, rp), // LD C,RES 1,(IX+dd)
        0x8A: LDBITOP('D', RES, 1, rp), // LD D,RES 1,(IX+dd)
        0x8B: LDBITOP('E', RES, 1, rp), // LD E,RES 1,(IX+dd)
        0x8C: LDBITOP('H', RES, 1, rp), // LD H,RES 1,(IX+dd)
        0x8D: LDBITOP('L', RES, 1, rp), // LD L,RES 1,(IX+dd)
        0x8E: RES(1, `(${rpn}+nn)`),
        0x8F: LDBITOP('A', RES, 1, rp), // LD A,RES 1,(IX+dd)
        0x90: LDBITOP('B', RES, 2, rp), // LD B,RES 2,(IX+dd)
        0x91: LDBITOP('C', RES, 2, rp), // LD C,RES 2,(IX+dd)
        0x92: LDBITOP('D', RES, 2, rp), // LD D,RES 2,(IX+dd)
        0x93: LDBITOP('E', RES, 2, rp), // LD E,RES 2,(IX+dd)
        0x94: LDBITOP('H', RES, 2, rp), // LD H,RES 2,(IX+dd)
        0x95: LDBITOP('L', RES, 2, rp), // LD L,RES 2,(IX+dd)
        0x96: RES(2, `(${rpn}+nn)`),
        0x97: LDBITOP('A', RES, 2, rp), // LD A,RES 2,(IX+dd)
        0x98: LDBITOP('B', RES, 3, rp), // LD B,RES 3,(IX+dd)
        0x99: LDBITOP('C', RES, 3, rp), // LD C,RES 3,(IX+dd)
        0x9A: LDBITOP('D', RES, 3, rp), // LD D,RES 3,(IX+dd)
        0x9B: LDBITOP('E', RES, 3, rp), // LD E,RES 3,(IX+dd)
        0x9C: LDBITOP('H', RES, 3, rp), // LD H,RES 3,(IX+dd)
        0x9D: LDBITOP('L', RES, 3, rp), // LD L,RES 3,(IX+dd)
        0x9E: RES(3, `(${rpn}+nn)`),
        0x9F: LDBITOP('A', RES, 3, rp), // LD A,RES 3,(IX+dd)
        0xA0: LDBITOP('B', RES, 4, rp), // LD B,RES 4,(IX+dd)
        0xA1: LDBITOP('C', RES, 4, rp), // LD C,RES 4,(IX+dd)
        0xA2: LDBITOP('D', RES, 4, rp), // LD D,RES 4,(IX+dd)
        0xA3: LDBITOP('E', RES, 4, rp), // LD E,RES 4,(IX+dd)
        0xA4: LDBITOP('H', RES, 4, rp), // LD H,RES 4,(IX+dd)
        0xA5: LDBITOP('L', RES, 4, rp), // LD L,RES 4,(IX+dd)
        0xA6: RES(4, `(${rpn}+nn)`),
        0xA7: LDBITOP('A', RES, 4, rp), // LD A,RES 4,(IX+dd)
        0xA8: LDBITOP('B', RES, 5, rp), // LD B,RES 5,(IX+dd)
        0xA9: LDBITOP('C', RES, 5, rp), // LD C,RES 5,(IX+dd)
        0xAA: LDBITOP('D', RES, 5, rp), // LD D,RES 5,(IX+dd)
        0xAB: LDBITOP('E', RES, 5, rp), // LD E,RES 5,(IX+dd)
        0xAC: LDBITOP('H', RES, 5, rp), // LD H,RES 5,(IX+dd)
        0xAD: LDBITOP('L', RES, 5, rp), // LD L,RES 5,(IX+dd)
        0xAE: RES(5, `(${rpn}+nn)`),
        0xAF: LDBITOP('A', RES, 5, rp), // LD A,RES 5,(IX+dd)
        0xB0: LDBITOP('B', RES, 6, rp), // LD B,RES 6,(IX+dd)
        0xB1: LDBITOP('C', RES, 6, rp), // LD C,RES 6,(IX+dd)
        0xB2: LDBITOP('D', RES, 6, rp), // LD D,RES 6,(IX+dd)
        0xB3: LDBITOP('E', RES, 6, rp), // LD E,RES 6,(IX+dd)
        0xB4: LDBITOP('H', RES, 6, rp), // LD H,RES 6,(IX+dd)
        0xB5: LDBITOP('L', RES, 6, rp), // LD L,RES 6,(IX+dd)
        0xB6: RES(6, `(${rpn}+nn)`),
        0xB7: LDBITOP('A', RES, 6, rp), // LD A,RES 6,(IX+dd)
        0xB8: LDBITOP('B', RES, 7, rp), // LD B,RES 7,(IX+dd)
        0xB9: LDBITOP('C', RES, 7, rp), // LD C,RES 7,(IX+dd)
        0xBA: LDBITOP('D', RES, 7, rp), // LD D,RES 7,(IX+dd)
        0xBB: LDBITOP('E', RES, 7, rp), // LD E,RES 7,(IX+dd)
        0xBC: LDBITOP('H', RES, 7, rp), // LD H,RES 7,(IX+dd)
        0xBD: LDBITOP('L', RES, 7, rp), // LD L,RES 7,(IX+dd)
        0xBE: RES(7, `(${rpn}+nn)`),
        0xBF: LDBITOP('A', RES, 7, rp), // LD A,RES 7,(IX+dd)
        0xC0: LDBITOP('B', SET, 0, rp), // LD B,SET 0,(IX+dd)
        0xC1: LDBITOP('C', SET, 0, rp), // LD C,SET 0,(IX+dd)
        0xC2: LDBITOP('D', SET, 0, rp), // LD D,SET 0,(IX+dd)
        0xC3: LDBITOP('E', SET, 0, rp), // LD E,SET 0,(IX+dd)
        0xC4: LDBITOP('H', SET, 0, rp), // LD H,SET 0,(IX+dd)
        0xC5: LDBITOP('L', SET, 0, rp), // LD L,SET 0,(IX+dd)
        0xC6: SET(0, `(${rpn}+nn)`),
        0xC7: LDBITOP('A', SET, 0, rp), // LD A,SET 0,(IX+dd)
        0xC8: LDBITOP('B', SET, 1, rp), // LD B,SET 1,(IX+dd)
        0xC9: LDBITOP('C', SET, 1, rp), // LD C,SET 1,(IX+dd)
        0xCA: LDBITOP('D', SET, 1, rp), // LD D,SET 1,(IX+dd)
        0xCB: LDBITOP('E', SET, 1, rp), // LD E,SET 1,(IX+dd)
        0xCC: LDBITOP('H', SET, 1, rp), // LD H,SET 1,(IX+dd)
        0xCD: LDBITOP('L', SET, 1, rp), // LD L,SET 1,(IX+dd)
        0xCE: SET(1, `(${rpn}+nn)`),
        0xCF: LDBITOP('A', SET, 1, rp), // LD A,SET 1,(IX+dd)
        0xD0: LDBITOP('B', SET, 2, rp), // LD B,SET 2,(IX+dd)
        0xD1: LDBITOP('C', SET, 2, rp), // LD C,SET 2,(IX+dd)
        0xD2: LDBITOP('D', SET, 2, rp), // LD D,SET 2,(IX+dd)
        0xD3: LDBITOP('E', SET, 2, rp), // LD E,SET 2,(IX+dd)
        0xD4: LDBITOP('H', SET, 2, rp), // LD H,SET 2,(IX+dd)
        0xD5: LDBITOP('L', SET, 2, rp), // LD L,SET 2,(IX+dd)
        0xD6: SET(2, `(${rpn}+nn)`),
        0xD7: LDBITOP('A', SET, 2, rp), // LD A,SET 2,(IX+dd)
        0xD8: LDBITOP('B', SET, 3, rp), // LD B,SET 3,(IX+dd)
        0xD9: LDBITOP('C', SET, 3, rp), // LD C,SET 3,(IX+dd)
        0xDA: LDBITOP('D', SET, 3, rp), // LD D,SET 3,(IX+dd)
        0xDB: LDBITOP('E', SET, 3, rp), // LD E,SET 3,(IX+dd)
        0xDC: LDBITOP('H', SET, 3, rp), // LD H,SET 3,(IX+dd)
        0xDD: LDBITOP('L', SET, 3, rp), // LD L,SET 3,(IX+dd)
        0xDE: SET(3, `(${rpn}+nn)`),
        0xDF: LDBITOP('A', SET, 3, rp), // LD A,SET 3,(IX+dd)
        0xE0: LDBITOP('B', SET, 4, rp), // LD B,SET 4,(IX+dd)
        0xE1: LDBITOP('C', SET, 4, rp), // LD C,SET 4,(IX+dd)
        0xE2: LDBITOP('D', SET, 4, rp), // LD D,SET 4,(IX+dd)
        0xE3: LDBITOP('E', SET, 4, rp), // LD E,SET 4,(IX+dd)
        0xE4: LDBITOP('H', SET, 4, rp), // LD H,SET 4,(IX+dd)
        0xE5: LDBITOP('L', SET, 4, rp), // LD L,SET 4,(IX+dd)
        0xE6: SET(4, `(${rpn}+nn)`),
        0xE7: LDBITOP('A', SET, 4, rp), // LD A,SET 4,(IX+dd)
        0xE8: LDBITOP('B', SET, 5, rp), // LD B,SET 5,(IX+dd)
        0xE9: LDBITOP('C', SET, 5, rp), // LD C,SET 5,(IX+dd)
        0xEA: LDBITOP('D', SET, 5, rp), // LD D,SET 5,(IX+dd)
        0xEB: LDBITOP('E', SET, 5, rp), // LD E,SET 5,(IX+dd)
        0xEC: LDBITOP('H', SET, 5, rp), // LD H,SET 5,(IX+dd)
        0xED: LDBITOP('L', SET, 5, rp), // LD L,SET 5,(IX+dd)
        0xEE: SET(5, `(${rpn}+nn)`),
        0xEF: LDBITOP('A', SET, 5, rp), // LD A,SET 5,(IX+dd)
        0xF0: LDBITOP('B', SET, 6, rp), // LD B,SET 6,(IX+dd)
        0xF1: LDBITOP('C', SET, 6, rp), // LD C,SET 6,(IX+dd)
        0xF2: LDBITOP('D', SET, 6, rp), // LD D,SET 6,(IX+dd)
        0xF3: LDBITOP('E', SET, 6, rp), // LD E,SET 6,(IX+dd)
        0xF4: LDBITOP('H', SET, 6, rp), // LD H,SET 6,(IX+dd)
        0xF5: LDBITOP('L', SET, 6, rp), // LD L,SET 6,(IX+dd)
        0xF6: SET(6, `(${rpn}+nn)`),
        0xF7: LDBITOP('A', SET, 6, rp), // LD A,SET 6,(IX+dd)
        0xF8: LDBITOP('B', SET, 7, rp), // LD B,SET 7,(IX+dd)
        0xF9: LDBITOP('C', SET, 7, rp), // LD C,SET 7,(IX+dd)
        0xFA: LDBITOP('D', SET, 7, rp), // LD D,SET 7,(IX+dd)
        0xFB: LDBITOP('E', SET, 7, rp), // LD E,SET 7,(IX+dd)
        0xFC: LDBITOP('H', SET, 7, rp), // LD H,SET 7,(IX+dd)
        0xFD: LDBITOP('L', SET, 7, rp), // LD L,SET 7,(IX+dd)
        0xFE: SET(7, `(${rpn}+nn)`),
        0xFF: LDBITOP('A', SET, 7, rp), // LD A,SET 7,(IX+dd)
        0x100: 'ddcb'
      };
    };
    OPCODE_RUN_STRINGS_DDCB = generateddfdcbOpcodeSet('DDCB');
    OPCODE_RUN_STRINGS_FDCB = generateddfdcbOpcodeSet('FDCB');
    // Generate the opcode runner lookup table for either the DD or FD set, acting on the
    // specified register pair (IX or IY)
    generateddfdOpcodeSet = function(prefix) {
      var rh, rhn, rl, rln, rp, rpn;
      if (prefix === 'DD') {
        rp = rpIX;
        rh = rIXH;
        rl = rIXL;
        rpn = 'IX';
        rhn = 'IXH';
        rln = 'IXL'; // prefix == 'FD'
      } else {
        rp = rpIY;
        rh = rIYH;
        rl = rIYL;
        rpn = 'IY';
        rhn = 'IYH';
        rln = 'IYL';
      }
      return {
        0x09: ADD_RR_RR(rp, rpBC), // ADD IX,BC
        0x19: ADD_RR_RR(rp, rpDE), // ADD IX,DE
        0x21: LD_RR_NN(rp), // LD IX,nnnn
        0x22: LD_iNNi_RR(rp), // LD (nnnn),IX
        0x23: INC_RR(rp), // INC IX
        0x24: INC(rhn), // INC IXh
        0x25: DEC(rhn), // DEC IXh
        0x26: LD_R_N(rh), // LD IXh, nn
        0x29: ADD_RR_RR(rp, rp), // ADD IX,IX
        0x2A: LD_RR_iNNi(rp), // LD IX,(nnnn)
        0x2B: DEC_RR(rp), // DEC IX
        0x2C: INC(rln), // INC IXl
        0x2D: DEC(rln), // DEC IXl
        0x2E: LD_R_N(rl), // LD IXl, nn
        0x34: INC(`(${rpn}+nn)`),
        0x35: DEC(`(${rpn}+nn)`),
        0x36: LD_iRRpNNi_N(rp), // LD (IX+nn),nn
        0x39: ADD_RR_RR(rp, rpSP), // ADD IX,SP
        0x44: LD_R_R(rB, rh), // LD B,IXh
        0x45: LD_R_R(rB, rl), // LD B,IXl
        0x46: LD_R_iRRpNNi(rB, rp), // LD B,(IX+nn)
        0x4C: LD_R_R(rC, rh), // LD C,IXh
        0x4D: LD_R_R(rC, rl), // LD C,IXl
        0x4E: LD_R_iRRpNNi(rC, rp), // LD C,(IX+nn)
        0x54: LD_R_R(rD, rh), // LD D,IXh
        0x55: LD_R_R(rD, rl), // LD D,IXl
        0x56: LD_R_iRRpNNi(rD, rp), // LD D,(IX+nn)
        0x5C: LD_R_R(rE, rh), // LD E,IXh
        0x5D: LD_R_R(rE, rl), // LD E,IXl
        0x5E: LD_R_iRRpNNi(rE, rp), // LD E,(IX+nn)
        0x60: LD_R_R(rh, rB), // LD IXh,B
        0x61: LD_R_R(rh, rC), // LD IXh,C
        0x62: LD_R_R(rh, rD), // LD IXh,D
        0x63: LD_R_R(rh, rE), // LD IXh,E
        0x64: LD_R_R(rh, rh), // LD IXh,IXh
        0x65: LD_R_R(rh, rl), // LD IXh,IXl
        0x66: LD_R_iRRpNNi(rH, rp), // LD H,(IX+nn)
        0x67: LD_R_R(rh, rA), // LD IXh,A
        0x68: LD_R_R(rl, rB), // LD IXl,B
        0x69: LD_R_R(rl, rC), // LD IXl,C
        0x6A: LD_R_R(rl, rD), // LD IXl,D
        0x6B: LD_R_R(rl, rE), // LD IXl,E
        0x6C: LD_R_R(rl, rh), // LD IXl,IXh
        0x6D: LD_R_R(rl, rl), // LD IXl,IXl
        0x6E: LD_R_iRRpNNi(rL, rp), // LD L,(IX+nn)
        0x6F: LD_R_R(rl, rA), // LD IXl,A
        0x70: LD_iRRpNNi_R(rp, rB), // LD (IX+nn),B
        0x71: LD_iRRpNNi_R(rp, rC), // LD (IX+nn),C
        0x72: LD_iRRpNNi_R(rp, rD), // LD (IX+nn),D
        0x73: LD_iRRpNNi_R(rp, rE), // LD (IX+nn),E
        0x74: LD_iRRpNNi_R(rp, rH), // LD (IX+nn),H
        0x75: LD_iRRpNNi_R(rp, rL), // LD (IX+nn),L
        0x77: LD_iRRpNNi_R(rp, rA), // LD (IX+nn),A
        0x7C: LD_R_R(rA, rh), // LD A,IXh
        0x7D: LD_R_R(rA, rl), // LD A,IXl
        0x7E: LD_R_iRRpNNi(rA, rp), // LD A,(IX+nn)
        0x84: ADD_A(rhn), // ADD A,IXh
        0x85: ADD_A(rln), // ADD A,IXl
        0x86: ADD_A(`(${rpn}+nn)`),
        0x8C: ADC_A(rhn), // ADC A,IXh
        0x8D: ADC_A(rln), // ADC A,IXl
        0x8E: ADC_A(`(${rpn}+nn)`),
        0x94: SUB_A(rhn), // SUB IXh
        0x95: SUB_A(rln), // SUB IXl
        0x96: SUB_A(`(${rpn}+nn)`),
        0x9C: SBC_A(rhn), // SBC IXh
        0x9D: SBC_A(rln), // SBC IXl
        0x9E: SBC_A(`(${rpn}+nn)`),
        0xA4: AND_A(rhn), // AND IXh
        0xA5: AND_A(rln), // AND IXl
        0xA6: AND_A(`(${rpn}+nn)`),
        0xAC: XOR_A(rhn), // XOR IXh
        0xAD: XOR_A(rln), // XOR IXl
        0xAE: XOR_A(`(${rpn}+nn)`),
        0xB4: OR_A(rhn), // OR IXh
        0xB5: OR_A(rln), // OR IXl
        0xB6: OR_A(`(${rpn}+nn)`),
        0xBC: CP_A(rhn), // CP IXh
        0xBD: CP_A(rln), // CP IXl
        0xBE: CP_A(`(${rpn}+nn)`),
        0xCB: SHIFT(prefix + 'CB'), // shift code
        0xDD: SHIFT('DD'), // shift code
        0xE1: POP_RR(rp), // POP IX
        0xE3: EX_iSPi_RR(rp), // EX (SP),IX
        0xE5: PUSH_RR(rp), // PUSH IX
        0xE9: JP_RR(rp), // JP (IX)
        0xF9: LD_RR_RR(rpSP, rp), // LD SP,IX
        0xFD: SHIFT('FD'), // shift code
        0x100: 'dd'
      };
    };
    OPCODE_RUN_STRINGS_DD = generateddfdOpcodeSet('DD');
    OPCODE_RUN_STRINGS_ED = {
      0x40: IN_R_iCi(rB), // IN B,(C)
      0x41: OUT_iCi_R(rB), // OUT (C),B
      0x42: SBC_HL_RR(rpBC), // SBC HL,BC
      0x43: LD_iNNi_RR(rpBC), // LD (nnnn),BC
      0x44: NEG(), // NEG
      0x45: RETN(), // RETN
      0x46: IM(0), // IM 0
      0x47: LD_R_R(rI, rA), // LD I,A
      0x48: IN_R_iCi(rC), // IN C,(C)
      0x49: OUT_iCi_R(rC), // OUT (C),C
      0x4A: ADC_HL_RR(rpBC), // ADC HL,BC
      0x4B: LD_RR_iNNi(rpBC), // LD BC,(nnnn)
      0x4C: NEG(), // NEG
      0x4D: RETN(), // RETN
      0x4E: IM(0), // IM 0
      0x4F: LD_R_R(rR, rA), // LD R,A
      0x50: IN_R_iCi(rD), // IN D,(C)
      0x51: OUT_iCi_R(rD), // OUT (C),D
      0x52: SBC_HL_RR(rpDE), // SBC HL,DE
      0x53: LD_iNNi_RR(rpDE), // LD (nnnn),DE
      0x54: NEG(), // NEG
      0x55: RETN(), // RETN
      0x56: IM(1), // IM 1
      0x57: LD_R_R(rA, rI), // LD A,I
      0x58: IN_R_iCi(rE), // IN E,(C)
      0x59: OUT_iCi_R(rE), // OUT (C),E
      0x5A: ADC_HL_RR(rpDE), // ADC HL,DE
      0x5B: LD_RR_iNNi(rpDE), // LD DE,(nnnn)
      0x5C: NEG(), // NEG
      0x5D: RETN(), // RETN
      0x5E: IM(2), // IM 2
      0x5F: LD_R_R(rA, rR), // LD A,R
      0x60: IN_R_iCi(rH), // IN H,(C)
      0x61: OUT_iCi_R(rH), // OUT (C),H
      0x62: SBC_HL_RR(rpHL), // SBC HL,HL
      0x63: LD_iNNi_RR(rpHL), // LD (nnnn),HL
      0x64: NEG(), // NEG
      0x65: RETN(), // RETN
      0x66: IM(0), // IM 0
      0x67: RRD(), // RRD
      0x68: IN_R_iCi(rL), // IN L,(C)
      0x69: OUT_iCi_R(rL), // OUT (C),L
      0x6A: ADC_HL_RR(rpHL), // ADC HL,HL
      0x6B: LD_RR_iNNi(rpHL, true), // LD HL,(nnnn)
      0x6C: NEG(), // NEG
      0x6D: RETN(), // RETN
      0x6E: IM(0), // IM 0
      0x6F: RLD(), // RLD
      0x70: IN_F_iCi(), // IN F,(C)
      0x71: OUT_iCi_0(), // OUT (C),0
      0x72: SBC_HL_RR(rpSP), // SBC HL,SP
      0x73: LD_iNNi_RR(rpSP), // LD (nnnn),SP
      0x74: NEG(), // NEG
      0x75: RETN(), // RETN
      0x76: IM(1), // IM 1
      0x78: IN_R_iCi(rA), // IN A,(C)
      0x79: OUT_iCi_R(rA), // OUT (C),A
      0x7A: ADC_HL_RR(rpSP), // ADC HL,SP
      0x7B: LD_RR_iNNi(rpSP), // LD SP,(nnnn)
      0x7C: NEG(), // NEG
      0x7D: RETN(), // RETN
      0x7E: IM(2), // IM 2
      0xA0: LDI(), // LDI
      0xA1: CPI(), // CPI
      0xA2: INI(), // INI
      0xA3: OUTI(), // OUTI
      0xA8: LDD(), // LDD
      0xA9: CPD(), // CPD
      0xAA: IND(), // IND
      0xAB: OUTD(), // OUTD
      0xB0: LDIR(), // LDIR
      0xb1: CPIR(), // CPIR
      0xB2: INIR(), // INIR
      0xB3: OTIR(), // OTIR
      0xB8: LDDR(), // LDDR
      0xb9: CPDR(), // CPDR
      0xBA: INDR(), // INDR
      0xBB: OTDR(), // OTDR
      0x100: 'ed'
    };
    OPCODE_RUN_STRINGS_FD = generateddfdOpcodeSet('FD');
    OPCODE_RUN_STRINGS = {
      0x00: NOP(), // NOP
      0x01: LD_RR_NN(rpBC), // LD BC,nnnn
      0x02: LD_iRRi_R(rpBC, rA), // LD (BC),A
      0x03: INC_RR(rpBC), // INC BC
      0x04: INC("B"), // INC B
      0x05: DEC("B"), // DEC B
      0x06: LD_R_N(rB), // LD B,nn
      0x07: RLCA(), // RLCA
      0x08: EX_RR_RR(rpAF, rpAF_), // EX AF,AF'
      0x09: ADD_RR_RR(rpHL, rpBC), // ADD HL,BC
      0x0A: LD_R_iRRi(rA, rpBC), // LD A,(BC)
      0x0B: DEC_RR(rpBC), // DEC BC
      0x0C: INC("C"), // INC C
      0x0D: DEC("C"), // DEC C
      0x0E: LD_R_N(rC), // LD C,nn
      0x0F: RRCA(), // RRCA
      0x10: DJNZ_N(), // DJNZ nn
      0x11: LD_RR_NN(rpDE), // LD DE,nnnn
      0x12: LD_iRRi_R(rpDE, rA), // LD (DE),A
      0x13: INC_RR(rpDE), // INC DE
      0x14: INC("D"), // INC D
      0x15: DEC("D"), // DEC D
      0x16: LD_R_N(rD), // LD D,nn
      0x17: RLA(), // RLA
      0x18: JR_N(), // JR nn
      0x19: ADD_RR_RR(rpHL, rpDE), // ADD HL,DE
      0x1A: LD_R_iRRi(rA, rpDE), // LD A,(DE)
      0x1B: DEC_RR(rpDE), // DEC DE
      0x1C: INC("E"), // INC E
      0x1D: DEC("E"), // DEC E
      0x1E: LD_R_N(rE), // LD E,nn
      0x1F: RRA(), // RRA
      0x20: JR_C_N(FLAG_Z, false), // JR NZ,nn
      0x21: LD_RR_NN(rpHL), // LD HL,nnnn
      0x22: LD_iNNi_RR(rpHL), // LD (nnnn),HL
      0x23: INC_RR(rpHL), // INC HL
      0x24: INC("H"), // INC H
      0x25: DEC("H"), // DEC H
      0x26: LD_R_N(rH), // LD H,nn
      0x27: DAA(), // DAA
      0x28: JR_C_N(FLAG_Z, true), // JR Z,nn
      0x29: ADD_RR_RR(rpHL, rpHL), // ADD HL,HL
      0x2A: LD_RR_iNNi(rpHL), // LD HL,(nnnn)
      0x2B: DEC_RR(rpHL), // DEC HL
      0x2C: INC("L"), // INC L
      0x2D: DEC("L"), // DEC L
      0x2E: LD_R_N(rL), // LD L,nn
      0x2F: CPL(), // CPL
      0x30: JR_C_N(FLAG_C, false), // JR NC,nn
      0x31: LD_RR_NN(rpSP), // LD SP,nnnn
      0x32: LD_iNNi_A(), // LD (nnnn),a
      0x33: INC_RR(rpSP), // INC SP
      0x34: INC("(HL)"), // INC (HL)
      0x35: DEC("(HL)"), // DEC (HL)
      0x36: LD_iRRi_N(rpHL), // LD (HL),nn
      0x37: SCF(), // SCF
      0x38: JR_C_N(FLAG_C, true), // JR C,nn
      0x39: ADD_RR_RR(rpHL, rpSP), // ADD HL,SP
      0x3A: LD_A_iNNi(), // LD A,(nnnn)
      0x3B: DEC_RR(rpSP), // DEC SP
      0x3C: INC("A"), // INC A
      0x3D: DEC("A"), // DEC A
      0x3E: LD_R_N(rA), // LD A,nn
      0x3F: CCF(), // CCF
      0x40: LD_R_R(rB, rB), // LD B,B
      0x41: LD_R_R(rB, rC), // LD B,C
      0x42: LD_R_R(rB, rD), // LD B,D
      0x43: LD_R_R(rB, rE), // LD B,E
      0x44: LD_R_R(rB, rH), // LD B,H
      0x45: LD_R_R(rB, rL), // LD B,L
      0x46: LD_R_iRRi(rB, rpHL), // LD B,(HL)
      0x47: LD_R_R(rB, rA), // LD B,A
      0x48: LD_R_R(rC, rB), // LD C,B
      0x49: LD_R_R(rC, rC), // LD C,C
      0x4a: LD_R_R(rC, rD), // LD C,D
      0x4b: LD_R_R(rC, rE), // LD C,E
      0x4c: LD_R_R(rC, rH), // LD C,H
      0x4d: LD_R_R(rC, rL), // LD C,L
      0x4e: LD_R_iRRi(rC, rpHL), // LD C,(HL)
      0x4f: LD_R_R(rC, rA), // LD C,A
      0x50: LD_R_R(rD, rB), // LD D,B
      0x51: LD_R_R(rD, rC), // LD D,C
      0x52: LD_R_R(rD, rD), // LD D,D
      0x53: LD_R_R(rD, rE), // LD D,E
      0x54: LD_R_R(rD, rH), // LD D,H
      0x55: LD_R_R(rD, rL), // LD D,L
      0x56: LD_R_iRRi(rD, rpHL), // LD D,(HL)
      0x57: LD_R_R(rD, rA), // LD D,A
      0x58: LD_R_R(rE, rB), // LD E,B
      0x59: LD_R_R(rE, rC), // LD E,C
      0x5a: LD_R_R(rE, rD), // LD E,D
      0x5b: LD_R_R(rE, rE), // LD E,E
      0x5c: LD_R_R(rE, rH), // LD E,H
      0x5d: LD_R_R(rE, rL), // LD E,L
      0x5e: LD_R_iRRi(rE, rpHL), // LD E,(HL)
      0x5f: LD_R_R(rE, rA), // LD E,A
      0x60: LD_R_R(rH, rB), // LD H,B
      0x61: LD_R_R(rH, rC), // LD H,C
      0x62: LD_R_R(rH, rD), // LD H,D
      0x63: LD_R_R(rH, rE), // LD H,E
      0x64: LD_R_R(rH, rH), // LD H,H
      0x65: LD_R_R(rH, rL), // LD H,L
      0x66: LD_R_iRRi(rH, rpHL), // LD H,(HL)
      0x67: LD_R_R(rH, rA), // LD H,A
      0x68: LD_R_R(rL, rB), // LD L,B
      0x69: LD_R_R(rL, rC), // LD L,C
      0x6a: LD_R_R(rL, rD), // LD L,D
      0x6b: LD_R_R(rL, rE), // LD L,E
      0x6c: LD_R_R(rL, rH), // LD L,H
      0x6d: LD_R_R(rL, rL), // LD L,L
      0x6e: LD_R_iRRi(rL, rpHL), // LD L,(HL)
      0x6f: LD_R_R(rL, rA), // LD L,A
      0x70: LD_iRRi_R(rpHL, rB), // LD (HL),B
      0x71: LD_iRRi_R(rpHL, rC), // LD (HL),C
      0x72: LD_iRRi_R(rpHL, rD), // LD (HL),D
      0x73: LD_iRRi_R(rpHL, rE), // LD (HL),E
      0x74: LD_iRRi_R(rpHL, rH), // LD (HL),H
      0x75: LD_iRRi_R(rpHL, rL), // LD (HL),L
      0x76: HALT(), // HALT
      0x77: LD_iRRi_R(rpHL, rA), // LD (HL),A
      0x78: LD_R_R(rA, rB), // LD A,B
      0x79: LD_R_R(rA, rC), // LD A,C
      0x7a: LD_R_R(rA, rD), // LD A,D
      0x7b: LD_R_R(rA, rE), // LD A,E
      0x7c: LD_R_R(rA, rH), // LD A,H
      0x7d: LD_R_R(rA, rL), // LD A,L
      0x7e: LD_R_iRRi(rA, rpHL), // LD A,(HL)
      0x7f: LD_R_R(rA, rA), // LD A,A
      0x80: ADD_A("B"), // ADD A,B
      0x81: ADD_A("C"), // ADD A,C
      0x82: ADD_A("D"), // ADD A,D
      0x83: ADD_A("E"), // ADD A,E
      0x84: ADD_A("H"), // ADD A,H
      0x85: ADD_A("L"), // ADD A,L
      0x86: ADD_A("(HL)"), // ADD A,(HL)
      0x87: ADD_A("A"), // ADD A,A
      0x88: ADC_A("B"), // ADC A,B
      0x89: ADC_A("C"), // ADC A,C
      0x8a: ADC_A("D"), // ADC A,D
      0x8b: ADC_A("E"), // ADC A,E
      0x8c: ADC_A("H"), // ADC A,H
      0x8d: ADC_A("L"), // ADC A,L
      0x8e: ADC_A("(HL)"), // ADC A,(HL)
      0x8f: ADC_A("A"), // ADC A,A
      0x90: SUB_A("B"), // SUB A,B
      0x91: SUB_A("C"), // SUB A,C
      0x92: SUB_A("D"), // SUB A,D
      0x93: SUB_A("E"), // SUB A,E
      0x94: SUB_A("H"), // SUB A,H
      0x95: SUB_A("L"), // SUB A,L
      0x96: SUB_A("(HL)"), // SUB A,(HL)
      0x97: SUB_A("A"), // SUB A,A
      0x98: SBC_A("B"), // SBC A,B
      0x99: SBC_A("C"), // SBC A,C
      0x9a: SBC_A("D"), // SBC A,D
      0x9b: SBC_A("E"), // SBC A,E
      0x9c: SBC_A("H"), // SBC A,H
      0x9d: SBC_A("L"), // SBC A,L
      0x9e: SBC_A("(HL)"), // SBC A,(HL)
      0x9f: SBC_A("A"), // SBC A,A
      0xa0: AND_A("B"), // AND A,B
      0xa1: AND_A("C"), // AND A,C
      0xa2: AND_A("D"), // AND A,D
      0xa3: AND_A("E"), // AND A,E
      0xa4: AND_A("H"), // AND A,H
      0xa5: AND_A("L"), // AND A,L
      0xa6: AND_A("(HL)"), // AND A,(HL)
      0xa7: AND_A("A"), // AND A,A
      0xA8: XOR_A("B"), // XOR B
      0xA9: XOR_A("C"), // XOR C
      0xAA: XOR_A("D"), // XOR D
      0xAB: XOR_A("E"), // XOR E
      0xAC: XOR_A("H"), // XOR H
      0xAD: XOR_A("L"), // XOR L
      0xAE: XOR_A("(HL)"), // XOR (HL)
      0xAF: XOR_A("A"), // XOR A
      0xb0: OR_A("B"), // OR B
      0xb1: OR_A("C"), // OR C
      0xb2: OR_A("D"), // OR D
      0xb3: OR_A("E"), // OR E
      0xb4: OR_A("H"), // OR H
      0xb5: OR_A("L"), // OR L
      0xb6: OR_A("(HL)"), // OR (HL)
      0xb7: OR_A("A"), // OR A
      0xb8: CP_A("B"), // CP B
      0xb9: CP_A("C"), // CP C
      0xba: CP_A("D"), // CP D
      0xbb: CP_A("E"), // CP E
      0xbc: CP_A("H"), // CP H
      0xbd: CP_A("L"), // CP L
      0xbe: CP_A("(HL)"), // CP (HL)
      0xbf: CP_A("A"), // CP A
      0xC0: RET_C(FLAG_Z, false), // RET NZ
      0xC1: POP_RR(rpBC), // POP BC
      0xC2: JP_C_NN(FLAG_Z, false), // JP NZ,nnnn
      0xC3: JP_NN(), // JP nnnn
      0xC4: CALL_C_NN(FLAG_Z, false), // CALL NZ,nnnn
      0xC5: PUSH_RR(rpBC), // PUSH BC
      0xC6: ADD_A("nn"), // ADD A,nn
      0xC7: RST(0x0000), // RST 0x00
      0xC8: RET_C(FLAG_Z, true), // RET Z
      0xC9: RET(), // RET
      0xCA: JP_C_NN(FLAG_Z, true), // JP Z,nnnn
      0xCB: SHIFT('CB'), // shift code
      0xCC: CALL_C_NN(FLAG_Z, true), // CALL Z,nnnn
      0xCD: CALL_NN(), // CALL nnnn
      0xCE: ADC_A("nn"), // ADC A,nn
      0xCF: RST(0x0008), // RST 0x08
      0xD0: RET_C(FLAG_C, false), // RET NC
      0xD1: POP_RR(rpDE), // POP DE
      0xD2: JP_C_NN(FLAG_C, false), // JP NC,nnnn
      0xD3: OUT_iNi_A(), // OUT (nn),A
      0xD4: CALL_C_NN(FLAG_C, false), // CALL NC,nnnn
      0xD5: PUSH_RR(rpDE), // PUSH DE
      0xD6: SUB_A("nn"), // SUB nn
      0xD7: RST(0x0010), // RST 0x10
      0xD8: RET_C(FLAG_C, true), // RET C
      0xD9: EXX(), // EXX
      0xDA: JP_C_NN(FLAG_C, true), // JP C,nnnn
      0xDB: IN_A_N(), // IN A,(nn)
      0xDC: CALL_C_NN(FLAG_C, true), // CALL C,nnnn
      0xDD: SHIFT('DD'), // shift code
      0xDE: SBC_A("nn"), // SBC A,nn
      0xDF: RST(0x0018), // RST 0x18
      0xE0: RET_C(FLAG_P, false), // RET PO
      0xE1: POP_RR(rpHL), // POP HL
      0xE2: JP_C_NN(FLAG_P, false), // JP PO,nnnn
      0xE3: EX_iSPi_RR(rpHL), // EX (SP),HL
      0xE4: CALL_C_NN(FLAG_P, false), // CALL PO,nnnn
      0xE5: PUSH_RR(rpHL), // PUSH HL
      0xE6: AND_A("nn"), // AND nn
      0xE7: RST(0x0020), // RST 0x20
      0xE8: RET_C(FLAG_P, true), // RET PE
      0xE9: JP_RR(rpHL), // JP (HL)
      0xEA: JP_C_NN(FLAG_P, true), // JP PE,nnnn
      0xEB: EX_RR_RR(rpDE, rpHL), // EX DE,HL
      0xEC: CALL_C_NN(FLAG_P, true), // CALL PE,nnnn
      0xED: SHIFT('ED'), // shift code
      0xEE: XOR_A("nn"), // XOR nn
      0xEF: RST(0x0028), // RST 0x28
      0xF0: RET_C(FLAG_S, false), // RET P
      0xF1: POP_RR(rpAF), // POP AF
      0xF2: JP_C_NN(FLAG_S, false), // JP NZ,nnnn
      0xF3: DI(), // DI
      0xF4: CALL_C_NN(FLAG_S, false), // CALL P,nnnn
      0xF5: PUSH_RR(rpAF), // PUSH AF
      0xF6: OR_A("nn"), // OR nn
      0xF7: RST(0x0030), // RST 0x30
      0xF8: RET_C(FLAG_S, true), // RET M
      0xF9: LD_RR_RR(rpSP, rpHL), // LD SP,HL
      0xFA: JP_C_NN(FLAG_S, true), // JP M,nnnn
      0xFB: EI(), // EI
      0xFC: CALL_C_NN(FLAG_S, true), // CALL M,nnnn
      0xFD: SHIFT('FD'), // shift code
      0xFE: CP_A("nn"), // CP nn
      0xFF: RST(0x0038), // RST 0x38
      0x100: 0
    };
    defineZ80JS = `window.JSSpeccy.Z80 = function(opts) {
	var self = {};

	${setUpStateJS}

	self.requestInterrupt = function() {
		interruptPending = true;
		/* TODO: use event scheduling to keep the interrupt line active for a fixed
		~48T window, to support retriggered interrupts and interrupt blocking via
		chains of EI or DD/FD prefixes */
	}
	var z80Interrupt = function() {
		if (iff1) {
			if (halted) {
				/* move PC on from the HALT opcode */
				regPairs[${rpPC}]++;
				halted = false;
			}

			iff1 = iff2 = 0;

			/* push current PC in readiness for call to interrupt handler */
			regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rpPC}] >> 8);
			regPairs[${rpSP}]--; WRITEMEM(regPairs[${rpSP}], regPairs[${rpPC}] & 0xff);
			
			/* TODO: R register */
			
			switch (im) {
				case 0:
					regPairs[${rpPC}] = 0x0038;
					tstates += 6;
					break;
				case 1:
					regPairs[${rpPC}] = 0x0038;
					tstates += 7;
					break;
				case 2:
					inttemp = (regs[${rI}] << 8) | 0xff;
					l = READMEM(inttemp);
					inttemp = (inttemp+1) & 0xffff;
					h = READMEM(inttemp);
					regPairs[${rpPC}] = (h<<8) | l;
					tstates += 7;
					break;
			}
		}
	};

	self.runFrame = function(frameLength) {
		var lastOpcodePrefix, offset, opcode;

		while (tstates < frameLength || opcodePrefix) {
			if (interruptible && interruptPending) {
				z80Interrupt();
				interruptPending = false;
			}
			interruptible = true; /* unless overridden by opcode */
			lastOpcodePrefix = opcodePrefix;
			opcodePrefix = '';
			switch (lastOpcodePrefix) {
				case '':
					CONTEND_READ(regPairs[${rpPC}], 4);
					opcode = memory.read(regPairs[${rpPC}]); regPairs[${rpPC}]++;
					regs[${rR}] = ((regs[${rR}] + 1) & 0x7f) | (regs[${rR}] & 0x80);
					${opcodeSwitch(OPCODE_RUN_STRINGS, null, opts.traps)}
					break;
				case 'CB':
					CONTEND_READ(regPairs[${rpPC}], 4);
					opcode = memory.read(regPairs[${rpPC}]); regPairs[${rpPC}]++;
					regs[${rR}] = ((regs[${rR}] + 1) & 0x7f) | (regs[${rR}] & 0x80);
					${opcodeSwitch(OPCODE_RUN_STRINGS_CB)}
					break;
				case 'DD':
					CONTEND_READ(regPairs[${rpPC}], 4);
					opcode = memory.read(regPairs[${rpPC}]); regPairs[${rpPC}]++;
					regs[${rR}] = ((regs[${rR}] + 1) & 0x7f) | (regs[${rR}] & 0x80);
					${opcodeSwitch(OPCODE_RUN_STRINGS_DD, OPCODE_RUN_STRINGS)}
					break;
				case 'DDCB':
					offset = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
					if (offset & 0x80) offset -= 0x100;
					CONTEND_READ(regPairs[${rpPC}], 3);
					opcode = memory.read(regPairs[${rpPC}]);
					CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
					CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
					regPairs[${rpPC}]++;
					${opcodeSwitch(OPCODE_RUN_STRINGS_DDCB)}
					break;
				case 'ED':
					CONTEND_READ(regPairs[${rpPC}], 4);
					opcode = memory.read(regPairs[${rpPC}]); regPairs[${rpPC}]++;
					regs[${rR}] = ((regs[${rR}] + 1) & 0x7f) | (regs[${rR}] & 0x80);
					${opcodeSwitch(OPCODE_RUN_STRINGS_ED)}
					break;
				case 'FD':
					CONTEND_READ(regPairs[${rpPC}], 4);
					opcode = memory.read(regPairs[${rpPC}]); regPairs[${rpPC}]++;
					regs[${rR}] = ((regs[${rR}] + 1) & 0x7f) | (regs[${rR}] & 0x80);
					${opcodeSwitch(OPCODE_RUN_STRINGS_FD, OPCODE_RUN_STRINGS)}
					break;
				case 'FDCB':
					offset = READMEM(regPairs[${rpPC}]); regPairs[${rpPC}]++;
					if (offset & 0x80) offset -= 0x100;
					CONTEND_READ(regPairs[${rpPC}], 3);
					opcode = memory.read(regPairs[${rpPC}]);
					CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
					CONTEND_READ_NO_MREQ(regPairs[${rpPC}], 1);
					regPairs[${rpPC}]++;
					${opcodeSwitch(OPCODE_RUN_STRINGS_FDCB)}
					break;
				default:
					throw("Unknown opcode prefix: " + lastOpcodePrefix);
			}
		}
		while (display.nextEventTime != null && display.nextEventTime <= tstates) display.doEvent();
	};

	self.reset = function() {
		regPairs[${rpPC}] = regPairs[${rpIR}] = 0;
		iff1 = 0; iff2 = 0; im = 0; halted = false;
	};

	self.loadFromSnapshot = function(snapRegs) {
		regPairs[${rpAF}] = snapRegs['AF'];
		regPairs[${rpBC}] = snapRegs['BC'];
		regPairs[${rpDE}] = snapRegs['DE'];
		regPairs[${rpHL}] = snapRegs['HL'];
		regPairs[${rpAF_}] = snapRegs['AF_'];
		regPairs[${rpBC_}] = snapRegs['BC_'];
		regPairs[${rpDE_}] = snapRegs['DE_'];
		regPairs[${rpHL_}] = snapRegs['HL_'];
		regPairs[${rpIX}] = snapRegs['IX'];
		regPairs[${rpIY}] = snapRegs['IY'];
		regPairs[${rpSP}] = snapRegs['SP'];
		regPairs[${rpPC}] = snapRegs['PC'];
		regPairs[${rpIR}] = snapRegs['IR'];
		iff1 = snapRegs['iff1'];
		iff2 = snapRegs['iff2'];
		im = snapRegs['im'];
	};

	/* Register / flag accessors (used for tape trapping and test harness) */
	self.getAF = function() {
		return regPairs[${rpAF}];
	}
	self.getBC = function() {
		return regPairs[${rpBC}];
	}
	self.getDE = function() {
		return regPairs[${rpDE}];
	}
	self.getHL = function() {
		return regPairs[${rpHL}];
	}
	self.getAF_ = function() {
		return regPairs[${rpAF_}];
	}
	self.getBC_ = function() {
		return regPairs[${rpBC_}];
	}
	self.getDE_ = function() {
		return regPairs[${rpDE_}];
	}
	self.getHL_ = function() {
		return regPairs[${rpHL_}];
	}
	self.getIX = function() {
		return regPairs[${rpIX}];
	}
	self.getIY = function() {
		return regPairs[${rpIY}];
	}
	self.getI = function() {
		return regs[${rI}];
	}
	self.getR = function() {
		return regs[${rR}];
	}
	self.getSP = function() {
		return regPairs[${rpSP}];
	}
	self.getPC = function() {
		return regPairs[${rpPC}];
	}
	self.getIFF1 = function() {
		return iff1;
	}
	self.getIFF2 = function() {
		return iff2;
	}
	self.getIM = function() {
		return im;
	}
	self.getHalted = function() {
		return halted;
	}

	self.setAF = function(val) {
		regPairs[${rpAF}] = val;
	}
	self.setBC = function(val) {
		regPairs[${rpBC}] = val;
	}
	self.setDE = function(val) {
		regPairs[${rpDE}] = val;
	}
	self.setHL = function(val) {
		regPairs[${rpHL}] = val;
	}
	self.setAF_ = function(val) {
		regPairs[${rpAF_}] = val;
	}
	self.setBC_ = function(val) {
		regPairs[${rpBC_}] = val;
	}
	self.setDE_ = function(val) {
		regPairs[${rpDE_}] = val;
	}
	self.setHL_ = function(val) {
		regPairs[${rpHL_}] = val;
	}
	self.setIX = function(val) {
		regPairs[${rpIX}] = val;
	}
	self.setIY = function(val) {
		regPairs[${rpIY}] = val;
	}
	self.setI = function(val) {
		regs[${rI}] = val;
	}
	self.setR = function(val) {
		regs[${rR}] = val;
	}
	self.setSP = function(val) {
		regPairs[${rpSP}] = val;
	}
	self.setPC = function(val) {
		regPairs[${rpPC}] = val;
	}
	self.setIFF1 = function(val) {
		iff1 = val;
	}
	self.setIFF2 = function(val) {
		iff2 = val;
	}
	self.setIM = function(val) {
		im = val;
	}
	self.setHalted = function(val) {
		halted = val;
	}

	self.getTstates = function() {
		return tstates;
	}
	self.setTstates = function(val) {
		tstates = val;
	}

	self.getCarry_ = function() {
		return regs[${rF_}] & ${FLAG_C};
	};
	self.setCarry = function(val) {
		if (val) {
			regs[${rF}] |= ${FLAG_C};
		} else {
			regs[${rF}] &= ${~FLAG_C};
		}
	};
	self.getA_ = function() {
		return regs[${rA_}];
	};

	return self;
};`;
    // Apply macro expansions
    defineZ80JS = defineZ80JS.replace(/READMEM\((.*?)\)/g, '(CONTEND_READ($1, 3), memory.read($1))');
    defineZ80JS = defineZ80JS.replace(/WRITEMEM\((.*?),(.*?)\)/g, `CONTEND_WRITE($1, 3);
while (display.nextEventTime != null && display.nextEventTime < tstates) display.doEvent();
memory.write($1,$2);`);
    if (opts.applyContention) {
      defineZ80JS = defineZ80JS.replace(/CONTEND_READ\((.*?),(.*?)\)/g, '(tstates += memory.contend($1, tstates) + ($2))');
      defineZ80JS = defineZ80JS.replace(/CONTEND_WRITE\((.*?),(.*?)\)/g, '(tstates += memory.contend($1, tstates) + ($2))');
      defineZ80JS = defineZ80JS.replace(/CONTEND_READ_NO_MREQ\((.*?),(.*?)\)/g, '(tstates += memory.contend($1, tstates) + ($2))');
      defineZ80JS = defineZ80JS.replace(/CONTEND_WRITE_NO_MREQ\((.*?),(.*?)\)/g, '(tstates += memory.contend($1, tstates) + ($2))');
      defineZ80JS = defineZ80JS.replace(/CONTEND_PORT_EARLY\((.*?)\)/g, `var isContendedMemory = memory.isContended($1);
var isULAPort = ioBus.isULAPort($1);
if (isContendedMemory) tstates += ioBus.contend($1, tstates);
tstates += 1;
while (display.nextEventTime != null && display.nextEventTime < tstates) display.doEvent();`);
      defineZ80JS = defineZ80JS.replace(/CONTEND_PORT_LATE\((.*?)\)/g, `if (isContendedMemory || isULAPort) {
	ioBus.contend($1);
	tstates += 1;
	if (!isULAPort) {
		ioBus.contend($1); tstates += 1;
		ioBus.contend($1); tstates += 1;
	} else {
		tstates += 2;
	}
} else {
	tstates += 3;
}`);
    } else {
      defineZ80JS = defineZ80JS.replace(/CONTEND_READ\((.*?),(.*?)\)/g, 'tstates += ($2)');
      defineZ80JS = defineZ80JS.replace(/CONTEND_WRITE\((.*?),(.*?)\)/g, 'tstates += ($2)');
      defineZ80JS = defineZ80JS.replace(/CONTEND_READ_NO_MREQ\((.*?),(.*?)\)/g, 'tstates += ($2)');
      defineZ80JS = defineZ80JS.replace(/CONTEND_WRITE_NO_MREQ\((.*?),(.*?)\)/g, 'tstates += ($2)');
      defineZ80JS = defineZ80JS.replace(/CONTEND_PORT_EARLY\((.*?)\)/g, 'tstates += 1');
      defineZ80JS = defineZ80JS.replace(/CONTEND_PORT_LATE\((.*?)\)/g, 'tstates += 3');
    }
    // console.log(defineZ80JS);
    indirectEval = eval;
    return indirectEval(defineZ80JS);
  };

}).call(this);
JSSpeccy.Z80File = function(data) {
	var file = new DataView(data);

	function extractMemoryBlock(data, fileOffset, isCompressed, unpackedLength) {
		if (!isCompressed) {
			/* uncompressed; extract a byte array directly from data */
			return new Uint8Array(data, fileOffset, unpackedLength);
		} else {
			/* compressed */
			var fileBytes = new Uint8Array(data, fileOffset);
			var memoryBytes = new Uint8Array(unpackedLength);
			var filePtr = 0; var memoryPtr = 0;
			while (memoryPtr < unpackedLength) {
				/* check for coded ED ED nn bb sequence */
				if (
					unpackedLength - memoryPtr >= 2 && /* at least two bytes left to unpack */
					fileBytes[filePtr] == 0xed &&
					fileBytes[filePtr + 1] == 0xed
				) {
					/* coded sequence */
					var count = fileBytes[filePtr + 2];
					var value = fileBytes[filePtr + 3];
					for (var i = 0; i < count; i++) {
						memoryBytes[memoryPtr++] = value;
					}
					filePtr += 4;
				} else {
					/* plain byte */
					memoryBytes[memoryPtr++] = fileBytes[filePtr++];
				}
			}
			return memoryBytes;
		}
	}

	var iReg = file.getUint8(10);
	var byte12 = file.getUint8(12);
	var rReg = (file.getUint8(11) & 0x7f) | ((byte12 & 0x01) << 7);
	var byte29 = file.getUint8(29);

	var snapshot = {
		registers: {
			'AF': file.getUint16(0, false), /* NB Big-endian */
			'BC': file.getUint16(2, true),
			'HL': file.getUint16(4, true),
			'PC': file.getUint16(6, true),
			'SP': file.getUint16(8, true),
			'IR': (iReg << 8) | rReg,
			'DE': file.getUint16(13, true),
			'BC_': file.getUint16(15, true),
			'DE_': file.getUint16(17, true),
			'HL_': file.getUint16(19, true),
			'AF_': file.getUint16(21, false), /* Big-endian */
			'IY': file.getUint16(23, true),
			'IX': file.getUint16(25, true),
			'iff1': !!file.getUint8(27),
			'iff2': !!file.getUint8(28),
			'im': byte29 & 0x03
		},
		ulaState: {
			borderColour: (byte12 & 0x0e) >> 1
		},
		memoryPages: {
		}
	};

	if (snapshot.registers.PC !== 0) {
		/* a non-zero value for PC at offset 6 indicates a version 1 file */
		snapshot.model = JSSpeccy.Spectrum.MODEL_48K;
		var memory = extractMemoryBlock(data, 30, byte12 & 0x20, 0xc000);

		/* construct byte arrays of length 0x4000 at the appropriate offsets into the data stream */
		snapshot.memoryPages[5] = new Uint8Array(memory, 0, 0x4000);
		snapshot.memoryPages[2] = new Uint8Array(memory, 0x4000, 0x4000);
		snapshot.memoryPages[0] = new Uint8Array(memory, 0x8000, 0x4000);
		/* FIXME: memory is a Uint8Array, not an ArrayBuffer - is this valid for the Uint8Array constructor? */
	} else {
		/* version 2-3 snapshot */
		var additionalHeaderLength = file.getUint16(30, true);
		var isVersion2 = (additionalHeaderLength == 23);
		snapshot.registers.PC = file.getUint16(32, true);
		var machineId = file.getUint8(34);
		var is48K = (isVersion2 ? machineId < 3 : machineId < 4);
		snapshot.model = (is48K ? JSSpeccy.Spectrum.MODEL_48K : JSSpeccy.Spectrum.MODEL_128K);
		if (!is48K) {
			snapshot.ulaState.pagingFlags = file.getUint8(35);
		}
		var tstateChunkSize = snapshot.model.frameLength / 4;
		var tstateLowCounter = tstateChunkSize - file.getUint16(55, true);
		var tstateHighCounter = file.getUint8(57);
		snapshot.tstates = tstateLowCounter + (tstateHighCounter * tstateChunkSize);

		var offset = 32 + additionalHeaderLength;

		/* translation table from the IDs Z80 assigns to pages, to the page numbers they
		actually get loaded into */
		var pageIdToNumber;
		if (is48K) {
			pageIdToNumber = {
				4: 2,
				5: 0,
				8: 5
			};
		} else {
			pageIdToNumber = {
				3: 0,
				4: 1,
				5: 2,
				6: 3,
				7: 4,
				8: 5,
				9: 6,
				10: 7
			};
		}
		while (offset < data.byteLength) {
			var compressedLength = file.getUint16(offset, true);
			var isCompressed = true;
			if (compressedLength == 0xffff) {
				compressedLength = 0x4000;
				isCompressed = false;
			}
			var pageId = file.getUint8(offset + 2);
			if (pageId in pageIdToNumber) {
				var pageNumber = pageIdToNumber[pageId];
				var pageData = extractMemoryBlock(data, offset + 3, isCompressed, 0x4000);
				snapshot.memoryPages[pageNumber] = pageData;
			}
			offset += compressedLength + 3;
		}
	}

	return snapshot;
};
